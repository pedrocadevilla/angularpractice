import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { OauthComponent } from './shared/components/oauth/oauth.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { SharedModule } from './shared/modules/shared.module';
import { InnerSharedModule } from './shared/modules/inner-shared.module';
import { HttpClientModule } from '@angular/common/http';
import { UserBaseService } from './shared/services/user-base.service';
import { NotificationsService } from './shared/services/notification.service';
import { DeviceService } from './shared/services/device.service';
import { SyncService } from './shared/services/sync.service';
import { PendingInterceptorService } from './shared/interceptors/pending.interceptor.service';
import { ToastService } from './shared/services/toast.service';
import { GuidService } from './shared/services/guid.service';
import { ChannelService } from './shared/services/channel.service';
import { SignalrWindow } from './shared/services/channel-base.service';
import { RestoreService } from './shared/services/restore.service';
import { DatePipe } from '@angular/common';
import { ExternalLoginServices } from './shared/services/external-login.service';
import { SignService } from './modules/login-v2/services/sign.service';

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    OauthComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TranslateModule.forRoot(),
    SharedModule,
    InnerSharedModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
