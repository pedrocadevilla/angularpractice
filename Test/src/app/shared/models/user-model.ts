import { Subscription } from './subscription-model';

export interface User {
  CanAddSyncAccount: boolean;
  DaysTillTrialEnd: number;
  DeploymentId: string;
  Email: string;
  FullName: string;
  HoursTillTrialEnd: number;
  Id: string;
  IsFirstTimeLogin: boolean | null | string;
  IsFullNameCorrect: boolean;
  IsTrial: boolean;
  Language: string;
  Login: string;
  MonthlyPrice: number | null;
  Name: string;
  Roles: Array<string>;
  ScheduleId: string;
  Subscription: Subscription;
  SyncAccountsLimit: number;
  isAdmin?: boolean;
  isFeaturesModifier?: boolean;
  isAlpha?: boolean;
  isBeta?: boolean;
  isEnterpriseAdmin: boolean;
  isNotEnterprise: boolean;
}
