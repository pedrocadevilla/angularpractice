export interface AddModel {
  Provider: string;
  Username?: string;
  Password?: string;
  Server?: string;
  ReturnUrl?: string;
  AddedForSharing?: boolean;
  SelectedReplicaTypes?: ProviderType[];
  BackupRightsRequired?: boolean;
  AutoStartManualSyncOnComplete?: boolean;
}
export interface RetryAddAccountModel {
  Id?: string;
  Password?: string;
  Url?: string;
  ValidateOldFirst?: string;
  Provider?: string;
  SelectedReplicaTypes?: ProviderType[];
  Migrate?: boolean;
}

export interface ChangeAccountPassword {
  Id: string;
  NewPassword: string;
}

export interface VerifyTokenModel {
  Id: string;
  Token: string;
}

export interface MakeReplicasValidModel {
  AccountId: string;
  ReplicasIds: string[];
}

export interface AcceptSyncAccountInviteModel {
  InviteId: string;
  Password: string;
  Server: string;
}

enum ProviderType {
  Undefined = 0,
  Contact = 1,
  Calendar = 2,
  Task = 3,
}
