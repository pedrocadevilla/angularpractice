export interface errorMessageObject {
  message: string;
  key: string;
}
