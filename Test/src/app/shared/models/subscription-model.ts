export interface Subscription {
  Currency: string;
  Discount: number;
  EndDate: string | null;
  GracePeriodEndDate: string | null;
  HasPaymentsHistory: boolean;
  IsMultiAccount: boolean;
  IsPrePaid: boolean;
  PricingPlanId: string;
  ProductId: string;
  ProductName: string | null;
  ProductPrice: number;
  SubscriptionId: string;
  SubscriptionStatus: string;
}
