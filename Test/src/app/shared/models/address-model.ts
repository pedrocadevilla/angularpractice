export interface Address {
  FirstName: string;
  LastName: string;
  Email: string;
  Company: string;
  CountryId: number;
  CountryName: string;
  StateProvinceId: number;
  StateProvinceName: string;
  City: string;
  Address1: string;
  Address2: string;
  ZipPostalCode: string;
  PhoneNumber: string;
  FaxNumber: string;
}
