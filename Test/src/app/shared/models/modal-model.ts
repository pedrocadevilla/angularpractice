export interface Modal {
  title?: string;
  text?: string;
  support?: boolean;
  link?: string;
  linkTarget?: string;
  linkTranslate?: string;
  primaryButton?: string;
  secondaryButton?: string;
  thirdButton?: string;
  primaryTextButton?: string;
  secondaryTextButton?: string;
  thirdTextButton?: string;
  data?: any;
  textList?: string[];
  checkbox?: string[];
  textTranslated?: boolean;
  titleTranslated?: boolean;
  options?: { [key: string]: boolean };
  table?: TableModel[];
  buttonTitle?: string;
  textArray?: string[];
  hasGoogle?: string;
}

export interface TableModel {
  td: string[];
  th: string;
}
