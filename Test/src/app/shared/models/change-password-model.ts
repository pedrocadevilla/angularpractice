export interface ChangePasswordModal {
  Email: string;
  Id: string;
  Token: string;
  NewPassword: string;
  ConfirmPassword: string;
}
