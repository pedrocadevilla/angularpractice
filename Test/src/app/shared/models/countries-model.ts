export interface Country {
  Id: number;
  Name: string;
  TwoLetterIsoCode: string;
  ThreeLetterIsoCode: string;
  NumericIsoCode: number;
  AllowsBilling: boolean;
  AllowsShipping: boolean;
  SubjectToVat: boolean;
  Published: boolean;
  DisplayOrder: number;
  StatesProvinces: StateProvince[];
}

export interface StateProvince {
  Id: number;
  CountryId: string;
  Name: string;
  Abbreviation: string;
  Published: boolean;
  DisplayOrder: number;
}
