import { Byte } from '@angular/compiler/src/util';

export interface BackupFilterModel {
  Id: number;
  Username: string;
  ProviderCode: string;
  ExcludeRemovedFolders: boolean;
  Replicas: BackupFilterReplicaModel[];
}
export interface BackupFilterReplicaModel {
  Id: number;
  TypeCode: string;
  TypeName: string;
  IsSelected: boolean;
  Folders: BackupFilterFolderModel[];
}

export interface BackupFilterFolderModel {
  PrimaryId: string;
  DisplayName: string;
  FolderType: Byte;
  IsDefault: boolean;
  IsSelected: boolean;
  LastRestored: string;
  BackupedFolders: BackupFilterBackupdedFolderModel[];
}

export interface BackupFilterBackupdedFolderModel {
  BackupedFolderId: number;
  BackupStatusHistoryId: number;
  Date: string;
  IsSelected: boolean;
  ArrangedByFolders: boolean;
}
