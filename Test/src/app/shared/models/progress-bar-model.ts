export interface progressBar {
  title: string;
  status: string;
  progress: number;
  accounts: Account[];
  actionType: string;
  expanded: boolean;
  lastActionTime: Date;
  update?: boolean;
  showLongRunningSyncWarning?: boolean;
}

export interface Account {
  result: string;
  accountId: number;
  progress: number;
}

export interface AccountProgressBar {
  expanded: boolean;
  show: boolean;
  text: string;
  showSyncBtn: boolean;
}
