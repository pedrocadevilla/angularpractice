export interface CurrentSubscriptionInfo {
  IsActive: boolean;
  IsGracePeriod: boolean;
  IsAutoRenewalEnabled: boolean;
  ExpirationDate: string;
  SubscriptionType: string;
  NextRenewalAmount: number;
  IsPrePaid: boolean;
  IsPaid: boolean;
  Currency: string;
  Status: string;
  BillingInfo: UserBillingInfoModel;
  CompanyType?: CompanyType;
}

export interface UserBillingInfoModel {
  Address: AddressModel;
  FundingSourceType: FundingSourceType;
  FundingSourceModel: FundingSourceModel;
}
export interface FundingSourceModel {}
export interface AddressModel {
  FirstName: string;
  LastName: string;
  Email: string;
  Company: string;
  CountryId?: number;
  CountryName: string;
  StateProvinceId?: number;
  StateProvinceName: string;
  City: string;
  Address1: string;
  Address2: string;
  ZipPostalCode: string;
  PhoneNumber: string;
  FaxNumber: string;
}

export enum FundingSourceType {
  CreditCard = 1,
  PayPal = 2,
  ApplePay = 3,
}

export enum CompanyType {
  Undefined = 0,
  Company,
  Family,
}
