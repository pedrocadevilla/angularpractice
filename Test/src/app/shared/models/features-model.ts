export enum FeaturesEnum {
  Unknown = 0,
  EnterpriseSyncSetup,
  EnterpriseBulkSources,
  TeamsAdmin,
  SyncAccounts,
  EnterpriseManageOtherUser,
}

export interface UserFeatures {
  LastUpdatedAtUtc: string;
  AutoSync: any;
  ManualSync: any;
  SyncAccounts: SyncAccountsFeature;
  Sync: any;
  Sharing: any;
  Backup: any;
  Report: any;
  TeamsManagement: any;
  BulkSources: any;
  SyncSetups: any;
  ManageOtherUser: any;
}
export interface SyncAccountsFeature {
  IsAvailable: boolean;
  MaxSyncAccountsCount: number;
  CurrentAccountsCount: number;
  CanAddSyncAccount: boolean;
  ErrorCode: string;
}
