export interface SyncAccountStatusChangeModel {
  AccountId: string;
  IsSelected: boolean;
}
