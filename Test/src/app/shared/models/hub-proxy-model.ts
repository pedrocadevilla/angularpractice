import { Account } from './progress-bar-model';
import { BackupFilterModel } from './restore-model';

export interface HubProxy {
  deploymentId?: string;
  accountUsername?: string;
  state?: string;
  progress?: number;
  showLongRunningSyncWarning?: boolean;
  accountId?: number;
  result?: string;
  folderType?: string;
  Entry?: Entry;
  account?: Account;
  backupFilterModel?: BackupFilterModel;
}

export interface Entry {
  ActionType?: string;
}
