export interface WebMessageModel {
  Id: number;
  Subject: string;
  Content: string;
  CanBeClosedByRecipient: boolean | null;
  ShowOnce: boolean;
  ExpirationDate: Date | null;
  IsDropDown: boolean | null;
  Language: string;
  CanBeClosedByUser: boolean | null;
  RedirectionTarget: NotificationRedirectionTarget;
  CreatedAt: string;
}

export enum NotificationRedirectionTarget {
  NoRedirection = 0,
  RedirectToNotificationsPage = 1,
  RedirectToPurchasePage = 2,
  RedirectToPaymentHistoryPage = 3,
}
