export interface SurveyModel {
  TryingToSaveMoney?: boolean;
  DontNeedPremiumFeatures?: boolean;
  PremiumPackageMissingFeatures?: boolean;
  TechnicalIssues?: boolean;
  Other?: boolean;
  OtherReasonText?: string;
  OneWaySync?: boolean;
  AdvancedMapping?: boolean;
  CompanyAccount?: boolean;
  ImmediateSync?: boolean;
  AdvansedSettings?: boolean;
  AutoSyncFrequency?: boolean;
  NoteSync?: boolean;
  OtherFeatureMissing?: boolean;
  CantAddIcloud?: boolean;
  CantAddExchange?: boolean;
  RequestsNotSynchronizing?: boolean;
  CalendarNotSynchronizing?: boolean;
  ContactsNotSynchronizing?: boolean;
  SyncTakesForever?: boolean;
  SyncShowsCancelling?: boolean;
  OtherTechnicalIssue?: boolean;
  OtherFeatureText?: string;
  OtherIssueText?: string;
  SubscriptionId: number;
  CancelReason: string;
}
