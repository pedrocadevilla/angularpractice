export interface SessionModel {
  access_token: string;
  expires_in: number;
  type: string;
  expires: number;
}
