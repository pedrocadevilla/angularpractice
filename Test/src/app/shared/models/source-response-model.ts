export interface SourceResponse {
  IsExactPageFound: boolean;
  IsSourceOneSpecified: boolean;
  IsSourceTwoSpecified: boolean;
  Pages: PagePair[];
}

export interface PagePair {
  ProviderTypes: number[];
  Source1: PageSource;
  Source2: PageSource;
}

export interface PageSource {
  Name: string;
  Icon: string;
  Alias: string;
  SyncSystem: number;
}
