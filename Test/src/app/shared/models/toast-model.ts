export interface Toast {
  body?: any;
  toastId?: string;
  toastContainerId?: number;
  onShowCallback?: OnActionCallback;
  onHideCallback?: OnActionCallback;
  timeout?: number;
  timeoutId?: number | null;
  clickHandler?: ClickHandler;
  showCloseButton?: boolean;
  closeHtml?: string;
  toasterConfig?: ToasterConfig;
  data?: any;
  redirectOnCloseTo?: string;
}

export type ClickHandler = (toast: Toast, isCloseButton?: boolean) => boolean;
export type OnActionCallback = (toast: Toast) => void;

export interface IToasterConfig {
  limit?: number | null;
  tapToDismiss?: boolean;
  showCloseButton?: boolean | Object;
  closeHtml?: string;
  newestOnTop?: boolean;
  timeout?: number;
  animation?: string;
  preventDuplicates?: boolean;
  mouseoverTimerStop?: boolean;
  toastContainerId?: number | null;
  redirectOnCloseTo?: string | null;
}

export class ToasterConfig implements IToasterConfig {
  limit?: number | null;
  tapToDismiss: boolean;
  showCloseButton: boolean | Object;
  closeHtml: string;
  newestOnTop: boolean;
  timeout: number;
  animation: string;
  preventDuplicates: boolean;
  mouseoverTimerStop: boolean;
  toastContainerId?: number | null;
  redirectOnCloseTo?: string | null;

  constructor(configOverrides?: IToasterConfig) {
    configOverrides = configOverrides || {};
    this.limit = configOverrides.limit || 2;
    this.tapToDismiss =
      configOverrides.tapToDismiss != null
        ? configOverrides.tapToDismiss
        : true;
    this.showCloseButton =
      configOverrides.showCloseButton != null
        ? configOverrides.showCloseButton
        : true;
    this.closeHtml = configOverrides.closeHtml || 'Close';
    this.newestOnTop =
      configOverrides.newestOnTop != null ? configOverrides.newestOnTop : true;
    this.timeout =
      configOverrides.timeout != null ? configOverrides.timeout : 30000;
    this.animation = configOverrides.animation || 'fade';
    this.preventDuplicates =
      configOverrides.preventDuplicates != null
        ? configOverrides.preventDuplicates
        : false;
    this.mouseoverTimerStop =
      configOverrides.mouseoverTimerStop != null
        ? configOverrides.mouseoverTimerStop
        : false;
    this.toastContainerId =
      configOverrides.toastContainerId != null
        ? configOverrides.toastContainerId
        : null;
    this.redirectOnCloseTo =
      configOverrides.redirectOnCloseTo != null
        ? configOverrides.redirectOnCloseTo
        : null;
  }
}
