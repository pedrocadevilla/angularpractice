import { PipeTransform, Pipe } from '@angular/core';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

@Pipe({ name: 'todayDate' })
export class TodayDatePipe implements PipeTransform {
  constructor(
    private datePipe: DatePipe,
    private translateService: TranslateService
  ) {}

  transform(date: Date) {
    if (date) {
      const sub = this.datePipe.transform(date, 'M/dd/yyyy'),
        today = this.datePipe.transform(new Date(), 'M/dd/yyyy');

      if (today === sub) {
        return this.translateService.instant('TODAY_AT', {
          var0: this.datePipe.transform(date, 'shortTime'),
        });
      } else {
        const d = new Date();
        d.setDate(d.getDate() - 1);

        if (this.datePipe.transform(d, 'M/dd/yyyy') === sub) {
          return this.translateService.instant('YESTERDAY_AT', {
            var0: this.datePipe.transform(date, 'shortTime'),
          });
        } else {
          return (
            this.datePipe.transform(date, 'mediumDate') +
            ' ' +
            this.datePipe.transform(date, 'shortTime')
          );
        }
      }
    } else {
      return '';
    }
  }
}
