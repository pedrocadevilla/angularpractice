import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'dataType' })
export class DataTypePipe implements PipeTransform {
  strings = ['', 'CONTACTS', 'CALENDAR', 'TASKS'];

  constructor() {}

  transform(id: number): string {
    if (id) {
      return this.strings[id];
    }

    return '-';
  }
}
