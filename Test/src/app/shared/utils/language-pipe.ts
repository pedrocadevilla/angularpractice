import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'languagePipe' })
export class LanguagePipe implements PipeTransform {
  constructor() {}

  transform(name: string) {
    switch (name) {
      case 'en':
        return 'English';
      case 'fr':
        return 'Francais';
      case 'de':
        return 'Deutsch';
      default:
        return '';
    }
  }
}
