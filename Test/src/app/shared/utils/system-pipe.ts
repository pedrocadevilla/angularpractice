import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'system' })
export class SystemPipe implements PipeTransform {
  strings = [
    '',
    'Exchange',
    'Google',
    'Outlook',
    'iCloud',
    'GoogleDrive',
    'SkyDrive',
    'DropBox',
    'Office365',
    'OutlookRest',
    'SalesForce',
  ];

  constructor() {}

  transform(id: number) {
    if (id) {
      return this.strings[id];
    }

    return '-';
  }
}
