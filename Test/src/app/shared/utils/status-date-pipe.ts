import { PipeTransform, Pipe } from '@angular/core';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

@Pipe({ name: 'statusDate' })
export class StatusDatePipe implements PipeTransform {
  constructor(
    private datePipe: DatePipe,
    private translateService: TranslateService
  ) {}

  transform(date: Date | string) {
    if (date) {
      date = new Date(date);
      const today = new Date();

      if (
        today.getFullYear() === date.getFullYear() &&
        today.getMonth() === date.getMonth() &&
        today.getDate() === date.getDate()
      ) {
        // get dates difference
        const difference = today.getTime() - date.getTime(),
          hours = Math.floor(difference / (1000 * 60 * 60));

        if (hours < 1) {
          const minutes = Math.floor(difference / (1000 * 60));

          if (minutes < 2) {
            return this.translateService.instant('LESS_THAN_MINUTE_AGO');
          } else {
            return this.translateService.instant('X_MINUTES_AGO', {
              var0: minutes,
            });
          }
        } else {
          return this.translateService.instant('X_HOURS_AGO', { var0: hours });
        }
      } else {
        today.setDate(today.getDate() - 1);

        if (today.getDate() === date.getDate()) {
          return this.translateService.instant('YESTERDAY_AT_LITE', {
            var0: this.datePipe.transform(date, 'shortTime'),
          });
        } else {
          return this.datePipe.transform(date, 'short');
        }
      }
    } else {
      return null;
    }
  }
}
