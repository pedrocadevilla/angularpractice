import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'defaultFolderNaming' })
export class DefaultFolderNamingPipe implements PipeTransform {
  //default folder naming for google, exchange, icloud
  translations = [
    {
      System: 2,
      Provider: 'Google',
      CONTACTS: 'ALL_CONTACTS',
    },
    {
      System: 1,
      Provider: 'Exchange',
      CONTACTS: 'CONTACTS',
      CALENDAR: 'CALENDAR',
      TASKS: 'TASKS',
    },
    {
      System: 4,
      Provider: 'iCloud',
      CONTACTS: 'ALL_CONTACTS',
    },
  ];

  constructor() {}

  transform(system: number, dataType: string): string {
    if (this.translations.find((el) => el.System === system && el[dataType])) {
      return (
        this.translations.find((el) => el.System === system && el[dataType])[
          dataType
        ] + '_DEFAULT'
      );
    }

    return '';
  }
}
