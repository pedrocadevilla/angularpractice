import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import { BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { WebMessageModel } from '../models/web-message-model';
import { DeviceService } from './device.service';
@Injectable()
export class NotificationsService {
  webMessages: BehaviorSubject<WebMessageModel[]>;
  forbiddenMobileNotifications = [
    'get premium',
    'free',
    'price',
    'trial ended',
    'premium for',
    'premium-version mit',
    'bénéficiez de la version premium',
    'kostenlose',
    'gratuite',
    'preis',
    'prix',
    'premium-funktionen abgelaufen',
    'la période d’essai',
    'Premium - Version für',
    'premium pour',
  ];

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private deviceService: DeviceService
  ) {}

  registerGCM() {
    // register browser for push notifications if not registered yet (only Chrome)
    if (!this.cookieService.check('gcmRegistered') && environment.production) {
      if (
        'serviceWorker' in navigator &&
        'PushManager' in window &&
        /Chrome/.test(navigator.userAgent) &&
        /Google Inc/.test(navigator.vendor)
      ) {
        // ask for permissions
        Notification.requestPermission().then((permission) => {
          if (permission === 'granted') {
            navigator.serviceWorker
              .register('/service_worker.js', {
                scope: '/',
              })
              .then((swr) => {
                swr.pushManager
                  .subscribe({
                    userVisibleOnly: true,
                  })
                  .then((subscription) => {
                    // Retrieve the user's public key.
                    const rawKey = subscription.getKey
                      ? subscription.getKey('p256dh')
                      : null;
                    let key: string = '';
                    if (rawKey !== null) {
                      let jsonKey: string = '';
                      new Uint8Array(rawKey).forEach(function (byte: number) {
                        jsonKey += String.fromCharCode(byte);
                      });
                      key = btoa(jsonKey);
                    }

                    const rawAuthSecret = subscription.getKey
                      ? subscription.getKey('auth')
                      : null;
                    let authSecret: string = '';
                    if (rawAuthSecret !== null) {
                      let jsonKey: string = '';
                      new Uint8Array(rawAuthSecret).forEach(function (
                        byte: number
                      ) {
                        jsonKey += String.fromCharCode(byte);
                      });
                      authSecret = btoa(jsonKey);
                    }

                    // push endpoint to server
                    this.http
                      .post('/api/messages/RegisterGCM', {
                        EndPoint: subscription.endpoint,
                        Auth: authSecret,
                        P256dh: key,
                      })
                      .subscribe(() => {
                        // set gcm cookie
                        this.cookieService.set('gcmRegistered', 'true');
                      });
                  });
              });
          }
        });
      }
    }
  }

  getWebMessages() {
    this.http
      .get('/api/messages/GetUserActiveWebMessages')
      .subscribe((resp: any) => {
        // hide free message on mobile apps
        if (!this.deviceService.isPurchaseLogicAvailabe()) {
          const messages: WebMessageModel[] = [];

          for (const i in resp) {
            let isForbidden = false;
            for (const j in this.forbiddenMobileNotifications) {
              if (
                resp[i].Subject.toLowerCase().indexOf(
                  this.forbiddenMobileNotifications[j]
                ) > -1
              ) {
                isForbidden = true;
                break;
              }
            }

            if (!isForbidden) {
              messages.push(resp[i]);
            }
          }

          this.webMessages.next(messages);
        } else {
          this.webMessages.next(resp);
        }
      });
  }

  clearWebMessages() {
    this.webMessages.next([]);
  }

  closeWebMessage(id: string) {
    return this.http.post('/api/message/close', {
      Id: id,
    });
  }

  getMessageContent(id: string) {
    return this.http.get('/api/message/GetMessageContent?Id=' + id);
  }

  deleteMessage(id: string) {
    return this.http.post('/api/message/delete', {
      Id: id,
    });
  }

  deleteAllMessages() {
    return this.http.delete('/api/message/deleteAll');
  }

  assignDeviceToUser(id: number) {
    return this.http.post('/api/assignDeviceToUser/' + id, {});
  }

  getNotificationRedirect(id: string) {
    return this.http.get('/api/messages/GetNotificationRedirect?Id=' + id);
  }
}
