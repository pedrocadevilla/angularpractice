import { HttpClient } from '@angular/common/http';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { BehaviorSubject, Observable, interval, EMPTY } from 'rxjs';
import { takeWhile, filter, map, share } from 'rxjs/operators';
import { ToastService } from './toast.service';
import { PendingInterceptorService } from '../interceptors/pending.interceptor.service';
import { NotificationsService } from './notification.service';
import { progressBar } from '../models/progress-bar-model';
import { Injectable } from '@angular/core';
@Injectable()
export class SyncService {
  private autoSyncObservable: Observable<any>;
  private statsObservable: Observable<any>;

  progressBar: progressBar = {
    title: '',
    status: '',
    progress: 0,
    accounts: [],
    actionType: '',
    expanded: false,
    lastActionTime: new Date(),
    update: false,
    showLongRunningSyncWarning: false,
  };
  progress: BehaviorSubject<progressBar> = new BehaviorSubject(
    this.progressBar
  );
  autoSyncStatus = new BehaviorSubject(false);
  pendingAction: boolean = false;
  alive: boolean = true;
  url: string;

  constructor(
    private http: HttpClient,
    private translate: TranslateService,
    private router: Router,
    private pendingInterceptorService: PendingInterceptorService,
    private toast: ToastService,
    private notificationsService: NotificationsService
  ) {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: any) => {
        this.url = event.url.split(/[?#]/)[0];
      });
  }

  resetProgressBar() {
    this.progressBar = {
      title: '',
      status: '',
      progress: 0,
      accounts: [],
      actionType: '',
      expanded: false,
      lastActionTime: new Date(),
      update: false,
      showLongRunningSyncWarning: false,
    };

    this.setProgress(this.progressBar);
  }

  closeProgressBarAfterTimeout() {
    if (
      (this.progressBar.actionType === 'Sync' &&
        this.url === '/synchronization/status') ||
      this.progressBar.actionType !== 'Sync'
    ) {
      setTimeout(() => {
        if (this.progressBar.progress === 100) {
          this.resetProgressBar();
        }
      }, 25000);
    }
  }

  setProgress(data: progressBar) {
    this.progressBar = data;
    this.progress.next(data);
  }

  updateProgressBar() {
    this.http.get('/api/progress/subscription').subscribe((resp: any) => {
      // set title & status
      this.translate
        .get(
          'PROGRESS_TITLE_' +
            resp.Action.toUpperCase() +
            '_' +
            resp.StateOrResult.toUpperCase()
        )
        .subscribe((res) => {
          let title = res,
            status = this.translate.instant(
              'PROGRESS_STATUS_' +
                resp.Action.toUpperCase() +
                '_' +
                resp.StateOrResult.toUpperCase(),
              {
                var0: resp.Progress,
              }
            );

          // if progress != 100, then we are still in progress. Why back end returns StateOrResult as 'Completed'?
          if (resp.Progress > 0 && resp.Progress !== 100) {
            title = this.translate.instant(
              'PROGRESS_TITLE_' + resp.Action.toUpperCase() + '_INPROGRESS'
            );
            status = this.translate.instant(
              'PROGRESS_STATUS_' + resp.Action.toUpperCase() + '_INPROGRESS',
              {
                var0: resp.Progress,
              }
            );
          }

          if (resp && resp.IsInAction) {
            this.progressBar = {
              title: title,
              status: status,
              progress: resp.Progress > 0 ? resp.Progress : 1,
              actionType: resp.Action,
              accounts: resp.SyncAccounts,
              lastActionTime: new Date(),
              expanded:
                this.progressBar && this.progressBar.expanded ? true : false,
              update: resp.Progress === 100 ? true : false,
              showLongRunningSyncWarning: resp.ShowLongRunningSyncWarning,
            };

            // show toast if progress == 100
            if (
              resp.Progress === 100 &&
              (resp.Action === 'Sync' || resp.Action === 'Restore')
            ) {
              // clear toasts
              this.toast.clear();

              // show progress ended toast
              this.toast.pop(title);
            }

            // set progress bar data
            this.setProgress(this.progressBar);
          } else if (this.progressBar && this.progressBar.progress > 0) {
            // maybe we have old data
            this.progressBar = {
              title: title,
              status: '',
              progress: 100,
              actionType: resp.Action,
              accounts: [],
              lastActionTime: new Date(),
              expanded:
                this.progressBar && this.progressBar.expanded ? true : false,
              update: true,
              showLongRunningSyncWarning: resp.ShowLongRunningSyncWarning,
            };

            // set progress bar data
            this.setProgress(this.progressBar);

            // close progress bar after timeout
            this.closeProgressBarAfterTimeout();

            // update top red notifications
            this.notificationsService.getWebMessages();
          }
        });
    });
  }

  getProgressData() {
    const now = new Date(Date.now() - 30000);
    let lastActionDate = new Date(Date.now());

    if (this.progressBar && this.progressBar.lastActionTime) {
      lastActionDate = this.progressBar.lastActionTime;
    }

    // if last progress action is older then 30 seconds, lets get latest status
    if (lastActionDate < now) {
      this.updateProgressBar();
    }
  }

  loadProgress() {
    this.updateProgressBar();

    // subscribe to pending http requests
    this.pendingInterceptorService.pendingRequestsStatus.subscribe(
      (resp: boolean) => {
        this.pendingAction = resp;
      }
    );

    // check progress bar every 60 seconds
    interval(60000)
      .pipe(takeWhile(() => this.alive))
      .subscribe(() => {
        if (!this.pendingAction) {
          this.getProgressData();
        }
      });
  }

  closeProgress() {
    this.alive = false;
    this.resetProgressBar();
  }

  getTypeStats() {
    if (this.statsObservable) {
      return this.statsObservable;
    } else {
      this.statsObservable = this.http
        .get('/api/synchronization/typestats')
        .pipe(
          map((resp) => {
            this.statsObservable = EMPTY;
            return resp;
          }),
          share()
        );

      return this.statsObservable;
    }
  }

  getAccountStats(model: any) {
    return this.http.post('/api/synchronization/accountstats', model);
  }

  toggleAutoSync() {
    return this.http.get('/api/features/toggleAutoSync');
  }

  clearFilters(model: any) {
    return this.http.post('/api/synchronization/clearfilters', model);
  }

  checkBackupAccounts() {
    this.http.get('/api/backupaccounts/activecount').subscribe(() => {
      this.startSync();
    });
  }

  startSync(redirectUrl?: string) {
    this.http.post('/api/synchronization/start', {}).subscribe(() => {
      const path = window.location.pathname.split('/');

      this.progressBar = {
        title: this.translate.instant('PROGRESS_TITLE_SYNC_REQUESTSENT'),
        status: this.translate.instant('PROGRESS_STATUS_SYNC_REQUESTSENT'),
        progress: 1,
        actionType: 'Sync',
        accounts: [],
        expanded:
          path[1].toLowerCase() === 'synchronization' &&
          path[2].toLowerCase() === 'status'
            ? true
            : false,
        lastActionTime: new Date(),
        update: false,
        showLongRunningSyncWarning: false,
      };

      // set progress bar data
      this.setProgress(this.progressBar);

      if (
        (path[1].toLowerCase() !== 'synchronization' ||
          path[2].toLowerCase() !== 'status') &&
        !redirectUrl
      ) {
        // redirect to status page
        this.router.navigate(['/synchronization/status']);
      } else if (redirectUrl) {
        this.router.navigate([redirectUrl], {
          queryParamsHandling: 'merge',
          replaceUrl: true,
        });
      }
    });
  }

  cancelAction() {
    this.http.post('/api/synchronization/cancel', {}).subscribe(() => {
      this.progressBar = {
        title: this.translate.instant('PROGRESS_TITLE_CANCELATION_REQUESTSENT'),
        status: this.translate.instant(
          'PROGRESS_STATUS_CANCELATION_REQUESTSENT'
        ),
        progress: 1,
        actionType: 'CancelAction',
        accounts: [],
        expanded: this.progressBar && this.progressBar.expanded ? true : false,
        lastActionTime: new Date(),
        update: false,
        showLongRunningSyncWarning: false,
      };

      // set progress bar data
      this.setProgress(this.progressBar);
    });
  }

  getAutoSyncStatus(id: string) {
    if (this.autoSyncObservable) {
      return this.autoSyncObservable;
    } else {
      this.autoSyncObservable = this.http
        .get('/api/account/AutoSyncStatus?Id=' + id)
        .pipe(
          map((resp: any) => {
            this.autoSyncObservable = EMPTY;
            this.autoSyncStatus.next(resp);
            return resp;
          }),
          share()
        );

      return this.autoSyncObservable;
    }
  }

  updateAutoSyncStatus(status: any) {
    this.autoSyncStatus.next(status);
  }

  skipBackups() {
    return this.http.get('/api/backups/skip');
  }

  swapSyncAccount() {
    return this.http.get('/api/Synchronization/SwapSyncAccount');
  }
}
