import { BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { map, share } from 'rxjs/operators';
import { User } from '../models/user-model';
import { NotificationsService } from './notification.service';
import { SyncService } from './sync.service';
//import { ChannelService } from './channel.service';
import { ToastService } from './toast.service';
import { CurrentSubscriptionInfo } from '../models/current-subscription-model';
import { SessionModel } from '../models/session-model';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { Address } from '../models/address-model';
import { SurveyModel } from 'src/app/shared/models/survey-model';
import { CancelSubscription } from '../models/cancel-subscription-model';
import { ChangePlanModel } from '../models/change-plan-model';

@Injectable()
export class UserBaseService {
  userObservable: Observable<any>;
  subscriptionObservable: Observable<any>;
  currentUser: BehaviorSubject<User | null>;
  currentSubscription: BehaviorSubject<CurrentSubscriptionInfo | null>;
  returnUrl: string;
  cancelData: any;

  constructor(
    protected http: HttpClient,
    protected router: Router,
    private translate: TranslateService,
    private notificationsService: NotificationsService,
    private syncService: SyncService,
    private toastService: ToastService,
    //private channelService: ChannelService,
    //private gtmService: GoogleTagManagerService,
    private cookieService: CookieService,
    private route: ActivatedRoute,
    private datePipe: DatePipe
  ) {}

  getSubscription(forceReload?: boolean) {
    if (!this.subscriptionObservable || forceReload) {
      this.subscriptionObservable = this.http
        .get('/api/subscription/getcurrentsubscriptioninfo')
        .pipe(
          map((resp: any) => {
            this.subscriptionObservable = EMPTY;
            this.currentSubscription.next(resp);
            return resp;
          })
        );
    }
    return this.subscriptionObservable;
  }

  loginByToken(grantType: string, token: string) {
    const body =
      'grant_type=' + grantType + '&token=' + token + '&clientId=self';
    this.loginByBody(body);
  }

  updateUser(user: User) {
    this.currentUser.next(user);
  }

  loginByBody(body: string, returnUrl?: string) {
    this.http
      .post('/token', body, {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        ),
      })
      .subscribe((resp: any) => {
        resp.type = 'Email';

        // store session in localStorage
        this.storeSession(resp);

        // get user data
        this.getUser().subscribe((res) => {
          this.processLogin(res, returnUrl);
        });
      });
  }

  getUserAddresses() {
    return this.http.get('/api/GetUserAddresses');
  }

  externalLogin(model: SessionModel, returnUrl: string) {
    // store data in localStorage
    this.storeSession(model);

    // get user data
    this.getUser().subscribe((res) => {
      this.processLogin(res, returnUrl);
    });
  }

  private processLogin(user: any, returnUrl?: string) {
    // save user data in localStorage
    localStorage.setItem('user', JSON.stringify(user));

    // first time login tracking
    if (user.IsFirstTimeLogin === 'social') {
      let signupEventName = 'SignUp';

      if (this.cookieService.get('sharing_accept') === 'true') {
        signupEventName = 'SignUpSharingAccept';
      }
      // send event to GTM
      const gtmTag = {
        event: 'AccountSignUpSocial',
        accountSignUpSocialEventName: signupEventName,
      };
      //this.gtmService.pushTag(gtmTag);
    }

    // save user email preferences on license message
    if (user.IsFirstTimeLogin && localStorage.getItem('privacySettings')) {
      const settings = JSON.parse(localStorage.getItem('privacySettings')!);

      this.http
        .post('/api/account/SaveStartPreferences', {
          ReceiveProductOffers: settings.sendupdates,
          ReceiveCompanyNews: settings.sendoffers,
        })
        .subscribe(() => {
          localStorage.removeItem('privacySettings');
        });
    }

    // start signalR connection
    //this.channelService.start(user.DeploymentId);
    // register browser push messages
    this.notificationsService.registerGCM();
    // load web messages after 2 sec timeout
    this.notificationsService.getWebMessages();
    // register mobile app to user account
    this.registerMobileApp();
    // load progress data
    this.syncService.loadProgress();
    // get return url
    if (returnUrl) {
      this.returnUrl = returnUrl;
    } else if (localStorage.getItem('sharing')) {
      this.returnUrl = 'share/manage-shared-calendars';
    } else if (this.route.snapshot.queryParams['returnUrl']) {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
    } else if (this.cookieService.check('RedirectUrl')) {
      this.returnUrl = this.cookieService.get('RedirectUrl');
      this.cookieService.delete('RedirectUrl');
    } else {
      this.returnUrl = 'synchronization/status';
    }
    //login to teams if redirect is teams link
    if (this.returnUrl && this.returnUrl.indexOf('teams') > -1) {
      window.location = ('/' + this.returnUrl) as any;
      return;
    }
    // login successful, redirect to return url || status page
    this.router.navigateByUrl(this.returnUrl, { replaceUrl: true });
  }

  private registerMobileApp() {
    // check if user is using mobile app
    const installId = localStorage.getItem('installId');

    if (installId) {
      // register mobile appId to account
      this.notificationsService.assignDeviceToUser(+installId).subscribe(() => {
        // on success remove installId
        localStorage.removeItem('installId');
      });
    }
  }

  private storeSession(data: SessionModel) {
    localStorage.setItem('access_token', data.access_token);
    localStorage.setItem(
      'token_expiration_time',
      (new Date().getTime() + data.expires_in * 1000).toString()
    );
    // send event to GTM
    const gtmTag = {
      event: 'AccountSignIn',
      accountSignInDataType: data.type,
    };
    //this.gtmService.pushTag(gtmTag);
  }

  isInRole(role: string) {
    // role detection
    if (this.currentUser && this.currentUser.value) {
      for (const i in this.currentUser.value.Roles) {
        if (this.currentUser.value.Roles[i] === role) {
          return true;
        }
      }
    }

    return false;
  }

  logout(
    backend?: boolean,
    returnUrl: string = '',
    skipRedirect?: boolean,
    errorCode?: string
  ) {
    if (!returnUrl || returnUrl.length < 1) {
      returnUrl = '/';
    }

    if (errorCode) {
      returnUrl += '?errorCode=' + errorCode;
    }

    if (backend) {
      this.http.post('/api/account/logout', {}).subscribe(() => {
        localStorage.removeItem('access_token');
        localStorage.removeItem('token_expiration_time');
        localStorage.removeItem('user');
        localStorage.removeItem('user_features');
        this.currentUser.next(null);

        this.logoutInternal();

        if (
          window.location.pathname.split('/').includes('teams') ||
          window.location.pathname.split('/').includes('admin')
        ) {
          window.location.href = returnUrl;
          return;
        }

        // redirect to login page
        if (returnUrl && returnUrl.indexOf('quick-setup/') > -1) {
          this.router.navigate(['/quick-setup/step1/login'], {
            queryParamsHandling: 'merge',
            replaceUrl: true,
          });
        } else {
          this.router.navigate(['/'], {
            replaceUrl: true,
            queryParams: {
              returnUrl: returnUrl === '/' ? null : returnUrl,
            },
          });
        }
      });
    } else {
      localStorage.removeItem('access_token');
      localStorage.removeItem('user');
      localStorage.removeItem('token_expiration_time');
      localStorage.removeItem('user_features');
      this.currentUser.next(null);

      this.logoutInternal();

      if (
        window.location.pathname.split('/').includes('teams') ||
        window.location.pathname.split('/').includes('admin')
      ) {
        window.location.href = returnUrl;
        return;
      }

      if (skipRedirect != true) {
        // redirect to login page
        this.router.navigate(['/'], { replaceUrl: true });
      }
    }
  }

  logoutInternal() {
    // stop signalR connection
    //this.channelService.stop();
    // clear toasts
    this.toastService.clear();
    // clear web messages
    this.notificationsService.clearWebMessages();
    // close progress data
    this.syncService.closeProgress();
  }

  getUser() {
    if (this.userObservable) {
      return this.userObservable;
    } else {
      this.userObservable = this.http.get('/api/user').pipe(
        map((resp: any) => {
          this.userObservable = EMPTY;
          return resp;
        }),
        share()
      );

      return this.userObservable;
    }
  }

  recoverPassword(email: string) {
    return this.http.post('/api/ForgotPassword', {
      Email: email,
    });
  }

  updateBillingAddress(address: Address) {
    return this.http.post('/api/UpdateBillingAddress', address);
  }

  updateContactAddress(address: Address) {
    return this.http.post('/api/UpdateContactAddress', address);
  }

  removeAccountModal() {
    // confirmation modal
    let text = this.translate.instant('REMOVE_ACCOUNT_MODAL_TEXT');

    // check if user have subscription and it is still active
    if (
      this.currentUser!.value!.Subscription &&
      this.currentUser!.value!.Subscription.SubscriptionStatus === 'Active'
    ) {
      if (
        new Date(this.currentUser!.value!.Subscription!.EndDate!).getTime() >
        new Date().getTime()
      ) {
        text = this.translate.instant('REMOVE_ACCOUNT_MODAL_TEXT_PAID', {
          var0: this.datePipe.transform(
            this.currentUser!.value!.Subscription.EndDate,
            'MMM d, y'
          ),
        });
      }
    }
    return text;
  }

  removeAccount() {
    // on close
    this.http.post('/api/user/delete', {}).subscribe(() => {
      // send event to GTM
      const gtmTag = {
        event: 'AccountRemove',
      };
      //this.gtmService.pushTag(gtmTag);
      // logout user
      setTimeout(() => {
        this.logout(true);
      });
    });
  }

  unsubscribe(id: string) {
    return this.http.get('/api/unsubscribe?Id=' + id);
  }

  setCancelData(data: SurveyModel | null) {
    this.cancelData = data;
  }

  getCancelData() {
    return this.cancelData;
  }

  cancelSubscription(model: CancelSubscription) {
    return this.http.post('/api/subscription/cancel', model);
  }

  getSubscriptionCancelSettings() {
    return this.http.get('/api/subscription/getcancelationeffects');
  }

  changeSubscriptionPlan(model: ChangePlanModel) {
    return this.http.post('/api/subscription/changeplan', model);
  }
}
