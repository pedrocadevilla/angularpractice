import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PurchaseServiceBase {
  constructor(
    protected http: HttpClient,
    protected gtmService: GoogleTagManagerService
  ) {}

  changePaymentMethod(model: any) {
    return this.http.post('/api/changePaymentMethod', model);
  }

  checkForPendingPayment() {
    return this.http.get('/api/checkPendingPayments');
  }

  processPaypal(token: any) {
    return this.http.post('/api/processPayment/' + token, {});
  }

  processPaypalChange(token: any) {
    return this.http.post('/api/processPaymentChange/' + token, {});
  }

  getPurchaseHistory(model: any) {
    return this.http.post('/api/payments', model);
  }

  purchase(model: any) {
    return this.http.post('/api/purchase', model);
  }

  payNow() {
    return this.http.get('/api/subscription/paynow');
  }

  // GTM purchase events tracking
  tracking(params: any) {
    let price = params.Subscription.ProductPrice;

    const gtmTag = {
      event: 'PurchaseCompleted',
      transactionId: params.InvoiceNumber.toString(),
      transactionTotal: price.toString(),
      currencyCode: params.Subscription.Currency,
      transactionProducts: [
        {
          id: params.InvoiceNumber.toString(),
          name: params.Subscription.ProductName,
          price: price.toString(),
          sku: params.InvoiceNumber.toString(),
          quantity: '1',
        },
      ],
      ecommerce: {
        currency: params.Subscription.Currency,
        transaction_id: params.InvoiceNumber,
        value: price.toString(),
        items: [
          {
            item_id: params.Subscription.FTProductId,
            item_name: params.Subscription.ProductName,
            price: price.toString(),
            quantity: '1',
          },
        ],
      },
    };

    this.gtmService.pushTag(gtmTag);
  }

  getPurchaseDataForPromoCode(promoCode: string): Observable<any> {
    return this.http.get(
      '/api/purchase/getpurchasedataforpromocode?promoCode=' + promoCode
    );
  }

  purchaseNow(promoCode: string, pricingPlanId: string): Observable<number> {
    return this.http.get<number>(
      `/api/subscription/purchaseplanfornextperiod?promoCode=${promoCode}&pricingPlanId=${pricingPlanId}`
    );
  }
}
