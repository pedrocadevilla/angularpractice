import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable()
export class ExternalLoginServices {
  constructor(private http: HttpClient) {}

  getAppleLoginUrl(returnUrl?: string, loginHint?: string): Observable<string> {
    let query = '';
    if (returnUrl && returnUrl.length > 0) {
      query += '?returnUrl=' + returnUrl;
    }
    if (loginHint && loginHint.length > 0) {
      query += query.length > 0 ? '&' : '?';
      query += 'loginHint=' + loginHint;
    }

    return this.http.get<string>('/api/GetAppleLoginUrl' + query);
  }
}
