import { Injectable, Inject } from '@angular/core';
import { User } from '../models/user-model';

declare global {
  interface Window {
    __lc: any;
    LiveChatWidget: any;
  }
}
export class LcWindow extends Window {
  override LiveChatWidget: any;
}
window.__lc = window.__lc || { license: 10998812 };
window.LiveChatWidget = window.LiveChatWidget || {};

@Injectable({
  providedIn: 'root',
})
export class WindowService {
  constructor(@Inject(LcWindow) protected window: LcWindow) {}

  setLiveChatNameEmail(resp: User) {
    this.logState('setLiveChatNameEmail');
    setTimeout(() => {
      const prevState = this.window.LiveChatWidget.get('state').visibility;
      this.window.LiveChatWidget.call('set_customer_name', resp.FullName);
      this.window.LiveChatWidget.call('set_customer_email', resp.Login);
      if (prevState === 'hidden') {
        setTimeout(this.hideCompletelyLiveChat, 100);
      }
    }, 1000);
  }

  updateLiveChatSessionVariables(subscription: string) {
    this.logState('updateLiveChatSessionVariables');
    this.window.LiveChatWidget.on('visibility_changed', (data: any) => {
      if (data.visibility === 'maximized') {
        this.window.LiveChatWidget.call('update_session_variables', {
          Subscription: subscription,
        });
      }
    });
  }

  hideCompletelyLiveChat() {
    this.logState('hideCompletelyLiveChat');
    this.window.LiveChatWidget.call('hide');
  }

  showLiveChat() {
    this.logState('showLiveChat');
    this.window.LiveChatWidget.call('minimize');
  }

  hideLiveChat() {
    this.logState('hideLiveChat');
    this.window.LiveChatWidget.call('hide_greeting');
    this.window.LiveChatWidget.on('greeting_displayed', () =>
      this.window.LiveChatWidget.call('hide_greeting')
    );
    this.window.LiveChatWidget.on('new_event', (data: any) => {
      if (data.greeting) {
        this.window.LiveChatWidget.call('hide_greeting');
      }
    });
  }

  logState(methodName: any) {
    return;

    const prevState = this.window.LiveChatWidget.get('state').visibility;
  }
}
