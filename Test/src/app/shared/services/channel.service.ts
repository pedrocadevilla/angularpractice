import { Injectable, Inject, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from './toast.service';
import { SyncService } from './sync.service';
import { RestoreService } from './restore.service';
import { ChannelBaseService, SignalrWindow } from './channel-base.service';
import { progressBar } from '../models/progress-bar-model';
import { NotificationsService } from './notification.service';
import { HubProxy } from '../models/hub-proxy-model';
@Injectable()
export class ChannelService extends ChannelBaseService {
  deploymentId: string;
  title: string;
  hideToast: boolean;
  status: string;
  progressBar: progressBar = {
    title: '',
    status: '',
    progress: 0,
    accounts: [],
    actionType: '',
    expanded: false,
    lastActionTime: new Date(),
    update: false,
    showLongRunningSyncWarning: false,
  };

  constructor(
    @Inject(SignalrWindow) window: SignalrWindow,
    private translate: TranslateService,
    private toast: ToastService,
    private syncService: SyncService,
    private restoreService: RestoreService,
    private notificationsService: NotificationsService,
    ngZone: NgZone
  ) {
    super(window, ngZone);

    this.hideToast =
      window.location.href.indexOf('quick-setup/') > -1 ? true : false;

    // subscribe for current progress data
    syncService.progress.subscribe((resp: any) => {
      this.progressBar = resp;
    });

    this.hubProxy.on('subscriptionScheduled', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (
          this.deploymentId === data.deploymentId &&
          this.progressBar.actionType !== 'CancelAction'
        ) {
          this.progressBar.title = translate.instant(
            'PROGRESS_TITLE_' +
              data.Entry!.ActionType!.toUpperCase() +
              '_SCHEDULED'
          );
          this.progressBar.status = translate.instant(
            'PROGRESS_STATUS_' +
              data.Entry!.ActionType!.toUpperCase() +
              '_REQUESTSENT'
          );
          this.progressBar.progress = 1;
          this.progressBar.actionType = data.Entry!.ActionType!;
          this.progressBar.lastActionTime = new Date();
          this.progressBar.showLongRunningSyncWarning =
            this.progressBar.showLongRunningSyncWarning;

          // set progress bar data
          syncService.setProgress(this.progressBar);

          // clear toasts
          toast.clear();
        }
      });
    });

    this.hubProxy.on('subscriptionSynchronizationStarted', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (
          this.deploymentId === data.deploymentId &&
          this.progressBar.actionType !== 'CancelAction'
        ) {
          this.progressBar.title = translate.instant(
            'PROGRESS_TITLE_SYNC_INPROGRESS'
          );
          this.progressBar.status = translate.instant(
            'PROGRESS_STATUS_SYNC_INPROGRESS',
            {
              var0: 1,
            }
          );
          this.progressBar.progress = 1;
          this.progressBar.lastActionTime = new Date();
          this.progressBar.showLongRunningSyncWarning =
            this.progressBar.showLongRunningSyncWarning;
          // set progress bar data
          syncService.setProgress(this.progressBar);

          // clear toasts
          toast.clear();
        }
      });
    });

    this.hubProxy.on(
      'subscriptionSynchronizationProgress',
      (data: HubProxy) => {
        this.ngZone.run(() => {
          if (
            this.deploymentId === data.deploymentId &&
            (data.state === 'CancelationInProgress' ||
              data.state === 'CancelationCompleted' ||
              this.progressBar.actionType !== 'CancelAction')
          ) {
            // increase progress bar value if it is higher but lower then 100
            if (data.progress! > this.progressBar.progress) {
              this.progressBar.progress =
                data.progress! === 100 ? 99 : data.progress!;
            }

            this.progressBar.showLongRunningSyncWarning =
              data.showLongRunningSyncWarning;

            if (
              data.state === 'CancelationInProgress' ||
              data.state === 'CancelationCompleted'
            ) {
              this.progressBar.title = translate.instant(
                'PROGRESS_TITLE_SYNC_CANCELATIONINPROGRESS'
              );
              this.progressBar.status = translate.instant(
                'PROGRESS_STATUS_SYNC_CANCELATIONINPROGRESS',
                {
                  var0: this.progressBar.progress,
                }
              );
            } else if (
              data.state === 'InProgress' ||
              data.state === 'Completed'
            ) {
              this.progressBar.title = translate.instant(
                'PROGRESS_TITLE_SYNC_INPROGRESS'
              );
              this.progressBar.status = translate.instant(
                'PROGRESS_STATUS_SYNC_INPROGRESS',
                {
                  var0: this.progressBar.progress,
                }
              );
            }

            // update last action date
            this.progressBar.lastActionTime = new Date();

            // set progress bar data
            syncService.setProgress(this.progressBar);
          }
        });
      }
    );

    this.hubProxy.on('accountSynchronizationStarted', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (
          this.deploymentId === data.deploymentId &&
          this.progressBar.actionType !== 'CancelAction'
        ) {
          let accountExists = false;

          if (this.progressBar && this.progressBar.accounts) {
            for (const i in this.progressBar.accounts) {
              if (data.accountId === this.progressBar.accounts[i].accountId) {
                accountExists = true;
                break;
              }
            }
          }

          if (!accountExists) {
            this.progressBar.accounts.push(data.account!);
          }

          this.progressBar.showLongRunningSyncWarning =
            this.progressBar.showLongRunningSyncWarning;

          // update last action date
          this.progressBar.lastActionTime = new Date();

          // set progress bar data
          syncService.setProgress(this.progressBar);
        }
      });
    });

    this.hubProxy.on('accountSynchronizationProgress', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (
          this.deploymentId === data.deploymentId &&
          this.progressBar.actionType !== 'CancelAction'
        ) {
          let accountExists = false;

          if (this.progressBar && this.progressBar.accounts) {
            for (const i in this.progressBar.accounts) {
              if (data.accountId === this.progressBar.accounts[i].accountId) {
                accountExists = true;
                this.progressBar.accounts[i].progress = data.progress!;
                break;
              }
            }
          }

          if (!accountExists) {
            this.progressBar.accounts.push(data.account!);
          }

          this.progressBar.showLongRunningSyncWarning =
            this.progressBar.showLongRunningSyncWarning;

          // update last action date
          this.progressBar.lastActionTime = new Date();

          // set progress bar data
          syncService.setProgress(this.progressBar);
        }
      });
    });

    this.hubProxy.on('accountSynchronizationEnded', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (
          this.deploymentId === data.deploymentId &&
          this.progressBar.actionType !== 'CancelAction'
        ) {
          let accountExists = false;

          if (this.progressBar && this.progressBar.accounts) {
            for (const i in this.progressBar.accounts) {
              if (data.accountId === this.progressBar.accounts[i].accountId) {
                accountExists = true;
                this.progressBar.accounts[i].progress = 100;
                break;
              }
            }
          }

          if (!accountExists) {
            this.progressBar.accounts.push(data.account!);
          }

          this.progressBar.showLongRunningSyncWarning =
            this.progressBar.showLongRunningSyncWarning;

          // update last action date
          this.progressBar.lastActionTime = new Date();

          // set progress bar data
          syncService.setProgress(this.progressBar);
        }
      });
    });

    this.hubProxy.on('subscriptionSynchronizationEnded', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (this.deploymentId === data.deploymentId) {
          const title = translate.instant(
            'PROGRESS_TITLE_SYNC_' + data.result!.toUpperCase()
          );

          this.progressBar = {
            title: title,
            status: '',
            progress: 100,
            actionType: 'Sync',
            accounts: [],
            lastActionTime: new Date(),
            expanded: true,
            update: true,
            showLongRunningSyncWarning: data.showLongRunningSyncWarning,
          };

          // clear toasts
          toast.clear();

          if (!this.hideToast) {
            // show action ended toast
            toast.pop(title);
          }

          // set progress bar data
          syncService.setProgress(this.progressBar);

          // close progress bar after timeout
          syncService.closeProgressBarAfterTimeout();

          // update top red notifications after timeout
          setTimeout(() => {
            this.notificationsService.getWebMessages();
          }, 1000);
        }
      });
    });

    this.hubProxy.on('backupStarted', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (this.deploymentId === data.deploymentId) {
          // clear toasts
          toast.clear();
        }
      });
    });

    this.hubProxy.on('backupEnded', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (
          this.deploymentId === data.deploymentId &&
          this.progressBar.actionType !== 'CancelAction'
        ) {
          // clear toasts
          toast.clear();

          // show backup ended toast
          if (!this.hideToast) {
            toast.pop(
              translate.instant(
                'PROGRESS_TITLE_BACKUP_' + data.result!.toUpperCase(),
                {
                  var0: data.accountUsername,
                }
              )
            );
          }
        }
      });
    });

    this.hubProxy.on('RestoreStarted', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (
          this.deploymentId === data.deploymentId &&
          this.progressBar.actionType !== 'CancelAction'
        ) {
          this.progressBar = {
            title: translate.instant('PROGRESS_TITLE_RESTORE_INPROGRESS'),
            status: translate.instant('PROGRESS_STATUS_RESTORE_INPROGRESS', {
              var0: 0,
            }),
            progress: 1,
            actionType: 'Restore',
            accounts: [],
            lastActionTime: new Date(),
            expanded: this.progressBar.expanded,
            update: false,
            showLongRunningSyncWarning:
              this.progressBar.showLongRunningSyncWarning,
          };

          // clear toasts
          toast.clear();

          // set progress bar data
          syncService.setProgress(this.progressBar);
        }
      });
    });

    this.hubProxy.on('RestoreProgress', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (
          this.deploymentId === data.deploymentId &&
          this.progressBar.actionType !== 'CancelAction'
        ) {
          this.progressBar.progress = data.progress!;
          this.progressBar.accounts = [];

          if (
            data.state === 'CancelationInProgress' ||
            data.state === 'CancelationCompleted'
          ) {
            this.progressBar.title = translate.instant(
              'PROGRESS_TITLE_RESTORE_CANCELATIONINPROGRESS'
            );
            this.progressBar.status = translate.instant(
              'PROGRESS_STATUS_RESTORE_CANCELATIONINPROGRESS',
              {
                var0: data.progress,
              }
            );
          } else if (
            data.state === 'InProgress' ||
            data.state === 'Completed'
          ) {
            this.progressBar.title = translate.instant(
              'PROGRESS_TITLE_RESTORE_INPROGRESS'
            );
            this.progressBar.status = translate.instant(
              'PROGRESS_STATUS_RESTORE_INPROGRESS',
              {
                var0: data.progress,
              }
            );
          }

          this.progressBar.showLongRunningSyncWarning =
            this.progressBar.showLongRunningSyncWarning;

          // update last action date
          this.progressBar.lastActionTime = new Date();

          // set progress bar data
          syncService.setProgress(this.progressBar);
        }
      });
    });

    this.hubProxy.on('RestoreEnded', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (
          this.deploymentId === data.deploymentId &&
          this.progressBar.actionType !== 'CancelAction'
        ) {
          this.progressBar = {
            title: translate.instant(
              'PROGRESS_TITLE_RESTORE_' + data.result!.toUpperCase()
            ),
            status: '',
            progress: 100,
            actionType: 'Restore',
            accounts: [],
            lastActionTime: new Date(),
            expanded: true,
            update: true,
            showLongRunningSyncWarning:
              this.progressBar.showLongRunningSyncWarning,
          };

          // clear toasts
          toast.clear();

          // show restore ended toast
          if (!this.hideToast) {
            toast.pop(
              translate.instant(
                'PROGRESS_TITLE_RESTORE_' + data.result!.toUpperCase()
              )
            );
          }

          // set progress bar data
          syncService.setProgress(this.progressBar);

          // set restore data
          restoreService.setRestoreProgress(data.backupFilterModel!);

          // close progress bar after timeout
          syncService.closeProgressBarAfterTimeout();
        }
      });
    });

    this.hubProxy.on('scenarioCancelationScheduled', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (this.deploymentId === data.deploymentId) {
          this.progressBar = {
            title: translate.instant('PROGRESS_TITLE_CANCELATION_SCHEDULED'),
            status:
              translate.instant('CANCELATION_SCHEDULED_FOR_TYPE') +
              translate.instant('FOLDERTYPE_' + data.folderType!.toUpperCase()),
            progress: 1,
            actionType: 'CancelAction',
            accounts: [],
            lastActionTime: new Date(),
            expanded: this.progressBar.expanded,
            update: false,
            showLongRunningSyncWarning:
              this.progressBar.showLongRunningSyncWarning,
          };

          // set progress bar data
          syncService.setProgress(this.progressBar);
        }
      });
    });

    this.hubProxy.on('scenarioCancelationStarted', (data: HubProxy) => {
      this.ngZone.run(() => {
        if (this.deploymentId === data.deploymentId) {
          this.progressBar = {
            title: translate.instant('PROGRESS_TITLE_CANCELATION_SCHEDULED'),
            status:
              translate.instant('CANCELATION_SCHEDULED_FOR_TYPE') +
              translate.instant('FOLDERTYPE_' + data.folderType!.toUpperCase()),
            progress: 1,
            actionType: 'CancelAction',
            accounts: [],
            lastActionTime: new Date(),
            expanded: this.progressBar.expanded,
            update: false,
            showLongRunningSyncWarning: data.showLongRunningSyncWarning,
          };

          // set progress bar data
          syncService.setProgress(this.progressBar);
        }
      });
    });
  }

  start(deploymentId: string): void {
    this.deploymentId = deploymentId;

    this.startInternal();
  }
}
