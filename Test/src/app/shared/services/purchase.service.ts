import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { PurchaseServiceBase } from './purchase-base.service';

@Injectable()
export class PurchaseService extends PurchaseServiceBase {
  constructor(
    protected override http: HttpClient,
    protected override gtmService: GoogleTagManagerService
  ) {
    super(http, gtmService);
  }

  getAvailablePromoPlan(name: any) {
    return this.http.get('/api/purchase/getavailablepromoplan/' + name);
  }

  getPricingPlan(id: any) {
    return this.http.get('/api/getpricingplan/' + id);
  }

  trackUserGoPremiumPress(url: any) {
    return this.http.get('/api/trackGoPremiumPress?url=' + url);
  }

  specialOfferNotificationIsShown(subject: any) {
    return this.http.get(
      '/api/specialOfferNotificationIsShown?subject=' + subject
    );
  }

  limitationsDialogIsShown(subject: any) {
    return this.http.get('/api/limitationsDialogIsShown?subject=' + subject);
  }

  sendFreePlanOffer(subject: any) {
    return this.http.get(
      '/api/SendUserSelectedFreePlanSpecialOffer?subject=' + subject
    );
  }
}
