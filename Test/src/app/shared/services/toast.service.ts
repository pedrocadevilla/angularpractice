import { Injectable } from '@angular/core';
import { Observable, Subject, Observer } from 'rxjs';
import { share } from 'rxjs/operators';

import { Toast } from '../models/toast-model';
import { GuidService } from './guid.service';
@Injectable()
export class ToastService {
  addToast: Observable<Toast>;
  private _addToast: Observer<Toast>;

  clearToasts: Observable<IClearWrapper>;
  private _clearToasts: Observer<IClearWrapper>;

  removeToast: Observable<IClearWrapper>;
  _removeToastSubject: Subject<IClearWrapper>;

  // Creates an instance of ToastService
  constructor(private guidService: GuidService) {
    this.addToast = new Observable<Toast>(
      (observer: any) => (this._addToast = observer)
    ).pipe(share());
    this.clearToasts = new Observable<IClearWrapper>(
      (observer: any) => (this._clearToasts = observer)
    ).pipe(share());
    this._removeToastSubject = new Subject<IClearWrapper>();
    this.removeToast = this._removeToastSubject.pipe(share());
  }

  // Synchronously create and show a new toast instance.
  pop(body?: string | Toast): Toast {
    const toast = typeof body === 'string' ? { body: body } : body;

    toast!.toastId = this.guidService.newGuid();

    if (!this._addToast) {
      throw new Error(
        'No Toaster Containers have been initialized to receive toasts.'
      );
    }

    this._addToast.next(toast!);
    return toast!;
  }

  // Asynchronously create and show a new toast instance
  popAsync(body?: string): Observable<Toast> {
    setTimeout(() => {
      this.pop(body);
    }, 0);

    return this.addToast;
  }

  clear(toastId?: string, toastContainerId?: number) {
    const clearWrapper: IClearWrapper = {
      toastId: toastId,
      toastContainerId: toastContainerId,
    };

    this._clearToasts.next(clearWrapper);
  }
}

export interface IClearWrapper {
  toastId?: string;
  toastContainerId?: number;
}
