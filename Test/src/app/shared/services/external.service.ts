import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ExternalService {
  private observable: Observable<any>;

  constructor(private http: HttpClient) {}

  setExtendedFlow(inviteId: string, isSync2Cloud: boolean) {
    return this.http.get(
      '/api/external/SetExtendedFlow?inviteCode=' +
        inviteId +
        '&sync2cloud=' +
        isSync2Cloud
    );
  }

  cleanup() {
    return this.http.get('/api/external/Cleanup');
  }
}
