import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EnterpriseService {
  constructor(private http: HttpClient) {}

  validateInvite(id: string) {
    return this.http.post('/api/enterprise/validateinvite', { Id: id });
  }

  accept(id: string) {
    return this.http.post('/api/enterprise/acceptinvite', { Id: id });
  }
}
