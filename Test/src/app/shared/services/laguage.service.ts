import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class LanguagesService {
  availableLanguages = ['en', 'fr', 'de'];
  language = new BehaviorSubject('en');
  isAutoDetected = true;

  constructor(
    private http: HttpClient,
    private translateService: TranslateService,
    private cookieService: CookieService
  ) {
    this.get();
  }

  private get(): void {
    let prevLanguage = localStorage.getItem('language');

    if (prevLanguage && this.availableLanguages.indexOf(prevLanguage) > -1) {
      this.isAutoDetected = false;
      this.language.next(prevLanguage);
    } else {
      let language = this.translateService!.getBrowserLang()!;
      this.updateInternal(language, false, true);
    }
  }

  updateFromQueryParams(language: string) {
    if (!this.isAutoDetected) return;

    this.isAutoDetected = false;

    this.update(language);
  }

  update(language: string) {
    this.isAutoDetected = false;

    return this.updateInternal(language, true, false) as Observable<any>;
  }

  set(language: string) {
    this.isAutoDetected = false;

    this.updateInternal(language, false, false);
  }

  private updateInternal(lang: string, callBackEnd?: boolean, force?: boolean) {
    if (this.availableLanguages.indexOf(lang) < 0) return false;

    if (this.language.value == lang && !force) return false;

    this.language.next(lang);
    localStorage.setItem('language', lang);
    this.cookieService.set('language', lang, 0, '/');

    if (!callBackEnd) return false;

    return this.http.post('/api/user/updatelanguage', { Language: lang });
  }
}
