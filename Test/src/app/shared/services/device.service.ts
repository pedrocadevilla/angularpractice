import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';
declare var window: any;
@Injectable()
export class DeviceService {
  wizardEnabled = new BehaviorSubject(false);
  isShareApp = new BehaviorSubject(false);

  constructor(
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService
  ) {
    // get url params
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['sharing'] || localStorage.getItem('sharing')) {
        this.isShareApp.next(true);
        localStorage.setItem('sharing', 'true');

        // set cookie if it is sharing app for back-end
        this.cookieService.set('sharing', 'true');
      }
    });
  }

  isPurchaseLogicAvailabe(): boolean {
    const appSource = localStorage.getItem('source');

    if (!appSource || appSource == 'browser') {
      return true;
    }

    const appVersionRaw = localStorage.getItem('app_version');

    if (!appVersionRaw) {
      return false;
    }

    const appVersion = Number(appVersionRaw) || 0;

    return appSource == 'ios' && appVersion > 1;
  }

  isMobileApp(): boolean {
    const source = localStorage.getItem('source');

    if (source) {
      return true;
    }

    return false;
  }

  mobileOS(): string | null {
    const source = localStorage.getItem('source');

    if (source) {
      return source;
    }

    return null;
  }

  mobileBrowserOS(): string | null {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    if (/android/i.test(userAgent)) {
      return 'android';
    }

    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      return 'ios';
    }

    return null;
  }

  iPhoneX() {
    const result = {
      iPhoneX: false,
      notchPos: '',
    };

    // Really basic check for the ios platform
    const iOS =
      /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

    // Get the device pixel ratio
    const ratio = window.devicePixelRatio || 1;

    // Define the users device screen dimensions
    const screen = {
      width: window.screen.width * ratio,
      height: window.screen.height * ratio,
    };

    // iPhone X Detection
    if (
      iOS &&
      ((screen.width === 1125 && screen.height === 2436) ||
        (screen.height === 1125 && screen.width === 2436))
    ) {
      result.iPhoneX = true;

      // Notch position checker
      if ('orientation' in window) {
        // Mobile
        if (window.orientation === 90) {
          result.notchPos = 'left';
        } else if (window.orientation === -90) {
          result.notchPos = 'right';
        }
      } else if ('orientation' in window.screen) {
        // Webkit
        if (window.screen.orientation.type === 'landscape-primary') {
          result.notchPos = 'left';
        } else if (window.screen.orientation.type === 'landscape-secondary') {
          result.notchPos = 'right';
        }
      } else if ('mozOrientation' in window.screen) {
        // Firefox
        if (window.screen.mozOrientation === 'landscape-primary') {
          result.notchPos = 'left';
        } else if (window.screen.mozOrientation === 'landscape-secondary') {
          result.notchPos = 'right';
        }
      }
    }

    return result;
  }

  closeWizard() {
    localStorage.setItem('wizard', 'closed');
    this.wizardEnabled.next(false);
  }

  openWizard() {
    // open wizard if it was not closed
    if (!localStorage.getItem('wizard')) {
      this.wizardEnabled.next(true);
    }
  }
}
