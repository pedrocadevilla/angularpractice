import { Inject, Injectable, NgZone } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
@Injectable()
export class SignalrWindow extends Window {
  srw: any;
}

@Injectable()
export class ChannelBaseService {
  public userForceLogout = new BehaviorSubject(null);
  public userUpdated = new BehaviorSubject(null);
  public userSubscriptionUpdated = new BehaviorSubject(null);
  public userFeaturesUpdated = new BehaviorSubject(null);

  // These are used to feed the public observables
  public startingSubject = new Subject<any>();
  public errorSubject = new Subject<any>();

  // These are used to track the internal SignalR state
  protected hubConnection: any;
  protected hubProxy: any;

  constructor(
    @Inject(SignalrWindow) public window: SignalrWindow,
    public ngZone: NgZone
  ) {
    // Set up our observables
    this.hubConnection = this.window.srw.hubConnection();

    // logging enabled?
    // this.hubConnection.logging = true;

    this.hubConnection.url = '/signalr/hubs';
    this.hubProxy = this.hubConnection.createHubProxy('notificationHub');

    this.hubProxy.on('UserForceLogout', (data: any) => {
      this.ngZone.run(() => {
        this.userForceLogout.next(data);
      });
    });

    this.hubProxy.on('UserUpdated', (data: any) => {
      this.ngZone.run(() => {
        this.userUpdated.next(data);
      });
    });

    this.hubProxy.on('UserSubscriptionUpdated', (data: any) => {
      this.ngZone.run(() => {
        this.userSubscriptionUpdated.next(data);
      });
    });

    this.hubProxy.on('UserFeaturesUpdated', (data: any) => {
      this.ngZone.run(() => {
        this.userFeaturesUpdated.next(data);
      });
    });
  }

  startInternal(): void {
    // set access token for connection
    this.hubConnection.qs = {
      access_token: localStorage.getItem('access_token'),
    };

    this.hubConnection
      .start()
      .done(() => {
        this.startingSubject.next(true);
      })
      .fail((err: any) => {
        this.startingSubject.error(err);
      });
  }

  stop(): void {
    this.hubConnection.stop();
  }
}
