import { Injectable } from '@angular/core';
import { Subject, Observable, combineLatest, BehaviorSubject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { UserBaseService } from './user-base.service';
import {
  UserFeatures,
  FeaturesEnum,
  SyncAccountsFeature,
} from '../models/features-model';

@Injectable()
export class FeaturesService {
  private ngUnsubscribe: Subject<any> = new Subject();

  private user = new BehaviorSubject<any>(null);
  private userFeatures: BehaviorSubject<UserFeatures>;

  private isLoadInProgress: boolean = false;

  constructor(
    protected http: HttpClient,
    private userService: UserBaseService
  ) {
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        if (!resp || resp == null) return;

        this.user.next(resp);

        this.initUserFeatures();
      });
  }

  private initUserFeatures(): void {
    let userFeaturesStr = localStorage.getItem('user_features');
    if (!userFeaturesStr || userFeaturesStr.length < 0) {
      this.loadFeaturesInfo();
    } else {
      let userFeaturesOld = JSON.parse(userFeaturesStr) as UserFeatures;
      this.http
        .get<string>('api/user/features/lastUpdateTimestamp')
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: string) => {
          let lastUpdateTimestamp = new Date(Date.parse(resp));
          let lastFeaturesReadTimestamp = new Date(
            Date.parse(userFeaturesOld.LastUpdatedAtUtc)
          );
          if (lastUpdateTimestamp > lastFeaturesReadTimestamp) {
            this.loadFeaturesInfo();
          } else {
            this.userFeatures.next(userFeaturesOld);
          }
        });
    }
  }

  private validateFeatures(
    featureEnum: FeaturesEnum,
    userFeatures: UserFeatures
  ): [isValid: boolean, feature: any] {
    switch (featureEnum) {
      case FeaturesEnum.EnterpriseBulkSources: {
        if (
          userFeatures.BulkSources !== undefined &&
          userFeatures.BulkSources !== null
        ) {
          return [true, userFeatures.BulkSources];
        }

        break;
      }
      case FeaturesEnum.EnterpriseSyncSetup: {
        if (
          userFeatures.SyncSetups !== undefined &&
          userFeatures.SyncSetups !== null
        ) {
          return [true, userFeatures.SyncSetups];
        }

        break;
      }
      case FeaturesEnum.TeamsAdmin: {
        if (
          userFeatures.TeamsManagement !== undefined &&
          userFeatures.TeamsManagement !== null
        ) {
          return [true, userFeatures.TeamsManagement];
        }

        break;
      }
      case FeaturesEnum.SyncAccounts: {
        if (
          userFeatures.SyncAccounts !== undefined &&
          userFeatures.SyncAccounts !== null
        ) {
          return [true, userFeatures.SyncAccounts];
        }

        break;
      }
      case FeaturesEnum.EnterpriseManageOtherUser: {
        if (
          userFeatures.ManageOtherUser !== undefined &&
          userFeatures.ManageOtherUser !== null
        ) {
          return [true, userFeatures.ManageOtherUser];
        }

        break;
      }
      default: {
        return [true, null];
      }
    }

    setTimeout(() => this.loadFeaturesInfo(true), 300);

    return [false, undefined];
  }

  public isFeatureAvailable(featureEnum: FeaturesEnum): Observable<boolean> {
    return combineLatest(this.user, this.userFeatures).pipe(
      map(([user, userFeatures]) => {
        if (!user || user == null || !userFeatures || userFeatures == null)
          return undefined;

        const result = this.validateFeatures(featureEnum, userFeatures);
        if (!result[0]) {
          return false;
        }

        return result[1] == null ? user.isAlpha : result[1].IsAvailable;
      })
    );
  }

  public getFeature<T>(
    featureEnum: FeaturesEnum
  ): Observable<SyncAccountsFeature | null> {
    return combineLatest(this.user, this.userFeatures).pipe(
      map(([user, userFeatures]) => {
        if (!user || user == null || !userFeatures || userFeatures == null)
          return null;

        if (!user || user == null || !userFeatures || userFeatures == null)
          return null;

        const result = this.validateFeatures(featureEnum, userFeatures);
        if (!result[0]) {
          return null;
        }

        return result[1] as SyncAccountsFeature;
      })
    );
  }

  public loadFeaturesInfo(forceReload?: boolean): void {
    if (this.isLoadInProgress) {
      return;
    }

    this.isLoadInProgress = true;

    let url = 'api/user/features';
    if (forceReload) {
      url += '?withForceReload=true';
    }

    this.http
      .get<UserFeatures>(url)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (resp: UserFeatures) => {
          resp.LastUpdatedAtUtc = new Date().toUTCString();
          localStorage.setItem('user_features', JSON.stringify(resp));
          this.userFeatures.next(resp);
          this.isLoadInProgress = false;
        },
        (_) => (this.isLoadInProgress = false)
      );
  }
}
