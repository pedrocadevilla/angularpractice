import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { LcWindow } from './window.service';

@Injectable()
export class HelpService {
  constructor(@Inject(LcWindow) private window: LcWindow) {}

  public helpOpen = new Subject<boolean>();

  close() {
    this.helpOpen.next(false);
  }

  open() {
    this.helpOpen.next(true);
  }

  checkSupportOnlineStatus() {
    return this.window.LiveChatWidget.get('state').availability === 'online';
  }
}
