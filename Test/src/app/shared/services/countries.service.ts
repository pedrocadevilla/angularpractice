import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { tap, publishReplay, refCount } from 'rxjs/operators';
import { Country } from '../models/countries-model';

@Injectable()
export class CountriesService {
  public countries: Country[];

  constructor(private http: HttpClient) {}

  get() {
    if (this.countries != null) {
      return of(this.countries);
    } else {
      return this.http.get('/api/countries').pipe(
        tap((countries) => (this.countries = countries as Country[])),
        publishReplay(1),
        refCount()
      );
    }
  }
}
