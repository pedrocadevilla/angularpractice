import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, EMPTY } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { SourceResponse } from '../models/source-response-model';
import {
  AcceptSyncAccountInviteModel,
  AddModel,
  ChangeAccountPassword,
  MakeReplicasValidModel,
  RetryAddAccountModel,
  VerifyTokenModel,
} from '../models/account-model';
import { SyncAccountStatusChangeModel } from '../models/sync-account-model';

@Injectable()
export class AccountsService {
  private observable: Observable<any>;
  activeAccounts = new BehaviorSubject(0);
  lastNavigationEndEvent = new BehaviorSubject(null);

  constructor(private http: HttpClient) {}

  updateLastNavigationEndEvent(navigationEvent: any) {
    this.lastNavigationEndEvent.next(navigationEvent);
  }

  get() {
    if (this.observable) {
      return this.observable;
    } else {
      let count = 0;

      this.observable = this.http.get('/api/accounts').pipe(
        map((resp: any) => {
          this.observable = EMPTY;

          // count active accounts
          for (const i in resp.SyncAccounts) {
            if (resp.SyncAccounts[i].IsSelected === true) {
              count++;
            }
          }

          this.activeAccounts.next(count);
          return resp;
        }),
        share()
      );

      return this.observable;
    }
  }

  remove(id: string) {
    return this.http.post('/api/accounts/remove', { id: id });
  }

  add(model: AddModel) {
    return this.http.post('/api/accounts/add', model);
  }

  bulkAdd(model: AddModel) {
    return this.http.post('/api/accounts/addbulk', model);
  }

  addGoogleDrive(account: AddModel, autoStartSync: boolean) {
    return this.http.post('/api/accounts/addgoogledrive', {
      UserName: account,
      Provider: 'Google',
      AutoStartManualSyncOnComplete: autoStartSync,
    });
  }

  toggle(account: SyncAccountStatusChangeModel) {
    return this.http.post('/api/accounts/updatestatus', {
      AccountId: account.AccountId,
      IsSelected: account.IsSelected,
    });
  }

  retryAddAccount(model: RetryAddAccountModel) {
    return this.http.post('/api/accounts/retryadd', model);
  }

  getSyncAccountReconnectUrl(model: RetryAddAccountModel) {
    return this.http.post('/api/accounts/getSyncAccountReconnectUrl', model);
  }

  changePass(model: ChangeAccountPassword) {
    return this.http.post('/api/accounts/changepassword', model);
  }

  verifyResetPasswordToken(model: VerifyTokenModel) {
    return this.http.post('/api/accounts/VerifyResetPasswordToken', model);
  }

  setActiveAccount(count: number) {
    this.activeAccounts.next(count);
  }

  validateSources(url: string): Observable<SourceResponse> {
    return this.http.post<SourceResponse>('/api/dynamic/ValidateSources', {
      Url: url,
    });
  }

  getAccounts() {
    return this.http.get('/api/dynamic/getaccounts');
  }

  resetReplicas(model: MakeReplicasValidModel) {
    return this.http.post('/api/dynamic/makeReplicasValid', model);
  }

  toggleOneWaySource(id: string) {
    return this.http.post('/api/accounts/toggleonewaysource', { id: id });
  }

  getPermissions() {
    return this.http.get('/api/accounts/getpermissions');
  }

  setPermissions(model: any) {
    return this.http.post('/api/accounts/setpermissions', model);
  }

  getSyncCompanies() {
    return this.http.get('/api/Synchronization/GetSyncCompanies');
  }

  getSyncAccountInvite(id: string) {
    return this.http.get('/api/Synchronization/GetSyncAccountInvite/' + id);
  }

  acceptSyncAccountInvite(model: AcceptSyncAccountInviteModel) {
    return this.http.post(
      '/api/Synchronization/AcceptSyncAccountInvite',
      model
    );
  }

  completeAcceptSyncAccountInvite(id: string) {
    return this.http.get('/api/Synchronization/CompleteFilters/' + id);
  }
}
