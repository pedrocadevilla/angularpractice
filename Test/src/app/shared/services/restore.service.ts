import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { SyncService } from './sync.service';
import { BehaviorSubject } from 'rxjs';
import { progressBar } from '../models/progress-bar-model';
import { BackupFilterModel } from '../models/restore-model';
import { Injectable } from '@angular/core';
@Injectable()
export class RestoreService {
  restoreProgress: BehaviorSubject<null>;

  constructor(
    private http: HttpClient,
    private translate: TranslateService,
    private syncService: SyncService
  ) {}

  setRestoreProgress(data: any) {
    this.restoreProgress.next(data);
  }

  get() {
    return this.http.get('/api/syncaccountswithbackup');
  }

  getBackupFolders(id: number) {
    return this.http.post('/api/filters/backup', { syncAccountId: id });
  }

  startRestore(model: BackupFilterModel) {
    this.http.post('/api/restore/start', model).subscribe((resp: any) => {
      const progBar: progressBar = {
        title: this.translate.instant('PROGRESS_TITLE_RESTORE_REQUESTSENT'),
        status: this.translate.instant('PROGRESS_STATUS_RESTORE_REQUESTSENT'),
        progress: 1,
        actionType: 'Restore',
        accounts: [],
        expanded: true,
        lastActionTime: new Date(),
      };

      // set progress bar data
      this.syncService.setProgress(progBar);
    });
  }
}
