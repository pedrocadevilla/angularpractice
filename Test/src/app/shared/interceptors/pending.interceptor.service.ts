import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { map, catchError, finalize } from 'rxjs/operators';
@Injectable()
export class PendingInterceptorService implements HttpInterceptor {
  private _pendingRequests = 0;
  private _pendingRequestsStatus: Subject<boolean> = new Subject<boolean>();

  private _pendingTimerRequests = 0;
  private _pendingTimerRequestsStatus: Subject<boolean> =
    new Subject<boolean>();

  private exceptionList = [
    'GetUserActiveWebMessages',
    'message/close',
    'progress/subscription',
    'assignDeviceToUser',
    'AutoSyncStatus',
    'getcurrentsubscriptioninfo',
  ];
  private timerList = ['/api/purchase', '/api/processPayment/'];

  get pendingRequestsStatus(): Observable<boolean> {
    return this._pendingRequestsStatus.asObservable();
  }

  get pendingTimerRequestsStatus(): Observable<boolean> {
    return this._pendingTimerRequestsStatus.asObservable();
  }

  get pendingRequests(): number {
    return this._pendingRequests;
  }

  pendingRequestsStatusSet() {
    this._pendingRequests++;
    this._pendingRequestsStatus.next(true);
  }

  pendingRequestsStatusRemove() {
    this._pendingRequests--;
    this._pendingRequestsStatus.next(false);
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const user = JSON.parse(localStorage.getItem('user')!);

    // do not show loader for exception list
    let show = true;
    let isTimerRequest = false;

    for (const i in this.exceptionList) {
      if (req.url.indexOf(this.exceptionList[i]) > -1) {
        show = false;
        break;
      }
    }

    for (const i in this.timerList) {
      if (
        req.url === this.timerList[i] ||
        req.url.includes(this.timerList[1])
      ) {
        const isPaypal = req.body?.PaymentMethod === 'paypal';
        isTimerRequest = !isPaypal;
        break;
      }
    }

    if (show) {
      this._pendingRequests++;
    }

    if (this._pendingRequests >= 1) {
      this._pendingRequestsStatus.next(true);
    }

    if (isTimerRequest) {
      this._pendingTimerRequests++;
    }

    if (this._pendingTimerRequests >= 1) {
      this._pendingTimerRequestsStatus.next(true);
    }

    const token = localStorage.getItem('access_token');

    // add Authorization & no cache headers
    if (token) {
      req = req.clone({
        headers: req.headers
          .set('Authorization', 'Bearer ' + token)
          .set('Cache-Control', 'no-cache')
          .set('Pragma', 'no-cache')
          .set('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT'),
      });
    }

    return next.handle(req).pipe(
      map((event) => {
        return event;
      }),
      catchError((err) => {
        return throwError(err);
      }),
      finalize(() => {
        if (show) {
          this._pendingRequests--;
        }

        if (this._pendingRequests === 0) {
          this._pendingRequestsStatus.next(false);
        }

        if (isTimerRequest) {
          this._pendingTimerRequests--;
        }

        if (this._pendingTimerRequests === 0) {
          this._pendingTimerRequestsStatus.next(false);
        }
      })
    );
  }
}

export function PendingInterceptorServiceFactory() {
  return new PendingInterceptorService();
}

export let PendingInterceptorServiceFactoryProvider = {
  provide: PendingInterceptorService,
  useFactory: PendingInterceptorServiceFactory,
};
