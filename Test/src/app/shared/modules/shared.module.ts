import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';
import { FooterComponent } from '../components/footer/footer.component';

import { PromotionComponent } from '../components/promotion/promotion.component';
import { ShowErrorsExternalComponent } from '../components/show-errors -external/show-errors.component';
import { TrustedLogosSharedComponent } from '../components/trusted-logos-shared/trusted-logos-shared.component';

@NgModule({
  imports: [TranslateModule.forRoot(), CommonModule],
  declarations: [
    FooterComponent,
    TrustedLogosSharedComponent,
    PromotionComponent,
    ShowErrorsExternalComponent,
  ],
  exports: [
    FooterComponent,
    TrustedLogosSharedComponent,
    TranslateModule,
    CommonModule,
    ShowErrorsExternalComponent,
    PromotionComponent,
  ],
})
export class SharedModule {}
