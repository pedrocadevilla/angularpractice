import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';
import { InnerFooterComponent } from '../components/inner-footer/inner-footer.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from '../components/header/header.component';
import { LeftMenuComponent } from '../components/left-menu/left-menu.component';
import { HelpComponent } from '../components/help/help.component';
import { FormsModule } from '@angular/forms';
import { AlertsComponent } from '../components/alerts/alerts.component';
import { StatusDatePipe } from '../utils/status-date-pipe';
import { SafeHtmlPipe } from '../utils/safe-html-pipe';
import { BreadcrumbComponent } from '../components/breadcrumb/breadcrumb.component';
import { ToastComponent } from '../components/toast/toast.component';
import { MobileWizardComponent } from '../components/mobile-wizard/mobile-wizard.component';
import { ShowErrorsComponent } from '../components/show-errors/show-errors.component';
import { NotificationsComponent } from '../components/notifications/notifications.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NavBarComponent } from '../components/nav-bar/nav-bar.component';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { EnterpriseAcceptComponent } from '../components/enterprise-accept/enterprise-accept.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ModalComponent } from '../components/modal/modal.component';
import { TodayDatePipe } from '../utils/today-pipe';
import { CurrencyComponent } from '../components/currency/currency.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { TrustedLogosComponent } from '../components/trusted-logos/trusted-logos.component';

@NgModule({
  imports: [
    TranslateModule.forRoot(),
    CommonModule,
    RouterModule,
    FormsModule,
    InfiniteScrollModule,
    NgbProgressbarModule,
    FontAwesomeModule,
    SlickCarouselModule,
  ],
  declarations: [
    InnerFooterComponent,
    HeaderComponent,
    LeftMenuComponent,
    BreadcrumbComponent,
    AlertsComponent,
    StatusDatePipe,
    TodayDatePipe,
    SafeHtmlPipe,
    ToastComponent,
    HelpComponent,
    MobileWizardComponent,
    ShowErrorsComponent,
    NotificationsComponent,
    NavBarComponent,
    EnterpriseAcceptComponent,
    ModalComponent,
    CurrencyComponent,
    TrustedLogosComponent
  ],
  exports: [
    TranslateModule,
    CommonModule,
    InnerFooterComponent,
    HeaderComponent,
    LeftMenuComponent,
    BreadcrumbComponent,
    AlertsComponent,
    StatusDatePipe,
    SafeHtmlPipe,
    ToastComponent,
    HelpComponent,
    MobileWizardComponent,
    ShowErrorsComponent,
    NotificationsComponent,
    NavBarComponent,
    EnterpriseAcceptComponent,
    CurrencyComponent,
    TodayDatePipe,
    TrustedLogosComponent
  ],
})
export class InnerSharedModule {}
