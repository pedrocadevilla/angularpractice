import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { Modal } from '../../models/modal-model';
import { DeviceService } from '../../services/device.service';
import { NotificationsService } from '../../services/notification.service';
import { ModalComponent } from '../modal/modal.component';
import * as $ from 'jquery';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./notifications.component.scss'],
  animations: [
    trigger('toggleAnimation', [
      state('in', style({ height: '*' })),
      transition('void => *', [
        style({ height: 0 }),
        animate(
          '0.3s ease',
          style({
            height: '*',
          })
        ),
      ]),
      transition('* => void', [
        style({ height: '*' }),
        animate(
          '0.3s ease',
          style({
            height: 0,
          })
        ),
      ]),
    ]),
  ],
  host: {
    '(window:resize)': 'onResize($event)',
  },
})
export class NotificationsComponent implements OnInit, OnDestroy {
  @ViewChild('scrollTop')
  private scrollContainer: ElementRef;
  private ngUnsubscribe: Subject<any> = new Subject();
  notifications: any;
  selected: number = 0;
  mobile: boolean;
  lastOneUp = false;
  lastOneDown = false;
  loaded = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private dialog: NgbModal,
    private translate: TranslateService,
    private notificationsService: NotificationsService,
    private deviceService: DeviceService
  ) {}

  ngOnInit() {
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
      // load notifications
      this.loadNotifications(params['notificationId'], true);
    });

    // mobile app?
    this.mobile = this.deviceService.isMobileApp();
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  onResize(event: any) {
    if (this.selected === null && event.target.innerWidth > 767) {
      // set first notification as selected to prevent empty preview on bigger screens
      this.selected = 0;

      // scroll left panel to top
      const left = document.getElementsByClassName('panel-left');

      left[0].scrollTop = 0;
    }
  }

  onScrollUp(mobile?: boolean) {
    if ((mobile && window.screen.width < 992) || !mobile) {
      this.loadNotifications(0, false);
    }
  }

  onScrollDown(mobile?: boolean) {
    if ((mobile && window.screen.width < 992) || !mobile) {
      this.loadNotifications(0, true);
    }
  }

  loadNotifications(id: number, direction: boolean): void {
    // direction: true - down, direction: false - up

    if ((direction && this.lastOneDown) || (!direction && this.lastOneUp)) {
      return;
    }

    let url = 'GetNextNotifications';
    const firstId =
      this.notifications && this.notifications.length > 0
        ? this.notifications[0].Id
        : null;

    if (direction) {
      if (id !== 0) {
        id =
          this.notifications && this.notifications.length > 0
            ? this.notifications[this.notifications.length - 1].Id
            : null;
      }
    } else {
      url = 'GetPreviousNotifications';
      id = this.notifications[0].Id;
    }

    this.http
      .post('/api/message/' + url, {
        NotificationId: id ? id : null,
        DesiredNumberOfResults: 20,
      })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (resp: any) => {
          // first load?
          if (!this.notifications) {
            this.notifications = resp;
          } else {
            if (direction) {
              for (const i of Object.keys(resp)) {
                if (id !== resp[i].Id && firstId !== resp[i].Id) {
                  this.notifications.push(resp[i]);
                }
              }
            } else {
              for (const i of Object.keys(resp)) {
                this.notifications.unshift(resp[i]);
              }
            }
          }

          // last one?
          if (resp && resp.length < 2) {
            if (direction) {
              this.lastOneDown = true;
            } else {
              this.lastOneUp = true;
            }
          }

          this.loaded = true;
        },
        (err) => {
          this.loaded = true;
        }
      );
  }

  scrollTo(index: number) {
    setTimeout(() => {
      const el = document.getElementById('notification' + index);

      $('html, body').animate(
        {
          scrollTop: el!.offsetTop,
        },
        300
      );
    }, 500);
  }

  select(index: number, mobile?: boolean) {
    if (mobile && this.selected === index) {
      this.selected = 0;
    } else {
      const notification = this.notifications[index];

      if (!notification.Contents) {
        this.notificationsService
          .getMessageContent(notification.Id)
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe((resp: any) => {
            this.notifications[index].Contents = resp;
            this.selected = index;

            if (mobile) {
              this.scrollTo(index);
            } else {
              // scroll content to top
              this.scrollContainer.nativeElement.scrollTop = 0;
            }
          });
      } else {
        this.selected = index;

        if (mobile) {
          this.scrollTo(index);
        } else {
          // scroll content to top
          this.scrollContainer.nativeElement.scrollTop = 0;
        }
      }
    }
  }

  delete() {
    // confirmation modal
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });

    let modalData: Modal = {
      title: 'NOTIFICATIONS_REMOVE_TITLE',
      text: 'NOTIFICATIONS_REMOVE_TEXT',
    };

    modal.componentInstance.modalData = modalData;

    modal.result.then((data) => {
      if (data === 'primary') {
        this.notificationsService
          .deleteMessage(this.notifications[this.selected].Id)
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(() => {
            // delete from front-end array
            this.notifications.splice(this.selected, 1);

            if (
              this.notifications.length !== 0 &&
              this.notifications.length <= 10
            ) {
              this.loadNotifications(0, true);
            }

            // select next notification
            if (this.selected + 1 <= this.notifications.length - 1) {
              this.select(this.selected);
            } else if (this.notifications.length > 0) {
              this.select(this.notifications.length - 1);
            }
          });
      }
    });
  }

  deleteAll() {
    // confirmation modal
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });

    let modalData: Modal = {
      title: 'NOTIFICATIONS_ALL_REMOVE_TITLE',
      text: 'NOTIFICATIONS_REMOVE_ALL_TEXT',
    };

    modal.componentInstance.modalData = modalData;

    modal.result.then((data) => {
      if (data === 'primary') {
        this.notificationsService
          .deleteAllMessages()
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(() => {
            this.notifications = [];
          });
      }
    });
  }
}
