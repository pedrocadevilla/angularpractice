import { Component, Input, OnInit } from '@angular/core';
import { ExternalLoginServices } from '../../services/external-login.service';

@Component({
  selector: 'app-external-login',
  templateUrl: './external-login.component.html',
  styleUrls: ['./external-login.component.scss'],
})
export class ExternalLoginComponent implements OnInit {
  @Input() returnUrl: string;
  @Input() loginHint: string;
  currentRoute: string =
    window.location.href.split('/')[window.location.href.split('/').length - 1];
  constructor(private loginsService: ExternalLoginServices) {}

  ngOnInit(): void {}

  externalLogin(type: string) {
    if (type === 'Apple') {
      this.loginsService
        .getAppleLoginUrl(this.returnUrl, this.loginHint)
        .subscribe((resp: string) => {
          window.location.href = resp;
        });

      return;
    }

    let url = '';
    let loginHintUrl = '';

    if (this.returnUrl && this.returnUrl !== undefined) {
      url = '&returnUrl=' + this.returnUrl;
    }

    if (this.loginHint && this.loginHint !== undefined) {
      loginHintUrl = '&loginHint=' + this.loginHint;
    }

    window.location.href =
      '/api/ExternalLogin?provider=' + type + url + loginHintUrl;
  }
}
