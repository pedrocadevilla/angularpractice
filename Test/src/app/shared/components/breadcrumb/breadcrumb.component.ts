import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { filter, map } from 'rxjs';
import { Breadcrumb } from '../../models/breadcrumb-model';
import { AccountsService } from '../../services/account.service';
import { HelpService } from '../../services/help.service';
import { WindowService } from '../../services/window.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent implements OnInit {
  isHideHelp: boolean;
  url: string;
  device: string;
  title: string;
  breadcrumb: Breadcrumb[];
  hideGreetingUrlList = [
    '/purchase',
    '/purchase/companyinformation',
    '/purchase/billinginformation',
    '/purchase/revieworder',
  ];

  constructor(
    private helpService: HelpService,
    private router: Router,
    private accountService: AccountsService,
    private windowService: WindowService,
    private translate: TranslateService,
    private titleService: Title
  ) {
    router.events
      .pipe(
        filter((event) => {
          if (event instanceof NavigationEnd) {
            accountService.updateLastNavigationEndEvent(event);
            this.url = event.urlAfterRedirects;
          }

          if (this.hideGreetingUrlList.some((url) => url == this.url)) {
            this.windowService.hideLiveChat();
          }

          return event instanceof NavigationEnd;
        }),
        map((route: any) => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          this.device = route.snapshot.queryParams['source'];

          this.isHideHelp =
            this.url.indexOf('purchase') >= 0 && this.device === 'ios';

          return route;
        })
      )
      .subscribe((event) => {
        this.title = event['title'];

        if (this.title && this.url !== '/' && this.url !== '/signup') {
          translate.get(this.title).subscribe((res: string) => {
            titleService.setTitle(res + ' - SyncGene');
          });
        } else {
          titleService.setTitle('SyncGene');
        }

        this.breadcrumb = event['breadcrumb'];
      });
  }

  ngOnInit(): void {}

  openHelp() {
    this.helpService.open();
  }
}
