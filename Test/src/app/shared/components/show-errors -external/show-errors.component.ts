import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { errorMessageObject } from '../../models/error-message-model';

@Component({
  selector: 'app-show-errors',
  templateUrl: './show-errors.component.html',
  styleUrls: ['./show-errors.component.scss'],
})
export class ShowErrorsExternalComponent implements OnInit {
  @Input() control: AbstractControl = new FormControl();
  @Input() errorMessage: errorMessageObject[] = [];

  constructor() {}

  ngOnInit(): void {}

  shouldShowErrors(): boolean {
    if (this.control && this.control.errors && this.control.touched !== null)
      return this.control && this.control.errors && this.control.touched;
    else return false;
  }

  listOfErrors(): string[] {
    if (this.control?.errors !== null) {
      return Object.keys(this.control?.errors).map(
        (err: string) => this.errorMessage.find((x) => x.key === err)!.message
      );
    } else return [];
  }
}
