import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowErrorsExternalComponent } from './show-errors.component';

describe('ShowErrorsComponent', () => {
  let component: ShowErrorsExternalComponent;
  let fixture: ComponentFixture<ShowErrorsExternalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShowErrorsExternalComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowErrorsExternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
