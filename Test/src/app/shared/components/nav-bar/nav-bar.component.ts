import {
  Component,
  ChangeDetectorRef,
  OnInit,
  OnDestroy,
  Input,
} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserBaseService } from '../../services/user-base.service';
import { SyncService } from '../../services/sync.service';
import { DeviceService } from '../../services/device.service';
import { AccountsService } from '../../services/account.service';
import { AccountProgressBar } from '../../models/progress-bar-model';
import { FeaturesService } from '../../services/features.service';
import { FeaturesEnum } from '../../models/features-model';
import { Modal } from '../../models/modal-model';
import { ModalComponent } from '../modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-navbar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
  animations: [
    trigger('progressBarAnimation', [
      state('in', style({ height: '*' })),
      transition('void => *', [
        style({ height: 0 }),
        animate(
          '0.3s ease',
          style({
            height: '*',
          })
        ),
      ]),
      transition('* => void', [
        style({ height: '*' }),
        animate(
          '0.3s ease',
          style({
            height: 0,
          })
        ),
      ]),
    ]),
    trigger('expandAnimation', [
      state('in', style({ 'margin-top': '*' })),
      transition('void => *', [
        style({ 'margin-top': '-200px' }),
        animate(
          '0.3s ease',
          style({
            'margin-top': '*',
          })
        ),
      ]),
      transition('* => void', [
        style({ 'margin-top': '*' }),
        animate(
          '0.3s ease',
          style({
            'margin-top': '-200px',
          })
        ),
      ]),
    ]),
    trigger('overflowAnimation', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(
          '0.3s ease',
          style({
            opacity: 0.4,
          })
        ),
      ]),
      transition('* => void', [
        style({ opacity: 0.4 }),
        animate(
          '0.3s ease',
          style({
            opacity: 0,
          })
        ),
      ]),
    ]),
    trigger('expandMenuAnimation', [
      state('in', style({ right: '*' })),
      transition('void => *', [
        style({ right: '-280px' }),
        animate(
          '0.3s ease',
          style({
            right: '*',
          })
        ),
      ]),
      transition('* => void', [
        style({ right: '*' }),
        animate(
          '0.3s ease',
          style({
            right: '-280px',
          })
        ),
      ]),
    ]),
    trigger('backButtonAnimation', [
      state('in', style({ width: '*' })),
      transition('void => *', [
        style({ width: 0 }),
        animate(
          '.15s ease-in-out',
          style({
            width: '*',
          })
        ),
      ]),
      transition('* => void', [
        style({ width: '*' }),
        animate(
          '.15s ease-in-out',
          style({
            width: 0,
          })
        ),
      ]),
    ]),
  ],
})
export class NavBarComponent implements OnInit, OnDestroy {
  @Input() webMessages: string;
  private ngUnsubscribe: Subject<any> = new Subject();
  progressBar: any;
  accountProgressBar: AccountProgressBar = {
    expanded: false,
    show: false,
    text: '',
    showSyncBtn: false,
  };
  currentUser: any;
  showAdminMenu = false;
  isFeaturesModifier = false;
  url: string;
  spl: any;
  showViewResultButton = true;
  showMenu = false;
  showShare = false;
  noBackBtn = false;
  isPurchaseLogicAvailabe: boolean = true;
  sharingApp: boolean;
  subscriptionData: any;
  subscriptionEndDateValid = false;
  hasHistory = false;
  previousUrl: string;
  isTeamsManagementAvailable: boolean = false;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserBaseService,
    private syncService: SyncService,
    private translate: TranslateService,
    private deviceService: DeviceService,
    private accountsService: AccountsService,
    private dialog: NgbModal,
    private features: FeaturesService
  ) {}

  ngOnInit() {
    // subscribe for url
    this.accountsService.lastNavigationEndEvent
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((event: any) => {
        this.previousUrl = this.url;
        this.hasHistory =
          this.previousUrl &&
          this.previousUrl.length > 0 &&
          this.previousUrl !== undefined
            ? true
            : false;
        this.url = event.url.split(/[?#]/)[0];
        this.spl = this.url.split(/[?/]/);

        if (
          this.spl[1].toLowerCase() === 'synchronization' &&
          (this.spl[2].toLowerCase() === 'status' ||
            this.spl[2].toLowerCase() === 'report' ||
            this.spl[2].toLowerCase() === 'restore')
        ) {
          this.showViewResultButton = false;
        } else {
          this.showViewResultButton = true;
        }

        // close account added bar
        this.accountProgressBar.expanded = false;
        this.accountProgressBar.show = false;

        // close mobile menus
        this.closeAll();

        // subscribe to router event for error code
        this.activatedRoute.queryParams.subscribe((params: Params) => {
          if (params['username'] && (params['code'] || params['errorCode'])) {
            this.accountProgressBar.showSyncBtn = false;
            if (
              params['code'] === 'SYNC_ACCOUNT_ADDED' ||
              params['code'] === 'SYNC_ACCOUNT_ADDED_START_SYNCHRONIZATION'
            ) {
              /*
              TODO INTERNAL VALIDATE THIS URLS
              */
              if (
                this.url.indexOf('share/') < 0 &&
                this.url.indexOf('quick-setup/') < 0 &&
                this.url.indexOf('external/validatefolder') < 0
              ) {
                // text account added bar
                this.translate
                  .get(params['code'], { var0: params['username'] })
                  .subscribe((res: string) => {
                    this.accountProgressBar.text = res;

                    if (
                      params['code'] ===
                      'SYNC_ACCOUNT_ADDED_START_SYNCHRONIZATION'
                    ) {
                      this.accountProgressBar.showSyncBtn = true;
                    }

                    if (this.url.indexOf('/addsyncaccount') < 0) {
                      this.router.navigate(['/synchronization/management'], {
                        replaceUrl: true,
                      });
                    }
                    setTimeout(() => {
                      this.accountProgressBar.expanded = true;
                      this.accountProgressBar.show = true;
                    }, 1000);
                  });
              }
            } else if (params['errorCode'] || params['code']) {
              if (
                params['code'] === 'SYNC_ACCOUNT_RECONNECTED' ||
                params['code'] === 'SYNC_ACCOUNT_RECONNECTED_1'
              ) {
                const modalTerm = this.dialog.open(ModalComponent, {
                  backdrop: 'static',
                  keyboard: false,
                });
                let modalData: Modal = {
                  title: 'INFO',
                  text: params['code'],
                };

                modalTerm.componentInstance.modalData = modalData;
              } else if (params['code'] === 'SYNC_ACCOUNT_RECONNECTED_2') {
                const modal = this.dialog.open(ModalComponent, {
                  backdrop: 'static',
                  keyboard: false,
                });
                let modalData: Modal = {
                  title: 'ERROR',
                  text: this.translate.instant('SYNC_ACCOUNT_RECONNECTED_2', { var0: params['custom'] }),
                  textTranslated: true,
                };

                modal.componentInstance.modalData = modalData;
              }else if (params['errorCode'] === 'WRONG_ACCOUNT_SELECTED') {
                const modal = this.dialog.open(ModalComponent, {
                  backdrop: 'static',
                  keyboard: false,
                });
                let modalData: Modal = {
                  title: 'ERROR',
                  text: params['errorCode'],
                };

                modal.componentInstance.modalData = modalData;
              } else if (params['errorCode'] == 'DUPLICATE_MAILBOX') {
                let vars = params['errorParams'].split(';');
                const modal = this.dialog.open(ModalComponent, {
                  backdrop: 'static',
                  keyboard: false,
                });
                let modalData: Modal = {
                  title: 'INFO',
                  text: this.translate.instant(params['errorCode'], {
                    var0: vars[0],
                    var1: vars[1],
                    var2: vars[2],
                  }),
                };

                modal.componentInstance.modalData = modalData;
              } else if (
                params['errorCode'] ===
                'CANNOT_ADD_ACCOUNT_UPGRADE_SUBSCRIPTION'
              ) {
                const message = {
                  title: 'ADD_ACCOUNT_LIMIT_TITLE',
                  text: 'ADD_ACCOUNT_LIMIT_TEXT',
                  button: 'BTN_GO_PREMIUM',
                };

                // set text for mobile device
                if (!this.isPurchaseLogicAvailabe) {
                  message.text = 'GO_TO_SYNCGENE_TO_LEARN_MORE';
                  message.button = 'SyncGene';
                }

                const modal = this.dialog.open(ModalComponent, {
                  backdrop: 'static',
                  keyboard: false,
                });
                let modalData: Modal = {
                  title: message.title,
                  text: message.text,
                };

                modal.componentInstance.modalData = modalData;

                modal.result.then(async (data) => {
                  if (data === 'primary') {
                    if (message.button === 'SyncGene') {
                      await window.open(
                        'http://www.syncgene.com?external=true',
                        '_blank'
                      );
                    }
                    await this.router.navigate(['/purchase']);
                  }
                });

                if (params['source1'] !== null && params['source2'] !== null) {
                  this.quickSetupReturnToPreviuosPage(params);
                } else {
                  this.router.navigate([this.url], {
                    replaceUrl: true,
                  });
                }
              } else if (
                params['errorCode'] ===
                'CANNOT_ADD_ACCOUNT_UPGRADE_SUBSCRIPTION_OR_REMOVE_EXISTING'
              ) {
                const message = {
                  title: 'ADD_ACCOUNT_LIMIT_TITLE',
                  text: 'ADD_ACCOUNT_LIMIT_EXTENDED_TEXT',
                  button: 'BTN_GO_PREMIUM',
                };

                // set text for mobile device
                if (!this.isPurchaseLogicAvailabe) {
                  message.text = 'GO_TO_SYNCGENE_TO_LEARN_MORE';
                  message.button = 'SyncGene';
                }

                const modal = this.dialog.open(ModalComponent, {
                  backdrop: 'static',
                  keyboard: false,
                });
                let modalData: Modal = {
                  title: message.title,
                  text: message.text,
                };

                modal.componentInstance.modalData = modalData;

                modal.result.then(async (data) => {
                  if (data === 'primary') {
                    if (message.button === 'SyncGene') {
                      await window.open(
                        'http://www.syncgene.com?external=true',
                        '_blank'
                      );
                    }
                    await this.router.navigate(['/purchase']);
                  }
                });

                //This is hardcoded solution
                //should be used this.url instead of provided path
                //but on page load, this.url is not always correct
                //should be fixed
                this.router.navigate(['/share/accept-on-account'], {
                  replaceUrl: true,
                  queryParams: {
                    inviteid: params['inviteid'],
                    sync2cloud: params['sync2cloud'],
                  },
                });
              } else if (
                params['errorCode'] === 'HTTP_4XX_ERROR_CODE_RECEIVED' ||
                params['errorCode'] === 'HTTP_5XX_ERROR_CODE_RECEIVED'
              ) {
                this.router.navigate([this.url], {
                  replaceUrl: true,
                });
              } else if (
                params['errorCode'] === 'CANNOT_ADD_ACCOUNT_CONTACT_SUPPORT'
              ) {
                const modal = this.dialog.open(ModalComponent, {
                  backdrop: 'static',
                  keyboard: false,
                });
                let modalData: Modal = {
                  title: this.translate.instant(
                    'CANNOT_ADD_ACCOUNT_CONTACT_SUPPORT_TITLE',
                    { var0: params['errorParams'] }
                  ),
                  text:  params['errorCode'],
                  primaryButton: 'CONTACT_US',
                  titleTranslated: true,
                };

                modal.componentInstance.modalData = modalData;

                modal.result.then(async (data) => {
                  if (data === 'primary') {
                    await window.open(
                      'http://www.syncgene.com/support?external=true',
                      '_blank'
                    );
                  }
                });

                this.router.navigate([this.url], {
                  replaceUrl: true,
                });
              } else {
                let redirectUrl: string;

                // sharing pages?
                if (
                  this.url === '/share/accept' ||
                  this.url === '/share/accept-on-account'
                ) {
                  redirectUrl = '/share/accept?inviteid=' + params['inviteid'];
                }

                // quick setup?
                if (this.url.indexOf('quick-setup') > -1) {
                  redirectUrl =
                    this.url +
                    '?source1=' +
                    params['source1'] +
                    '&source2=' +
                    params['source2'];
                }

                // add login?
                if (this.url.indexOf('account/mylogins') > -1) {
                  redirectUrl = 'account/mylogins/addlogin';
                }

                if (
                  params['errorCode'] === 'LOGIN_ACCOUNT_USED_BY_OTHERS' ||
                  params['errorCode'] === 'DUPLICATE_ENTRIES'
                ) {
                  const modal = this.dialog.open(ModalComponent, {
                    backdrop: 'static',
                    keyboard: false,
                  });
                  let modalData: Modal = {
                    title: 'INFO',
                    text: params['errorCode'],
                    primaryButton: 'ADD_DIFFERENT_LOGIN',
                  };

                  modal.componentInstance.modalData = modalData;

                  modal.result.then(
                    (resp: boolean) => {
                      if (resp) {
                        this.userService.logout(
                          true,
                          redirectUrl ? redirectUrl : undefined
                        );
                        return;
                      }
                      if (this.url == '/account/mylogins') {
                        this.router.navigate(['/account/mylogins/addlogin'], {
                          replaceUrl: true,
                        });
                        return;
                      }
                      this.router.navigate(
                        ['/synchronization/addsyncaccount'],
                        {
                          replaceUrl: true,
                          queryParams: {
                            inviteid: params['inviteid']
                              ? params['inviteid']
                              : null,
                            returnUrl: params['returnUrl']
                              ? params['returnUrl']
                              : null,
                          },
                        }
                      );
                    },
                    () => {
                      // do nothing on no
                    }
                  );
                } else if (params['errorCode'] === 'ACCOUNT_USED_BY_OTHERS') {
                  let allParams = params['errorParams'].split(';');

                  const modal = this.dialog.open(ModalComponent, {
                    backdrop: 'static',
                    keyboard: false,
                  });
                  let modalData: Modal = {
                    title: 'INFO',
                    text: this.translate.instant('ACCOUNT_USED_BY_OTHERS', {
                      var2: allParams[2],
                    }),
                    primaryButton: 'SWITCH_TO_ACCOUNT_BUTTON_TEXT',
                    textTranslated: true,
                  };

                  modal.componentInstance.modalData = modalData;

                  modal.result.then(
                    (resp: string) => {
                      if (resp === 'primary') {
                        this.userService.logout(
                          true,
                          redirectUrl ? redirectUrl : undefined
                        );
                      } else {
                        const modal = this.dialog.open(ModalComponent, {
                          backdrop: 'static',
                          keyboard: false,
                        });
                        let modalData: Modal = {
                          title: 'MODAL_REMOVE_ACCOUNT_TITLE',
                          text: this.translate.instant(
                            'REMOVE_ACCOUNT_TEXT_BACKUP_OWS_LAST_OWNER',
                            { var0: allParams[0] }
                          ),
                          primaryTextButton: 'Yes',
                          secondaryTextButton: 'No',
                          textTranslated: true,
                        };

                        modal.componentInstance.modalData = modalData;

                        modal.result.then(
                          (_: any) => {
                            this.syncService
                              .swapSyncAccount()
                              .pipe(takeUntil(this.ngUnsubscribe))
                              .subscribe((_) => {
                                const modal = this.dialog.open(ModalComponent, {
                                  backdrop: 'static',
                                  keyboard: false,
                                });
                                let modalData: Modal = {
                                  title: 'INFO',
                                  text: 'SYNC_ACCOUNT_ADDED_AFTER_SWAP',
                                };

                                modal.componentInstance.modalData = modalData;
                                if (
                                  redirectUrl &&
                                  redirectUrl.indexOf('/accept') > -1
                                ) {
                                  this.router.navigate(['/share/accept'], {
                                    replaceUrl: true,
                                    queryParams: {
                                      inviteid: params['inviteid'],
                                    },
                                  });
                                } else if (
                                  redirectUrl &&
                                  redirectUrl.indexOf('account/mylogins') > -1
                                ) {
                                  this.router.navigate([redirectUrl], {
                                    replaceUrl: true,
                                  });
                                } else if (
                                  redirectUrl &&
                                  redirectUrl.indexOf('/quick-setup') > -1
                                ) {
                                  const newUrl = this.url
                                    .replace('/step3', '/step4')
                                    .replace('/step2', '/step3');
                                  const qParams: any = {};
                                  const paramSource1 = params['source1'];
                                  const paramSource2 = params['source2'];
                                  if (paramSource1) {
                                    qParams.source1 = paramSource1;
                                  }
                                  if (paramSource2) {
                                    qParams.source2 = paramSource2;
                                  }

                                  this.router.navigate([newUrl], {
                                    replaceUrl: true,
                                    queryParams: qParams,
                                  });
                                } else {
                                  this.router.navigate(
                                    ['/synchronization/addsyncaccount'],
                                    {
                                      replaceUrl: true,
                                    }
                                  );
                                }
                              });
                          },
                          () => {
                            // do nothing on no
                          }
                        );
                      }
                    },
                    () => {
                      // do nothing on no
                    }
                  );

                  // clean url params
                  this.router.navigate([this.url], {
                    replaceUrl: true,
                    queryParams: {
                      inviteid: params['inviteid'] ? params['inviteid'] : null,
                      system: params['system'] ? params['system'] : null,
                      source1: params['source1'] ? params['source1'] : null,
                      source2: params['source2'] ? params['source2'] : null,
                    },
                  });
                } else {
                  this.translate
                    .get(
                      params['errorCode']
                        ? params['errorCode']
                        : params['code'],
                      {
                        var0: params['username'],
                        var1:
                          params['type'] ?? params['errorParams'].split(';')[1],
                      }
                    )
                    .subscribe((res: string) => {
                      const modal = this.dialog.open(ModalComponent, {
                        backdrop: 'static',
                        keyboard: false,
                      });
                      let modalData: Modal = {
                        title: 'INFO',
                        text: res,
                        textTranslated: true,
                      };

                      modal.componentInstance.modalData = modalData;

                      if (this.url === '/share/accept-on-account') {
                        this.router.navigate([this.url], {
                          replaceUrl: true,
                          queryParams: {
                            inviteid: params['inviteid']
                              ? params['inviteid']
                              : null,
                            system: params['system'] ? params['system'] : null,
                          },
                        });
                      } else {
                        this.router.navigate([this.url], {
                          replaceUrl: true,
                          queryParams: {
                            source1: params['source1'],
                            source2: params['source2'],
                          },
                        });
                      }
                    });
                }
              }
            }
          } else if (params['errorCode'] === 'CODE_IS_NULL') {
            switch (this.url) {
              case '/share/accept-on-account':
                this.router.navigate(['/share/accept'], {
                  replaceUrl: true,
                  queryParams: {
                    inviteid: params['inviteid'],
                  },
                });
                break;

              case '/synchronization/management':
              case '/share/select-account':
                this.router.navigate([this.url], {
                  replaceUrl: true,
                });
                break;

              case '/quick-setup/step3':
              case '/quick-setup/step4':
                this.quickSetupReturnToPreviuosPage(params);
                break;

              default:
                const modalTerm = this.dialog.open(ModalComponent, {
                  backdrop: 'static',
                  keyboard: false,
                });
                let modalData: Modal = {
                  title: 'ERROR',
                  text: params['errorCode'],
                };

                modalTerm.componentInstance.modalData = modalData;

                modalTerm.result.then((data) => {
                  this.router.navigate([this.url], {
                    replaceUrl: true,
                  });
                });
            }
          } else if (params['errorCode']) {
            const modalTerm = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            let modalData: Modal = {
              title: 'ERROR',
              text: params['errorCode'],
            };

            modalTerm.componentInstance.modalData = modalData;

            modalTerm.result.then((data) => {
              this.router.navigate([this.url], {
                replaceUrl: true,
              });
            });
          }
        });
      });

    // subscribe for user info
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.currentUser = resp;

        if (this.currentUser) {
          // check user role
          this.showAdminMenu =
            this.userService.isInRole('Administrator') ||
            this.userService.isInRole('Support User') ||
            this.userService.isInRole('Sales User');
          this.isFeaturesModifier =
            this.userService.isInRole('FeaturesModifier');

          this.userService.currentSubscription
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((resp) => {
              this.subscriptionData = resp;
            });

          //check if subscription end date is in future
          const today = new Date();

          if (
            this.currentUser.Subscription &&
            new Date(this.currentUser.Subscription.EndDate) > today
          ) {
            this.subscriptionEndDateValid = true;
          } else {
            this.subscriptionEndDateValid = false;
          }
        }

        // mobile device?
        this.isPurchaseLogicAvailabe =
          this.deviceService.isPurchaseLogicAvailabe();

        // update UI
        this.changeDetectorRef.detectChanges();
      });

    // subscribe for progress info
    this.syncService.progress
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.progressBar = resp;
        // hide user added bar
        if (
          this.progressBar &&
          this.progressBar.progress > 0 &&
          this.progressBar.progress < 100
        ) {
          this.accountProgressBar.show = false;
          this.accountProgressBar.expanded = false;
        }

        // update UI
        this.changeDetectorRef.detectChanges();
      });

    // set back button for mobile devices
    if (
      document.referrer &&
      document.referrer.indexOf('localhost') < 0 &&
      document.referrer.indexOf('syncgene') < 0
    ) {
      this.noBackBtn = true;
    }

    // sharing app?
    this.deviceService.isShareApp
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        this.sharingApp = resp;
      });

    this.features
      .isFeatureAvailable(FeaturesEnum.TeamsAdmin)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        this.isTeamsManagementAvailable = resp;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  back() {
    this.router.navigate([this.previousUrl]);
  }

  openAdmin(path: String) {
    // close mobile menus
    this.closeAll();

    window.open(window.location.origin + '/admin/' + path, '_blank');
  }

  logout() {
    this.toggleMenu(false);
    this.toggleShare(false);
    this.userService.logout(true);
  }

  toggleProgressBar(toggle?: boolean) {
    this.progressBar.expanded =
      toggle !== undefined ? toggle : !this.progressBar.expanded;

    // inform all app that progress bar has changed
    this.syncService.setProgress(this.progressBar);
  }

  toggleAccountProgressBar() {
    this.accountProgressBar.expanded = !this.accountProgressBar.expanded;
  }

  cancelAction() {
    // disable cancel button
    this.progressBar.actionType = 'CancelAction';

    const modalTerm = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'CONFIRM_SYNC_CANCEL_TITLE',
      text: 'CONFIRM_SYNC_CANCEL_TEXT',
    };

    modalTerm.componentInstance.modalData = modalData;

    modalTerm.result.then((data) => {
      // on close
      if (data !== 'primary') {
        this.cancelDeny();
        return;
      }
      this.cancelConfirm();
    });
  }

  cancelConfirm() {
    // inform all app that progress bar has changed
    this.syncService.setProgress(this.progressBar);

    // cancel currently running action
    this.syncService.cancelAction();
  }

  cancelDeny() {
    this.progressBar.actionType = 'Sync';
  }

  viewResults() {
    let url = '/synchronization/status';

    if (this.progressBar.actionType === 'Restore') {
      url = '/synchronization/restore';
    }

    this.syncService.resetProgressBar();
    this.router.navigate([url]);
  }

  sync() {
    this.accountProgressBar.expanded = false;
    this.accountProgressBar.show = false;
    this.syncService.checkBackupAccounts();
  }

  addSyncAccount() {
    this.accountProgressBar.expanded = false;
    this.accountProgressBar.show = false;
    this.router.navigate(['/synchronization/addsyncaccount']);
  }

  toggleMenu(toggle?: boolean) {
    // close share menu
    this.showShare = false;
    this.showMenu = toggle !== undefined ? toggle : !this.showMenu;

    if (this.showMenu) {
      if (this.progressBar) {
        this.toggleProgressBar(false);
      }

      this.accountProgressBar.expanded = false;
    }
  }

  toggleShare(toggle?: boolean) {
    // close menu
    this.showMenu = false;
    this.showShare = toggle !== undefined ? toggle : !this.showShare;

    if (this.showShare) {
      if (this.progressBar) {
        this.toggleProgressBar(false);
      }

      this.accountProgressBar.expanded = false;
    }
  }

  closeAll() {
    this.showMenu = false;
    this.showShare = false;

    this.accountProgressBar.expanded = false;

    if (this.progressBar) {
      this.toggleProgressBar(false);
    }
  }

  openHref(url: string) {
    // close mobile menus
    this.closeAll();

    window.open(url, '_blank');
  }

  mobileHelp() {
    let url: string;

    switch (this.url) {
      case '/synchronization/status':
        url = 'Sync_Status.htm';
        break;

      case '/synchronization/management':
        url = 'manage_accounts.htm';
        break;

      case '/synchronization/whattosync':
        url = 'Filters.htm';
        break;

      case '/synchronization/addsyncaccount':
        url = 'Synchronization_setup.htm';
        break;

      case '/synchronization/addsyncaccount/add-exchange':
        url = '2_factor_exchange_authetication.htm';
        break;

      case '/synchronization/addsyncaccount/add-icloud':
        url = '2_factor_apple_authetication.htm';
        break;

      case '/purchase':
        url = 'Free_and_Premium_comparison.htm';
        break;

      case '/share/select-account':
      case '/share/select-folder':
      case '/share/manage-shared-calendars':
        url = 'Sharing_Setup.htm';
        break;

      case '/share/accepted-calendars':
        url = 'Accepting_a_shared_calendar.htm';
        break;

      case '/synchronization/restore':
        url = 'Backup.htm';
        break;

      case '/account/billinginformation':
        url = 'Recover_account_password.htm';
        break;

      case '/account/mylogins':
        url = 'Login_Management.htm';
        break;
    }

    window.open(
      'http://www.syncgene.com/webhelp' +
        (url! ? '/Content/' + url : '') +
        '?external=true',
      '_blank'
    );
  }

  quickSetupReturnToPreviuosPage(params: any) {
    switch (this.url) {
      case '/quick-setup/step3':
        this.router.navigate(['/quick-setup/step2'], {
          queryParams: {
            source1: params['source1'],
            source2: params['source2'],
          },
          replaceUrl: true,
        });
        break;

      case '/quick-setup/step4':
        this.router.navigate(['/quick-setup/step3'], {
          queryParams: {
            source1: params['source1'],
            source2: params['source2'],
          },
          replaceUrl: true,
        });
        break;
    }
  }
}
