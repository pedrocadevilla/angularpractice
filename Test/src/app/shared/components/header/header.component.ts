import { Component, Input, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { User } from '../../models/user-model';
import { DeviceService } from '../../services/device.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() user: User;
  private ngUnsubscribe: Subject<any> = new Subject();
  sharingApp: boolean;
  currentRoute: string =
    window.location.href.split('/')[window.location.href.split('/').length - 1];
  constructor(private deviceService: DeviceService) {}

  ngOnInit(): void {
    this.deviceService.isShareApp
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        this.sharingApp = resp;
      });
  }
}
