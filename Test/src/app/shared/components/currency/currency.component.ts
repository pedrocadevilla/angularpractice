import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss'],
})
export class CurrencyComponent implements OnInit {
  @Input() currencyCode: string;
  @Input() price: number;
  firstNumber: number;
  secondNumber: number;
  constructor() {}

  ngOnInit() {
    this.seperatePrice();
  }

  seperatePrice() {
    const priceSplit = String(this.price).split('.');
    (this.firstNumber = Number(priceSplit[0])),
      (this.secondNumber = Number(priceSplit[1]));
  }
}
