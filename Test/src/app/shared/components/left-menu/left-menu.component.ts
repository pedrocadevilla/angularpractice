import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { filter, Subject, takeUntil } from 'rxjs';
import { PendingInterceptorService } from '../../interceptors/pending.interceptor.service';
import { Modal } from '../../models/modal-model';
import { AccountsService } from '../../services/account.service';
import { DeviceService } from '../../services/device.service';
import { SyncService } from '../../services/sync.service';
import { UserBaseService } from '../../services/user-base.service';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss'],
})
export class LeftMenuComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() url: string;
  currentRoute: string =
    window.location.href.split('/')[window.location.href.split('/').length - 1];
  private ngUnsubscribe: Subject<any> = new Subject();
  pendingAction = false;
  progressBar: any;
  currentUser: any;
  activeAccounts = 0;
  hidePurchaseLogic: boolean;
  mobile: boolean;
  subscriptionData: any;
  subscriptionEndDateValid = false;
  parameterlessUrl: string = '';
  LEFT_MENU_SYNC_ALL_ARRAY = [
    '/synchronization/status',
    '/synchronization/management',
    '/synchronization/addsyncaccount',
    '/synchronization/addsyncaccount/add-exchange',
    '/synchronization/addsyncaccount/add-icloud',
    '/synchronization/report',
    '/synchronization/whattosync',
    '/synchronization/direction',
    '/synchronization/bulkaddoutlook',
    '/synchronization/bulkaddoutlook/selectfolders',
    '/synchronization/bulkfilters',
  ];
  LEFT_MENU_CANCEL_SYNC_ARRAY = [
    '/synchronization/status',
    '/synchronization/management',
    '/synchronization/addsyncaccount',
    '/synchronization/addsyncaccount/add-exchange',
    '/synchronization/addsyncaccount/add-icloud',
    '/synchronization/report',
    '/synchronization/whattosync',
    '/synchronization/direction',
    '/synchronization/bulkaddoutlook',
    '/synchronization/bulkaddoutlook/selectfolders',
    '/synchronization/bulkfilters',
  ];
  LEFT_MENU_ADD_ACCOUNT_ARRAY = [
    '/synchronization/status',
    '/synchronization/management',
    '/synchronization/addsyncaccount',
    '/synchronization/addsyncaccount/add-exchange',
    '/synchronization/addsyncaccount/add-icloud',
    '/synchronization/report',
    '/synchronization/whattosync',
    '/synchronization/direction',
    '/synchronization/bulkaddoutlook',
    '/synchronization/bulkaddoutlook/selectfolders',
    '/synchronization/bulkfilters',
  ];
  LEFT_MENU_ADD_LOGIN_ARRAY = [
    '/account/mylogins/addlogin',
    '/account/mylogins',
  ];
  LEFT_MENU_PAYMENT_HISTORY_ARRAY = [
    '/account/billinginformation',
    '/account/billinginformation/editcontactinformation',
    '/account/billinginformation/editbillinginformation',
    '/account/billinginformation/currentsubscription',
    '/account/billinginformation/currentsubscription/redeempromocode',
    '/account/mylogins',
    '/account/mylogins/addlogin',
    '/account/preferences',
    '/account/paymenthistory',
    '/account/changepassword',
    '/account/feedback',
    '/purchase',
    '/purchase/billinginformation',
    '/purchase/revieworder',
    '/purchase/processing',
    '/purchase/complete',
    '/purchase/enterpriseactivated',
    '/purchase/teamspurchased',
    '/confirmsubscriptionextension',
  ];
  LEFT_MENU_CANCEL_SUBSCRIPTION_ARRAY = [
    '/account/billinginformation',
    '/account/billinginformation/editcontactinformation',
    '/account/billinginformation/editbillinginformation',
    '/account/billinginformation/currentsubscription',
    '/account/billinginformation/currentsubscription/redeempromocode',
    '/account/mylogins',
    '/account/mylogins/addlogin',
    '/account/preferences',
    '/account/paymenthistory',
    '/account/changepassword',
    '/account/feedback',
    '/purchase',
    '/purchase/billinginformation',
    '/purchase/revieworder',
    '/purchase/processing',
    '/purchase/complete',
    '/purchase/enterpriseactivated',
    '/purchase/teamspurchased',
    '/confirmsubscriptionextension',
  ];
  LEFT_MENU_STATUS_ARRAY = [
    '/synchronization/status',
    '/synchronization/management',
    '/synchronization/addsyncaccount',
    '/synchronization/addsyncaccount/add-exchange',
    '/synchronization/addsyncaccount/add-icloud',
    '/synchronization/report',
    '/synchronization/whattosync',
    '/synchronization/direction',
    '/synchronization/bulkaddoutlook',
    '/synchronization/bulkaddoutlook/selectfolders',
    '/synchronization/bulkfilters',
  ];
  LEFT_MENU_ACCOUNTS_ARRAY = [
    '/synchronization/status',
    '/synchronization/management',
    '/synchronization/addsyncaccount',
    '/synchronization/addsyncaccount/add-exchange',
    '/synchronization/addsyncaccount/add-icloud',
    '/synchronization/report',
    '/synchronization/whattosync',
    '/synchronization/direction',
    '/synchronization/bulkaddoutlook',
    '/synchronization/bulkaddoutlook/selectfolders',
    '/synchronization/bulkfilters',
  ];
  LEFT_MENU_REPORT_ARRAY = [
    '/synchronization/status',
    '/synchronization/management',
    '/synchronization/addsyncaccount',
    '/synchronization/addsyncaccount/add-exchange',
    '/synchronization/addsyncaccount/add-icloud',
    '/synchronization/report',
    '/synchronization/whattosync',
    '/synchronization/direction',
    '/synchronization/bulkaddoutlook',
    '/synchronization/bulkaddoutlook/selectfolders',
    '/synchronization/bulkfilters',
  ];
  LEFT_MENU_RECOVER_PASSWORD_ARRAY = [
    '/account/billinginformation',
    '/account/billinginformation/editcontactinformation',
    '/account/billinginformation/editbillinginformation',
    '/account/billinginformation/currentsubscription',
    '/account/billinginformation/currentsubscription/redeempromocode',
    '/account/mylogins',
    '/account/mylogins/addlogin',
    '/account/preferences',
    '/purchase',
    '/purchase/billinginformation',
    '/purchase/revieworder',
    '/purchase/processing',
    '/purchase/complete',
    '/purchase/enterpriseactivated',
    '/purchase/teamspurchased',
    '/account/paymenthistory',
    '/account/changepassword',
    '/account/feedback',
    '/confirmsubscriptionextension',
  ];
  LEFT_MENU_REMOVE_ACCOUNT_ARRAY = [
    '/account/billinginformation',
    '/account/billinginformation/editcontactinformation',
    '/account/billinginformation/editbillinginformation',
    '/account/billinginformation/currentsubscription',
    '/account/billinginformation/currentsubscription/redeempromocode',
    '/account/mylogins',
    '/account/mylogins/addlogin',
    '/account/preferences',
    '/purchase',
    '/purchase/billinginformation',
    '/purchase/revieworder',
    '/purchase/processing',
    '/purchase/complete',
    '/purchase/enterpriseactivated',
    '/purchase/teamspurchased',
    '/account/paymenthistory',
    '/account/changepassword',
    '/account/feedback',
    '/confirmsubscriptionextension',
  ];
  LEFT_MENU_PROFILE_ARRAY = [
    '/account/billinginformation',
    '/account/billinginformation/editcontactinformation',
    '/account/billinginformation/editbillinginformation',
    '/account/billinginformation/currentsubscription',
    '/account/billinginformation/currentsubscription/redeempromocode',
    '/account/mylogins',
    '/account/mylogins/addlogin',
    '/account/preferences',
    '/purchase',
    '/purchase/billinginformation',
    '/purchase/revieworder',
    '/purchase/processing',
    '/purchase/complete',
    '/purchase/enterpriseactivated',
    '/purchase/teamspurchased',
    '/account/paymenthistory',
    '/account/changepassword',
    '/account/feedback',
    '/confirmsubscriptionextension',
  ];
  LEFT_MENU_MYLOGINS_ARRAY = [
    '/account/billinginformation',
    '/account/billinginformation/editcontactinformation',
    '/account/billinginformation/editbillinginformation',
    '/account/billinginformation/currentsubscription',
    '/account/billinginformation/currentsubscription/redeempromocode',
    '/account/mylogins',
    '/account/mylogins/addlogin',
    '/account/preferences',
    '/purchase',
    '/purchase/billinginformation',
    '/purchase/revieworder',
    '/purchase/complete',
    '/purchase/enterpriseactivated',
    '/purchase/teamspurchased',
    '/account/paymenthistory',
    '/account/changepassword',
    '/account/feedback',
    '/confirmsubscriptionextension',
  ];
  LEFT_MENU_NOTIFICATIONS_PREFERENCES_ARRAY = [
    '/account/billinginformation',
    '/account/billinginformation/editcontactinformation',
    '/account/billinginformation/editbillinginformation',
    '/account/billinginformation/currentsubscription',
    '/account/billinginformation/currentsubscription/redeempromocode',
    '/account/mylogins',
    '/account/mylogins/addlogin',
    '/account/preferences',
    '/purchase',
    '/purchase/billinginformation',
    '/purchase/revieworder',
    '/purchase/processing',
    '/purchase/complete',
    '/purchase/enterpriseactivated',
    '/purchase/teamspurchased',
    '/account/paymenthistory',
    '/account/changepassword',
    '/account/feedback',
    '/confirmsubscriptionextension',
  ];
  showSyncAll = false;
  showCancelSync = false;
  showAddAccount = false;
  showAddLogin = false;
  showPaymentHistory = false;
  showCancelSubscription = false;
  showStatus = false;
  showAccounts = false;
  showReport = false;
  showRecoverPassword = false;
  showRemoveAccount = false;
  showProfile = false;
  showMyLogins = false;
  showNotificationsPreferences = false;
  canPurchaseOrChangePlanBeShown = false;
  canPaymentHistoryBeShown = false;
  canCancelSubscrBeShown = false;
  urlIncludesShar = false;
  urlIncludesShare = false;
  urlIncludesCancelSubscr = false;
  rolesIncludesEnterpriseUser = false;
  rolesIncludesEnterpriseAdmin = false;
  isMobileIos = false;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private translate: TranslateService,
    private pendingInterceptorService: PendingInterceptorService,
    private userService: UserBaseService,
    private syncService: SyncService,
    private accountsService: AccountsService,
    private dialog: NgbModal,
    private deviceService: DeviceService
  ) {}

  ngOnInit() {
    this.parameterlessUrl = this.url.split(/[?#]/)[0];

    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: any) => {
        const url = event.url.split(/[?#]/)[0],
          path = url.split(/[?/]/);

        this.parameterlessUrl = url;
        this.checkWhatToShow();

        if (
          path &&
          path[1] === 'synchronization' &&
          path[2] &&
          path[2] !== 'restore'
        ) {
          // get accounts count
          this.accountsService.activeAccounts
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((resp: number) => {
              this.activeAccounts = resp;
            });
        }
      });

    // subscribe for user info
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.currentUser = resp;

        if (this.currentUser) {
          // subscribe for progress info
          this.syncService.progress
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((res) => {
              this.progressBar = res;
            });

          // check if subscription end date is in future
          const today = new Date();

          if (
            this.currentUser.Subscription &&
            new Date(this.currentUser.Subscription.EndDate) > today
          ) {
            this.subscriptionEndDateValid = true;
          } else {
            this.subscriptionEndDateValid = false;
          }
          this.checkWhatToShow();
        }
      });

    // subscribe for subscription info
    this.userService.currentSubscription
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.subscriptionData = resp;
      });

    // subscribe to pending http requests
    this.pendingInterceptorService.pendingRequestsStatus
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        this.pendingAction = resp;

        // update UI
        this.changeDetectorRef.detectChanges();
      });

    // mobile device?
    this.hidePurchaseLogic = this.deviceService.isPurchaseLogicAvailabe();
    this.mobile = this.deviceService.isMobileApp();
    this.isMobileIos = this.deviceService.mobileOS() === 'ios';
  }

  checkWhatToShow() {
    this.showSyncAll =
      this.LEFT_MENU_SYNC_ALL_ARRAY.find((el) => el == this.parameterlessUrl)
        ?.length! > 0;
    this.showCancelSync =
      this.LEFT_MENU_CANCEL_SYNC_ARRAY.find((el) => el == this.parameterlessUrl)
        ?.length! > 0;
    this.showAddAccount =
      this.LEFT_MENU_ADD_ACCOUNT_ARRAY.find((el) => el == this.parameterlessUrl)
        ?.length! > 0;
    this.showAddLogin =
      this.LEFT_MENU_ADD_LOGIN_ARRAY.find((el) => el == this.parameterlessUrl)
        ?.length! > 0;
    this.showPaymentHistory =
      this.LEFT_MENU_PAYMENT_HISTORY_ARRAY.find(
        (el) => el == this.parameterlessUrl
      )?.length! > 0;
    this.showCancelSubscription =
      this.LEFT_MENU_CANCEL_SUBSCRIPTION_ARRAY.find(
        (el) => el == this.parameterlessUrl
      )?.length! > 0;
    this.showStatus =
      this.LEFT_MENU_STATUS_ARRAY.find((el) => el == this.parameterlessUrl)
        ?.length! > 0;
    this.showAccounts =
      this.LEFT_MENU_ACCOUNTS_ARRAY.find((el) => el == this.parameterlessUrl)
        ?.length! > 0;
    this.showReport =
      this.LEFT_MENU_REPORT_ARRAY.find((el) => el == this.parameterlessUrl)
        ?.length! > 0;
    this.showRecoverPassword =
      this.LEFT_MENU_RECOVER_PASSWORD_ARRAY.find(
        (el) => el == this.parameterlessUrl
      )?.length! > 0;
    this.showRemoveAccount =
      this.LEFT_MENU_REMOVE_ACCOUNT_ARRAY.find(
        (el) => el == this.parameterlessUrl
      )?.length! > 0;
    this.showProfile =
      this.LEFT_MENU_PROFILE_ARRAY.find((el) => el == this.parameterlessUrl)
        ?.length! > 0;
    this.showMyLogins =
      this.LEFT_MENU_MYLOGINS_ARRAY.find((el) => el == this.parameterlessUrl)
        ?.length! > 0;
    this.showNotificationsPreferences =
      this.LEFT_MENU_NOTIFICATIONS_PREFERENCES_ARRAY.find(
        (el) => el == this.parameterlessUrl
      )?.length! > 0;
    this.urlIncludesShare = this.parameterlessUrl?.includes('share');
    this.urlIncludesShar = this.parameterlessUrl?.includes('shar');
    this.urlIncludesCancelSubscr =
      this.parameterlessUrl?.includes('cancelsubscription');
    this.rolesIncludesEnterpriseUser =
      this.currentUser?.Roles.includes('EnterpriseUser');
    this.rolesIncludesEnterpriseUser =
      this.currentUser?.Roles.includes('EnterpriseAdmin');
    this.canPurchaseOrChangePlanBeShown =
      this.hidePurchaseLogic &&
      this.currentUser?.isNotEnterprise &&
      ((this.currentUser?.Subscription?.SubscriptionStatus === 'Active' &&
        this.url != '/account/notifications' &&
        this.url != '/synchronization/restore' &&
        !this.parameterlessUrl?.includes('synchronization') &&
        !this.urlIncludesShare) ||
        !this.currentUser?.Subscription ||
        this.currentUser?.Subscription?.SubscriptionStatus !== 'Active' ||
        !this.rolesIncludesEnterpriseUser ||
        !this.rolesIncludesEnterpriseAdmin);
    this.canPaymentHistoryBeShown =
      (this.currentUser?.isNotEnterprise &&
        this.currentUser?.Subscription?.HasPaymentsHistory &&
        this.showPaymentHistory) ||
      this.urlIncludesCancelSubscr;
    this.canCancelSubscrBeShown =
      (this.currentUser?.isNotEnterprise &&
        this.currentUser?.Subscription?.SubscriptionStatus == 'Active' &&
        this.subscriptionData?.IsAutoRenewalEnabled &&
        !this.isMobileIos &&
        this.showCancelSubscription) ||
      this.urlIncludesCancelSubscr;
  }

  ngAfterViewInit() {
    this.checkWhatToShow();
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  isLinkActive(url: string[]): boolean {
    for (const i in url) {
      if (this.url === url[i]) {
        return true;
      }
    }

    return false;
  }

  sync() {
    this.syncService.checkBackupAccounts();
  }

  cancel() {
    // disable cancel button
    this.progressBar.actionType = 'CancelAction';
    const modalResponse = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'CONFIRM_SYNC_CANCEL_TITLE',
      text: 'CONFIRM_SYNC_CANCEL_TEXT',
      primaryTextButton: 'Yes',
      secondaryTextButton: 'No',
    };

    modalResponse.componentInstance.modalData = modalData;

    modalResponse.result.then((data) => {
      if (data === 'primary') {
        this.cancelConfirm();
        return;
      }
      this.cancelDeny();
    });
  }

  cancelConfirm() {
    // inform all app that progress bar has changed
    this.syncService.setProgress(this.progressBar);

    // cancel currently running action
    this.syncService.cancelAction();
  }
  cancelDeny() {
    this.progressBar.actionType = 'Sync';
  }

  recoverPassword() {
    // confirmation modal

    const modalResponse = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'RESET_PASSWORD',
      text: 'RECOVER_PASSWORD_MODAL_TEXT',
      primaryTextButton: 'Yes',
      secondaryTextButton: 'No',
    };

    modalResponse.componentInstance.modalData = modalData;

    modalResponse.result.then(() => {
      this.userService
        .recoverPassword(this.currentUser.Login)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(() => {
          // show success message
          const modal = this.dialog.open(ModalComponent, {
            backdrop: 'static',
            keyboard: false,
          });
          let moodalData: Modal = {
            title: 'SUCCESS',
            text: 'PASSWORD_RESET_SUCCESS',
          };

          modal.componentInstance.modalData = modalData;
        });
    });
  }

  removeAccount() {
    this.userService.removeAccount();
  }
}
