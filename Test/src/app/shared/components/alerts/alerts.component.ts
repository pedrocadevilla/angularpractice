import { Component, Input, OnInit } from '@angular/core';
import { interval, Subject, takeUntil } from 'rxjs';
import { User } from '../../models/user-model';
import { WebMessageModel } from '../../models/web-message-model';
import { NotificationsService } from '../../services/notification.service';
import { PurchaseService } from '../../services/purchase.service';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss'],
})
export class AlertsComponent implements OnInit {
  @Input() user: User;
  private ngUnsubscribe: Subject<any> = new Subject();
  showDropDownMessage: boolean;
  showBgOverflow: boolean;
  webMessages: WebMessageModel[];
  offerMessagesHeaders = ['get premium', 'trial ended', 'premium for'];
  freePlanUpgradeNotificationShown = false;

  constructor(
    private notificationsService: NotificationsService,
    private purchaseService: PurchaseService
  ) {}

  ngOnInit(): void {
    // subscribe for web messages
    this.notificationsService.webMessages
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        // update if we got new messages only
        if (
          resp &&
          ((this.webMessages && resp.length !== this.webMessages.length) ||
            !this.webMessages)
        ) {
          this.webMessages = [];

          let shouldBeExpanded = false;

          for (let i = 0; i < resp.length; i++) {
            for (let j = 0; j < this.offerMessagesHeaders.length; j++) {
              if (
                !this.freePlanUpgradeNotificationShown &&
                resp[i].Subject.toLowerCase().indexOf(
                  this.offerMessagesHeaders[j]
                ) >= 0
              ) {
                shouldBeExpanded = true;
                this.freePlanUpgradeNotificationShown = true;

                this.webMessages.unshift(resp[i]);

                continue;
              }
            }

            this.webMessages.push(resp[i]);
          }

          if (shouldBeExpanded) {
            setTimeout(() => {
              this.toggleMessage(true);
            }, 1500);
          }

          // close opened message
          this.toggleMessage(false);
        }
      });

    // update web messages every minute
    interval(60000).subscribe(() => {
      if (!this.showDropDownMessage && this.user) {
        this.notificationsService.getWebMessages();
      }
    });
  }

  toggleMessage(close?: boolean) {
    this.showDropDownMessage =
      close !== undefined ? close : !this.showDropDownMessage;
    this.showBgOverflow =
      this.showDropDownMessage && this.webMessages.length > 0;

    if (this.showDropDownMessage) {
      setTimeout(() => this.addClickListeners(), 500);

      for (let j = 0; j < this.offerMessagesHeaders.length; j++) {
        if (
          this.webMessages.length > 0 &&
          this.webMessages[0].Subject.toLowerCase().indexOf(
            this.offerMessagesHeaders[j]
          ) >= 0
        ) {
          this.purchaseService
            .specialOfferNotificationIsShown(this.webMessages[0].Subject)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((res: any) => {});

          break;
        }
      }
    }
  }

  addClickListeners() {
    let dMessages = window.document.getElementsByClassName(
      'dropdown-message-content'
    );

    if (
      dMessages === undefined ||
      dMessages === null ||
      dMessages.length === 0
    ) {
      return;
    }

    for (let i = 0; i < dMessages.length; i++) {
      let msgIdx = dMessages[i].getAttribute('messageidx');

      let buttons = dMessages[i].getElementsByClassName('btn-free-plan');
      if (
        (buttons !== undefined && buttons !== null) ||
        dMessages[i].getElementsByClassName('btn-free-plan').length > 0
      ) {
        for (let i = 0; i < buttons.length; i++) {
          buttons[i].addEventListener('click', () => {
            this.userClickedOnFreePlan(msgIdx);
          });
        }
      }

      buttons = dMessages[i].getElementsByClassName('btn-purchase');
      if (
        (buttons !== undefined && buttons !== null) ||
        dMessages[i].getElementsByClassName('btn-purchase').length > 0
      ) {
        for (let i = 0; i < buttons.length; i++) {
          buttons[i].addEventListener('click', () => {
            this.userClickedOnPurchase(msgIdx);
          });
        }
      }
    }
  }
  userClickedOnFreePlan(messageIdx: any) {
    let msg = this.webMessages[messageIdx];

    this.purchaseService
      .sendFreePlanOffer('Notification: ' + msg.Subject)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        this.toggleMessage(false);
        this.closeMessage(msg);
      });
  }

  userClickedOnPurchase(messageIdx: any) {
    let msg = this.webMessages[messageIdx];

    this.purchaseService
      .trackUserGoPremiumPress('Notification: ' + msg.Subject)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        this.toggleMessage(false);
      });
  }

  closeMessage(message: any) {
    if (
      message.CanBeClosedByRecipient ||
      (!message.CanBeClosedByRecipient && message.ShowOnce)
    ) {
      // remove message from front end
      for (const i in this.webMessages) {
        if (this.webMessages[i].Id === message.Id) {
          this.webMessages.splice(+i, 1);

          // close opened message
          this.toggleMessage(false);
          break;
        }
      }

      // remove from back end
      this.notificationsService.closeWebMessage(message.Id).subscribe();
    }
  }
}
