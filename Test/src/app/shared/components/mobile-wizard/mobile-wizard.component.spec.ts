import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileWizardComponent } from './mobile-wizard.component';

describe('MobileWizardComponent', () => {
  let component: MobileWizardComponent;
  let fixture: ComponentFixture<MobileWizardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MobileWizardComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
