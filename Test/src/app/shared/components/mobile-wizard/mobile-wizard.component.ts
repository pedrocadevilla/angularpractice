import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { DeviceService } from '../../services/device.service';

@Component({
  selector: 'app-mobile-wizard',
  templateUrl: './mobile-wizard.component.html',
  styleUrls: ['./mobile-wizard.component.scss'],
})
export class MobileWizardComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  currentSlide = 0;
  slideConfig = {
    infinite: false,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  sharingApp: boolean;

  constructor(private deviceService: DeviceService, private router: Router) {}

  ngOnInit() {
    // sharing app?
    this.deviceService.isShareApp
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        this.sharingApp = resp;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  afterChange(event: any) {
    this.currentSlide = event.currentSlide;
  }

  closeWizard(action?: boolean) {
    // signup on true
    if (action) {
      this.router.navigate(['/signup']);
    }

    this.deviceService.closeWizard();
  }
}
