import { Component, OnInit } from '@angular/core';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { CookieService } from 'ngx-cookie-service';
import { DeviceService } from '../../services/device.service';
import { UserBaseService } from '../../services/user-base.service';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss'],
})
export class PromotionComponent implements OnInit {
  hideAppPromotion = false;
  mobileBrowserDevice: string | null;
  user: any;

  constructor(
    private cookieService: CookieService,
    private deviceService: DeviceService,
    private userService: UserBaseService,
    private gtmService: GoogleTagManagerService
  ) {}

  ngOnInit(): void {
    this.mobileBrowserDevice = this.deviceService.mobileBrowserOS();
    // subscribe for user info
    this.userService.currentUser.subscribe((resp) => {
      this.user = resp;
      if (this.user && this.user.Id) {
        //set the userId cookie for both app and www
        this.cookieService.set(
          'userId',
          this.user.Id,
          undefined,
          '/',
          'syncgene.com'
        );
        // send event to GTM
        const gtmTag = {
          event: 'AccountSessionStart',
          userId: this.user.Id,
        };
        this.gtmService.pushTag(gtmTag);
      }
    });
  }

  closeAppPromotion() {
    this.cookieService.set('hideAppPromotion', 'true');
    this.hideAppPromotion = true;
  }
}
