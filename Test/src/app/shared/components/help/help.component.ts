import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, filter, Subject, takeUntil } from 'rxjs';
import { HelpService } from '../../services/help.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
  animations: [
    trigger('helpAnimation', [
      state('in', style({ right: 0 })),
      transition('void => *', [
        style({
          right: '-300px',
        }),
        animate('0.15s ease-in-out'),
      ]),
      transition('* => void', [
        animate(
          '0.15s ease-in-out',
          style({
            right: '-300px',
          })
        ),
      ]),
    ]),
  ],
})
export class HelpComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  questions: BehaviorSubject<any>;
  show = false;
  phrase: string;
  results: any;
  isOnline: boolean;

  constructor(
    private router: Router,
    private http: HttpClient,
    private helpService: HelpService
  ) {
    this.http.get('/Content/questions.min.json?v=6').subscribe((resp) => {
      this.questions.next(resp);
    });
  }

  ngOnInit() {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: any) => {
        const url = event.url.split(/[?#]/)[0],
          path = url.split(/[?/]/);

        let page = path[path.length - 1];

        if (!page) {
          page = '/';
        }

        this.questions.subscribe((resp) => {
          if (resp) {
            this.results = resp[page];
          }
        });
      });

    // subscribe for help
    this.helpService.helpOpen
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        this.show = resp;
        if (this.show) {
          this.isOnline = this.helpService.checkSupportOnlineStatus();
        }
      });
  }

  close() {
    this.helpService.close();
  }

  search() {
    if (this.phrase) {
      window.open(
        'http://www.syncgene.com/webhelp/Content/Search.htm?external=true#search-' +
          this.phrase,
        '_blank'
      );
    }
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
