import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrustedLogosSharedComponent } from './trusted-logos-shared.component';

describe('TrustedLogosComponent', () => {
  let component: TrustedLogosSharedComponent;
  let fixture: ComponentFixture<TrustedLogosSharedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrustedLogosSharedComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrustedLogosSharedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
