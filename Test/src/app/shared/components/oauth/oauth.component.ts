import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-oauth',
  templateUrl: './oauth.component.html',
  styleUrls: ['./oauth.component.scss'],
})
export class OauthComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  clientId: string;
  clientSecret: string;
  redirectUrl: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.clientId = params['client_id'];
      this.clientSecret = params['client_secret'];
      this.redirectUrl = params['redirect_url'];

      var code = params['code'];
      var username = params['username'];

      if (!code && !username) {
        this.http
          .post('/api/auth/oauth', {
            ClientId: this.clientId,
            ClientSecret: this.clientSecret,
            RedirectUrl: this.redirectUrl,
          })
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe((resp: any) => {
            window.location.href = resp.url;
          });
      }
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
