import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterpriseAcceptComponent } from './enterprise-accept.component';

describe('EnterpriseAcceptComponent', () => {
  let component: EnterpriseAcceptComponent;
  let fixture: ComponentFixture<EnterpriseAcceptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EnterpriseAcceptComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterpriseAcceptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
