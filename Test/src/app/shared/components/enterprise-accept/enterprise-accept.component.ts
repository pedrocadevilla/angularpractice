import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { Modal } from '../../models/modal-model';
import { User } from '../../models/user-model';
import { EnterpriseService } from '../../services/enterprise.service';
import { NotificationsService } from '../../services/notification.service';
import { UserBaseService } from '../../services/user-base.service';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-enterprise-accept',
  templateUrl: './enterprise-accept.component.html',
  styleUrls: ['./enterprise-accept.component.scss'],
})
export class EnterpriseAcceptComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  userName: string;
  companyName: string;
  hasErrors = false;
  errors: string[];
  email: string;
  isLoggedIn = false;
  id: string;
  loaded = false;
  isLinkExpired = false;
  hasActiveSubscription = false;

  constructor(
    private userService: UserBaseService,
    private translate: TranslateService,
    private enterpriseService: EnterpriseService,
    private route: ActivatedRoute,
    private router: Router,
    private notificationsService: NotificationsService,
    private dialog: NgbModal
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id')!;

    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.isLoggedIn = resp != null;
        if (this.isLoggedIn) {
          this.email = resp!.Email;
          if (
            resp!.Subscription &&
            resp!.Subscription.EndDate! &&
            new Date(resp!.Subscription.EndDate) > new Date()
          ) {
            this.hasActiveSubscription = true;
          }
        }
      });

    this.enterpriseService
      .validateInvite(this.id)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (resp: any) => {
          this.userName = resp.UserName;
          this.companyName = resp.CompanyName;
          this.email = resp.Email;

          this.hasErrors = resp && !resp.IsValid;
          if (this.hasErrors) {
            this.processErrors(resp.Errors);
          }

          this.loaded = true;
        },
        (err) => {
          this.isLinkExpired = err.error.Code === 'ENTERPRISE_INVITE_NOT_FOUND';
          this.hasErrors = true;
          this.loaded = true;
        }
      );
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  joinInternal() {
    let list: string[] = ['ADMIN_CAN_DELETE_YOU'];
    if (this.hasActiveSubscription)
      list.unshift('YOUR_SUSBCRIPTION_WILL_BE_LOST');
    const modalResponse = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'CONFIRM',
      text: this.translate.instant('YOU_WERE_INVITED_IF_YOU_JOIN', {
        var0: this.companyName,
      }),
      primaryButton: 'AGREE_AND_ACCEPT',
      secondaryButton: 'CANCEL',
      textList: list,
      textTranslated: true,
    };

    modalResponse.componentInstance.modalData = modalData;
    // redirect
    modalResponse.result.then(() => {
      this.enterpriseService
        .accept(this.id)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: any) => {
          this.hasErrors = resp && !resp.IsValid;
          if (this.hasErrors) {
            this.processErrors(resp.Errors);
          } else {
            this.notificationsService.getWebMessages();

            this.userService
              .getUser()
              .pipe(takeUntil(this.ngUnsubscribe))
              .subscribe(() => {
                this.router.navigate(['/']);
              });
          }
        });
    });
  }

  join() {
    if (this.isLoggedIn) {
      this.joinInternal();
    } else {
      this.router.navigate(['/signup'], {
        queryParams: {
          returnUrl: 'enterprise/accept/' + this.id,
          loginHint: this.email,
        },
      });
    }
  }

  /*
  TODO VALIDATE WHY THIS METHOD IS BEEN USED SINCE THIS.ERRORS IS NOT BEEN USED 
  */
  processErrors(errors: any) {
    this.errors = [];
    for (let i = 0; i < errors.length; i++) {
      let params: { [k: string]: any } = {};

      if (
        errors[i].Parameters !== undefined &&
        errors[i].Parameters != null &&
        errors[i].Parameters.length > 0
      ) {
        for (let j = 0; j < errors[i].Parameters.length; j++) {
          params['var' + j] = errors[i].Parameters[j];
        }
      }

      let errTranslated: string = this.translate.instant(
        errors[i].Error,
        params
      );
      this.errors.push(errTranslated);
    }
  }

  logOut() {
    this.userService.logout(true, 'enterprise/accept/' + this.id);
  }
}
