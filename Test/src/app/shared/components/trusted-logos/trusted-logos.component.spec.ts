import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrustedLogosComponent } from './trusted-logos.component';

describe('TrustedLogosComponent', () => {
  let component: TrustedLogosComponent;
  let fixture: ComponentFixture<TrustedLogosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrustedLogosComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrustedLogosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
