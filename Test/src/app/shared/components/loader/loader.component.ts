import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { Subject, takeUntil } from 'rxjs';
import { PendingInterceptorService } from '../../interceptors/pending.interceptor.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  animations: [
    trigger('loaderAnimation', [
      state('in', style({ opacity: 1 })),
      transition('void => *', [
        style({
          opacity: 0,
        }),
        animate('0.3s ease'),
      ]),
      transition('* => void', [
        animate(
          '0.3s ease',
          style({
            opacity: 0,
          })
        ),
      ]),
    ]),
  ],
})
export class LoaderComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  public showLoader = true;
  public showTimer = false;
  public timer: any;
  public count = 60;

  constructor(
    private ref: ChangeDetectorRef,
    private pendingInterceptorService: PendingInterceptorService,
    private gtmService: GoogleTagManagerService
  ) {}

  ngOnInit() {
    this.pendingInterceptorService.pendingRequestsStatus
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        this.showLoader = resp;
        this.ref.detectChanges();
      });

    this.pendingInterceptorService.pendingTimerRequestsStatus
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        if (resp === true && this.showTimer !== true) {
          this.startTimer();
        } else if (resp === false && this.showTimer === true) {
          this.stopTimer();
        }

        this.ref.detectChanges();
      });
  }

  startTimer() {
    this.showTimer = true;
    this.timer = setInterval(() => {
      this.count > 0 ? this.count-- : this.triggerFunction();
    }, 1000);
  }

  stopTimer() {
    this.showTimer = false;
    this.count = 60;
    clearInterval(this.timer);
  }

  triggerFunction() {
    clearInterval(this.timer);
    // send event to GTM
    const gtmTag = {
      event: 'OrderTookLongerThanAMinute',
    };
    this.gtmService.pushTag(gtmTag);
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    clearInterval(this.timer);
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
