import { AbstractControl, ValidatorFn } from '@angular/forms';

export function patternValidator(control: AbstractControl) {
  let regex: RegExp = / /;
  if (regex.test(control.value)) {
    return { pattern: true };
  }
  return null;
}

export function passwordMatchValidator(control: AbstractControl) {
  if (control.value !== control.get('confirmPassword')!.value) {
    return { match: true };
  }
  return null;
}
