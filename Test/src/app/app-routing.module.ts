import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./modules/login-v2/login-v2.module').then((m) => m.LoginV2Module),
  },
  {
    path: 'v2/cancel',
    loadChildren: () =>
      import(
        './modules/cancel-subscription-v2/cancel-subscription-v2.module'
      ).then((m) => m.CancelSubscriptionV2Module),
  },
  {
    path: 'v2/billing',
    loadChildren: () =>
      import(
        './modules/billing-information-v2/billing-information-v2.module'
      ).then((m) => m.BillingInformationV2Module),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
