import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {
  constructor(private http: HttpClient) {}

  get() {
    return this.http.get('/api/logins');
  }

  validatePassword(inputString: string) {
    if (
      inputString === undefined ||
      inputString === null ||
      inputString.length < 8
    ) {
      return {
        isSuccessful: false,
        message: 'PASSWORD_TOO_SHORT',
      };
    } else if (
      inputString.indexOf(' ') > -1 ||
      inputString.indexOf('+') > -1 ||
      inputString.indexOf('&') > -1
    ) {
      return {
        isSuccessful: false,
        message: 'PASSWORD_CANT_CONTAIN_CHARACTERS',
      };
    } else {
      return {
        isSuccessful: true,
        message: '',
      };
    }
  }
}
