import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FeedbackModel } from '../models/feedback-model';

@Injectable()
export class FeedbackService {
  returnUrl: string;
  cancelData: any;

  constructor(protected http: HttpClient) {}

  sendFeedback(model: FeedbackModel) {
    return this.http.post('/api/feedback/save', model);
  }
}
