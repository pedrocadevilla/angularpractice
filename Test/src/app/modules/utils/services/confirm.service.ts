import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EmailConfirmationModel } from '../models/email-confirmation-model';
import { VerifyTokenModel } from '../models/verify-token-model';

@Injectable()
export class ConfirmService {
  returnUrl: string;
  cancelData: any;

  constructor(protected http: HttpClient, protected router: Router) {}

  verifyConfirmEmailToken(model: VerifyTokenModel) {
    return this.http.post('/api/accounts/VerifyConfirmEmailToken', model);
  }

  confirm(model: EmailConfirmationModel) {
    return this.http.post('/api/ConfirmAccount', model);
  }
}
