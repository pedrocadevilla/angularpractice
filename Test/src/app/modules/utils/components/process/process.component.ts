import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.scss'],
})
export class ProcessComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  clientId: string;
  clientSecret: string;
  redirectUrl: string;
  accountName: string;
  clientType: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.clientId = params['client_id'];
      this.clientSecret = params['client_secret'];
      this.redirectUrl = params['redirect_url'];
      this.accountName = params['account_name'];
      this.clientType = params['client_type'];

      this.http
        .post('/api/external/process', {
          ClientId: this.clientId,
          ClientSecret: this.clientSecret,
          RedirectUrl: this.redirectUrl,
          AccountName: this.accountName,
          ClientType: this.clientType,
        })
        .subscribe((resp: any) => {
          if (resp.Url.startsWith('http')) {
            window.location = resp.Url;
          } else {
            this.router.navigateByUrl(resp.Url);
          }
        });
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
