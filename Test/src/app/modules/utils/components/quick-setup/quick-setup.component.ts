import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Params, Router, UrlSegment } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { debounceTime, distinctUntilChanged, Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { AccountsService } from 'src/app/shared/services/account.service';
import { SyncService } from 'src/app/shared/services/sync.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { SystemPipe } from 'src/app/shared/utils/system-pipe';
import { IPageSource } from '../../models/page-source-model';
import { ISourcesResponse } from '../../models/sources-response-model';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-quick-setup',
  templateUrl: './quick-setup.component.html',
  styleUrls: ['./quick-setup.component.scss'],
  animations: [
    trigger('inputAnimation', [
      state('in', style({ height: 'auto', opacity: 1 })),
      transition('void => *', [
        style({
          height: 0,
          'margin-bottom': 0,
          opacity: 0,
        }),
        animate('0.25s ease'),
      ]),
      transition('* => void', [
        animate(
          '0.25s ease',
          style({
            height: 0,
            'margin-bottom': 0,
            opacity: 0,
          })
        ),
      ]),
    ]),
  ],
})
export class QuickSetupComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  searchControl: FormControl;
  private debounce: number = 1500;
  loginForm: FormGroup;
  signupForm: FormGroup;
  addAccountForm: FormGroup;
  loginView: boolean;
  syncView: boolean;
  step: number;
  currentResp: ISourcesResponse;
  current: IPageSource;
  currentUser: any;
  returnUrl: string;
  invalidLogin = {
    username: false,
    password: false,
    invalidPasswordMessage: '',
  };
  invalidSignup = {
    Email: false,
  };
  invalidAccount = {
    username: false,
    password: false,
    server: false,
  };
  progress: any = {
    title: '',
    status: '',
    progress: 1,
    actionType: 'Sync',
    accounts: [],
    expanded: false,
    lastActionTime: new Date(),
    update: false,
  };
  accounts: any[];
  selectAccountView: boolean;
  loaded: boolean;
  isEmailLogin = false;
  simpleEmailPattern = /\S+@\S+\.\S+/;
  isCurrentSourceSpecified: boolean = false;
  header: string;
  subheader: string;
  sourcesToSelect: IPageSource[];
  filteredSourcesToSelect: IPageSource[];

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private accountsService: AccountsService,
    private dialog: NgbModal,
    private userService: UserBaseService,
    private syncService: SyncService,
    private translate: TranslateService,
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private systemPipe: SystemPipe
  ) {}

  ngOnInit() {
    // init login form
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      rememberMe: [false],
    });

    this.searchControl = new FormControl('');
    this.searchControl.valueChanges
      .pipe(debounceTime(this.debounce), distinctUntilChanged())
      .subscribe((query) => {
        this.filteredSourcesToSelect = this.sourcesToSelect
          .filter(
            (x) =>
              x.Alias.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
              x.Name.toLowerCase().indexOf(query.toLowerCase()) > -1
          )
          .filter((v, i, a) => a.findIndex((x) => x.Name === v.Name) === i);
      });

    this.activatedRoute.url.subscribe((segment: UrlSegment[]) => {
      // check if this route is valid & get sources data from back end
      this.accountsService
        .validateSources(window.location.href)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(
          (resp: any) => {
            this.currentResp = resp;
            // set return url
            this.returnUrl = '/quick-setup/step2?';
            if (resp.IsSourceOneSpecified) {
              this.returnUrl += 'source1=' + resp.Pages[0].Source1.Alias;
              if (resp.IsSourceTwoSpecified) {
                this.returnUrl += '&';
              }
            }
            if (resp.IsSourceTwoSpecified) {
              this.returnUrl += 'source2=' + resp.Pages[0].Source2.Alias;
            }

            // set step & current account data
            this.step = 1;
            this.calcHeaders();
            if (
              segment[1] &&
              segment[1].path === 'step1' &&
              segment[2] &&
              segment[2].path === 'login'
            ) {
              this.loginView = true;

              this.activatedRoute.queryParams.subscribe((params: Params) => {
                if (params['email']) {
                  this.loginForm.patchValue({ username: params['email'] });
                  this.isEmailLogin = true;
                }
                this.calcHeaders();
              });
            } else if (segment[1] && segment[1].path === 'step2') {
              this.step = 2;
              if (resp.IsSourceOneSpecified) {
                this.current = resp.Pages[0].Source1;
                this.isCurrentSourceSpecified = true;
              } else {
                this.isCurrentSourceSpecified = false;
                this.sourcesToSelect = resp.Pages.map((x: any) => x.Source1).filter(
                  (v: any, i: any, a: any) => a.findIndex((x: any) => x.Name === v.Name) === i
                );
                this.filteredSourcesToSelect = this.sourcesToSelect;
              }
              this.calcHeaders();
            } else if (segment[1] && segment[1].path === 'step3') {
              this.step = 3;
              if (resp.IsSourceTwoSpecified) {
                this.current = resp.Pages[0].Source2;
                this.isCurrentSourceSpecified = true;
              } else {
                this.isCurrentSourceSpecified = false;
                this.sourcesToSelect = resp.Pages.map((x: any) => x.Source2).filter(
                  (v: any, i: any, a: any) => a.findIndex((x: any) => x.Name === v.Name) === i
                );
                this.filteredSourcesToSelect = this.sourcesToSelect;
              }
              this.calcHeaders();
            } else if (segment[1] && segment[1].path === 'step4') {
              this.step = 4;
            } else if (segment[1] && segment[1].path === 'step5') {
              this.step = 5;
              // set status text
              this.translate
                .get('PROGRESS_STATUS_SYNC_SCHEDULED')
                .subscribe((res: string) => {
                  this.progress.status = res;
                });

              // show promo modal after timeout
              setTimeout(() => {
                this.dialog.promo();
              }, 10000);
            } else if (segment[1] && segment[1].path === 'step6') {
              this.step = 6;

              // set status text
              this.translate
                .get('PROGRESS_STATUS_SYNC_INPROGRESS', {
                  var0: 100,
                })
                .subscribe((res: string) => {
                  this.progress.status = res;
                });
            }

            // get current accounts on step 2 & 3
            if (
              segment[1] &&
              (segment[1].path === 'step2' || segment[1].path === 'step3')
            ) {
              let userNameValidation = [];
              if (
                this.current !== undefined &&
                this.current != null &&
                this.current.SyncSystem === 1
              ) {
                //Exchange
                userNameValidation = ['', [Validators.required]];
              } else {
                userNameValidation = [
                  '',
                  [Validators.required, Validators.email],
                ];
              }

              // init add account form
              this.addAccountForm = this.formBuilder.group({
                username: userNameValidation,
                password: ['', Validators.required],
                autoDiscovery: [true],
                server: [''],
              });

              this.accountsService
                .getAccounts()
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe((resp: any) => {
                  this.accounts = resp.Accounts;

                  for (const i in this.accounts) {
                    if (
                      this.isCurrentSourceSpecified &&
                      this.accounts[i]['system'] === this.current.SyncSystem
                    ) {
                      this.selectAccountView = true;
                      break;
                    }
                  }

                  this.loaded = true;
                });
            } else {
              this.loaded = true;
            }
          },
          () => {
            // source pair not found, move to home page
            this.router.navigate(['/'], {
              replaceUrl: true,
            });

            return;
          }
        );
    });

    // get current user info
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.currentUser = resp;
      });

    // init signup form
    this.signupForm = this.formBuilder.group({
      Email: ['', [Validators.required, Validators.email]],
    });

    // subscribe for progress info
    this.syncService.progress
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        if (this.step === 5) {
          // update if status is received
          if (resp.status) {
            this.progress = resp;
          }

          if (resp.progress === 100) {
            // move to next step
            this.moveToNextStep();
          }

          // update UI
          this.changeDetectorRef.detectChanges();
        }
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  private moveToNextStep() {
    const queryParams: { [key: string]: string } = {};
    if (this.currentResp.IsSourceOneSpecified) {
      queryParams['source1'] = this.currentResp.Pages[0].Source1.Alias;
    }
    if (this.currentResp.IsSourceTwoSpecified) {
      queryParams['source2'] = this.currentResp.Pages[0].Source2.Alias;
    }

    this.router.navigate(['/quick-setup/step' + (this.step + 1)], {
      queryParamsHandling: 'merge',
      replaceUrl: true,
      queryParams: queryParams,
    });
  }

  /* step 1 - login/signup */
  externalLogin(type: string) {
    window.location.href =
      '/api/ExternalLogin?provider=' +
      type +
      '&returnUrl=' +
      encodeURIComponent(this.returnUrl);
  }

  login() {
    let firstErrInput: string = '';

    // reset errors
    this.invalidLogin = {
      username: false,
      password: false,
      invalidPasswordMessage: '',
    };

    // custom validation
    if (this.loginForm.get('username')!.invalid) {
      this.invalidLogin.username = true;
      firstErrInput = 'email';
    }

    if (this.loginForm.get('password')!.invalid) {
      this.invalidLogin.password = true;

      if (firstErrInput !== '') {
        firstErrInput = 'password';
      }
    }

    // custom validation
    let passwordValidationResult = this.loginService.validatePassword(
      this.loginForm.get('password')?.value
    );

    if (!passwordValidationResult.isSuccessful) {
      this.invalidLogin.password = true;
      this.invalidLogin.invalidPasswordMessage =
        passwordValidationResult.message;
      firstErrInput = 'newPassword';
    }

    if (firstErrInput) {
      document.getElementById(firstErrInput)?.focus();
      return;
    }

    this.userService.login(
      {
        username: this.loginForm.get('username')?.value,
        password: this.loginForm.get('password')?.value,
        rememberMe: this.loginForm.get('rememberMe')?.value,
        clientId: 'self',
      },
      this.returnUrl
    );
  }

  externalSignup(type: string) {
    // license confirmation modal
    this.dialog.confirmLicense().then(
      () => {
        window.location.href =
          '/api/ExternalLogin?provider=' +
          type +
          '&returnUrl=' +
          encodeURIComponent(this.returnUrl);
      },
      () => {
        // do nothing on no
      }
    );
  }

  signup() {
    let firstErrInput: string = '';

    // reset errors
    this.invalidSignup = {
      Email: false,
    };

    // custom validation
    if (this.signupForm.get('Email')?.invalid) {
      this.invalidSignup.Email = true;
      firstErrInput = 'email';
    }

    if (firstErrInput !== '') {
      document.getElementById(firstErrInput)?.focus();
      return;
    }

    let loginReturnUrl =
      this.returnUrl.toLowerCase().replace('step2', 'step1/login') +
      '&email=' +
      encodeURIComponent(this.signupForm.value.Email);

    // license confirmation modal
    this.dialog.confirmLicense().then(
      () => {
        this.userService.signup({
          Email: this.signupForm.value.Email,
          ReturnUrl: loginReturnUrl,
        });
      },
      () => {
        // do nothing on no
      }
    );
  }
  /* step 1 end */

  /* step 2/3 - add account */
  addExistinAccount(account: any) {
    let reconnectReplicas = [],
      resetReplicas: any = [];

    for (const i in account.replicas) {
      if (
        this.currentResp.Pages[0].ProviderTypes.indexOf(
          account.replicas[i].replicaType
        ) > -1
      ) {
        if (!account.replicas[i].hasAcces) {
          reconnectReplicas.push(account.replicas[i].replicaType);
        }

        if (!account.replicas[i].isValid) {
          resetReplicas.push(account.replicas[i].id);
        }
      }
    }

    if (reconnectReplicas.length > 0) {
      this.accountsService
        .retryAddAccount({
          Id: account.Id != undefined ? account.Id : account.id,
          Provider:
            account.system === 4
              ? 'ICloud'
              : account.system === 2
              ? 'Google'
              : 'Exchange',
          SelectedReplicaTypes: reconnectReplicas,
        })
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(
          () => {
            // move to next step
            this.moveToNextStep();
          },
          (err) => {
            if (err.error.Code !== 'ACCOUNT_PARTIAL_PERMISSIONS_GRANTED') {
              return;
            }
            //possible only for Google now
            this.accountsService
              .add({
                Provider: account.system,
                Username: account.username,
                ReturnUrl: this.returnUrl,
                SelectedReplicaTypes: [1, 2, 3],
              })
              .pipe(takeUntil(this.ngUnsubscribe))
              .subscribe((resp: any) => {
                window.location.href = resp;
              });
          }
        );
    } else if (resetReplicas.length > 0) {
      // confirmation modal
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'RESET_REPLICAS_TITLE',
        text: 'RESET_REPLICAS_TEXT',
        primaryButton: 'yesButton',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then((data) => {
        if (data === 'primary') {
          this.accountsService
            .resetReplicas({
              AccountId: account.Id != undefined ? account.Id : account.id,
              ReplicasIds: resetReplicas,
            })
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(() => {
              // move to next step
              this.moveToNextStep();
            });
        }
      });
    } else {
      // move to next step
      this.moveToNextStep();
    }
  }

  calcHeaders(): void {
    const source1Name: string = this.systemPipe.transform(
      this.currentResp.Pages[0].Source1.SyncSystem
    );
    const source2Name: string = this.systemPipe.transform(
      this.currentResp.Pages[0].Source2.SyncSystem
    );
    if (
      !this.currentResp.IsSourceOneSpecified &&
      this.currentResp.IsSourceTwoSpecified
    ) {
      if (this.step === 1) {
        if (!this.loginView) {
          this.subheader = this.translate.instant('WIZARD_SIGNUP_TEXT_1', {
            var0: this.currentResp.Pages[0].Source2.Name,
          });
        } else {
          this.subheader = this.translate.instant('WIZARD_LOGIN_TEXT_1', {
            var0: this.currentResp.Pages[0].Source2.Name,
          });
        }
      } else if (this.step === 2) {
        this.header = this.translate.instant('WIZARD_ADD_ACCOUNT1_TITLE');
        this.subheader = this.translate.instant('WIZARD_ADD_ACCOUNT_TEXT_2', {
          var0: this.currentResp.Pages[0].Source2.Name,
        });
      } else {
        this.header = this.translate.instant('WIZARD_ADD_ACCOUNT_HEADER', {
          var0: this.step,
          var1: source2Name,
        });
        this.subheader = this.translate.instant('WIZARD_ADD_ACCOUNT_TEXT_3', {
          var0: this.currentResp.Pages[0].Source2.Name,
          var1: source2Name,
        });
      }
    } else if (
      this.currentResp.IsSourceOneSpecified &&
      !this.currentResp.IsSourceTwoSpecified
    ) {
      if (this.step === 1) {
        if (!this.loginView) {
          this.subheader = this.translate.instant('WIZARD_SIGNUP_TEXT_1', {
            var0: this.currentResp.Pages[0].Source1.Name,
          });
        } else {
          this.subheader = this.translate.instant('WIZARD_LOGIN_TEXT_1', {
            var0: this.currentResp.Pages[0].Source1.Name,
          });
        }
      } else if (this.step === 2) {
        this.header = this.translate.instant('WIZARD_ADD_ACCOUNT_HEADER', {
          var0: this.step,
          var1: source1Name,
        });
        this.subheader = this.translate.instant('WIZARD_ADD_ACCOUNT_TEXT_3', {
          var0: this.currentResp.Pages[0].Source1.Name,
          var1: source1Name,
        });
      } else {
        this.header = this.translate.instant('WIZARD_ADD_ANOTHER_SOURCE_TITLE');
        this.subheader = this.translate.instant('WIZARD_ADD_ACCOUNT_TEXT_2', {
          var0: this.currentResp.Pages[0].Source1.Name,
        });
      }
    } else if (
      this.currentResp.IsSourceOneSpecified &&
      this.currentResp.IsSourceTwoSpecified
    ) {
      if (this.step === 1) {
        if (!this.loginView) {
          this.subheader = this.translate.instant('WIZARD_SIGNUP_TEXT_2', {
            var0: this.currentResp.Pages[0].Source1.Name,
            var1: this.currentResp.Pages[0].Source2.Name,
          });
        } else {
          this.subheader = this.translate.instant('WIZARD_LOGIN_TEXT_2', {
            var0: this.currentResp.Pages[0].Source1.Name,
            var1: this.currentResp.Pages[0].Source2.Name,
          });
        }
      } else if (this.step === 2) {
        this.header = this.translate.instant('WIZARD_ADD_ACCOUNT_HEADER', {
          var0: this.step,
          var1: source1Name,
        });
        this.subheader = this.translate.instant('WIZARD_ADD_ACCOUNT_TEXT_1', {
          var0: this.currentResp.Pages[0].Source1.Name,
          var1: this.currentResp.Pages[0].Source2.Name,
          var2: source1Name,
        });
      } else {
        this.header = this.translate.instant('WIZARD_ADD_ACCOUNT_HEADER', {
          var0: this.step,
          var1: source2Name,
        });
        this.subheader = this.translate.instant('WIZARD_ADD_ACCOUNT_TEXT_1', {
          var0: this.currentResp.Pages[0].Source1.Name,
          var1: this.currentResp.Pages[0].Source2.Name,
          var2: source2Name,
        });
      }
    } else {
      if (this.step === 1) {
        if (!this.loginView) {
          this.subheader = this.translate.instant('WIZARD_SIGNUP_TEXT');
        } else {
          this.subheader = this.translate.instant('WIZARD_LOGIN_TEXT');
        }
      } else if (this.step === 2) {
        this.header = this.translate.instant('WIZARD_ADD_ACCOUNT1_TITLE');
        this.subheader = this.translate.instant('WIZARD_ADD_ACCOUNT_TEXT');
      } else {
        this.header = this.translate.instant('WIZARD_ADD_ANOTHER_SOURCE_TITLE');
        this.subheader = this.translate.instant('WIZARD_ADD_ACCOUNT_TEXT');
      }
    }
  }

  getReturnUrl(): string {
    let returnUrl = '/quick-setup/step' + (this.step + 1) + '?';
    if (this.currentResp.IsSourceOneSpecified) {
      returnUrl += 'source1=' + this.currentResp.Pages[0].Source1.Alias;
      if (this.currentResp.IsSourceTwoSpecified) {
        returnUrl += '&';
      }
    }
    if (this.currentResp.IsSourceTwoSpecified) {
      returnUrl += 'source2=' + this.currentResp.Pages[0].Source2.Alias;
    }

    return returnUrl;
  }

  externalAccountAdd(syncSystem: string) {
    this.accountsService
      .add({
        Provider: syncSystem,
        SelectedReplicaTypes: this.currentResp.Pages[0].ProviderTypes,
        ReturnUrl: this.getReturnUrl(),
      })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        window.location.href = resp;
      });
  }

  googleHelp() {
    // count rights and pass for modal
    let rights = {
      contacts: false,
      calendar: false,
      tasks: false,
    };

    for (const i in this.currentResp.Pages[0].ProviderTypes) {
      if (this.currentResp.Pages[0].ProviderTypes[i] === 1) {
        rights.contacts = true;
      } else if (this.currentResp.Pages[0].ProviderTypes[i] === 2) {
        rights.calendar = true;
      } else if (this.currentResp.Pages[0].ProviderTypes[i] === 3) {
        rights.tasks = true;
      }
    }

    this.dialog.GoogleInfo(rights);
  }

  icloudHelp() {
    this.dialog.iCloudInfo();
  }

  accountAdd() {
    let firstErrInput: string = '';

    // reset errors
    this.invalidAccount = {
      username: false,
      password: false,
      server: false,
    };

    // custom validation
    if (this.addAccountForm.get('username')?.invalid) {
      this.invalidAccount.username = true;
      firstErrInput = 'email';
    }

    if (this.addAccountForm.get('password')?.invalid) {
      this.invalidAccount.password = true;

      if (firstErrInput !== '') {
        firstErrInput = 'password';
      }
    }

    if (
      !this.addAccountForm.value.autoDiscovery &&
      !this.addAccountForm.value.server &&
      this.current.SyncSystem === 1
    ) {
      this.invalidAccount.server = true;

      if (!firstErrInput) {
        firstErrInput = 'server';
      }
    }

    if (firstErrInput) {
      document.getElementById(firstErrInput)?.focus();
      return;
    }

    if (
      this.current.SyncSystem === 1 &&
      !this.simpleEmailPattern.test(this.addAccountForm.value.username) &&
      (this.addAccountForm.value.autoDiscovery ||
        !this.addAccountForm.value.server)
    ) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'INFORMATION',
        text: 'EXCHANGE_USERNAME_WITHOUT_SERVER_USED',
      };

      modal.componentInstance.modalData = modalData;
      return;
    }

    // add missing attributes
    this.addAccountForm.value.provider =
      this.current.SyncSystem === 4 ? 'ICloud' : 'Exchange';

    if (this.currentResp.Pages[0].ProviderTypes) {
      this.addAccountForm.value.SelectedReplicaTypes =
        this.currentResp.Pages[0].ProviderTypes;
    }

    const formValue: any = this.addAccountForm.value;
    formValue.ReturnUrl = this.getReturnUrl();

    this.accountsService
      .add(formValue)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        () => {
          // move to next step
          this.moveToNextStep();
        },
        (err) => {
          if (
            this.addAccountForm.value.autoDiscovery &&
            err.error.Code !== 'ACCOUNT_ALLREADY_IN_LIST'
          ) {
            this.addAccountForm.value.server = null;
          }

          if (
            err.error.Code === 'EXCHANGE_ACCOUNT_VERIFICATION_FAILED_TRY_AGAIN'
          ) {
            var tmp = { autoDiscovery: false };
            this.addAccountForm.patchValue(tmp);

            const modal = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            let modalData: Modal = {
              title: 'ERROR',
              text: 'EXCHANGE_ACCOUNT_VERIFICATION_FAILED_TRY_AGAIN',
            };

            modal.componentInstance.modalData = modalData;
          } else if (
            err.error.Code === 'UNVERIFIED_ACCOUNT' &&
            this.current.SyncSystem === 1
          ) {
            const modal = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            let modalData: Modal = {
              title: 'ERROR',
              text: 'INCORRECT_EMAIL_OR_PASSWORD',
            };

            modal.componentInstance.modalData = modalData;
          } else if (
            err.error.Code === 'SYNC_ACCOUNT_RECONNECTED' ||
            err.error.Code === 'SYNC_ACCOUNT_RECONNECTED_1' ||
            err.error.Code === 'SYNC_ACCOUNT_RECONNECTED_2'
          ) {
            // move to next step
            this.moveToNextStep();
          }
        }
      );
  }
  /* step 2/3 end */

  /* step 4 - start sync */
  sync() {
    this.syncService.startSync('/quick-setup/step' + (this.step + 1));
  }
  /* step 4 end */

  logOut() {
    this.userService.logout(true, '/quick-setup/step1/login');
  }

  openHelp(text: string) {
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'INFORMATION',
      text: text,
    };

    modal.componentInstance.modalData = modalData;
  }
  sourceSelected(idx: number) {
    const selectedSource = this.filteredSourcesToSelect[idx];
    this.currentResp.Pages = this.currentResp.Pages.filter((x) =>
      this.step === 2
        ? x.Source1.Name == selectedSource.Name
        : x.Source2.Name == selectedSource.Name
    );

    if (this.step === 2) {
      this.currentResp.IsSourceOneSpecified = true;
      this.returnUrl += 'source1=' + selectedSource.Alias;
    } else {
      this.currentResp.IsSourceTwoSpecified = true;
      if (!this.returnUrl.endsWith('?')) {
        this.returnUrl += '&';
      }
      this.returnUrl += 'source2=' + selectedSource.Alias;
    }

    this.current = selectedSource;
    this.isCurrentSourceSpecified = true;

    this.calcHeaders();
  }
}
