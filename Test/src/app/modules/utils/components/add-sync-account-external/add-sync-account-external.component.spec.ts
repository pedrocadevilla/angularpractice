import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSyncAccountExternalComponent } from './add-sync-account-external.component';

describe('AddSyncAccountExternalComponent', () => {
  let component: AddSyncAccountExternalComponent;
  let fixture: ComponentFixture<AddSyncAccountExternalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddSyncAccountExternalComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSyncAccountExternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
