import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AccountsService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-add-sync-account-external',
  templateUrl: './add-sync-account-external.component.html',
  styleUrls: ['./add-sync-account-external.component.scss'],
})
export class AddSyncAccountExternalComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(
    private accountsService: AccountsService,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.http
      .get('/api/external/getaddsyncaccountmodel')
      .subscribe((resp: any) => {
        //synchronization/addsyncaccount/add-exchange

        if (resp.Provider.toLowerCase() === 'google') {
          this.accountsService
            .add(resp)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((intResp: any) => {
              window.location.href = intResp;
            });
        } else {
          this.router.navigate(
            [
              '/synchronization/addsyncaccount/add-' +
                resp.Provider.toLowerCase(),
            ],
            {
              queryParams: {
                accountName: resp.Username,
                returnUrl: resp.ReturnUrl,
              },
            }
          );
        }
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
