import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-validate-folder',
  templateUrl: './validate-folder.component.html',
  styleUrls: ['./validate-folder.component.scss'],
})
export class ValidateFolderComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.http.get('/api/external/validatefolder').subscribe((resp: any) => {
      if (resp.Url.startsWith('http')) {
        window.location = resp.Url;
      } else {
        this.router.navigateByUrl(resp.Url, { replaceUrl: true });
      }
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
