import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateFolderComponent } from './validate-folder.component';

describe('ValidateFolderComponent', () => {
  let component: ValidateFolderComponent;
  let fixture: ComponentFixture<ValidateFolderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ValidateFolderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
