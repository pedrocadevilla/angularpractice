import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { FeedbackModel } from '../../models/feedback-model';
import { FeedbackService } from '../../services/feedback.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  form: FeedbackModel = {
    Rating: 0,
    GroupedFeedbacks: [
      {
        IsSelected: false,
        Name: 'MissingFeature',
        FeedbackLines: [
          {
            IsSelected: false,
            Name: 'OneWaySync',
          },
          {
            IsSelected: false,
            Name: 'AdvancedMapping',
          },
          {
            IsSelected: false,
            Name: 'CompanyAccount',
          },
          {
            IsSelected: false,
            Name: 'ImmediateSync',
          },
          {
            IsSelected: false,
            Name: 'AdvansedSettings',
          },
          {
            IsSelected: false,
            Name: 'AutoSyncFrequency',
          },
          {
            IsSelected: false,
            Name: 'NoteSync',
          },
          {
            IsSelected: false,
            Name: 'OtherFeatureMissing',
            TextValue: '',
          },
        ],
      },
      {
        IsSelected: false,
        Name: 'TechnicalIssue',
        FeedbackLines: [
          {
            IsSelected: false,
            Name: 'CantAddIcloud',
          },
          {
            IsSelected: false,
            Name: 'CantAddExchange',
          },
          {
            IsSelected: false,
            Name: 'RequestsNotSynchronizing',
          },
          {
            IsSelected: false,
            Name: 'CalendarNotSynchronizing',
          },
          {
            IsSelected: false,
            Name: 'ContactsNotSynchronizing',
          },
          {
            IsSelected: false,
            Name: 'SyncTakesForever',
          },
          {
            IsSelected: false,
            Name: 'SyncShowsCancelling',
          },
          {
            IsSelected: false,
            Name: 'OtherTechnicalIssue',
            TextValue: '',
          },
        ],
      },
    ],
    SingleFeedbacks: [
      {
        IsSelected: false,
        Name: 'Compliment',
        TextValue: '',
      },
      {
        IsSelected: false,
        Name: 'Idea',
        TextValue: '',
      },
      {
        IsSelected: false,
        Name: 'OtherCategory',
        TextValue: '',
      },
    ],
  };
  showThankYou: boolean = false;
  rateValue: number = 0;
  rateValues: number[] = [1, 2, 3, 4, 5];

  constructor(private feedBackService: FeedbackService) {}

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  rate(value: number) {
    this.rateValue = value;
  }

  clearSelection(name: string, value: boolean) {
    if (value === true) {
      return;
    } else {
      this.form.GroupedFeedbacks.forEach((group) => {
        if (group.Name === name) {
          group.FeedbackLines!.forEach((line) => {
            line.IsSelected = false;
            if (line.hasOwnProperty('TextValue')) {
              line.TextValue = '';
            }
          });
        }
      });
    }
  }

  clearText(name: string, value: boolean) {
    if (value === true) {
      return;
    } else {
      this.form.SingleFeedbacks.forEach((feedback) => {
        if (feedback.Name === name) {
          feedback.TextValue = '';
        }
      });
      if (name === 'OtherFeatureMissing') {
        this.form.GroupedFeedbacks![0].FeedbackLines![7].TextValue = '';
      }
      if (name === 'OtherTechnicalIssue') {
        this.form.GroupedFeedbacks![1].FeedbackLines![7].TextValue = '';
      }
    }
  }

  submit() {
    this.form.Rating = this.rateValue;

    this.feedBackService
      .sendFeedback(this.form)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        // show thank you
        this.showThankYou = true;
      });
  }
}
