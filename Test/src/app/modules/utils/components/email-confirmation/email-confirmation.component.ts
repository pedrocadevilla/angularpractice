import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { CookieService } from 'ngx-cookie-service';
import { errorMessages } from 'src/app/modules/login-v2/models/login-form-err-message';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { Modal } from 'src/app/shared/models/modal-model';
import { initializeForm } from '../../../account/models/password-constructor';
import { ConfirmService } from '../../services/confirm.service';

@Component({
  selector: 'app-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.scss'],
})
export class EmailConfirmationComponent implements OnInit {
  passwordErrorList: errorMessageObject[] = [];
  usernameErrorList: errorMessageObject[] = [];
  form: FormGroup;
  id: string;
  token: string;
  loaded = false;
  returnUrl: string;
  invalid = {
    NewPassword: '',
    ConfirmPassword: false,
  };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private confirmService: ConfirmService,
    private gtmService: GoogleTagManagerService,
    private dialog: NgbModal,
    private cookieService: CookieService
  ) {
    this.passwordErrorList = errorMessages.passwordMessages;
    this.usernameErrorList = errorMessages.userNameMessages;
  }

  ngOnInit() {
    // init form
    this.form = initializeForm();

    // get required query params
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params['id'] && params['token']) {
        this.id = params['id'];
        this.token = params['token'];
        this.returnUrl = params['returnUrl'] ? params['returnUrl'] : '';

        this.confirmService
          .verifyConfirmEmailToken({ Id: params['id'], Token: params['token'] })
          .subscribe(
            () => {
              this.loaded = true;
            },
            () => {
              // redirect to login page if token is not valid
              this.router.navigate(['/'], { replaceUrl: true });
            }
          );
      } else {
        // redirect to login page if no params found
        this.router.navigate(['/'], { replaceUrl: true });
      }
    });
  }

  confirm() {
    if (!this.form.valid) {
      return;
    }
    // add id & token to model
    this.form.value.Id = this.id;
    this.form.value.Token = this.token;

    // confirm account in back-end
    this.confirmService.confirm(this.form.value).subscribe((resp: any) => {
      let signupEventName = 'SignUp';

      if (this.cookieService.get('sharing_accept') === 'true') {
        signupEventName = 'SignUpSharingAccept';
      }
      // send event to GTM
      const gtmTag = {
        event: 'AccountSignUpEmail',
        accountSignUpEmailEventName: signupEventName,
      };
      this.gtmService.pushTag(gtmTag);

      const modalResponse = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'SUCCESS',
        text: 'ACCOUNT_PASSWORD_CONFIRMED',
      };

      modalResponse.componentInstance.modalData = modalData;
      // redirect
      modalResponse.result.then(() => {
        this.router.navigate(['/']);
      });
    });
  }
}
