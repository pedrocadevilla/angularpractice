import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-sharing-login',
  templateUrl: './sharing-login.component.html',
  styleUrls: ['./sharing-login.component.scss'],
})
export class SharingLoginComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  clientId: string;
  clientSecret: string;
  redirectUrl: string;
  shareSubscriptionId: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.clientId = params['client_id'];
      this.clientSecret = params['client_secret'];
      this.redirectUrl = params['redirect_url'];
      this.shareSubscriptionId = params['share_subscription_id'];

      this.http
        .post('/api/external/SharingLogin', {
          ClientId: this.clientId,
          ClientSecret: this.clientSecret,
          RedirectUrl: this.redirectUrl,
          ShareSubscriptionId: this.shareSubscriptionId,
        })
        .subscribe((resp: any) => {
          if (resp.Url.startsWith('http')) {
            window.location = resp.Url;
          } else {
            this.router.navigateByUrl(resp.Url);
          }
        });
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
