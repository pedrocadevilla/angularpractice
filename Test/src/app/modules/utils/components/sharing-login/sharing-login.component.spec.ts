import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharingLoginComponent } from './sharing-login.component';

describe('SharingLoginComponent', () => {
  let component: SharingLoginComponent;
  let fixture: ComponentFixture<SharingLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SharingLoginComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharingLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
