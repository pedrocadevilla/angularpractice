export interface EmailConfirmationModel {
  Id: number;
  Token: string;
  NewPassword: string;
  ConfirmPassword: string;
}
