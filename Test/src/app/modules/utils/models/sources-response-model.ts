import { IPageSource } from './page-source-model';

export interface ISourcesResponse {
  IsExactPageFound: boolean;
  IsSourceOneSpecified: boolean;
  IsSourceTwoSpecified: boolean;
  Pages: IPagePair[];
}

export interface IPagePair {
  ProviderTypes: number[];
  Source1: IPageSource;
  Source2: IPageSource;
  Name: String;
}
