export interface IPageSource {
  Name: string;
  Icon: string;
  Alias: string;
  SyncSystem: number;
}
