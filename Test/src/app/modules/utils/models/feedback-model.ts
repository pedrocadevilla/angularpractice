export interface FeedbackModel {
  Rating: number;
  GroupedFeedbacks: GroupFeedbackModel[];
  SingleFeedbacks: FeedbackLine[];
}

export interface GroupFeedbackModel {
  IsSelected: boolean;
  Name: string;
  FeedbackLines?: FeedbackLine[];
}

export interface FeedbackLine {
  IsSelected: boolean;
  Name: string;
  TextValue?: string;
}
