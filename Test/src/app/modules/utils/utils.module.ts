import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtilsRoutingModule } from './utils-routing.module';
import { InnerSharedModule } from 'src/app/shared/modules/inner-shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmailConfirmationComponent } from './components/email-confirmation/email-confirmation.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { AddSyncAccountExternalComponent } from './components/add-sync-account-external/add-sync-account-external.component';
import { ProcessComponent } from './components/process/process.component';
import { SharingLoginComponent } from './components/sharing-login/sharing-login.component';
import { ValidateFolderComponent } from './components/validate-folder/validate-folder.component';
import { ValidateLoginComponent } from './components/validate-login/validate-login.component';
import { QuickSetupComponent } from './components/quick-setup/quick-setup.component';
import { SystemPipe } from 'src/app/shared/utils/system-pipe';
import { StoresComponent } from './components/stores/stores.component';

@NgModule({
  declarations: [
    EmailConfirmationComponent,
    FeedbackComponent,
    AddSyncAccountExternalComponent,
    ProcessComponent,
    SharingLoginComponent,
    ValidateFolderComponent,
    ValidateLoginComponent,
    QuickSetupComponent,
    SystemPipe,
    StoresComponent,
  ],
  imports: [
    CommonModule,
    UtilsRoutingModule,
    InnerSharedModule,
    ReactiveFormsModule,
    FormsModule,
  ],
})
export class UtilsModule {}
