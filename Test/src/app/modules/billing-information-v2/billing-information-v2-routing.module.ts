import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingComponent } from './components/billing/billing.component';
import { EditBillingFormComponent } from './components/edit-billing-form/edit-billing-form.component';
import { EditContactFormComponent } from './components/edit-contact-form/edit-contact-form.component';

const routes: Routes = [
  { path: '', component: BillingComponent },
  { path: 'editBilling', component: EditBillingFormComponent },
  { path: 'editContact', component: EditContactFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BillingInformationV2RoutingModule {}
