import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Address } from 'src/app/shared/models/address-model';

export function initializeForm() {
  return new FormGroup({
    FirstName: new FormControl('', Validators.required),
    LastName: new FormControl('', Validators.required),
    Company: new FormControl(''),
    PhoneNumber: new FormControl(''),
    Email: new FormControl('', [Validators.required, Validators.email]),
    CountryId: new FormControl(null, Validators.required),
    StateProvinceId: new FormControl(null, Validators.required),
    City: new FormControl('', Validators.required),
    Address1: new FormControl('', Validators.required),
    ZipPostalCode: new FormControl('', Validators.required),
  });
}

export function fillForm(resp: Address): FormGroup {
  return new FormGroup({
    FirstName: new FormControl(resp.FirstName, Validators.required),
    LastName: new FormControl(resp.LastName, Validators.required),
    Company: new FormControl(resp.Company),
    PhoneNumber: new FormControl(resp.PhoneNumber),
    Email: new FormControl(resp.Email, [Validators.required, Validators.email]),
    CountryId: new FormControl(resp.CountryId, Validators.required),
    StateProvinceId: new FormControl(resp.StateProvinceId, Validators.required),
    City: new FormControl(resp.City, Validators.required),
    Address1: new FormControl(resp.Address1, Validators.required),
    ZipPostalCode: new FormControl(resp.ZipPostalCode, Validators.required),
  });
}
