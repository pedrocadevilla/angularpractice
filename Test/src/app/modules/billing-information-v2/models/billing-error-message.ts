export const errorMessages = {
  editMessage: [
    { message: 'The field is required', key: 'required' },
    { message: 'The email is invalid', key: 'email' },
  ],
};
