import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { Modal } from 'src/app/shared/models/modal-model';
import { CountriesService } from 'src/app/shared/services/countries.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { errorMessages } from '../../models/billing-error-message';
import { fillForm, initializeForm } from '../../models/billing-information';

@Component({
  selector: 'app-edit-billing-form',
  templateUrl: './edit-billing-form.component.html',
  styleUrls: ['./edit-billing-form.component.scss'],
})
export class EditBillingFormComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  message = {
    title: '',
    text: '',
  };
  editErrorList: errorMessageObject[] = [];
  form: FormGroup;
  addresses: any;
  countries: any;
  states: any;
  invalid = {
    FirstName: false,
    LastName: false,
    Email: false,
    CountryId: false,
    StateProvinceId: false,
    City: false,
    Address1: false,
    ZipPostalCode: false,
  };
  loaded = false;

  constructor(
    private userService: UserBaseService,
    private countriesService: CountriesService,
    private dialog: NgbModal,
    private router: Router
  ) {
    this.form = initializeForm();
    this.editErrorList = errorMessages.editMessage;
  }

  ngOnInit() {
    // get countries
    this.countriesService.get().subscribe((resp) => {
      this.countries = resp;
    });

    // get user addresses
    this.userService
      .getUserAddresses()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (resp: any) => {
          // set form values
          if (resp && resp.BillingAddress) {
            this.addresses = resp.BillingAddress;

            this.form = fillForm(resp.BillingAddress);

            // get states to show
            this.getStatest();
          }

          // show content
          this.loaded = true;
        },
        (err) => {
          // show content
          this.loaded = true;
        }
      );
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  getStatest() {
    for (const i in this.countries) {
      if (this.countries[i].Id == this.form.value.CountryId) {
        this.states = this.countries[i].StateProvinces;
        break;
      }
    }
  }

  stateDetect() {
    this.form.patchValue({ StateProvinceId: null });
    this.invalid.StateProvinceId = false;
    this.getStatest();
  }

  save() {
    if (!this.form.valid) {
      //dialog message
      return;
    }

    this.userService.updateBillingAddress(this.form.value).subscribe(() => {
      // show success message
      const modalResponse = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'SUCCESS',
        text: 'BILLING_INFORMATION_SAVED',
      };

      modalResponse.componentInstance.modalData = modalData;
      // redirect
      modalResponse.result.then(() => {
        this.router.navigate(['/']);
      });
    });
  }
}
