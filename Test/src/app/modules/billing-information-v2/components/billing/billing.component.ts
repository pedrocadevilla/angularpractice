import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { takeUntil } from 'rxjs';
import { Subject } from 'rxjs/internal/Subject';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { DeviceService } from 'src/app/shared/services/device.service';
import { LanguagesService } from 'src/app/shared/services/laguage.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss'],
})
export class BillingComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  user: any;
  hidePurchaseLogic: boolean;
  addresses: any;
  timezones: any;
  subscriptionEndDateValid = false;
  subscriptionData: any;
  selectedLanguage: string;
  languages: string[];
  canPaymentHistoryBeShown: boolean;

  constructor(
    private http: HttpClient,
    private userService: UserBaseService,
    private deviceService: DeviceService,
    private languagesService: LanguagesService,
    private dialog: NgbModal
  ) {}

  ngOnInit() {
    this.userService
      .getUserAddresses()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        this.addresses = resp;
      });

    this.http
      .get('/api/account/timezones')
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        this.timezones = resp;
      });

    this.languages = this.languagesService.availableLanguages;

    this.languagesService.language
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.selectedLanguage = resp;
      });

    // subscribe for user info
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.user = resp;

        if (this.user && this.user.Subscription) {
          this.userService.currentSubscription
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((resp) => {
              this.subscriptionData = resp;
            });

          //check if subscription end date is in future
          const today = new Date();

          if (new Date(this.user.Subscription.EndDate) > today) {
            this.subscriptionEndDateValid = true;
          } else {
            this.subscriptionEndDateValid = false;
          }
        }
      });

    // mobile device?
    this.hidePurchaseLogic = this.deviceService.isPurchaseLogicAvailabe();
    this.canPaymentHistoryBeShown =
      this.user?.isNotEnterprise && this.user?.Subscription?.HasPaymentsHistory;
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  updateTimezone() {
    this.http
      .post('/api/account/SetTimezone', {
        TimeZoneId: this.timezones.SelectedTimeZone,
      })
      .subscribe();
  }

  updateLanguage() {
    this.languagesService
      .update(this.selectedLanguage)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.user.Language = this.selectedLanguage;
        this.userService.updateUser(this.user);
      });
    this.languagesService.language
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.selectedLanguage = resp;
      });
  }

  resetPassword() {
    // confirmation modal
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'RESET_PASSWORD',
      text: 'RECOVER_PASSWORD_MODAL_TEXT',
      primaryButton: 'yesButton',
      secondaryButton: 'noButton',
    };

    modal.componentInstance.modalData = modalData;

    modal.result.then((data) => {
      if (data === 'primary') {
        this.userService
          .recoverPassword(this.user.Login)
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(() => {
            const modalResponse = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            // show success message
            let modalData: Modal = {
              title: 'SUCCESS',
              text: 'PASSWORD_RESET_SUCCESS',
            };

            modalResponse.componentInstance.modalData = modalData;
          });
      }
    });
  }

  removeAccount() {
    this.userService.removeAccountModal().subscribe((text: string) => {
      // show success message
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'REMOVE_ACCOUNT_MODAL_TITLE',
        text: 'REMOVE_ACCOUNT_MODAL_TEXT',
        primaryButton: 'yesButton',
        secondaryButton: 'noButton',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then((data) => {
        if (data === 'primary') {
          this.userService.removeAccount();
        }
      });
    });
  }
}
