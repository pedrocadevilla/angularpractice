import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingInformationV2RoutingModule } from './billing-information-v2-routing.module';
import { BillingComponent } from './components/billing/billing.component';
import { EditBillingFormComponent } from './components/edit-billing-form/edit-billing-form.component';
import { EditContactFormComponent } from './components/edit-contact-form/edit-contact-form.component';
import { InnerSharedModule } from 'src/app/shared/modules/inner-shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LanguagePipe } from 'src/app/shared/utils/language-pipe';

@NgModule({
  declarations: [
    BillingComponent,
    EditBillingFormComponent,
    EditContactFormComponent,
    LanguagePipe,
  ],
  imports: [
    CommonModule,
    BillingInformationV2RoutingModule,
    InnerSharedModule,
    ReactiveFormsModule,
    FormsModule,
  ],
})
export class BillingInformationV2Module {}
