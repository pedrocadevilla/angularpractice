import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { InnerSharedModule } from 'src/app/shared/modules/inner-shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddLoginComponent } from './components/add-login/add-login.component';
import { ResetPasswordFormComponent } from './components/reset-password-form/reset-password-form.component';
import { MyLoginsComponent } from './components/my-logins/my-logins.component';
import { ChangePasswordValidateComponent } from './components/change-password-validate/change-password-validate.component';
import { PaymentHistoryComponent } from './components/payment-history/payment-history.component';
import { PreferencesComponent } from './components/preferences/preferences.component';
import { UnsubscribeComponent } from './components/unsubscribe/unsubscribe.component';

@NgModule({
  declarations: [
    AddLoginComponent,
    ResetPasswordFormComponent,
    MyLoginsComponent,
    ChangePasswordValidateComponent,
    PaymentHistoryComponent,
    PreferencesComponent,
    UnsubscribeComponent,
  ],
  imports: [
    CommonModule,
    AccountRoutingModule,
    InnerSharedModule,
    ReactiveFormsModule,
    FormsModule,
  ],
})
export class AccountModule {}
