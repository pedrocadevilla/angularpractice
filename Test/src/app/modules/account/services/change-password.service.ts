import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { ChangeAccountModel } from '../models/change-account-model';
import { ChangeUserModel } from '../models/change-user-model';

@Injectable()
export class ChangePasswordService {
  private observable: Observable<any>;
  activeAccounts = new BehaviorSubject(0);
  lastNavigationEndEvent = new BehaviorSubject(null);

  constructor(private http: HttpClient) {}

  changePass(model: ChangeAccountModel) {
    return this.http.post('/api/accounts/changepassword', model);
  }

  changePassUser(model: ChangeUserModel) {
    return this.http.post('/api/ChangePassword', model);
  }
}
