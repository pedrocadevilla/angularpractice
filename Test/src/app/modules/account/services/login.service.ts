import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserLoginModel } from '../models/user-login-model';
@Injectable()
export class LoginService {
  constructor(private http: HttpClient) {}

  getAppleLoginUrl(returnUrl?: string, loginHint?: string): Observable<string> {
    let query = '';
    if (returnUrl && returnUrl.length > 0) {
      query += '?returnUrl=' + returnUrl;
    }
    if (loginHint && loginHint.length > 0) {
      query += query.length > 0 ? '&' : '?';
      query += 'loginHint=' + loginHint;
    }

    return this.http.get<string>('/api/GetAppleLoginUrl' + query);
  }

  get() {
    return this.http.get('/api/logins');
  }

  remove(model: UserLoginModel) {
    return this.http.post('/api/logins/remove', model);
  }
}
