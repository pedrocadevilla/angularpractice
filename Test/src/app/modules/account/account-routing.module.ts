import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddLoginComponent } from './components/add-login/add-login.component';
import { ChangePasswordValidateComponent } from './components/change-password-validate/change-password-validate.component';
import { MyLoginsComponent } from './components/my-logins/my-logins.component';
import { PaymentHistoryComponent } from './components/payment-history/payment-history.component';
import { PreferencesComponent } from './components/preferences/preferences.component';
import { ResetPasswordFormComponent } from './components/reset-password-form/reset-password-form.component';
import { UnsubscribeComponent } from './components/unsubscribe/unsubscribe.component';

const routes: Routes = [
  { path: 'myLogins', component: MyLoginsComponent },
  { path: 'changePassword', component: ChangePasswordValidateComponent },
  { path: 'resetPassword', component: ResetPasswordFormComponent },
  { path: 'paymentHistory', component: PaymentHistoryComponent },
  { path: 'addLogin', component: AddLoginComponent },
  { path: 'preferences', component: PreferencesComponent },
  { path: 'unsubscribe', component: UnsubscribeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
