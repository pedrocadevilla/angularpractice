import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { UserBaseService } from 'src/app/shared/services/user-base.service';

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.scss'],
})
export class UnsubscribeComponent implements OnInit {
  private ngUnsubscribe: Subject<any> = new Subject();
  loaded = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserBaseService
  ) {
    // subscribe to router event
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params['id']) {
        this.userService
          .unsubscribe(params['id'])
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(
            () => {
              this.loaded = true;
            },
            () => {
              this.router.navigate(['/'], { replaceUrl: true });
            }
          );
      } else {
        this.router.navigate(['/'], { replaceUrl: true });
      }
    });
  }

  ngOnInit() {}
}
