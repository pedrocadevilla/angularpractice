import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, takeUntil } from 'rxjs';
import { errorMessages } from 'src/app/modules/login-v2/models/login-form-err-message';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { Modal } from 'src/app/shared/models/modal-model';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { initializeForm } from '../../models/add-login-constructor';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-add-login',
  templateUrl: './add-login.component.html',
  styleUrls: ['./add-login.component.scss'],
})
export class AddLoginComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  passwordErrorList: errorMessageObject[] = [];
  usernameErrorList: errorMessageObject[] = [];

  form: FormGroup;
  invalid = {
    Email: false,
    Password: '',
    ConfirmedPassword: false,
  };
  currentUser: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private dialog: NgbModal,
    private userService: UserBaseService,
    private loginsService: LoginService
  ) {
    this.passwordErrorList = errorMessages.passwordMessages;
    this.usernameErrorList = errorMessages.userNameMessages;
  }

  ngOnInit() {
    // init form
    this.form = initializeForm();

    // subscribe for user info
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.currentUser = resp;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  addLogin() {
    if (this.form.valid) {
      this.http.post('/api/logins/add', this.form.value).subscribe(() => {
        // signup successful, redirect to logins page
        const modal = this.dialog.open(ModalComponent, {
          backdrop: 'static',
          keyboard: false,
        });

        let modalData: Modal = {
          title: 'SUCCESS',
          text: 'LOGIN_ACCOUNT_ADDED_CONFIRM',
        };

        modal.componentInstance.modalData = modalData;

        modal.result.then(() => {
          this.router.navigate(['/myLogins']);
        });
      });
    }
  }

  addExternalLogin(type: string) {
    if (type === 'Apple') {
      this.loginsService
        .getAppleLoginUrl()
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: string) => {
          window.location.href = resp;
        });

      return;
    }

    window.location.href =
      '/api/ExternalLogin?provider=' + type + '&UserId=' + this.currentUser.Id;
  }
}
