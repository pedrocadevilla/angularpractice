import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordValidateComponent } from './change-password-validate.component';

describe('ChangePasswordValidateComponent', () => {
  let component: ChangePasswordValidateComponent;
  let fixture: ComponentFixture<ChangePasswordValidateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChangePasswordValidateComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordValidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
