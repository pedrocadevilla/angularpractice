import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { Modal } from 'src/app/shared/models/modal-model';
import { errorMessages } from '../../../login-v2/models/login-form-err-message';
import { initializeFormOld } from '../../models/password-constructor';
import { ChangePasswordService } from '../../services/change-password.service';

@Component({
  selector: 'app-change-password-validate',
  templateUrl: './change-password-validate.component.html',
  styleUrls: ['./change-password-validate.component.scss'],
})
export class ChangePasswordValidateComponent implements OnInit {
  @Input() account: number;
  @Input() change: boolean;
  form: FormGroup;
  passwordErrorList: errorMessageObject[] = [];
  partialLoader = false;
  unverifiedAccount = false;

  constructor(
    public changePasswordService: ChangePasswordService,
    private router: Router,
    private dialog: NgbModal
  ) {
    this.passwordErrorList = errorMessages.passwordMessages;
    this.form = initializeFormOld();
  }

  ngOnInit() {}

  closeModal() {
    this.change = !this.change;
  }

  changePassword() {
    // add missing data to model
    if (!this.form.valid) {
      return;
    }
    this.form.value.Id = this.account;

    this.changePasswordService.changePassUser(this.form.value).subscribe(() => {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });

      let modalData: Modal = {
        title: 'SUCCESS',
        text: 'PASSWORD_CHANGED',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then(() => {
        this.router.navigate(['/addLogin']);
      });
    });
  }
}
