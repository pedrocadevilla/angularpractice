import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss'],
})
export class PreferencesComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  checked: boolean;
  preferences: any;
  partial = false;

  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit() {
    this.http
      .get('/api/account/preferences')
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        this.preferences = resp;

        // check for any checked notifications
        this.toggleOne();
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  toggleAll() {
    this.checked = !this.checked;
    this.partial = false;

    for (const i of Object.keys(this.preferences)) {
      this.preferences[i].IsEmailNotificationEnabled = this.checked;
    }
  }

  toggleOne() {
    let enabledCount = 0;

    for (const i in this.preferences) {
      if (this.preferences[i].IsEmailNotificationEnabled) {
        enabledCount += 1;
      }
    }

    if (enabledCount > 0) {
      this.checked = true;
    } else {
      this.checked = false;
    }

    if (enabledCount > 0 && enabledCount < this.preferences.length) {
      this.partial = true;
    } else {
      this.partial = false;
    }
  }

  save() {
    this.http
      .post('/api/account/SavePreferences', this.preferences)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.router.navigate(['/synchronization/status']);
      });
  }

  cancel() {
    this.router.navigate(['/synchronization/status']);
  }
}
