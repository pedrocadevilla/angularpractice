import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyLoginsComponent } from './my-logins.component';

describe('MyLoginsComponent', () => {
  let component: MyLoginsComponent;
  let fixture: ComponentFixture<MyLoginsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MyLoginsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyLoginsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
