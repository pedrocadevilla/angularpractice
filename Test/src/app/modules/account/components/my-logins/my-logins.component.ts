import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-my-logins',
  templateUrl: './my-logins.component.html',
  styleUrls: ['./my-logins.component.scss'],
  animations: [
    trigger('toggleAnimation', [
      state('in', style({ height: '*' })),
      transition('void => *', [
        style({ height: 0 }),
        animate(
          '0.3s ease',
          style({
            height: '*',
          })
        ),
      ]),
      transition('* => void', [
        style({ height: '*' }),
        animate(
          '0.3s ease',
          style({
            height: 0,
          })
        ),
      ]),
    ]),
  ],
})
export class MyLoginsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  logins: any;
  selected: number = 0;
  change: boolean = false;

  constructor(private loginService: LoginService, private dialog: NgbModal) {}

  ngOnInit() {
    // load logins
    this.loginService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        this.logins = resp;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  select(index: number, mobile?: boolean) {
    if (mobile && this.selected === index) {
      this.selected = 0;
    } else {
      this.selected = index;
    }
  }

  remove() {
    if (this.selected !== 0 && this.logins.length > 1) {
      // confirmation modal
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });

      let modalData: Modal = {
        title: 'REMOVE_LOGIN_MODAL_TITLE',
        text: 'REMOVE_LOGIN_MODAL_TEXT',
        primaryButton: 'yesButton',
        secondaryButton: 'noButton',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then((data) => {
        if (data === 'primary') {
          this.loginService.remove(this.logins[this.selected]).subscribe(() => {
            // delete from front-end array
            this.logins.splice(this.selected, 1);

            // reset selected item
            this.selected = 0;
          });
        }
      });
    }
  }

  changePass() {
    if (
      this.selected !== 0 &&
      this.logins[this.selected].LoginProvider !== 'Google' &&
      this.logins[this.selected].LoginProvider !== 'Microsoft'
    ) {
      this.change = true;
    }
  }
}
function takeUntil(
  ngUnsubscribe: Subject<any>
): import('rxjs').OperatorFunction<Object, unknown> {
  throw new Error('Function not implemented.');
}
