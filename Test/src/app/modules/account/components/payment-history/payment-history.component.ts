import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { NotificationsService } from 'src/app/shared/services/notification.service';
import { PurchaseService } from 'src/app/shared/services/purchase.service';

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.scss'],
  animations: [
    trigger('toggleAnimation', [
      state('in', style({ height: '*' })),
      transition('void => *', [
        style({ height: 0 }),
        animate(
          '0.3s ease',
          style({
            height: '*',
          })
        ),
      ]),
      transition('* => void', [
        style({ height: '*' }),
        animate(
          '0.3s ease',
          style({
            height: 0,
          })
        ),
      ]),
    ]),
  ],
  host: {
    '(window:resize)': 'onResize($event)',
  },
})
export class PaymentHistoryComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  payments: any;
  selected: number = 0;
  loaded = false;
  itemsPerPage: number = 1000;

  constructor(
    private purchaseService: PurchaseService,
    private dialog: NgbModal,
    private notificationsService: NotificationsService
  ) {}

  ngOnInit() {
    this.onPageLoad();
  }

  onPageLoad() {
    // get payments history
    this.purchaseService
      .getPurchaseHistory({
        Page: 1,
        ItemsPerPage: this.itemsPerPage,
        Pattern: null,
      })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (resp: any) => {
          this.payments = resp.PaymentHistoryItems;
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  onResize(event: any) {
    if (this.selected === null && event.target.innerWidth > 767) {
      // set first invoice as selected to prevent empty preview on bigger screens
      this.selected = 0;

      // scroll left panel to top
      const left = document.getElementsByClassName('panel-left');

      left[0].scrollTop = 0;
    }
  }

  scrollTo(index: number) {
    setTimeout(() => {
      const el = document.getElementById('payment' + index);

      $('html, body').animate(
        {
          scrollTop: el!.offsetTop,
        },
        300
      );
    }, 500);
  }

  select(index: number, mobile?: boolean) {
    if (mobile && this.selected === index) {
      this.selected = 0;
    } else {
      this.selected = index;

      if (mobile) {
        this.scrollTo(index);
      }
    }
  }

  print() {
    window.print();
  }

  pay() {
    this.purchaseService
      .payNow()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        if (resp || resp === 0) {
          let title: string = '';
          let text: string = '';
          switch (resp) {
            case 0:
              this.onPageLoad();
              title = 'SUCCESS';
              text = 'PAYMENT_SUCCESSFUL';
              break;

            case 2:
              this.notificationsService.getWebMessages();
              title = 'PAYMENT_FAILURE_TITLE';
              text = 'GENERAL_FAILURE';
              break;
          }
          const modal = this.dialog.open(ModalComponent, {
            backdrop: 'static',
            keyboard: false,
          });

          let modalData: Modal = {
            title: title,
            text: text,
          };

          modal.componentInstance.modalData = modalData;
        } else {
          const modal = this.dialog.open(ModalComponent, {
            backdrop: 'static',
            keyboard: false,
          });

          let modalData: Modal = {
            title: 'PAYMENT_FAILURE_TITLE',
            text: 'GENERAL_FAILURE',
          };

          modal.componentInstance.modalData = modalData;
        }
      });
  }
}
