import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { Modal } from 'src/app/shared/models/modal-model';
import { AccountsService } from 'src/app/shared/services/account.service';
import { errorMessages } from '../../../login-v2/models/login-form-err-message';
import { ChangePasswordService } from '../../../login-v2/services/change-password.service';
import { initializeForm } from '../../models/password-constructor';

@Component({
  selector: 'app-change-password-form',
  templateUrl: './reset-password-form.component.html',
  styleUrls: ['./reset-password-form.component.scss'],
})
export class ResetPasswordFormComponent implements OnInit {
  passwordErrorList: errorMessageObject[] = [];
  form: FormGroup;
  id: string;
  token: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private accountsService: AccountsService,
    private router: Router,
    private changePasswordService: ChangePasswordService,
    private dialog: NgbModal
  ) {
    this.passwordErrorList = errorMessages.passwordMessages;
    this.form = initializeForm();
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params['id'] && params['token']) {
        this.id = params['id'];
        this.token = params['token'];

        this.accountsService
          .verifyResetPasswordToken({
            Id: params['id'],
            Token: params['token'],
          })
          .subscribe(
            () => {
              //this.loaded = true;
            },
            () => {
              // redirect to login page if token is not valid
              this.router.navigate(['/'], { replaceUrl: true });
            }
          );
      } else {
        // redirect to login page if no params found
        this.router.navigate(['/'], { replaceUrl: true });
      }
    });
  }

  change() {
    if (!this.form.valid) {
      //dialog message
      return;
    }
    this.changePasswordService.reset(this.form.value).subscribe(() => {
      // show success message
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'SUCCESS',
        text: 'PASSWORD_CHANGED',
      };

      modal.componentInstance.modalData = modalData;
      // redirect to login page || account page
      setTimeout(() => {
        this.router.navigate(['/account/billinginformation'], {
          replaceUrl: true,
        });
      });
    });
  }
}
