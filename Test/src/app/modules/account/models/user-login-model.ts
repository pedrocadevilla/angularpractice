export interface UserLoginModel {
  Email: string;
  LoginProvider: string;
  ProviderKey: string;
  DateAdded: string;
}
