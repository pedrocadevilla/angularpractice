import { FormControl, FormGroup, Validators } from '@angular/forms';

export function initializeForm(currency: string) {
  return new FormGroup({
    companyInfo: new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      company: new FormControl('', [Validators.required]),
      countryId: new FormControl('', [Validators.required]),
      stateProvinceId: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      address1: new FormControl('', [Validators.required]),
      zipPostalCode: new FormControl('', [Validators.required]),
      phoneNumber: new FormControl('', [Validators.required]),
      companyType: new FormControl('', [Validators.required]),
    }),
    currency: new FormControl(currency),
  });
}
