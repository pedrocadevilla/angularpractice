export interface PricingPlan {
  Id: number;
  Name: string;
  IsMultiAccount: boolean;
  Strategies: { [key: string]: any[] };
  ListBenefits?: { iconName: string; title: string; text: string }[];
  IsPrePaid?: boolean;
}
