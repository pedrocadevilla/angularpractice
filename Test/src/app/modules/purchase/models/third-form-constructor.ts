import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

export function initializeForm(
  company: FormControl,
  currencyValue?: string,
  selectedPlan?: string
) {
  return new FormGroup({
    address: new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      company,
      email: new FormControl('', [Validators.required, Validators.email]),
      countryId: new FormControl(null, Validators.required),
      stateProvinceId: new FormControl(null, Validators.required),
      city: new FormControl('', Validators.required),
      address1: new FormControl('', Validators.required),
      zipPostalCode: new FormControl('', Validators.required),
      phoneNumber: new FormControl(''),
    }),
    paymentInformation: new FormGroup({
      number: new FormControl('', [
        Validators.required,
        Validators.pattern('[0-9]{11,19}'),
      ]),
      type: new FormControl('Visa', Validators.required),
      expirationMonth: new FormControl(null, [
        Validators.required,
        Validators.pattern('[0-1]{1}[0-9]{1}'),
      ]),
      expirationYear: new FormControl(null, [
        Validators.required,
        Validators.pattern('2[0-9]{3}'),
      ]),
      code: new FormControl('', [
        Validators.required,
        Validators.pattern('[0-9]{3,4}'),
      ]),
    }),
    companyInfo: new FormGroup({
      company: new FormControl(''),
      email: new FormControl(''),
      countryId: new FormControl(null),
      stateProvinceId: new FormControl(null),
      city: new FormControl(''),
      address1: new FormControl(''),
      zipPostalCode: new FormControl(''),
      phoneNumber: new FormControl(''),
      companyType: new FormControl(0),
    }),
    contact: new FormGroup({
      usersToSync: new FormControl(5, [Validators.required, Validators.min(1)]),
      fullName: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      company: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      countryId: new FormControl(null, [Validators.required]),
      phoneNumber: new FormControl('', [Validators.required]),
    }),
    paymentMethod: new FormControl('credit'),
    policy: new FormControl(false, Validators.pattern('true')),
    pricingPlanId: new FormControl(selectedPlan),
    currency: new FormControl(currencyValue),
  });
}
