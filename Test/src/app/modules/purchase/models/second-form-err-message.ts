export const errorMessages = {
  emailMessages: [
    { message: 'The field is required', key: 'required' },
    { message: 'The email is invalid', key: 'email' },
  ],
  requiredMessages: [{ message: 'The field is required', key: 'required' }],
};
