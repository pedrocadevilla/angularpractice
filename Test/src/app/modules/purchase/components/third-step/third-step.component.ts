import { animate, style, transition, trigger } from '@angular/animations';
import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { CountriesService } from 'src/app/shared/services/countries.service';
import { NotificationsService } from 'src/app/shared/services/notification.service';
import { PurchaseService } from 'src/app/shared/services/purchase.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { initializeForm } from '../../models/third-form-constructor';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-third-step',
  templateUrl: './third-step.component.html',
  styleUrls: ['./third-step.component.scss'],
  animations: [
    trigger('expandAnimation', [
      transition('void => *', [
        style({ height: 0, opacity: 0, 'padding-top': 0, 'padding-bottom': 0 }),
        animate(
          '0.2s ease',
          style({
            height: '*',
            opacity: '*',
            'padding-top': '*',
            'padding-bottom': '*',
          })
        ),
      ]),
      transition('* => void', [
        style({
          height: '*',
          opacity: '*',
          'padding-top': '*',
          'padding-bottom': '*',
        }),
        animate(
          '0.2s ease',
          style({
            height: 0,
            opacity: 0,
            'padding-top': 0,
            'padding-bottom': 0,
          })
        ),
      ]),
    ]),
  ],
})
export class ThirdStepComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();

  @ViewChild('month') public expMonth: ElementRef;
  @ViewChild('year') public expYear: ElementRef;
  @ViewChild('country') public country: ElementRef;
  @ViewChild('cardType') public cardType: ElementRef;

  form: FormGroup;
  countries: any;
  states: any;
  years: any;
  months = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
  ];
  cards = ['Visa', 'MasterCard', 'Amex', 'Diners', 'Discover'];
  selectedPlan: any;
  invalid = {
    FirstName: false,
    LastName: false,
    Company: false,
    Email: false,
    CountryId: false,
    StateProvinceId: false,
    City: false,
    Address1: false,
    ZipPostalCode: false,
    Number: false,
    Type: false,
    ExpirationMonth: false,
    ExpirationYear: false,
    Code: false,
  };
  invalidContact = {
    UsersToSync: false,
    FullName: false,
    Company: false,
    Email: false,
    CountryId: false,
    Number: false,
  };
  isChangeAction: boolean = false;
  isLoaded: boolean = false;
  currency: string;
  currencies: string[] = [];
  company: FormControl = new FormControl('');

  constructor(
    private router: Router,
    private productsService: ProductsService,
    private countriesService: CountriesService,
    private userService: UserBaseService,
    private dialog: NgbModal,
    private purchaseService: PurchaseService,
    private notificationsService: NotificationsService
  ) {
    initializeForm(this.company);
  }

  ngOnInit() {
    this.isChangeAction = this.router.url.indexOf('/changepaymentmethod') > -1;
    if (!this.isChangeAction) {
      // get selected plan
      this.selectedPlan = this.productsService.getProduct();

      if (this.selectedPlan === null || !this.selectedPlan) {
        this.router.navigate(['/purchase']);
        return;
      }

      if (this.selectedPlan.IsMultiAccount) {
        this.company = new FormControl('', Validators.required);
      }
      for (let key in this.selectedPlan.Strategies) {
        if (this.currencies.indexOf(key) < 0) {
          this.currencies.push(key);
        }
      }
      this.currencies.sort();
    } else {
      this.selectedPlan = null;
    }

    this.currency = this.productsService.getCurrency();
    initializeForm(this.company, this.currency, this.selectedPlan);

    // get countries
    this.countriesService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.countries = resp;
      });

    // calculate years for credit card
    let i = 0;
    this.years = [];

    while (i < 15) {
      if (i === 0) {
        this.years.push(new Date().getFullYear().toString());
      } else {
        this.years.push(
          (parseInt(this.years[this.years.length - 1], 10) + 1).toString()
        );
      }

      i++;
    }

    // get & set order data is available
    const data = this.productsService.getPurchaseData();

    if (data) {
      this.form.patchValue(data);
      this.isLoaded = true;
      this.getStatest();
    } else {
      // get user billing address
      this.userService
        .getUserAddresses()
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(
          (resp: any) => {
            // set form values
            if (resp && resp.BillingAddress) {
              this.form.controls['Address'].patchValue({
                FirstName: resp.BillingAddress.FirstName,
                LastName: resp.BillingAddress.LastName,
                Company: resp.BillingAddress.Company,
                PhoneNumber: resp.BillingAddress.PhoneNumber,
                Email: resp.BillingAddress.Email,
                CountryId: resp.BillingAddress.CountryId,
                StateProvinceId: resp.BillingAddress.StateProvinceId,
                City: resp.BillingAddress.City,
                Address1: resp.BillingAddress.Address1,
                ZipPostalCode: resp.BillingAddress.ZipPostalCode,
              });

              // get states to show
              this.getStatest();

              // set data for step 3
              this.productsService.setPurchaseData(this.form.value);
            }
            this.isLoaded = true;
          },
          () => {
            this.isLoaded = true;
          }
        );
    }
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  getStatest() {
    for (const i in this.countries) {
      if (this.countries[i].Id == this.form.value.Address.CountryId) {
        this.states = this.countries[i].StateProvinces;
        break;
      }
    }
  }

  stateDetect() {
    this.form.patchValue({ StateProvinceId: null });
    this.invalid.StateProvinceId = false;

    this.getStatest();
  }

  changeInfo() {
    if (!this.form.valid) {
      return;
    }

    this.purchaseService
      .changePaymentMethod(this.form.value)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        if (resp) {
          if (this.form.value.PaymentMethod === 'paypal') {
            const url = resp.replace(/"/g, '');
            window.location.href = url;
          } else if (this.form.value.PaymentMethod === 'credit') {
            // update user subscription info
            this.userService.getUser().subscribe(() => {
              switch (resp.PaymentStatus) {
                case 0:
                  setTimeout(() => {
                    this.router.navigate(['/changeCompleted']);
                  });

                  break;

                case 2:
                  // update web notifications
                  this.notificationsService.getWebMessages();

                  const modalTerm = this.dialog.open(ModalComponent, {
                    backdrop: 'static',
                    keyboard: false,
                  });
                  let modalData: Modal = {
                    title: 'CREDIT_CARD_FAILURE_TITLE',
                    text: 'CREDIT_CARD_FAILURE_CONTENT',
                  };

                  modalTerm.componentInstance.modalData = modalData;
                  break;
              }
            });
          }
        } else {
          const modalTerm = this.dialog.open(ModalComponent, {
            backdrop: 'static',
            keyboard: false,
          });
          let modalData: Modal = {
            title: 'PAYMENT_FAILURE_TITLE',
            text: 'GENERAL_FAILURE',
          };

          modalTerm.componentInstance.modalData = modalData;
        }
      });
  }

  reviewOrder() {
    if (!this.form.valid) {
      return;
    }

    // maybe we got OrderId from payment history?
    const data = this.productsService.getPurchaseData();

    if (data && data.OrderId) {
      this.form.value.OrderId = data.OrderId;
    }

    // set data for step 3
    this.productsService.setCurrency(this.form.value.Currency);
    // set data for step 3
    this.productsService.setPurchaseData(this.form.value);

    // navigate to next step
    this.router.navigate(['/fourthStep']);
  }

  //next code is due to bug: autofiller does not set values in correct way for select tag
  monthFocus() {
    setTimeout(() => {
      let value = this.expMonth.nativeElement.value;
      if (this.form.value.PaymentInformation.ExpirationMonth != value) {
        this.form.patchValue({
          PaymentInformation: { ExpirationMonth: value },
        });
      }
    }, 100);
  }

  yearFocus() {
    setTimeout(() => {
      let value = this.expYear.nativeElement.value;
      if (this.form.value.PaymentInformation.ExpirationYear != value) {
        this.form.patchValue({ PaymentInformation: { ExpirationYear: value } });
      }
    }, 100);
  }

  cardTypeFocus() {
    setTimeout(() => {
      let value = this.cardType.nativeElement.value;
      if (this.form.value.PaymentInformation.Type != value) {
        this.form.patchValue({ PaymentInformation: { Type: value } });
      }
    }, 100);
  }

  countryFocus() {
    setTimeout(() => {
      let value = this.country.nativeElement.value;
      if (this.form.value.Address.CountryId != value) {
        this.form.patchValue({ Address: { CountryId: value } });
        this.stateDetect();
      }
    }, 100);
  }
}
