import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { CountriesService } from 'src/app/shared/services/countries.service';
import { initializeForm } from '../../models/second-form-constructor';
import { errorMessages } from '../../models/second-form-err-message';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-second-step',
  templateUrl: './second-step.component.html',
  styleUrls: ['./second-step.component.scss'],
  animations: [
    trigger('expandAnimation', [
      transition('void => *', [
        style({ height: 0, opacity: 0, 'padding-top': 0, 'padding-bottom': 0 }),
        animate(
          '0.2s ease',
          style({
            height: '*',
            opacity: '*',
            'padding-top': '*',
            'padding-bottom': '*',
          })
        ),
      ]),
      transition('* => void', [
        style({
          height: '*',
          opacity: '*',
          'padding-top': '*',
          'padding-bottom': '*',
        }),
        animate(
          '0.2s ease',
          style({
            height: 0,
            opacity: 0,
            'padding-top': 0,
            'padding-bottom': 0,
          })
        ),
      ]),
    ]),
  ],
})
export class SecondStepComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  form: FormGroup;
  countries: any;
  states: any;
  selectedPlan: any;
  isLoaded: boolean = false;
  currency: string;
  invalid = {
    Company: false,
    Email: false,
    CountryId: false,
    StateProvinceId: false,
    City: false,
    Address1: false,
    ZipPostalCode: false,
    PhoneNumber: false,
  };
  isTypeSelectable = false;
  isCompany = true;
  currencies: string[] = [];
  emailErrorList: errorMessageObject[] = [];
  requiredErrorList: errorMessageObject[] = [];

  constructor(
    private router: Router,
    private productsService: ProductsService,
    private countriesService: CountriesService
  ) {
    this.form = initializeForm(this.currency);
    this.emailErrorList = errorMessages.emailMessages;
    this.requiredErrorList = errorMessages.requiredMessages;
  }

  ngOnInit() {
    this.selectedPlan = this.productsService.getProduct();
    this.currency = this.productsService.getCurrency();
    if (this.selectedPlan === null || !this.selectedPlan || !this.currency) {
      this.router.navigate(['/purchase']);
      return;
    }

    for (let key in this.selectedPlan.Strategies) {
      if (this.currencies.indexOf(key) < 0) {
        this.currencies.push(key);
      }
    }
    this.currencies.sort();

    // get countries
    this.countriesService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.countries = resp;
        this.isLoaded = true;
      });

    // get & set order data is available
    const data = this.productsService.getPurchaseData();

    if (data && data.OrderId) {
      this.form.patchValue(data);
    } else {
      // get states to show
      this.getStatest();

      // set data for step 2
      this.productsService.setPurchaseData(this.form.value);
    }
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  getStatest() {
    for (const i in this.countries) {
      if (this.countries[i].Id == this.form.value.CompanyInfo.CountryId) {
        this.states = this.countries[i].StateProvinces;
        break;
      }
    }
  }

  stateDetect() {
    this.form.controls['companyInfo']!.get('StateProvinceId')!.setValue(null);
    this.invalid.StateProvinceId = false;

    this.getStatest();
  }

  continue() {
    if (!this.form.valid) {
      return;
    }

    let companyInfo = this.form.value.companyInfo;
    let address = Object.assign({}, this.form.value.companyInfo);

    if (address.CompanyType != 1) {
      address.Company = '';
    }
    // set currency for step 2
    this.productsService.setCurrency(this.form.value.currency);

    // set data for step 2
    this.productsService.setPurchaseData({
      Address: address,
      CompanyInfo: companyInfo,
      Contact: companyInfo,
    });
    // navigate to next step
    this.router.navigate(['/thirdStep']);
  }
}
