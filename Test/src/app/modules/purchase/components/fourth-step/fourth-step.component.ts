import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { NotificationsService } from 'src/app/shared/services/notification.service';
import { PurchaseService } from 'src/app/shared/services/purchase.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-fourth-step',
  templateUrl: './fourth-step.component.html',
  styleUrls: ['./fourth-step.component.scss'],
})
export class FourthStepComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  data: any;
  selectedPlan: any;
  promoCode: string;
  applied: boolean = false;
  invalid: boolean = false;
  isLoaded: boolean = false;
  isExtensionProcess: boolean = false;
  subscriptionData: any;
  expirationDate: any;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserBaseService,
    private productsService: ProductsService,
    private purchaseService: PurchaseService,
    private dialog: NgbModal,
    private notificationsService: NotificationsService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.isExtensionProcess =
      this.router.url.indexOf('/confirmsubscriptionextension') > -1;

    if (this.isExtensionProcess) {
      this.initDataForExtensionProcess();

      return;
    }

    this.initPurchaseDataForPuchaseProcess();
  }

  initPurchaseDataForPuchaseProcess(): void {
    // get order data
    this.data = this.productsService.getPurchaseData();

    // get promo code
    this.promoCode = this.productsService.getPromoCode();

    // get selected plan
    this.selectedPlan = this.productsService.getProduct();

    if (!this.selectedPlan) {
      this.router.navigate(['/purchase']);
      return;
    }
    this.isLoaded = true;
  }

  initDataForExtensionProcess(): void {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.promoCode = params['promoCode'];

      this.getPurchaseDataForPromoCode(this.promoCode);
    });
  }

  getPurchaseDataForPromoCode(promoCode: string): void {
    this.purchaseService
      .getPurchaseDataForPromoCode(promoCode)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res) => {
        this.selectedPlan = res.PricingPlan;
        this.data = {
          Currency: res.Currency,
          PaymentMethod: 'previously-defined',
          PaymentInformation: { Type: 'fake' },
        };

        this.productsService.setProduct(this.selectedPlan);
        this.productsService.setCurrency(res.Currency);
        this.productsService.setPromoCode(this.promoCode);
        this.productsService.setPurchaseData(this.data);

        //res.NextRenewalTime
        this.expirationDate = new Date(res.RenewalDateAfterSuccessfullPurchase);

        this.isLoaded = true;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  checkPromo() {
    // first remove white space
    const tempPromoCode = this.promoCode.replace(/^\s+|\s+$/g, '');

    const regex = /^[a-zA-Z0-9-]*$/;

    if (regex.test(tempPromoCode)) {
      this.applied = false;
      this.invalid = false;

      this.productsService
        .get(tempPromoCode)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: any) => {
          this.selectedPlan.Strategies[this.data.Currency][0].Discount = null;
          let found = false;

          // process plans
          for (const i of Object.keys(resp)) {
            if (
              resp[i].Strategies[this.data.Currency][0].Discount &&
              this.selectedPlan.Id === resp[i].Id
            ) {
              this.selectedPlan.Strategies[this.data.Currency][0].Discount =
                resp[i].Strategies[this.data.Currency][0].Discount;
              this.selectedPlan.Strategies[
                this.data.Currency
              ][0].PriceWithDiscount =
                resp[i].Strategies[this.data.Currency][0].PriceWithDiscount;

              this.applied = true;
              this.promoCode = tempPromoCode;
              found = true;

              setTimeout(() => {
                this.applied = false;
              }, 15000);

              break;
            }
          }

          if (!found) {
            this.invalid = true;

            setTimeout(() => {
              this.invalid = false;
            }, 15000);
          }
        });
    }
  }

  placeOrder() {
    // send payment data to back-end
    this.purchaseService
      .purchase(this.data)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        if (resp) {
          if (this.data.PaymentMethod === 'paypal') {
            const url = resp.replace(/"/g, '');
            localStorage.removeItem('promocode');
            window.location.href = url;
          } else if (
            this.data.PaymentMethod === 'credit' ||
            this.data.PaymentMethod === 'other'
          ) {
            // update user subscription info
            this.userService.getUser().subscribe(() => {
              switch (resp.PaymentStatus) {
                case 0:
                  // call analytics tracking
                  this.purchaseService.tracking(resp);

                  localStorage.removeItem('promocode');

                  let redirectUrl = '/purchase/complete';
                  if (
                    resp.Subscription.IsMultiAccount &&
                    !resp.Subscription.HasPaymentsHistory
                  ) {
                    redirectUrl = '/purchase/enterpriseactivated';
                  } else if (resp.Subscription.IsMultiAccount) {
                    redirectUrl = '/purchase/teamspurchased';
                  }

                  setTimeout(() => {
                    this.router.navigate([redirectUrl]);
                  });

                  break;

                case 2:
                  // update web notifications
                  this.notificationsService.getWebMessages();

                  // add OrderId on wrong credit card info received
                  if (resp.InvoiceNumber) {
                    this.data.OrderId = resp.InvoiceNumber;
                  }

                  const errorCode = 'PAYPAL_ERRORCODE_' + resp.ErrorCode;
                  let error = this.translate.instant(errorCode);

                  if (error === errorCode) {
                    error = resp.ErrorMessage;
                  }

                  const modal = this.dialog.open(ModalComponent, {
                    backdrop: 'static',
                    keyboard: false,
                  });
                  let modalData: Modal = {
                    title: 'DATA_USAPAYMENT_FAILED_TITLEGE_TERMS',
                    text: error,
                  };

                  modal.componentInstance.modalData = modalData;
                  break;
              }
            });
          }
        } else {
          const modal = this.dialog.open(ModalComponent, {
            backdrop: 'static',
            keyboard: false,
          });
          let modalData: Modal = {
            title: 'PAYMENT_FAILURE_TITLE',
            text: 'GENERAL_FAILURE',
          };

          modal.componentInstance.modalData = modalData;
        }
      });
  }

  checkPendingPayment() {
    // check for pending payment
    this.purchaseService
      .checkForPendingPayment()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        // on true we got pending payment, show confirmation modal
        if (resp && resp.Pending === true) {
          const modal = this.dialog.open(ModalComponent, {
            backdrop: 'static',
            keyboard: false,
          });

          let modalData: Modal = {
            title: 'CONFIRM_ORDER_TITLE',
            text: 'CONFIRM_ORDER_TEXT',
            primaryButton: 'yesButton',
            secondaryButton: 'noButton',
          };

          modal.componentInstance.modalData = modalData;

          modal.result.then((data) => {
            if (data === 'primary') {
              this.placeOrder();
            }
          });
        } else {
          this.placeOrder();
        }
      });
  }

  checkBeforePurchase() {
    if (
      this.promoCode &&
      !this.selectedPlan.Strategies[this.data.Currency][0].Discount
    ) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });

      let modalData: Modal = {
        title: 'INCORRECT_PROMO_TITLE',
        text: 'INCORRECT_PROMO_TEXT',
        primaryButton: 'yesButton',
        secondaryButton: 'noButton',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then((data) => {
        if (data === 'primary') {
          this.checkPendingPayment();
        }
      });
    } else {
      // set promo code
      this.data.PromoCode = this.promoCode;
      // move to next step
      this.checkPendingPayment();
    }
  }

  submit(): void {
    if (!this.isExtensionProcess) {
      this.checkBeforePurchase();

      return;
    }

    this.purchaseNow();
  }

  purchaseNow() {
    this.purchaseService
      .purchaseNow(this.promoCode, this.selectedPlan.Id)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: number) => {
        switch (res) {
          case 0:
            this.userService.subscriptionObservable
              .pipe(takeUntil(this.ngUnsubscribe))
              .subscribe((_) => {});

            this.userService.userObservable
              .pipe(takeUntil(this.ngUnsubscribe))
              .subscribe((_) => {});
            const modal = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            let modalData: Modal = {
              title: 'SUCCESS',
              text: 'PAYMENT_SUCCESSFUL',
            };

            modal.componentInstance.modalData = modalData;

            this.router.navigate(['/synchronization/status']);

            break;

          case 2:
            const modalTerm = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            modalData = {
              title: 'PAYMENT_FAILED_TITLE',
              text: 'PAYMENT_FAILED_UPDATE_PROMO_CODE_TEXT',
            };

            modalTerm.componentInstance.modalData = modalData;

            modalTerm.result.then(() => {
              this.router.navigate([
                '/account/billinginformation/changepaymentmethod',
              ]);
            });

            break;
        }
      });
  }
}
