import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { DeviceService } from 'src/app/shared/services/device.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { WindowService } from 'src/app/shared/services/window.service';
import { PricingPlan } from '../../models/pricing-plan-model';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.scss'],
})
export class FirstStepComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  user: any;
  promoCode: string;
  subscriptionEndDateValid = false;
  subscription: any;
  pricingPlans: PricingPlan[] = [];
  singlePlans: PricingPlan[] = [];
  teamsPlans: PricingPlan[] = [];
  enterprisePlans: PricingPlan[] = [];
  currency: string = 'USD';
  currencies: string[] = [];
  isLoaded: boolean = false;
  isSubscriptionNew: boolean = false;
  selectedProductName: string = 'PREMIUM_YEARLY';
  listBenefits = [
    {
      iconName: 'purchase-mobile-icon-1',
      title: 'PURCHASE_MOBILE_PLAN_BENEFIT_TITLE_1',
      text: 'PURCHASE_MOBILE_PLAN_BENEFIT_TEXT_1',
    },
    {
      iconName: 'purchase-mobile-icon-2',
      title: 'PURCHASE_MOBILE_PLAN_BENEFIT_TITLE_2',
      text: 'PURCHASE_MOBILE_PLAN_BENEFIT_TEXT_2',
    },
    {
      iconName: 'purchase-mobile-icon-3',
      title: 'PURCHASE_MOBILE_PLAN_BENEFIT_TITLE_3',
      text: 'PURCHASE_MOBILE_PLAN_BENEFIT_TEXT_3',
    },
    {
      iconName: 'purchase-mobile-icon-4',
      title: 'PURCHASE_MOBILE_PLAN_BENEFIT_TITLE_4',
      text: 'PURCHASE_MOBILE_PLAN_BENEFIT_TEXT_4',
    },
    {
      iconName: 'purchase-mobile-icon-5',
      title: 'PURCHASE_MOBILE_PLAN_BENEFIT_TITLE_5',
      text: 'PURCHASE_MOBILE_PLAN_BENEFIT_TEXT_5',
    },
    {
      iconName: 'purchase-mobile-icon-6',
      title: 'PURCHASE_MOBILE_PLAN_BENEFIT_TITLE_6',
      text: 'PURCHASE_MOBILE_PLAN_BENEFIT_TEXT_6',
    },
  ];
  planTypes = {
    single: 0,
    teams: 1,
    enterprise: 2,
  };
  currentSlide = 0;
  slideConfig = {
    dots: true,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    prevArrow:
      '<div class="slick-prev d-flex justify-content-center align-items-center"><svg><use xlink:href="/Images/svg-sprite.svg#arrow-r-icon"></use></svg></div>',
    nextArrow:
      '<div class="slick-next d-flex justify-content-center align-items-center"><svg><use xlink:href="/Images/svg-sprite.svg#arrow-r-icon"></use></svg></div>',
  };
  isAppOnIos: boolean;
  chosenPlansType: number;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private productsService: ProductsService,
    private userService: UserBaseService,
    private dialog: NgbModal,
    private deviceService: DeviceService,
    private windowService: WindowService
  ) {}

  ngOnInit() {
    this.isAppOnIos = this.deviceService.mobileOS() === 'ios';
    this.chosenPlansType =
      this.router.url.indexOf('?teams=true') > -1
        ? this.planTypes['teams']
        : this.router.url.indexOf('?enterprise=true') > -1
        ? this.planTypes['enterprise']
        : this.planTypes['single'];

    // subscribe for user info
    this.userService.currentUser.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      (resp) => {
        this.user = resp;
        this.isSubscriptionNew =
          this.user &&
          (!this.user.Subscription ||
            this.user.Subscription.SubscriptionStatus === 'New');
        if (this.user && this.user.Subscription && !this.isSubscriptionNew) {
          this.currency = this.user.Subscription.Currency;

          this.userService.currentSubscription
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
              (resp) => {
                this.subscription = resp;
                this.isLoaded = true;
              },
              (err) => {
                this.isLoaded = true;
              }
            );
        } else if (this.user) {
          if (this.user.Language === 'de' || this.user.Language === 'fr') {
            this.currency = 'EUR';
          }
          this.isLoaded = true;
        }
      },
      (err) => {
        this.isLoaded = true;
      }
    );

    // subscribe to router event
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.promoCode = params['promoCode'];

      // set promocode for last step
      this.productsService.setPromoCode(this.promoCode);
    });

    // load products
    this.productsService
      .get(this.promoCode)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: PricingPlan[]) => {
        let freePlan: PricingPlan = {
          Id: 0,
          Name: 'FREE',
          IsMultiAccount: false,
          Strategies: { USD: [] },
        };

        let enterpriseCustomPlan: PricingPlan = {
          Id: 6,
          Name: 'ENTERPRISE_CUSTOM',
          IsMultiAccount: true,
          Strategies: { USD: [] },
        };

        if (resp) {
          for (let i = 0; i < resp.length; i++) {
            if (resp[i].Name == 'PREMIUM_YEARLY') {
              this.singlePlans[0] = resp[i];
              this.singlePlans[0].ListBenefits = this.listBenefits;
            } else if (resp[i].Name == 'PREMIUM_MONTHLY') {
              this.singlePlans[2] = resp[i];
            } else if (resp[i].Name == 'FAMILY_PREMIUM_MONTHLY') {
              this.teamsPlans[0] = resp[i];
            } else if (resp[i].Name == 'FAMILY_PREMIUM_YEARLY') {
              this.teamsPlans[1] = resp[i];
            } else if (resp[i].Name == 'ENTERPRISE_PREMIUM_MONTHLY') {
              this.enterprisePlans[0] = resp[i];
            }

            for (let key in resp[i].Strategies) {
              if (this.currencies.indexOf(key) < 0) {
                this.currencies.push(key);
              }
            }
          }
          this.currencies.sort();
        }

        for (let key in this.currencies) {
          freePlan.Strategies[this.currencies[key]] = [
            {
              Priority: 0,
              Count: null,
              RecurrenceIntervalId: 0,
              FullPrice: 0,
              PriceWithDiscount: 0,
              PricePerMonth: 0,
              PriceWithDiscountPerYear: 0,
              FullPricePerYear: 0,
              Discount: null,
              ProductId: null,
              Currency: this.currencies[key],
            },
          ];
        }

        for (let key in this.currencies) {
          enterpriseCustomPlan.Strategies[this.currencies[key]] = [
            {
              Priority: 0,
              Count: null,
              RecurrenceIntervalId: 0,
              FullPrice: 0,
              PriceWithDiscount: 0,
              PricePerMonth: 0,
              PriceWithDiscountPerYear: 0,
              FullPricePerYear: 0,
              Discount: null,
              ProductId: null,
              Currency: this.currencies[key],
            },
          ];
        }

        this.singlePlans[1] = freePlan;
        this.enterprisePlans[1] = enterpriseCustomPlan;
        this.changeType(this.chosenPlansType);
      });

    this.checkAppOnIos();
  }
  afterChange(event: any) {
    this.currentSlide = event.currentSlide;
  }

  checkAppOnIos() {
    if (!this.isAppOnIos) {
      return;
    }
    this.windowService.hideCompletelyLiveChat();

    const tempCurrency = localStorage.getItem('currency');

    if (
      tempCurrency &&
      Object.keys(this.singlePlans[0].Strategies).includes(tempCurrency)
    ) {
      this.currency = tempCurrency;
    }
  }

  queryString(data: any) {
    var queryString = Object.keys(data)
      .map((key) => key + '=' + data[key])
      .join('&');
    return queryString;
  }

  selectProduct(name: string) {
    this.selectedProductName = name;
  }

  openNextPage() {
    if (!this.selectedProductName) {
      return;
    }
    const data = {
      ftUserId: this.user.FTUserId,
      subscriptionId: this.user.Subscription.SubscriptionId,
      productName: this.selectedProductName,
    };

    window.location.href = 'mobile-app/purchase?' + this.queryString(data);
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.windowService.showLiveChat();
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  changeType(chosenPlansType: number) {
    this.chosenPlansType = chosenPlansType;
    switch (chosenPlansType) {
      case 0:
        this.pricingPlans = this.singlePlans;
        break;
      case 1:
        this.pricingPlans = this.teamsPlans;
        break;
      case 2:
        this.pricingPlans = this.enterprisePlans;
        break;
    }
  }

  showInfoMessage(title: string, text: string) {
    const modalResponse = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: title,
      text: text,
    };

    modalResponse.componentInstance.modalData = modalData;
  }

  selectPlan(plan: PricingPlan) {
    // reset old purchase data
    this.productsService.setPurchaseData(null);

    // set plan for next step
    this.productsService.setProduct(plan);

    //set currency
    this.productsService.setCurrency(this.currency);

    // navigate to next step
    if (plan.IsMultiAccount) {
      this.router.navigate(['/purchase/companyinformation']);
    } else {
      this.router.navigate(['/purchase/billinginformation']);
    }
  }

  externalLink(url: string) {
    window.open(url, '_blank');
  }

  changePlan(id: string) {
    this.userService
      .changeSubscriptionPlan({ PlanId: id, Currency: this.currency })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.showInfoMessage('SUCCESS', 'PRICING_PLAN_CHANGED');
        this.userService
          .getUser()
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe((resp) => {
            // navigate to subscription page
            this.router.navigate([
              '/account/billinginformation/currentsubscription',
            ]);
          });
      });
  }
}
