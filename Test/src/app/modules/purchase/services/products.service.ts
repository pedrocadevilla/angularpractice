import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, publishReplay, refCount } from 'rxjs/operators';

@Injectable()
export class ProductsService {
  public products: any;
  public selectedPlan: any;
  public purchaseData: any;
  public promoCode: any;
  public currency: any;

  constructor(private http: HttpClient) {}

  get(promoCode: string): Observable<any> {
    const target = localStorage.getItem('source');

    if (
      this.products != null &&
      this.products.length > 0 &&
      promoCode == null
    ) {
      return of(this.products);
    } else {
      return this.http
        .get('/api/GetProducts?promoCode=' + promoCode + '&target=' + target)
        .pipe(
          tap((products: any) => (this.products = products)),
          publishReplay(1),
          refCount()
        );
    }
  }

  validatePromoCode(promoCode: string) {
    return this.http.get('/api/validatepromocode?promoCode=' + promoCode);
  }

  setProduct(plan: any) {
    this.selectedPlan = plan;
  }

  getProduct() {
    return this.selectedPlan;
  }

  setPurchaseData(data: any) {
    this.purchaseData = data;
  }

  getPurchaseData() {
    return this.purchaseData;
  }

  setPromoCode(promo: string) {
    this.promoCode = promo;
  }

  getPromoCode() {
    return this.promoCode;
  }

  setCurrency(currency: string) {
    this.currency = currency;
  }

  getCurrency() {
    return this.currency;
  }
}
