import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchaseRoutingModule } from './purchase-routing.module';
import { FirstStepComponent } from './components/first-step/first-step.component';
import { InnerSharedModule } from 'src/app/shared/modules/inner-shared.module';
import { SecondStepComponent } from './components/second-step/second-step.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThirdStepComponent } from './components/third-step/third-step.component';
import { FourthStepComponent } from './components/fourth-step/fourth-step.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
@NgModule({
  declarations: [
    FirstStepComponent,
    SecondStepComponent,
    ThirdStepComponent,
    FourthStepComponent,
  ],
  imports: [
    CommonModule,
    PurchaseRoutingModule,
    InnerSharedModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    SlickCarouselModule,
  ],
})
export class PurchaseModule {}
