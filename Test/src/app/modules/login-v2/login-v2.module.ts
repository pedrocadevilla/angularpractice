import { NgModule } from '@angular/core';

import { LoginV2RoutingModule } from './login-v2-routing.module';
import { LoginV2Component } from './components/login-v2/login-v2.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { ExternalLoginComponent } from '../../shared/components/external-login/external-login.component';
import { LoginService } from './services/login.service';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ForgotFormComponent } from './components/forgot-form/forgot-form.component';
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { NotificationsService } from 'src/app/shared/services/notification.service';
import { DeviceService } from 'src/app/shared/services/device.service';
import { SyncService } from 'src/app/shared/services/sync.service';
import { PendingInterceptorService } from 'src/app/shared/interceptors/pending.interceptor.service';
import { ToastService } from 'src/app/shared/services/toast.service';
import { GuidService } from 'src/app/shared/services/guid.service';
import { DatePipe } from '@angular/common';
import { SignalrWindow } from 'src/app/shared/services/channel-base.service';
import { ChannelService } from 'src/app/shared/services/channel.service';
import { ExternalLoginServices } from 'src/app/shared/services/external-login.service';
import { RestoreService } from 'src/app/shared/services/restore.service';
import { SignService } from './services/sign.service';


@NgModule({
  declarations: [
    LoginV2Component,
    LoginFormComponent,
    ExternalLoginComponent,
    ForgotFormComponent,
    SignupFormComponent,
  ],
  imports: [LoginV2RoutingModule, ReactiveFormsModule, SharedModule],
  providers: [LoginService, UserBaseService, NotificationsService, DeviceService, SyncService, PendingInterceptorService, ToastService, GuidService, ChannelService, SignalrWindow, RestoreService, DatePipe, ExternalLoginServices, SignService],
})
export class LoginV2Module {}
