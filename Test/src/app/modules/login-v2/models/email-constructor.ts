import { FormControl, FormGroup, Validators } from '@angular/forms';

export function initializeForm(loginHint: string) {
  return new FormGroup({
    username: new FormControl(loginHint, [
      Validators.required,
      Validators.email,
    ]),
  });
}
