import { FormControl, FormGroup, Validators } from '@angular/forms';
import { patternValidator } from 'src/app/shared/directives/password.directive';

export function initializeForm(loginHint: string) {
  return new FormGroup({
    username: new FormControl(loginHint, [
      Validators.required,
      Validators.email,
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      patternValidator,
    ]),
    rememberMe: new FormControl(false),
    clientId: new FormControl('self'),
  });
}
