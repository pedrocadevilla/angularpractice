import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { initializeForm } from '../../models/email-constructor';
import { errorMessages } from '../../models/login-form-err-message';
import { ForgotService } from '../../services/forgot.service';

@Component({
  selector: 'app-forgot-form',
  templateUrl: './forgot-form.component.html',
  styleUrls: ['./forgot-form.component.scss'],
})
export class ForgotFormComponent implements OnInit {
  @Input() returnUrl: string;
  @Input() loginHint: string;
  form: FormGroup;
  usernameErrorList: errorMessageObject[] = [];

  constructor(private forgotService: ForgotService) {
    this.usernameErrorList = errorMessages.userNameMessages;
    this.form = initializeForm(this.loginHint);
  }

  ngOnInit(): void {}

  reset() {
    if (!this.form.valid) {
      //dialog message
      return;
    }

    this.forgotService
      .recoverPassword(this.form.value.username)
      .subscribe(() => {
        // show success message
      });
  }
}
