import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { Modal } from 'src/app/shared/models/modal-model';
import { initializeForm } from '../../models/email-constructor';
import { errorMessages } from '../../models/login-form-err-message';
import { SignService } from '../../services/sign.service';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss'],
})
export class SignupFormComponent implements OnInit {
  @Input() returnUrl: string;
  @Input() loginHint: string;
  @Input() isExtendedProcess: boolean;
  form: FormGroup;
  usernameErrorList: errorMessageObject[] = [];

  constructor(private signService: SignService, private dialog: NgbModal) {
    this.usernameErrorList = errorMessages.userNameMessages;
    this.form = initializeForm(this.loginHint);
  }

  ngOnInit(): void {}

  signup() {
    if (!this.form.valid) {
      return;
    }
    // license confirmation modal
    const modalTerm = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'DATA_USAGE_TERMS',
      text: 'POLICY_TEXT',
      primaryButton: 'AGREE',
      secondaryButton: 'DO_NOT_AGREE',
      checkbox: [
        'POLICY_CHECK_BOX1*sendupdates',
        'POLICY_CHECK_BOX2*sendoffers',
      ],
      options: {
        sendupdates: false,
        sendoffers: false,
      },
    };

    modalTerm.componentInstance.modalData = modalData;

    modalTerm.result.then((data) => {
      // on close
      if (data !== 'secondary') {
        localStorage.setItem('privacySettings', JSON.stringify(data));
        this.signService
          .signup({ Email: this.form.value.Email, ReturnUrl: this.returnUrl })
          .subscribe(() => {
            var text = 'ACCOUNT_CREATED';

            if (this.isExtendedProcess) {
              text += 'DO_NOT_CLOSE_TAB';
            }

            const modal = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            let modalData: Modal = {
              title: 'SUCCESS',
              text: text,
            };

            modal.componentInstance.modalData = modalData;
          });
      }
    });
  }
}
