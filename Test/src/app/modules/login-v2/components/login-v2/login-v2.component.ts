import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment-timezone';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';

@Component({
  selector: 'app-login-v2',
  templateUrl: './login-v2.component.html',
  styleUrls: ['./login-v2.component.scss'],
})
export class LoginV2Component implements OnInit {
  currentRoute: string =
    window.location.href.split('/')[window.location.href.split('/').length - 1];
  returnUrl: string = '';
  loginHint: string = '';

  token: string;
  grantType: string;
  isExtendedProcess: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserBaseService,
    private cookieService: CookieService,
    private dialog: NgbModal
  ) {
    // subscribe to router event
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.returnUrl = params['returnUrl'];

      if (
        params['forceLogout'] &&
        params['forceLogout'] !== undefined &&
        params['forceLogout'] == 'true'
      ) {
        this.userService.logout(false, '', true);
      }

      if (params['loginHint'] && params['loginHint'] !== undefined) {
        this.loginHint = params['loginHint'];
      }

      if (
        params['isExtendedProcess'] &&
        params['isExtendedProcess'] !== undefined &&
        params['isExtendedProcess'] == 'true'
      ) {
        this.isExtendedProcess = true;
      }
      // maybe we need to show promo code message
      if (
        params['returnUrl'] &&
        params['returnUrl'].indexOf('promocode') > -1 &&
        !params['access_token']
      ) {
        const modal = this.dialog.open(ModalComponent, {
          backdrop: 'static',
          keyboard: false,
        });
        let modalData: Modal = {
          title: 'PROMOCODE_LOGIN_TITLE',
          text: 'PROMOCODE_LOGIN_CONTENT',
          primaryButton: 'LOGIN',
        };

        modal.componentInstance.modalData = modalData;
      }

      if (params['grant_type'] && params['token']) {
        this.token = params['token'];
        this.grantType = params['grant_type'];
      }
    });
  }

  ngOnInit() {
    // register time zone offset
    if (!this.cookieService.get('timezone_offset')) {
      this.cookieService.set(
        'timezone_offset',
        new Date().getTimezoneOffset().toString()
      );
    }

    // register time zone name
    if (!this.cookieService.get('timezone_name')) {
      this.cookieService.set('timezone_name', moment.tz.guess());
    }

    if (this.grantType && this.token) {
      this.userService.loginByToken(this.grantType, this.token);
    }
  }
}
