import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { initializeForm } from '../../models/login-form-contructor';
import { errorMessages } from '../../models/login-form-err-message';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  @Input() isExtendedProcess: boolean;
  form: FormGroup;
  loginHint: string = '';
  usernameErrorList: errorMessageObject[] = [];
  passwordErrorList: errorMessageObject[] = [];

  constructor(private loginService: LoginService) {
    this.form = initializeForm(this.loginHint);
    this.usernameErrorList = errorMessages.userNameMessages;
    this.passwordErrorList = errorMessages.passwordMessages;
  }

  ngOnInit(): void {}

  login() {
    if (!this.form.valid) {
      //dialog message
      return;
    }
    this.loginService.login(this.form);
  }
}
