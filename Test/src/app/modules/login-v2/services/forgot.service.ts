import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable()
export class ForgotService {
  constructor(protected http: HttpClient) {}

  recoverPassword(email: string) {
    return this.http.post('/api/ForgotPassword', {
      Email: email,
    });
  }
}
