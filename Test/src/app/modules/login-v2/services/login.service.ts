import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
@Injectable()
export class LoginService {
  constructor(private userBase: UserBaseService) {}

  login(model: FormGroup, returnUrl?: string) {
    // set body
    var body =
      'clientId=' +
      model.value.clientId +
      '&username=' +
      model.value.username +
      '&password=' +
      model.value.password +
      '&rememberMe=' +
      model.value.rememberMe +
      '&grant_type=password';

    if (model.value.clientSecret) {
      body = body + '&clientSecret=' + model.value.clientSecret;
    }

    this.userBase.loginByBody(body, returnUrl);
  }
}
