import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ChangePasswordModal } from 'src/app/shared/models/change-password-model';
@Injectable()
export class ChangePasswordService {
  constructor(protected http: HttpClient) {}

  reset(model: ChangePasswordModal) {
    return this.http.post('/api/ResetPassword', model);
  }
}
