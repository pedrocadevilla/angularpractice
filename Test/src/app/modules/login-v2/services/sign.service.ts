import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable()
export class SignService {
  constructor(protected http: HttpClient) {}

  signup(model: any) {
    return this.http.post('/api/signup', model);
  }
}
