import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginV2Component } from './components/login-v2/login-v2.component';
import { SignupFormComponent } from './components/signup-form/signup-form.component';

const routes: Routes = [
  { path: '', component: LoginV2Component },
  { path: 'signup', component: SignupFormComponent },
  { path: 'resetPassword', component: LoginV2Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginV2RoutingModule {}
