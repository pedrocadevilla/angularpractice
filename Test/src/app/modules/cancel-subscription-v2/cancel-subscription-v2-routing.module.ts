import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { IndexComponent } from './components/index/index.component';
import { SurveyComponent } from './components/survey/survey.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'confirmation', component: ConfirmationComponent },
  { path: 'survey', component: SurveyComponent },
  {
    path: 'billing',
    loadChildren: () =>
      import('../billing-information-v2/billing-information-v2.module').then(
        (m) => m.BillingInformationV2Module
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CancelSubscriptionV2RoutingModule {}
