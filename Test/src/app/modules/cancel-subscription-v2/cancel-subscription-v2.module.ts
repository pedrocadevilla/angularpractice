import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CancelSubscriptionV2RoutingModule } from './cancel-subscription-v2-routing.module';
import { IndexComponent } from './components/index/index.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { InnerSharedModule } from 'src/app/shared/modules/inner-shared.module';
import { SurveyComponent } from './components/survey/survey.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [IndexComponent, SurveyComponent, ConfirmationComponent],
  imports: [
    CommonModule,
    CancelSubscriptionV2RoutingModule,
    InnerSharedModule,
    ReactiveFormsModule,
  ],
})
export class CancelSubscriptionV2Module {}
