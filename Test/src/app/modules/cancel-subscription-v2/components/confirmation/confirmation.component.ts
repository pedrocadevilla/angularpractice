import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { Subject, takeUntil } from 'rxjs';
import { UserBaseService } from 'src/app/shared/services/user-base.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
})
export class ConfirmationComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  cancelData: any;
  settings: any;
  loaded = false;

  constructor(
    private router: Router,
    private userService: UserBaseService,
    private gtmService: GoogleTagManagerService
  ) {}

  ngOnInit() {
    this.cancelData = this.userService.getCancelData();

    // subscribe for cancel settings
    this.userService
      .getSubscriptionCancelSettings()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.settings = resp;
        this.loaded = true;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  finish() {
    this.userService
      .cancelSubscription(this.cancelData)
      .subscribe((resp: any) => {
        // send event to GTM
        const gtmTag = {
          event: 'SubscriptionCancel',
        };
        this.gtmService.pushTag(gtmTag);

        // update user subscription info
        this.userService.getUser().subscribe(() => {
          // reset cancel reason
          this.userService.setCancelData(null);

          // redirect to last step
          this.router.navigate([''], {
            replaceUrl: true,
          });
        });
      });
  }
}
