import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { initializeForm } from '../../models/survey-constructor';
import { SurveyModel } from '../../../../shared/models/survey-model';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss'],
})
export class SurveyComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  form: FormGroup;
  currentUser: any;

  constructor(private router: Router, private userService: UserBaseService) {}

  ngOnInit() {
    // init form
    this.form = initializeForm();

    // subscribe for user info
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.currentUser = resp;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  cancel() {
    const model: SurveyModel = Object.assign({}, this.form.value);

    // add subscription id
    model.SubscriptionId = this.currentUser.Subscription
      ? this.currentUser.Subscription.SubscriptionId
      : null;

    // add cancel reason
    model.CancelReason =
      this.form.value.CancelReason === 'other'
        ? this.form.value.CancelReasonCustom
        : this.form.value.CancelReason;

    // set cancel reason for next step
    this.userService.setCancelData(model);

    // redirect to next step
    this.router.navigate(['/confirmation']);
  }
}
