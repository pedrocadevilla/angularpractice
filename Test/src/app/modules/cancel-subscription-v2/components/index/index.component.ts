import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { UserBaseService } from 'src/app/shared/services/user-base.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  date: string;
  currentRoute: string =
    window.location.href.split('/')[window.location.href.split('/').length - 1];

  constructor(
    private userService: UserBaseService,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    // subscribe for user info
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        if (resp && resp.Subscription) {
          this.date = this.datePipe.transform(
            resp.Subscription.EndDate,
            'MMM d, y'
          )!;
        }
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
