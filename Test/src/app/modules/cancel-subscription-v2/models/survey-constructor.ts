import { FormControl, FormGroup, Validators } from '@angular/forms';

export function initializeForm() {
  return new FormGroup({
    TryingToSaveMoney: new FormControl(false),
    DontNeedPremiumFeatures: new FormControl(false),
    PremiumPackageMissingFeatures: new FormControl(false),
    TechnicalIssues: new FormControl(false),
    Other: new FormControl(false),
    OtherReasonText: new FormControl(['']),
    OneWaySync: new FormControl(false),
    AdvancedMapping: new FormControl(false),
    CompanyAccount: new FormControl(false),
    ImmediateSync: new FormControl(false),
    AdvansedSettings: new FormControl(false),
    AutoSyncFrequency: new FormControl(false),
    NoteSync: new FormControl(false),
    OtherFeatureMissing: new FormControl(false),
    CantAddIcloud: new FormControl(false),
    CantAddExchange: new FormControl(false),
    RequestsNotSynchronizing: new FormControl(false),
    CalendarNotSynchronizing: new FormControl(false),
    ContactsNotSynchronizing: new FormControl(false),
    SyncTakesForever: new FormControl(false),
    SyncShowsCancelling: new FormControl(false),
    OtherTechnicalIssue: new FormControl(false),
    OtherFeatureText: new FormControl(['']),
    OtherIssueText: new FormControl(['']),
  });
}
