import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SynchronizationRoutingModule } from './synchronization-routing.module';
import { AddSyncAccountComponent } from './components/add-aync-account/add-sync-account.component';
import { InnerSharedModule } from 'src/app/shared/modules/inner-shared.module';
import { ExchangeComponent } from './components/exchange/exchange.component';
import { ReactiveFormsModule } from '@angular/forms';
import { IcloudComponent } from './components/icloud/icloud.component';
import { BackupRestoreComponent } from './components/backup-restore/backup-restore.component';
import { DirectionComponent } from './components/direction/direction.component';
import { FiltersComponent } from './components/filters/filters.component';
import { ManagementComponent } from './components/management/management.component';
import { ReportComponent } from './components/report/report.component';
import { StatusComponent } from './components/status/status.component';
import { WhatToSyncComponent } from './components/what-to-sync/what-to-sync.component';
import { SyncAccountInviteComponent } from './components/sync-account-invite/sync-account-invite.component';
import { FinishComponent } from './components/finish/finish.component';
import { BulkAddOneComponent } from './components/bulk-add-one/bulk-add-one.component';
import { BulkAddTwoComponent } from './components/bulk-add-two/bulk-add-two.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SystemPipe } from 'src/app/shared/utils/system-pipe';
import { DataTypePipe } from 'src/app/shared/utils/data-type-pipe';
import { StatusFormComponent } from './components/status-form/status-form.component';
@NgModule({
  declarations: [
    AddSyncAccountComponent,
    ExchangeComponent,
    IcloudComponent,
    BackupRestoreComponent,
    DirectionComponent,
    FiltersComponent,
    ManagementComponent,
    ReportComponent,
    StatusComponent,
    WhatToSyncComponent,
    SyncAccountInviteComponent,
    FinishComponent,
    BulkAddOneComponent,
    BulkAddTwoComponent,
    SystemPipe,
    DataTypePipe,
    StatusFormComponent,
  ],
  imports: [
    CommonModule,
    InnerSharedModule,
    ReactiveFormsModule,
    NgbModule,
    SynchronizationRoutingModule,
    SlickCarouselModule,
  ],
})
export class SynchronizationModule {}
