import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { AccountsService } from 'src/app/shared/services/account.service';
import { HelpService } from 'src/app/shared/services/help.service';
import { SyncService } from 'src/app/shared/services/sync.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { FiltersService } from '../../services/filter.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  progressBar: any;
  filterFolders: any = [];
  replicasList: any = [];
  filterAccounts: any = [];
  originalAccounts: any = [];
  errorAccounts: any[];
  currentReplica: string;
  hasTasks = false;
  currentAutoDeleteStatus = false;
  currentAutoDeleteStatusPartial = false;
  currentAutoSyncStatus = false;
  currentAutoSyncStatusPartial = false;
  isAdmin = false;
  loaded = false;
  slide1Config = {
    infinite: false,
    draggable: false,
    prevArrow:
      '<button class="slick-prev slick-arrow" style="width: 24px; height: 24px; left: -35px" type="button"><svg style="width: 24px; height: 24px"><use xlink:href="/Images/svg-sprite.svg#arrow-l-icon"></use></svg></button>',
    nextArrow:
      '<button class="slick-next slick-arrow" style="width: 24px; height: 24px; right: 20px" type="button"><svg style="width: 24px; height: 24px"><use xlink:href="/Images/svg-sprite.svg#arrow-r-icon"></use></svg></button>',
    slidesToShow: 2,
    slidesToScroll: 1,
    asNavFor: '.slider2',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          draggable: true,
        },
      },
    ],
  };
  slide2Config = {
    infinite: false,
    draggable: false,
    arrows: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    asNavFor: '.slider1',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          draggable: true,
        },
      },
    ],
  };

  constructor(
    private router: Router,
    private filtersService: FiltersService,
    private dialog: NgbModal,
    private translate: TranslateService,
    private syncService: SyncService,
    private userService: UserBaseService,
    private helpService: HelpService,
    private accountsService: AccountsService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    // get filters
    this.filtersService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (resp: any) => {
          let hasGoogle;

          for (const i in resp.SyncAccounts) {
            if (resp.SyncAccounts[i].Status === 1) {
              this.filterAccounts.push(resp.SyncAccounts[i]);
            } else {
              // push accounts with bad statuses
              this.errorAccounts.push(resp.SyncAccounts[i].Username);

              // we have google
              if (resp.SyncAccounts[i].ProviderCode === 'Google') {
                hasGoogle = true;
              }
            }
          }

          // if no active accounts found, user should be redirected to management page
          if (this.errorAccounts.length === resp.SyncAccounts.length) {
            this.router.navigate(['/synchronization/management'], {
              replaceUrl: true,
            });
          } else {
            // update slider settings by account count
            this.slide1Config.slidesToShow =
              this.filterAccounts.length === 1 ? 1 : 2;
            this.slide2Config.slidesToShow =
              this.filterAccounts.length === 1 ? 1 : 2;

            if (this.errorAccounts.length > 0) {
              // show error message on error account
              const modal = this.dialog.open(ModalComponent, {
                backdrop: 'static',
                keyboard: false,
              });

              let modalData: Modal = {
                title: 'ERROR_ACCESSING_ACCOUNT',
                textList: this.errorAccounts,
                hasGoogle: 'ERROR_ACCESSING_ACCOUNT_HELP',
              };

              modal.componentInstance.modalData = modalData;
            }

            // set original data object
            this.originalAccounts = this.filterAccounts;

            if (this.filterAccounts.length > 0) {
              for (const i of Object.keys(this.filterAccounts)) {
                this.filterAccounts[i].AutoDeleteRemovedFolders = null;
                this.filterAccounts[i].AutoSyncNewFolders = null;

                for (const j of Object.keys(this.filterAccounts[i].Replicas)) {
                  let needAdd = true;

                  if (this.filterAccounts[i].Replicas[j].TypeCode === 'Task') {
                    this.hasTasks = true;
                  }

                  for (const r in this.replicasList) {
                    if (
                      this.replicasList[r].TypeCode ===
                      this.filterAccounts[i].Replicas[j].TypeCode
                    ) {
                      needAdd = false;
                      break;
                    }
                  }

                  if (needAdd) {
                    this.replicasList.push({
                      TypeCode: this.filterAccounts[i].Replicas[j].TypeCode,
                      TypeName: this.filterAccounts[i].Replicas[j].TypeName,
                    });

                    this.filterFolders.push({
                      title: this.filterAccounts[i].Replicas[j].TypeCode,
                      list: [],
                    });

                    if (this.replicasList.length === 1) {
                      this.currentReplica =
                        this.filterAccounts[i].Replicas[j].TypeCode;
                    }
                  }
                }
              }

              this.checkAutoDeleteValues();
              this.checkAutoSyncValues();
              this.createFoldersList();
            }

            this.loaded = true;
          }
        },
        (err) => {
          this.router.navigate(['/synchronization/management'], {
            replaceUrl: true,
          });
        }
      );

    // subscribe for progress info
    this.syncService.progress
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res) => {
        this.progressBar = res;
      });

    // is user admin?
    if (
      this.userService.isInRole('Administrator') ||
      this.userService.isInRole('Sales User') ||
      this.userService.isInRole('Support User')
    ) {
      this.isAdmin = true;
    }
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  checkAutoDeleteValues() {
    let onVal = 0,
      offVal = 0;

    for (const a of Object.keys(this.filterAccounts)) {
      for (const r in this.filterAccounts[a].Replicas) {
        if (
          this.filterAccounts[a].Replicas[r].TypeCode == this.currentReplica
        ) {
          if (this.filterAccounts[a].Replicas[r].AutoDeleteRemovedFolders) {
            onVal++;
          } else {
            offVal++;
          }

          break;
        }
      }
    }

    this.currentAutoDeleteStatusPartial = false;

    if (onVal === 0 && offVal > 0) {
      this.currentAutoDeleteStatus = false;
    } else if (onVal > 0 && offVal === 0) {
      this.currentAutoDeleteStatus = true;
    } else {
      this.currentAutoDeleteStatusPartial = true;
    }
  }

  checkAutoSyncValues() {
    let onVal = 0,
      offVal = 0;

    for (const a of Object.keys(this.filterAccounts)) {
      for (const r in this.filterAccounts[a].Replicas) {
        if (
          this.filterAccounts[a].Replicas[r].TypeCode == this.currentReplica
        ) {
          if (this.filterAccounts[a].Replicas[r].AutoSyncNewFolders) {
            onVal++;
          } else {
            offVal++;
          }

          break;
        }
      }
    }

    this.currentAutoSyncStatusPartial = false;

    if (onVal === 0 && offVal > 0) {
      this.currentAutoSyncStatus = false;
    } else if (onVal > 0 && offVal === 0) {
      this.currentAutoSyncStatus = true;
    } else {
      this.currentAutoSyncStatusPartial = true;
    }
  }

  createFoldersList() {
    for (const f of Object.keys(this.filterFolders)) {
      for (const g of Object.keys(this.filterAccounts)) {
        for (const r in this.filterAccounts[g].Replicas) {
          if (
            this.filterFolders[f].title ==
            this.filterAccounts[g].Replicas[r].TypeCode
          ) {
            if (this.filterAccounts[g].Replicas[r].Folders.length > 0) {
              for (const i of Object.keys(
                this.filterAccounts[g].Replicas[r].Folders
              )) {
                let onVal = 0,
                  offVal = 0,
                  selVal = 2; // 2 = partial checkbox

                for (const a of Object.keys(this.filterAccounts)) {
                  for (const fr of Object.keys(
                    this.filterAccounts[a].Replicas
                  )) {
                    for (const ff in this.filterAccounts[a].Replicas[fr]
                      .Folders) {
                      if (
                        this.filterAccounts[a].Replicas[fr].Folders[ff]
                          .GlobalId ===
                        this.filterAccounts[g].Replicas[r].Folders[i].GlobalId
                      ) {
                        if (
                          this.filterAccounts[a].Replicas[fr].Folders[ff]
                            .IsSelected
                        ) {
                          onVal++;
                        } else {
                          offVal++;
                        }

                        break;
                      }
                    }
                  }
                }

                if (onVal === 0 && offVal > 0) {
                  selVal = 0; // false
                } else if (onVal > 0 && offVal === 0) {
                  selVal = 1; // true
                }

                if (
                  this.filterFolders[f].list.length === 0 ||
                  (this.filterFolders[f].list[0].ProviderCode ===
                    this.filterAccounts[g].ProviderCode &&
                    this.filterFolders[f].list[0].Username ===
                      this.filterAccounts[g].Username)
                ) {
                  this.filterFolders[f].list.push({
                    id: this.filterAccounts[g].Replicas[r].Folders[i].GlobalId,
                    name: this.filterAccounts[g].Replicas[r].Folders[i].Name,
                    ProviderCode: this.filterAccounts[g].ProviderCode,
                    Username: this.filterAccounts[g].Username,
                    isSelected: selVal,
                  });
                }
              }
            }

            break;
          }
        }
      }
    }
  }

  setCurrentReplica(replica: any) {
    this.currentReplica = replica;
    this.checkAutoDeleteValues();
    this.checkAutoSyncValues();
  }

  folderIsEnabled(folderId: number, account: string) {
    for (const a in this.filterAccounts) {
      if (this.filterAccounts[a].Username === account) {
        for (const r in this.filterAccounts[a].Replicas) {
          if (
            this.filterAccounts[a].Replicas[r].TypeCode === this.currentReplica
          ) {
            for (const k in this.filterAccounts[a].Replicas[r].Folders) {
              if (
                this.filterAccounts[a].Replicas[r].Folders[k].GlobalId ===
                folderId
              ) {
                return this.filterAccounts[a].Replicas[r].Folders[k].IsSelected;
              }
            }
          }
        }
      }
    }

    return false;
  }

  checkSourceAccounts(account: string, status: boolean) {
    for (const a in this.filterAccounts) {
      if (this.filterAccounts[a].Username !== account) {
        for (const r in this.filterAccounts[a].Replicas) {
          if (
            this.filterAccounts[a].Replicas[r].TypeCode === this.currentReplica
          ) {
            for (const k in this.filterAccounts[a].Replicas[r].Folders) {
              for (const d in this.filterAccounts[a].Replicas[r].Folders[k]
                .SourceAccounts) {
                if (
                  this.filterAccounts[a].Replicas[r].Folders[k].SourceAccounts[
                    d
                  ].Username === account &&
                  status
                ) {
                  this.filterAccounts[a].Replicas[r].Folders[
                    k
                  ].IsSourceSelected = this.folderIsEnabled(
                    this.filterAccounts[a].Replicas[r].Folders[k].GlobalId,
                    account
                  );
                } else {
                  this.filterAccounts[a].Replicas[r].Folders[
                    k
                  ].IsSourceSelected = false;
                }
              }
            }
          }
        }
      }
    }
  }

  toggleFiltersOn(accountId: string) {
    for (const a in this.filterAccounts) {
      if (this.filterAccounts[a].Id === accountId) {
        for (const r in this.filterAccounts[a].Replicas) {
          if (
            this.filterAccounts[a].Replicas[r].TypeCode === this.currentReplica
          ) {
            // if no folders - show error box
            if (this.filterAccounts[a].Replicas[r].Folders.length > 0) {
              this.filterAccounts[a].Replicas[r].IsSelected =
                !this.filterAccounts[a].Replicas[r].IsSelected;
            } else {
              const vars = {
                var0: this.translate.instant(
                  this.filterAccounts[a].Replicas[r].TypeName
                ),
                var1: this.filterAccounts[a].Username,
              };

              const modal = this.dialog.open(ModalComponent, {
                backdrop: 'static',
                keyboard: false,
              });

              let modalData: Modal = {
                title: 'ERROR',
                text: this.translate.instant('FILTER_FOLDERS_ERROR', vars),
                textTranslated: true,
              };

              modal.componentInstance.modalData = modalData;
            }

            this.checkSourceAccounts(
              this.filterAccounts[a].Username,
              this.filterAccounts[a].Replicas[r].IsSelected
            );
          }
        }

        break;
      }
    }
  }

  toggleAllFiltersOnTab() {
    let firstFound = false,
      firstValue = false;

    for (const a of Object.keys(this.filterAccounts)) {
      for (const r in this.filterAccounts[a].Replicas) {
        if (
          this.filterAccounts[a].Replicas[r].TypeCode == this.currentReplica
        ) {
          if (!firstFound) {
            firstValue = this.filterAccounts[a].Replicas[r].IsSelected;
            firstFound = true;
          }

          if (firstValue) {
            this.filterAccounts[a].Replicas[r].IsSelected = false;
          } else {
            this.filterAccounts[a].Replicas[r].IsSelected = true;
          }
        }
      }
    }
  }

  accountIsEnabled(accounts: any) {
    if (accounts.length > 0) {
      for (const i in accounts) {
        for (const a of Object.keys(this.filterAccounts)) {
          if (accounts[i].Username === this.filterAccounts[a].Username) {
            for (const r in this.filterAccounts[a].Replicas) {
              if (
                this.filterAccounts[a].Replicas[r].TypeCode ==
                  this.currentReplica &&
                this.filterAccounts[a].Replicas[r].IsSelected
              ) {
                return true;
              }
            }
          }
        }
      }
    } else {
      return true;
    }

    return false;
  }

  toggleAllFolders(folder: any) {
    let nVal = true,
      hasAutoSyncDisabled = false;

    if (folder.isSelected === 1 || folder.isSelected === 2) {
      folder.isSelected = 0;
      nVal = false;
    } else {
      folder.isSelected = 1;
    }

    // if setting to true, check Auto sync folders values first
    const autoSync: any = {};

    if (nVal) {
      for (const a of Object.keys(this.filterAccounts)) {
        for (const r in this.filterAccounts[a].Replicas) {
          if (
            this.filterAccounts[a].Replicas[r].TypeCode == this.currentReplica
          ) {
            autoSync[this.filterAccounts[a].Replicas[r].Id] =
              this.filterAccounts[a].Replicas[r].AutoSyncNewFolders;

            if (!this.filterAccounts[a].Replicas[r].AutoSyncNewFolders) {
              hasAutoSyncDisabled = true;
            }
          }
        }
      }
    }

    // check all folders - sources which has no SourceAccounts
    for (const a of Object.keys(this.filterAccounts)) {
      for (const r in this.filterAccounts[a].Replicas) {
        if (
          this.filterAccounts[a].Replicas[r].IsSelected &&
          this.filterAccounts[a].Replicas[r].TypeCode == this.currentReplica
        ) {
          for (const f in this.filterAccounts[a].Replicas[r].Folders) {
            if (
              this.filterAccounts[a].Replicas[r].Folders[f].GlobalId ===
              folder.id
            ) {
              if (
                this.accountIsEnabled(
                  this.filterAccounts[a].Replicas[r].Folders[f].SourceAccounts
                )
              ) {
                if (
                  this.filterAccounts[a].Replicas[r].Folders[f].SourceAccounts
                    .length > 0
                ) {
                  this.filterAccounts[a].Replicas[r].Folders[
                    f
                  ].IsSourceSelected = nVal;
                }

                if (
                  this.filterAccounts[a].Replicas[r].Folders[f].IsSourceSelected
                ) {
                  this.filterAccounts[a].Replicas[r].Folders[f].IsSelected =
                    nVal;
                } else {
                  this.filterAccounts[a].Replicas[r].Folders[f].IsSelected =
                    false;
                }
              } else {
                this.filterAccounts[a].Replicas[r].Folders[f].IsSelected =
                  false;
              }
            }
          }

          break;
        }
      }
    }

    // check all new folders which has at least one account in SourceAccounts
    if (nVal) {
      for (const a of Object.keys(this.filterAccounts)) {
        for (const r in this.filterAccounts[a].Replicas) {
          if (
            this.filterAccounts[a].Replicas[r].TypeCode == this.currentReplica
          ) {
            for (const f in this.filterAccounts[a].Replicas[r].Folders) {
              if (
                this.filterAccounts[a].Replicas[r].Folders[f].GlobalId ===
                folder.id
              ) {
                if (
                  autoSync[this.filterAccounts[a].Replicas[r].Id] &&
                  this.filterAccounts[a].Replicas[r].Folders[f].SourceAccounts
                    .length > 0 &&
                  this.filterAccounts[a].Replicas[r].Folders[f].IsSourceSelected
                ) {
                  this.filterAccounts[a].Replicas[r].Folders[f].IsSelected =
                    nVal;
                }

                break;
              }
            }

            break;
          }
        }
      }
    }
  }

  toggleFoldersInTab() {
    for (const f in this.filterFolders) {
      if (this.currentReplica == this.filterFolders[f].title) {
        let enabling = true;

        for (const l in this.filterFolders[f].list) {
          if (this.filterFolders[f].list[l].isSelected) {
            enabling = false;
            break;
          }
        }

        for (const l in this.filterFolders[f].list) {
          if (
            (enabling && !this.filterFolders[f].list[l].isSelected) ||
            (!enabling && this.filterFolders[f].list[l].isSelected)
          ) {
            this.toggleAllFolders(this.filterFolders[f].list[l]);
          }
        }
      }
    }
  }

  compareOriginalBackupedStatus() {
    // this is required to set Folder's IsBackuped values to "false" if folder is reSelected for Sync
    for (const a of Object.keys(this.filterAccounts)) {
      for (const r of Object.keys(this.filterAccounts[a].Replicas)) {
        for (const f in this.filterAccounts[a].Replicas[r].Folders) {
          if (
            this.originalAccounts[a].Replicas[r].Folders[f].IsBackuped &&
            this.filterAccounts[a].Replicas[r].Folders[f].IsSelected &&
            !this.originalAccounts[a].Replicas[r].Folders[f].IsSelected
          ) {
            this.filterAccounts[a].Replicas[r].Folders[f].IsBackuped = false;
          }
        }
      }
    }
  }

  toggleAutoSync(accountId: number, replicaId: string) {
    for (const a in this.filterAccounts) {
      if (this.filterAccounts[a].Id === accountId) {
        for (const r in this.filterAccounts[a].Replicas) {
          if (this.filterAccounts[a].Replicas[r].Id === replicaId) {
            if (this.filterAccounts[a].Replicas[r].IsSelected) {
              this.filterAccounts[a].Replicas[r].AutoSyncNewFolders =
                !this.filterAccounts[a].Replicas[r].AutoSyncNewFolders;
            }
          }
        }

        break;
      }
    }

    this.checkAutoSyncValues();
  }

  toggleAllAutoSync() {
    let nVal = true;
    this.currentAutoSyncStatusPartial = false;

    if (this.currentAutoSyncStatus) {
      nVal = false;
      this.currentAutoSyncStatus = false;
    } else {
      this.currentAutoSyncStatus = true;
    }

    for (const a of Object.keys(this.filterAccounts)) {
      for (const r in this.filterAccounts[a].Replicas) {
        if (
          this.filterAccounts[a].Replicas[r].TypeCode == this.currentReplica
        ) {
          this.filterAccounts[a].Replicas[r].AutoSyncNewFolders = nVal;
          break;
        }
      }
    }
  }

  toggleAutoDelete(accountId: number, replicaId: string) {
    for (const a in this.filterAccounts) {
      if (this.filterAccounts[a].Id === accountId) {
        for (const r in this.filterAccounts[a].Replicas) {
          if (this.filterAccounts[a].Replicas[r].Id === replicaId) {
            if (this.filterAccounts[a].Replicas[r].IsSelected) {
              this.filterAccounts[a].Replicas[r].AutoDeleteRemovedFolders =
                !this.filterAccounts[a].Replicas[r].AutoDeleteRemovedFolders;
            }

            break;
          }
        }

        break;
      }
    }

    this.checkAutoDeleteValues();
  }

  toggleAllAutoDelete() {
    let nVal = true;
    this.currentAutoDeleteStatusPartial = false;

    if (this.currentAutoDeleteStatus) {
      nVal = false;
      this.currentAutoDeleteStatus = false;
    } else {
      this.currentAutoDeleteStatus = true;
    }

    for (const a of Object.keys(this.filterAccounts)) {
      for (const r in this.filterAccounts[a].Replicas) {
        if (
          this.filterAccounts[a].Replicas[r].TypeCode == this.currentReplica
        ) {
          this.filterAccounts[a].Replicas[r].AutoDeleteRemovedFolders = nVal;
          break;
        }
      }
    }
  }

  checkFolderValues(folder: number) {
    let onVal = 0,
      offVal = 0;

    for (const a of Object.keys(this.filterAccounts)) {
      for (const r in this.filterAccounts[a].Replicas) {
        if (
          this.filterAccounts[a].Replicas[r].TypeCode == this.currentReplica
        ) {
          for (const f in this.filterAccounts[a].Replicas[r].Folders) {
            if (
              this.filterAccounts[a].Replicas[r].Folders[f].GlobalId == folder
            ) {
              if (this.filterAccounts[a].Replicas[r].Folders[f].IsSelected) {
                onVal++;
              } else {
                offVal++;
              }

              break;
            }
          }

          break;
        }
      }
    }

    for (const r in this.filterFolders) {
      if (this.filterFolders[r].title == this.currentReplica) {
        for (const f in this.filterFolders[r].list) {
          if (this.filterFolders[r].list[f].id == folder) {
            if (onVal === 0 && offVal > 0) {
              this.filterFolders[r].list[f].isSelected = 0;
            } else if (onVal > 0 && offVal === 0) {
              this.filterFolders[r].list[f].isSelected = 1;
            } else {
              this.filterFolders[r].list[f].isSelected = 2;
            }

            break;
          }
        }

        break;
      }
    }
  }

  toggleFolderSelected(folder: any, doChange: boolean) {
    if (doChange) {
      if (folder.IsSelected) {
        folder.IsSelected = false;
      } else if (folder.IsSourceSelected) {
        folder.IsSelected = true;
      }

      if (folder.SourceAccounts.length === 0) {
        for (const a of Object.keys(this.filterAccounts)) {
          for (const r in this.filterAccounts[a].Replicas) {
            if (
              this.filterAccounts[a].Replicas[r].TypeCode ===
              this.currentReplica
            ) {
              for (const f in this.filterAccounts[a].Replicas[r].Folders) {
                if (
                  folder.GlobalId ===
                  this.filterAccounts[a].Replicas[r].Folders[f].GlobalId
                ) {
                  if (
                    this.filterAccounts[a].Replicas[r].Folders[f].SourceAccounts
                      .length > 0
                  ) {
                    if (folder.IsSelected) {
                      this.filterAccounts[a].Replicas[r].Folders[
                        f
                      ].IsSourceSelected = true;

                      if (
                        this.filterAccounts[a].Replicas[r].AutoSyncNewFolders
                      ) {
                        this.filterAccounts[a].Replicas[r].Folders[
                          f
                        ].IsSelected = true;
                      }
                    } else {
                      let sourceSelected = false;

                      for (const w in this.filterAccounts) {
                        if (
                          a != w &&
                          this.filterAccounts[w].Replicas[r].Folders[f]
                            .SourceAccounts.length === 0 &&
                          this.filterAccounts[w].Replicas[r].Folders[f]
                            .IsSelected
                        ) {
                          sourceSelected = true;
                        }
                      }

                      if (!sourceSelected) {
                        this.filterAccounts[a].Replicas[r].Folders[
                          f
                        ].IsSourceSelected = false;
                        this.filterAccounts[a].Replicas[r].Folders[
                          f
                        ].IsSelected = false;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

      this.checkFolderValues(folder.GlobalId);
    }
  }

  save() {
    const appliedAccounts: any = [];
    this.compareOriginalBackupedStatus();

    for (const i of Object.keys(this.filterAccounts)) {
      let applied = false,
        replicas = 0,
        folders = 0;

      if (this.filterAccounts[i].AutoSyncNewFolders == false) {
        applied = true;
      }

      for (const k of Object.keys(this.filterAccounts[i].Replicas)) {
        if (!this.filterAccounts[i].Replicas[k].IsSelected) {
          applied = true;
        } else {
          replicas++;
        }

        for (const l in this.filterAccounts[i].Replicas[k].Folders) {
          if (!this.filterAccounts[i].Replicas[k].Folders[l].IsSelected) {
            applied = true;
          } else {
            folders++;
          }
        }
      }

      appliedAccounts[i] = {
        id: this.filterAccounts[i].Id,
        applied: applied,
      };
    }

    for (const i of Object.keys(appliedAccounts)) {
      for (const j in this.filterAccounts) {
        if (this.filterAccounts[j].AccountId === appliedAccounts[i].id) {
          this.filterAccounts[j].IsFilterApplied = appliedAccounts[i].applied;
          break;
        }
      }
    }

    this.filtersService
      .save({ SyncAccounts: this.filterAccounts })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.router.navigate(['/synchronization/management']);
      });
  }

  openHelp() {
    this.helpService.open();
  }

  allowFullAccess(account: any) {
    if (account.ProviderCode === 'Google') {
      this.accountsService
        .add({ Provider: account.ProviderCode, Username: account.Username })
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: any) => {
          window.location.href = resp;
        });
    }
  }
}
