import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  Observable,
  Subject,
  takeUntil,
} from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { RetryAddAccountModel } from 'src/app/shared/models/account-model';
import { FeaturesEnum } from 'src/app/shared/models/features-model';
import { Modal } from 'src/app/shared/models/modal-model';
import { AccountsService } from 'src/app/shared/services/account.service';
import { DeviceService } from 'src/app/shared/services/device.service';
import { FeaturesService } from 'src/app/shared/services/features.service';
import { PurchaseService } from 'src/app/shared/services/purchase.service';
import { SyncService } from 'src/app/shared/services/sync.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { SyncAccountsFeature } from '../../models/sync-account-feature-model';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss'],
  animations: [
    trigger('toggleAnimation', [
      state('in', style({ height: '*' })),
      transition('void => *', [
        style({ height: 0 }),
        animate(
          '0.3s ease',
          style({
            height: '*',
          })
        ),
      ]),
      transition('* => void', [
        style({ height: '*' }),
        animate(
          '0.3s ease',
          style({
            height: 0,
          })
        ),
      ]),
    ]),
  ],
})
export class ManagementComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  progressBar: any;
  loaded = false;
  activeAccounts = 0;
  allAccounts: any;
  allFilteredAccounts: any;
  filteredAccountsPage: any;
  lastSyncDate: string;
  selected: number;
  hasOneWaySync: false;
  hasSyncCompanyAccount = false;
  searchControl: FormControl;
  private debounce: number = 1000;
  currentPage = 1;
  pageSize = 20;
  searchValue = '';
  collectionSize: number;
  maxContactLimit: number;
  enabledTypes: Array<string>;
  isMaxContactLimit: boolean = false;
  syncAccountsFeature: SyncAccountsFeature | null;

  constructor(
    private router: Router,
    private accountsService: AccountsService,
    private activatedRoute: ActivatedRoute,
    private syncService: SyncService,
    private dialog: NgbModal,
    private translate: TranslateService,
    private userService: UserBaseService,
    private changeDetectorRef: ChangeDetectorRef,
    private featuresService: FeaturesService,
    private deviceService: DeviceService,
    private purchaseService: PurchaseService
  ) {}

  ngOnInit() {
    // subscribe for progress info
    this.syncService.progress
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.progressBar = resp;
      });

    // load accounts
    this.initAccounts();

    // get active accounts count
    this.accountsService.activeAccounts
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.activeAccounts = resp;
      });

    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params['startSync'] === 'true') {
        this.sync();
      }
    });

    this.searchControl = new FormControl('searchControl');
    this.searchControl.valueChanges
      .pipe(debounceTime(this.debounce), distinctUntilChanged())
      .subscribe((query) => {
        this.changeDetectorRef.detectChanges();
        this.searchValue = query.Username ? query.Username : query;
        this.allFilteredAccounts = this.getAllFilteredAccounts(
          this.searchValue
        );
        this.collectionSize = this.allFilteredAccounts.length;
        this.currentPage = 1;
        this.changePage();
      });

    this.featuresService
      .getFeature<SyncAccountsFeature>(FeaturesEnum.SyncAccounts)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.syncAccountsFeature = resp;
      });
    this.showRateNowMobile();
  }

  showRateNowMobile() {
    const mobileOS = this.deviceService.mobileOS();
    const isAppOnIos = mobileOS === 'ios';
    const isAppOnAndroid = mobileOS === 'anroid';
    const rateApp = Number(localStorage.getItem('rateApp'));
    const currentDate = new Date().getTime();
    if (!isAppOnIos && !isAppOnAndroid) {
      return;
    }
    if (
      this.syncAccountsFeature!.CurrentAccountsCount === 2 &&
      (currentDate > rateApp || !rateApp)
    ) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'MODAL_MOBILE_RATE_THIS_APP_TITLE',
        text: 'MODAL_MOBILE_RATE_THIS_APP_TEXT ',
        primaryButton: 'RATE_NOW',
        secondaryButton: 'REMIND_LATER',
        thirdButton: 'DO_NOT_SHOW',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then((data) => {
        if (data === 'primary') {
          localStorage.setItem('rateApp', '8640000000000000');
          const link =
            mobileOS === 'ios'
              ? 'https://apps.apple.com/us/app/syncgene-sync-contact-calendar/id1133538068?external=true'
              : 'https://play.google.com/store/apps/details?id=com.forteam.syncgene&hl=en&gl=US?external=true';
          window.location.href = link;
        } else if (data === 'secondary') {
          const days = 1000 * 60 * 60 * 24 * 7;
          localStorage.setItem('rateApp', String(new Date().getTime() + days));
        } else {
          localStorage.setItem('rateApp', '8640000000000000');
        }
      });
    }
  }

  initAccounts() {
    this.accountsService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        this.enabledTypes = resp.Enabled;
        this.maxContactLimit = resp.Limit;
        this.checkEnabledTypes();
        this.allAccounts = resp.SyncAccounts;
        this.allFilteredAccounts = resp.SyncAccounts;
        this.filteredAccountsPage = this.allAccounts.slice(0, this.pageSize);
        this.collectionSize = this.allFilteredAccounts.length;
        this.hasSyncCompanyAccount = resp.HasAnySyncCompany;
        this.lastSyncDate = resp.LastSyncDate;
        this.loaded = true;
      });
  }

  checkEnabledTypes() {
    this.enabledTypes.forEach((x) => {
      if (
        x === 'Contact' &&
        this.enabledTypes.length === 1 &&
        this.maxContactLimit > 0
      ) {
        this.isMaxContactLimit = true;
      }
    });
  }

  changePage() {
    this.filteredAccountsPage = this.allFilteredAccounts.slice(
      this.pageSize * (this.currentPage - 1),
      this.pageSize * this.currentPage
    );
    this.selected = 0;
  }

  getAllFilteredAccounts(searchValue: string) {
    return this.allAccounts.filter((a: any) =>
      a.FullName
        ? a.Username.toLowerCase().indexOf(searchValue.toLowerCase()) > -1 ||
          a.FullName.toLowerCase().indexOf(searchValue.toLowerCase()) > -1
        : a.Username.toLowerCase().indexOf(searchValue.toLowerCase()) > -1
    );
  }

  // formats typeahead input, output
  formatter = (result: any) => result.Username;

  // tags typeahead
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map((term: string) =>
        term.length < 2
          ? []
          : this.getAllFilteredAccounts(term).slice(
              this.pageSize * (this.currentPage - 1),
              this.pageSize * this.currentPage
            )
      )
    );

  onAccountsChange() {
    //update account info
    this.userService
      .getUser()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {});

    // recount active accounts
    this.activeAccounts = 0;

    for (const i in this.allAccounts) {
      if (
        this.allAccounts[i].Status === 'Active' ||
        this.allAccounts[i].Status === 'ErrorSuspended' ||
        this.allAccounts[i].Status === 'Error' ||
        this.allAccounts[i].Status === 'ErrorHTTPCode4XX' ||
        this.allAccounts[i].Status === 'ErrorHTTPCode5XX'
      ) {
        this.activeAccounts++;
      }
    }
    // update accounts service
    this.accountsService.setActiveAccount(this.activeAccounts);
  }

  select(index: number, mobile?: boolean) {
    if (mobile && this.selected === index) {
      this.selected = 0;
    } else {
      this.selected = index;
    }
  }

  toggle(index: number) {
    if (
      !this.progressBar ||
      (this.progressBar &&
        (this.progressBar.progress === 0 || this.progressBar.progress === 100))
    ) {
      this.selected = index;
      const tempStatus = this.filteredAccountsPage[index].Status;

      if (
        this.filteredAccountsPage[index].Status === 'Active' ||
        this.filteredAccountsPage[index].Status === 'Inactive' ||
        this.filteredAccountsPage[index].Status === 'Error' ||
        this.filteredAccountsPage[index].Status === 'ErrorSuspended' ||
        this.filteredAccountsPage[index].Status === 'ErrorHTTPCode4XX' ||
        this.filteredAccountsPage[index].Status === 'ErrorHTTPCode5XX'
      ) {
        if (
          !this.syncAccountsFeature!.CanAddSyncAccount &&
          this.syncAccountsFeature!.ErrorCode ===
            'ACCOUNT_LIMIT_REACHED_CONTACT_SUPPORT' &&
          !this.filteredAccountsPage[index].IsSelected
        ) {
          const modal = this.dialog.open(ModalComponent, {
            backdrop: 'static',
            keyboard: false,
          });
          let modalData: Modal = {
            title: this.translate.instant(
              'CANNOT_ADD_ACCOUNT_CONTACT_SUPPORT_TITLE',
              { var0: this.syncAccountsFeature!.MaxSyncAccountsCount }
            ),
            titleTranslated: true,
            text: this.translate.instant(
              'CANNOT_ADD_ACCOUNT_CONTACT_SUPPORT_TEXT',
              { var0: this.syncAccountsFeature!.MaxSyncAccountsCount }
            ),
            textTranslated: true,
            primaryButton: 'CONTACT_US',
          };

          modal.componentInstance.modalData = modalData;

          modal.result.then((data) => {
            if (data === 'primary') {
              window.open(
                'http://www.syncgene.com/support?external=true',
                '_blank'
              );
            }
          });
          return;
        } else if (
          !this.syncAccountsFeature!.CanAddSyncAccount &&
          this.syncAccountsFeature!.ErrorCode ===
            'ACCOUNT_LIMIT_REACHED_GO_PREMIUM' &&
          !this.filteredAccountsPage[index].IsSelected
        ) {
          const modal = this.dialog.open(ModalComponent, {
            backdrop: 'static',
            keyboard: false,
          });
          this.openInfoModal(
            this.translate.instant('ADD_ACCOUNT_LIMIT_TITLE_VAR0', {
              var0: this.syncAccountsFeature!.MaxSyncAccountsCount,
            }),
            'ADD_ACCOUNT_LIMIT_TEXT'
          );
          return;
        }
        this.filteredAccountsPage[index].IsSelected =
          !this.filteredAccountsPage[index].IsSelected;
        this.filteredAccountsPage[index].Status = 'Pending';

        // change status in back-end
        this.accountsService
          .toggle(this.filteredAccountsPage[index])
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(
            () => {
              // set status
              if (
                tempStatus === 'Active' ||
                tempStatus === 'ErrorHTTPCode4XX' ||
                tempStatus === 'ErrorHTTPCode5XX'
              ) {
                this.filteredAccountsPage[index].Status = 'Inactive';
              } else if (tempStatus === 'Inactive') {
                this.filteredAccountsPage[index].Status = 'Active';
              } else if (
                tempStatus === 'Error' ||
                tempStatus === 'ErrorSuspended'
              ) {
                this.filteredAccountsPage[index].Status = 'ErrorInactive';
              }
              this.onAccountsChange();
            },
            (err) => {
              if (err.error.Code === 'HTTP_4XX_ERROR_CODE_RECEIVED') {
                this.filteredAccountsPage[index].Status = 'ErrorHTTPCode4XX';
              } else if (err.error.Code === 'HTTP_5XX_ERROR_CODE_RECEIVED') {
                this.filteredAccountsPage[index].Status = 'ErrorHTTPCode5XX';
              }
              // request failed, return old status in front-end
              else if (
                err.error.Code === 'UNVERIFIED_ACCOUNT' ||
                err.error.Code === 'EXCHANGE_UNVERIFIED_ACCOUNT' ||
                err.error.Code === 'TWO_STEP_VERIFICATION_MUST_BE_ENABLED'
              ) {
                this.filteredAccountsPage[index].Status = 'ErrorSuspended';

                const modal = this.dialog.open(ModalComponent, {
                  backdrop: 'static',
                  keyboard: false,
                });
                let modalData: Modal = {
                  title: 'ERROR',
                  text: this.translate.instant('UNVERIFIED_ACCOUNT_RECONNECT', {
                    var0: this.filteredAccountsPage[this.selected].Username,
                  }),
                  textTranslated: true,
                  primaryButton: 'RECONNECT',
                };

                modal.componentInstance.modalData = modalData;

                modal.result.then((data) => {
                  if (data === 'primary') {
                    this.reconnect();
                  } else {
                    this.router.navigate(['/synchronization/management']);
                  }
                });
              } else {
                this.filteredAccountsPage[index].IsSelected =
                  !this.filteredAccountsPage[index].IsSelected;
                this.filteredAccountsPage[index].Status = tempStatus;
              }
            }
          );
      } else {
        if (this.filteredAccountsPage[index].ProviderCode === 'Google') {
          const accountData: RetryAddAccountModel = {
            Id: this.filteredAccountsPage[index].AccountId,
            ValidateOldFirst: 'true',
          };

          this.filteredAccountsPage[index].IsSelected =
            !this.filteredAccountsPage[index].IsSelected;
          this.filteredAccountsPage[index].Status = 'Pending';
          // call back-end
          this.accountsService.retryAddAccount(accountData).subscribe(
            () => {
              // change status in back-end
              this.accountsService
                .toggle(this.filteredAccountsPage[index])
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(
                  () => {
                    // set status
                    this.filteredAccountsPage[index].IsSelected = true;
                    this.filteredAccountsPage[index].Status = 'Active';

                    this.onAccountsChange();
                  },
                  () => {
                    // request failed, return old status in front-end
                    this.filteredAccountsPage[index].IsSelected =
                      !this.filteredAccountsPage[index].IsSelected;
                    this.filteredAccountsPage[index].Status = tempStatus;
                  }
                );
            },
            () => {
              this.reconnect();
            }
          );
        } else {
          this.reconnect();
        }
      }
    }
  }
  registerDialogIsShown(title: string) {
    this.purchaseService
      .limitationsDialogIsShown(title)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {});
  }

  purchase(title: string) {
    this.purchaseService
      .trackUserGoPremiumPress('Free plan dialog: ' + title)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.router.navigate(['/purchase']);
      });
  }

  userClickedFreeOffer(title: string) {
    this.purchaseService
      .sendFreePlanOffer('Free plan dialog: ' + title)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {});
  }

  openInfoModal(title: string, text: string) {
    this.registerDialogIsShown(title);
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: title,
      titleTranslated: true,
      textArray: [text, 'FREE_PREMIUM_DIFFERENCE'],
      primaryButton: 'UPGRADE_NOW_IN_CAPITALS',
      secondaryButton: 'ALWAYS_FREE_IN_CAPITALS',
      buttonTitle: 'RECOMMENDED_PLAN',
      table: [
        {
          td: [
            'FREE_PLAN_DRAWBACK_1',
            'FREE_PLAN_DRAWBACK_2',
            'FREE_PLAN_DRAWBACK_3',
          ],
          th: 'FREE_PLAN_PLAIN',
        },
        {
          td: [
            'PREMIUM_PLAN_BENEFIT_1',
            'PREMIUM_PLAN_BENEFIT_2',
            'PREMIUM_PLAN_BENEFIT_3',
          ],
          th: 'PREMIUM_PLAN',
        },
      ],
    };

    modal.componentInstance.modalData = modalData;
    modal.result.then((data) => {
      if (data === 'primary') {
        this.purchase(title);
        return;
      }
      this.userClickedFreeOffer(title);
    });
  }

  openFilters() {
    if (
      this.activeAccounts > 0 &&
      (!this.progressBar ||
        (this.progressBar &&
          (this.progressBar.progress === 0 ||
            this.progressBar.progress === 100)))
    ) {
      if (!this.hasSyncCompanyAccount) {
        this.router.navigate(['/synchronization/whattosync']);
      } else {
        this.router.navigate(['/synchronization/bulkfilters']);
      }
    }
  }

  openDirection() {
    if (
      this.activeAccounts > 0 &&
      (!this.progressBar ||
        (this.progressBar &&
          (this.progressBar.progress === 0 ||
            this.progressBar.progress === 100)))
    ) {
      this.accountsService
        .get()
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: any) => {
          if (
            resp.SyncAccounts.filter((el: any) => el.HasSyncableStatus === true)
              .length < 2
          ) {
            const modal = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            let modalData: Modal = {
              title: 'INFO',
              text: 'ERROR_SYNC_DIRECTION_LESS_THAN_2_SOURCES',
            };

            modal.componentInstance.modalData = modalData;
          } else if (
            resp.SyncAccounts.filter((el: any) => el.HasSyncableStatus === true)
              .length == 2
          ) {
            this.router.navigate(['/synchronization/direction']);
          } else {
            const modal = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            let modalData: Modal = {
              title: 'INFO',
              text: 'ERROR_SYNC_DIRECTION_MORE_THAN_2_SOURCES',
              support: true,
            };

            modal.componentInstance.modalData = modalData;
          }
        });
    }
  }

  remove() {
    if (
      this.selected != null &&
      (!this.progressBar ||
        (this.progressBar &&
          (this.progressBar.progress === 0 ||
            this.progressBar.progress === 100)))
    ) {
      let text = '';
      this.filteredAccountsPage[this.selected].HasBackups
        ? (text += this.translate.instant('BACKUP_EXISTS_FOR_THIS_SOURCE'))
        : (text = '');

      if (this.filteredAccountsPage[this.selected].IsLastSourceInOneWaySync) {
        text.length > 0
          ? (text += this.translate.instant(
              'CANT_REMOVE_ACCOUNT_BECAUSE_ONE_WAY_SYNC_LAST_OWNER_ADDITIONAL_LINE'
            ))
          : (text = this.translate.instant(
              'CANT_REMOVE_ACCOUNT_BECAUSE_ONE_WAY_SYNC_LAST_OWNER',
              { var0: this.filteredAccountsPage[this.selected].Username }
            ));
      }
      // is this account is last owner of share?
      if (this.filteredAccountsPage[this.selected].LastOwnerInShare) {
        text.length > 0
          ? (text += this.translate.instant(
              'MODAL_REMOVE_ACCOUNT_LAST_OWNER_TEXT_ADDITIONAL_LINE'
            ))
          : (text = this.translate.instant(
              'MODAL_REMOVE_ACCOUNT_LAST_OWNER_TEXT',
              { var0: this.filteredAccountsPage[this.selected].Username }
            ));
      }

      text.length > 0
        ? (text +=
            '</ul><br/>' + this.translate.instant('MODAL_REMOVE_ACCOUNT_TEXT'))
        : (text += this.translate.instant('MODAL_REMOVE_ACCOUNT_TEXT'));

      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'MODAL_REMOVE_ACCOUNT_TITLE',
        text: text,
        textTranslated: true,
        primaryButton: 'yesButton',
        secondaryButton: 'yesButton',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then((data) => {
        if (data === 'primary') {
          this.accountsService
            .remove(this.filteredAccountsPage[this.selected].AccountId)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(() => {
              // delete from front-end array both on all and filtered accounts
              this.allAccounts.splice(
                this.allAccounts.findIndex(
                  (a: any) =>
                    a.AccountId ===
                    this.filteredAccountsPage[this.selected].AccountId
                ),
                1
              );
              this.filteredAccountsPage.splice(this.selected, 1);

              // reset selected item
              this.selected = 0;

              this.onAccountsChange();
            });
        }
      });
    }
  }

  changePass() {
    if (
      this.selected !== null &&
      (this.filteredAccountsPage[this.selected].IsSelected ||
        this.filteredAccountsPage[this.selected].Status !== 'Inactive') &&
      this.filteredAccountsPage[this.selected].ProviderCode !== 'Google'
    ) {
      const prevStatus = this.filteredAccountsPage[this.selected].Status;

      // change password modal
      this.dialog
        .changeAccountPass(this.filteredAccountsPage[this.selected])
        .then(
          (resp: any) => {
            if (resp === 1) {
              this.filteredAccountsPage[this.selected].IsSelected = true;
              this.filteredAccountsPage[this.selected].Status = 'Active';
              this.onAccountsChange();

              const modal = this.dialog.open(ModalComponent, {
                backdrop: 'static',
                keyboard: false,
              });
              let modalData: Modal = {
                title: 'SUCCESS',
                text: 'PASSWORD_CHANGED',
              };

              modal.componentInstance.modalData = modalData;
            } else if (resp === 2) {
              // update front end
              if (prevStatus === 'ErrorDisabled') {
                this.filteredAccountsPage[this.selected].Status =
                  'ErrorDisabled';
                this.filteredAccountsPage[this.selected].IsSelected = false;
              } else if (prevStatus === 'ErrorSuspended') {
                this.filteredAccountsPage[this.selected].Status =
                  'ErrorSuspended';
                this.filteredAccountsPage[this.selected].IsSelected = true;
              } else if (prevStatus === 'ErrorHTTPCode4XX') {
                this.filteredAccountsPage[this.selected].Status =
                  'ErrorHTTPCode4XX';
                this.filteredAccountsPage[this.selected].IsSelected = true;
              } else if (prevStatus === 'ErrorHTTPCode5XX') {
                this.filteredAccountsPage[this.selected].Status =
                  'ErrorHTTPCode5XX';
                this.filteredAccountsPage[this.selected].IsSelected = true;
              }

              this.onAccountsChange();
            }
          },
          () => {
            // do nothing on no
          }
        );
    }
  }

  redirectOrShowReconnectDialog(prevStatus: string, migrate: boolean) {
    if (
      this.filteredAccountsPage[this.selected].ProviderCode === 'Google' ||
      this.filteredAccountsPage[this.selected].ProviderCode === 'OutlookRest' ||
      this.filteredAccountsPage[this.selected].ProviderCode === 'Office365' ||
      this.filteredAccountsPage[this.selected].IsSyncCompanySource ||
      (this.filteredAccountsPage[this.selected].ProviderCode == 'Exchange' &&
        this.filteredAccountsPage[this.selected].IsAlternativeCredentials)
    ) {
      this.accountsService
        .getSyncAccountReconnectUrl({
          Provider: this.filteredAccountsPage[this.selected].ProviderCode,
          Id: this.filteredAccountsPage[this.selected].AccountId,
          Migrate: migrate,
        })
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: any) => {
          window.location.href = resp;
        });
    } else {
      this.filteredAccountsPage[this.selected].Status = 'Pending';

      // reconnect modal
      this.dialog
        .reconnectAccount(this.filteredAccountsPage[this.selected])
        .then(
          (resp: any) => {
            if (resp === 1) {
              // update front end
              this.filteredAccountsPage[this.selected].IsSelected = true;
              this.filteredAccountsPage[this.selected].Status = 'Active';

              this.onAccountsChange();

              const modal = this.dialog.open(ModalComponent, {
                backdrop: 'static',
                keyboard: false,
              });
              let modalData: Modal = {
                title: 'SUCCESS',
                text: 'ACCOUNT_RECONNECTED',
              };

              modal.componentInstance.modalData = modalData;
            } else if (resp === 2) {
              // update front end

              if (prevStatus === 'ErrorDisabled') {
                this.filteredAccountsPage[this.selected].Status =
                  'ErrorDisabled';
                this.filteredAccountsPage[this.selected].IsSelected = false;
              } else if (prevStatus === 'ErrorSuspended') {
                this.filteredAccountsPage[this.selected].Status =
                  'ErrorSuspended';
                this.filteredAccountsPage[this.selected].IsSelected = true;
              } else if (prevStatus === 'ErrorHTTPCode4XX') {
                this.filteredAccountsPage[this.selected].Status =
                  'ErrorHTTPCode4XX';
                this.filteredAccountsPage[this.selected].IsSelected = true;
              } else if (prevStatus === 'ErrorHTTPCode5XX') {
                this.filteredAccountsPage[this.selected].Status =
                  'ErrorHTTPCode5XX';
                this.filteredAccountsPage[this.selected].IsSelected = true;
              }

              this.onAccountsChange();
            }
          },
          () => {
            // do nothing on no
            this.filteredAccountsPage[this.selected].Status = prevStatus;
            this.filteredAccountsPage[this.selected].IsSelected = false;
          }
        );
    }
  }

  reconnect() {
    if (
      this.selected != null &&
      (this.filteredAccountsPage[this.selected].IsSelected ||
        this.filteredAccountsPage[this.selected].Status !== 'Inactive') &&
      (!this.progressBar ||
        (this.progressBar &&
          (this.progressBar.progress === 0 ||
            this.progressBar.progress === 100)))
    ) {
      const prevStatus = this.filteredAccountsPage[this.selected].Status;
      this.filteredAccountsPage[this.selected].IsSelected =
        !this.filteredAccountsPage[this.selected].IsSelected;
      var migrate = this.filteredAccountsPage[this.selected].CanMigrate; // use parameter from back end

      if (
        this.filteredAccountsPage[this.selected].ProviderCode === 'Google' &&
        this.filteredAccountsPage[this.selected].PromptMigrate
      ) {
        this.dialog
          .switchToPeople({
            data: {
              warning: 'GOOGLE_CONTACTS_API_IS_BEING_DEPRECATED_WARNING',
              list: [
                'GOOGLE_CONTACTS_API_IS_BEING_DEPRECATED_LIST_ITEM_1',
                'GOOGLE_CONTACTS_API_IS_BEING_DEPRECATED_LIST_ITEM_2',
                'GOOGLE_CONTACTS_API_IS_BEING_DEPRECATED_LIST_ITEM_3',
                'GOOGLE_CONTACTS_API_IS_BEING_DEPRECATED_LIST_ITEM_4',
              ],
              question: 'GOOGLE_CONTACTS_API_IS_BEING_DEPRECATED_QUESTION',
              note: 'YOU_WILL_BE_REDIRECTED_TO_AUTH_PAGE',
            },
          })
          .then(
            (res: any) => {
              this.redirectOrShowReconnectDialog(prevStatus, res.migrate);
            },
            () => {
              // do nothing on no
            }
          );
      } else {
        this.redirectOrShowReconnectDialog(prevStatus, migrate);
      }
    }
  }

  sync() {
    if (this.isMaxContactLimit) {
      this.openMaximuOfContacts();
      return;
    }
    this.syncService.checkBackupAccounts();
  }

  openMaximuOfContacts() {
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'INFO',
      text: 'REACHED_MAXIMUM_NUMBER_OF_CONTACTS',
    };

    modal.componentInstance.modalData = modalData;
  }

  allowFullAccess() {
    if (
      this.filteredAccountsPage[this.selected].ProviderCode === 'Google' &&
      (!this.filteredAccountsPage[this.selected].HasFullAccess ||
        this.filteredAccountsPage[this.selected].BackupAccountCanBeAdded)
    ) {
      this.accountsService
        .add({
          Provider: this.filteredAccountsPage[this.selected].ProviderCode,
          Username: this.filteredAccountsPage[this.selected].Username,
          SelectedReplicaTypes: [1, 2, 3],
          BackupRightsRequired: true,
        })
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: any) => {
          window.location.href = resp;
        });
    }
  }

  toggleOneWaySource() {
    this.accountsService
      .toggleOneWaySource(this.filteredAccountsPage[this.selected].AccountId)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        //// reset selected item
        //this.selected = null;

        this.initAccounts();
        this.filteredAccountsPage[this.selected].IsMainSource =
          !this.filteredAccountsPage[this.selected].IsMainSource;
        this.onAccountsChange();
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
