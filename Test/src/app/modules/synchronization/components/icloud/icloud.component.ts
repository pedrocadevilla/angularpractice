import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { AccountsService } from 'src/app/shared/services/account.service';
import { ExternalService } from 'src/app/shared/services/external.service';
import { errorMessages } from '../../models/exchange-form-err-message';
import { initializeForm } from '../../models/icloud-form-constructor';

@Component({
  selector: 'app-icloud',
  templateUrl: './icloud.component.html',
  styleUrls: ['./icloud.component.scss'],
})
export class IcloudComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  form: FormGroup;
  activeAccounts = 0;
  returnUrl: string;
  sync2cloud = false;
  inviteId = '';
  usernameErrorList: errorMessageObject[] = [];
  passwordErrorList: errorMessageObject[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private accountsService: AccountsService,
    private externalService: ExternalService
  ) {
    this.usernameErrorList = errorMessages.userNameMessages;
    this.passwordErrorList = errorMessages.userNameMessages;
    this.form = initializeForm();
  }

  ngOnInit() {
    // subscribe to router event for params
    var userName: string;
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.returnUrl = params['returnUrl'];
      this.inviteId = params['inviteid'];
      userName = params['accountName'];
      this.sync2cloud = params['sync2cloud'] == 'true';
    });

    // get accounts count
    this.accountsService.activeAccounts
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.activeAccounts = resp;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  add() {
    if (!this.form.valid) {
      return;
    }

    // add missing attributes
    this.form.value.provider = 'ICloud';

    if (this.returnUrl !== undefined && this.returnUrl.indexOf('share/') > -1) {
      this.form.value.addedForSharing = true;
    }

    this.accountsService
      .add(this.form.value)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        () => {
          // redirect to return url if we have it
          if (this.inviteId) {
            this.router.navigateByUrl(
              '/share/accept-on-account?inviteid=' +
                this.inviteId +
                '&system=icloud&select=1&sync2cloud=' +
                this.sync2cloud
            );
          } else if (this.returnUrl) {
            this.router.navigateByUrl(this.returnUrl);
          } else {
            let text = 'SYNC_ACCOUNT_ADDED';
            let redirectUrl = '/synchronization/addsyncaccount';

            // text for account added bar
            if (this.activeAccounts > 0) {
              text = 'SYNC_ACCOUNT_ADDED_START_SYNCHRONIZATION';
              redirectUrl = '/synchronization/management';
            }

            this.router.navigate([redirectUrl], {
              queryParams: {
                username: this.form.value.username,
                code: text,
              },
            });
          }
        },
        (err) => {
          if (err.error.Code === 'SYNC_ACCOUNT_RECONNECTED') {
            // redirect to return url if we have it
            if (this.returnUrl) {
              this.router.navigateByUrl(this.returnUrl);
            } else {
              let text = 'SYNC_ACCOUNT_ADDED';
              let redirectUrl = '/synchronization/addsyncaccount';

              // text for account added bar
              if (this.activeAccounts > 1) {
                text = 'SYNC_ACCOUNT_ADDED_START_SYNCHRONIZATION';
                redirectUrl = '/synchronization/management';
              }

              this.router.navigate([redirectUrl], {
                queryParams: {
                  username: this.form.value.username,
                  code: text,
                },
              });
            }
          }
        }
      );
  }
}
