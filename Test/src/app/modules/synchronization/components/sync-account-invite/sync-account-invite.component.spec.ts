import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncAccountInviteComponent } from './sync-account-invite.component';

describe('SyncAccountInviteComponent', () => {
  let component: SyncAccountInviteComponent;
  let fixture: ComponentFixture<SyncAccountInviteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SyncAccountInviteComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncAccountInviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
