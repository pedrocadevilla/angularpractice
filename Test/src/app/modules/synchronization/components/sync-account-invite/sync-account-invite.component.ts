import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, takeUntil } from 'rxjs';
import { AccountsService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-sync-account-invite',
  templateUrl: './sync-account-invite.component.html',
  styleUrls: ['./sync-account-invite.component.scss'],
  animations: [
    trigger('inputAnimation', [
      state('in', style({ height: 'auto', opacity: 1 })),
      transition('void => *', [
        style({
          height: 0,
          'margin-bottom': 0,
          opacity: 0,
        }),
        animate('0.25s ease'),
      ]),
      transition('* => void', [
        animate(
          '0.25s ease',
          style({
            height: 0,
            'margin-bottom': 0,
            opacity: 0,
          })
        ),
      ]),
    ]),
  ],
})
export class SyncAccountInviteComponent implements OnInit {
  private ngUnsubscribe: Subject<any> = new Subject();
  isLoaded = false;
  form: FormGroup;
  data: any;
  invalidAccount: any = {
    password: false,
    server: false,
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private accountsService: AccountsService,
    private dialog: NgbModal,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      let inviteId = params.get('id');
      this.accountsService
        .getSyncAccountInvite(inviteId!)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(
          (resp: any) => {
            this.data = resp;
            this.form = this.formBuilder.group({
              InviteId: [inviteId],
              Password: null,
              Server: null,
              UserName: [{ value: resp.UserName, disabled: true }],
              AutoDiscovery: true,
            });

            this.isLoaded = true;
          },
          (err) => {
            this.isLoaded = true;
          }
        );
    });
  }

  icloudHelp() {
    this.dialog.iCloudInfo();
  }

  validate() {
    let firstErrInput;

    // reset errors
    this.invalidAccount = {
      password: false,
      server: false,
    };

    // custom validation

    if (this.form.get('Password')?.invalid) {
      this.invalidAccount.password = true;

      if (!firstErrInput) {
        firstErrInput = 'Password';
      }
    }

    if (
      !this.form.value.AutoDiscovery &&
      !this.form.value.Server &&
      this.data.SyncSystem === 1
    ) {
      this.invalidAccount.server = true;

      if (!firstErrInput) {
        firstErrInput = 'Server';
      }
    }

    if (firstErrInput) {
      document.getElementById(firstErrInput)?.focus();
      return false;
    }
    return true;
  }

  confirm(validationNeeded?: any) {
    if (validationNeeded) {
      if (!this.validate()) {
        return;
      }
    } else {
      this.form.patchValue({
        Password: null,
        Server: null,
        AutoDiscovery: null,
      });
    }

    this.accountsService
      .acceptSyncAccountInvite(this.form.getRawValue())
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        window.location.href = resp;
      });
  }
}
