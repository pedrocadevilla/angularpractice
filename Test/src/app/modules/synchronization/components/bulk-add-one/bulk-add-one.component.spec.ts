import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkAddOneComponent } from './bulk-add-one.component';

describe('BulkAddOneComponent', () => {
  let component: BulkAddOneComponent;
  let fixture: ComponentFixture<BulkAddOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BulkAddOneComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkAddOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
