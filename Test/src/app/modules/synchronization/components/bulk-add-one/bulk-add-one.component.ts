import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { BulkService } from '../../services/bulk.service';

@Component({
  selector: 'app-bulk-add-one',
  templateUrl: './bulk-add-one.component.html',
  styleUrls: ['./bulk-add-one.component.scss'],
})
export class BulkAddOneComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  isLoaded = false;
  selected = 0;
  allSelected = false;
  isIndeterminate = false;
  validAccounts = 0;
  otherUsedAccounts: number;
  maxAccounts: number;
  companyName: string;
  accountsUsedByOthers = 0;
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private bulkAddService: BulkService,
    private dialog: NgbModal,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      let companyId = params['tenant']; //depends on syncSystem
      if (companyId) {
        this.bulkAddService
          .getCompanyUsers(companyId)
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(
            (resp: any) => {
              let users = resp.Users;
              this.otherUsedAccounts = resp.UsedAccounts;
              this.companyName = resp.CompanyName;
              this.maxAccounts = resp.MaxAccounts;

              for (let i = 0; i < users.length; i++) {
                if (
                  users[i].Status === 'DUPLICATE_MAILBOX' &&
                  users[i].IsEnabled
                ) {
                  users[i].IsSelected = true;
                  this.selected++;
                  this.validAccounts++;
                  this.otherUsedAccounts--;
                  users[i].IsUsedByOthers = false;
                } else if (users[i].Status === 'ACCOUNT_USED_BY_OTHERS') {
                  this.accountsUsedByOthers++;
                  users[i].IsUsedByOthers = true;
                } else {
                  this.validAccounts++;
                  users[i].IsUsedByOthers = false;
                }
              }

              for (let i = 0; i < users.length; i++) {
                if (users[i].IsSelected || users[i].IsUsedByOthers) {
                  continue;
                }

                if (
                  this.selected + this.otherUsedAccounts ===
                  this.maxAccounts
                ) {
                  break;
                }

                users[i].IsSelected = true;
                this.selected++;
              }

              this.isIndeterminate =
                this.selected > 0 && this.selected !== this.validAccounts;

              let userGroups = [];
              for (let i = 0; i < users.length; i++) {
                userGroups.push(
                  this.formBuilder.group({
                    Name: users[i].Name,
                    Email: users[i].Email,
                    IsSelected: [
                      {
                        value: users[i].IsSelected ? true : false,
                        disabled: users[i].IsUsedByOthers,
                      },
                    ],
                    IsUsedByOthers: users[i].IsUsedByOthers,
                    Id: users[i].Id,
                    UniqueMailboxId: users[i].UniqueMailboxId,
                  })
                );
              }

              this.form = this.formBuilder.group({
                allSelected: this.selected === this.validAccounts,
                companyId: [companyId],
                users: this.formBuilder.array(userGroups),
                syncSystem: resp.SyncSystem,
                folders: this.formBuilder.array(resp.Folders),
              });

              //if users length 0, redirect to management page
              if (users.length == 0) {
                this.showNoUsersFoundDialog();
              }
              //show dialog and and reset allSelected value to false
              if (
                this.validAccounts + this.otherUsedAccounts >
                this.maxAccounts
              ) {
                this.showLimitationsDialog();
              }

              this.isLoaded = true;
            },
            () => {
              this.router.navigate(['/synchronization/addsyncaccount']);
            }
          );
      } else {
        this.router.navigate(['/synchronization/addsyncaccount']);
      }
    });
  }

  get users() {
    return (this.form.get('users') as FormArray).controls;
  }

  onNext() {
    let users = this.form.get('users') as FormArray;

    for (let i = users.value.length - 1; i >= 0; i--) {
      if (!users.value[i].IsSelected) {
        users.removeAt(i);
      }
    }

    this.bulkAddService.setData(this.form.value);

    this.router.navigate(['/synchronization/bulkaddoutlook/selectfolders']);
  }

  toggleSelection(idx: number) {
    let users = this.form.get('users') as FormArray;
    let allSelected = this.form.get('allSelected')!;

    var isSelected = !users.value[idx].IsSelected;
    if (isSelected) {
      if (this.maxAccounts === this.selected + this.otherUsedAccounts) {
        users.controls[idx].patchValue({ IsSelected: false });

        this.showLimitationsDialog();

        return;
      }
      users.controls[idx].patchValue({ IsSelected: true });
      this.selected++;
    } else {
      users.controls[idx].patchValue({ IsSelected: false });
      this.selected--;
    }

    allSelected.patchValue(this.selected === this.validAccounts);
    this.isIndeterminate =
      this.selected > 0 && this.selected !== this.validAccounts;
  }

  toggleAll() {
    let allSelected = this.form.get('allSelected')!;

    if (
      !allSelected.value &&
      this.validAccounts + this.otherUsedAccounts > this.maxAccounts
    ) {
      allSelected.patchValue(false);
      this.showLimitationsDialog();

      return;
    }
    allSelected.patchValue(!allSelected.value);

    let users = this.form.get('users') as FormArray;

    for (let i = 0; i < users.value.length; i++) {
      if (users.value[i].IsUsedByOthers) {
        continue;
      }
      users.controls[i].patchValue({ IsSelected: allSelected.value });
    }

    this.selected = allSelected.value ? this.validAccounts : 0;
    this.isIndeterminate =
      this.selected > 0 && this.selected !== this.validAccounts;
  }

  showLimitationsDialog() {
    let allSelected = this.form.get('allSelected')!;
    const modalTerm = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'TOO_MANY_ACCOUNTS_SELECTED_CONTACT_SUPPORT_TITLE',
      text: this.translate.instant(
        'TOO_MANY_ACCOUNTS_SELECTED_CONTACT_SUPPORT',
        {
          var0: this.companyName,
        }
      ),
      textTranslated: true,
    };

    modalTerm.componentInstance.modalData = modalData;

    modalTerm.result.then((data: string) => {
      allSelected.patchValue(false);
    });
  }

  showNoUsersFoundDialog() {
    const modalTerm = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'NO_USERS_FOUND',
      text: this.translate.instant('NO_USERS_FOUND_ON_SOURCENAME', {
        var0: this.companyName,
      }),
      textTranslated: true,
    };

    modalTerm.componentInstance.modalData = modalData;

    modalTerm.result.then((data: string) => {
      this.router.navigate(['synchronization/management']);
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
