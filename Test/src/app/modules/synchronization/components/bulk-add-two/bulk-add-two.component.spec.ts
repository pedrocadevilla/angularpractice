import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkAddTwoComponent } from './bulk-add-two.component';

describe('BulkAddTwoComponent', () => {
  let component: BulkAddTwoComponent;
  let fixture: ComponentFixture<BulkAddTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BulkAddTwoComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkAddTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
