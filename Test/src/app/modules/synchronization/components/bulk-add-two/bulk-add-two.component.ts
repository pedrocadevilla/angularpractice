import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { BulkService } from '../../services/bulk.service';

@Component({
  selector: 'app-bulk-add-two',
  templateUrl: './bulk-add-two.component.html',
  styleUrls: ['./bulk-add-two.component.scss'],
})
export class BulkAddTwoComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();

  form: FormGroup;
  isLoaded = false;

  editNewContactFolder = false;
  editNewCalendarFolder = false;
  editNewTaskFolder = false;
  addFolderList = '';
  isNewFolderValid = false;

  previousEditableFolderName = '';
  editableFolderIdx = -1;
  isEditableFolderValid = false;
  editFolderList = '';
  showDialog = false;

  areTasksSupported = true;
  isFilters = false;
  //respectively, these syncSystems: Exchange, Outlook, Office365, OutlookRest
  syncSystemsForTrim = [1, 3, 8, 9];
  isTrimNeeded = false;

  constructor(
    private router: Router,
    private bulkAddService: BulkService,
    private dialog: NgbModal,
    private translate: TranslateService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.isFilters =
      this.router.url.indexOf('/synchronization/bulkfilters') > -1;
    if (this.isFilters) {
      this.bulkAddService
        .getFolders()
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(
          (resp) => {
            this.init({ folders: resp });
          },
          (err) => {
            this.isLoaded = true;
          }
        );
    } else {
      let data = this.bulkAddService.getData();
      if (!data) {
        this.router.navigate(['/synchronization/addsyncaccount']);
      }

      this.init(data);
    }
  }

  init(data: any) {
    this.areTasksSupported = data.syncSystem != 9 && data.syncSystem != 8;
    this.isTrimNeeded = this.syncSystemsForTrim.includes(data.syncSystem);

    this.form = this.formBuilder.group({
      SyncDefaultCalendarFolder: [false],
      SyncDefaultTaskFolder: [
        { value: false, disabled: !this.areTasksSupported },
      ],
      SyncDefaultContactFolder: [false],
      ContactFolders: this.formBuilder.array([]),
      TaskFolders: this.formBuilder.array([]),
      CalendarFolders: this.formBuilder.array([]),
      CompanyId: [data.companyId],
      Users: [data.users],
      StartSync: true,
      SyncSystem: data.syncSystem,
      NewName: '',
    });

    for (let i = 0; i < data.folders.length; i++) {
      let folder = data.folders[i];
      if (folder.IsDefault) {
        switch (folder.FolderType) {
          case 1:
            this.form.patchValue({
              SyncDefaultContactFolder: folder.IsSelected,
            });
            break;
          case 2:
            this.form.patchValue({
              SyncDefaultCalendarFolder: folder.IsSelected,
            });
            break;
          case 3:
            this.form.patchValue({
              SyncDefaultTaskFolder:
                this.areTasksSupported && folder.IsSelected,
            });
            break;
        }

        continue;
      }

      let listName = '';
      switch (folder.FolderType) {
        case 1:
          listName = 'ContactFolders';
          break;
        case 2:
          listName = 'CalendarFolders';
          break;
        case 3:
          listName = 'TaskFolders';
          break;
      }

      let list = this.form.get(listName) as FormArray;
      list.push(
        this.formBuilder.group({
          Name: folder.Name,
          IsEditable: false,
          IsValid: true,
          IsSelected: folder.IsSelected,
        })
      );
    }

    this.isLoaded = true;
  }

  get ContactFolders() {
    return (this.form.get('ContactFolders') as FormArray).controls;
  }
  get TaskFolders() {
    return (this.form.get('TaskFolders') as FormArray).controls;
  }
  get CalendarFolders() {
    return (this.form.get('CalendarFolders') as FormArray).controls;
  }

  showFolderNameIncorrect() {
    const modalTerm = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'ERROR',
      text: 'ONE_OR_MORE_FOLDER_NAMED_INCORRECTLY',
    };

    modalTerm.componentInstance.modalData = modalData;
  }

  onNext() {
    //if new folder editable and invalid, throw error and return
    if (
      !this.isNewFolderValid &&
      (this.editNewTaskFolder ||
        this.editNewContactFolder ||
        this.editNewCalendarFolder)
    ) {
      this.saveNewFolder(true);
      return;
    }

    //if old folder editable and invalid, throw error and return
    if (
      this.form.value.ContactFolders.find((x: any) => x.IsEditable === true) ||
      this.form.value.CalendarFolders.find((x: any) => x.IsEditable === true) ||
      (this.form.value.TaskFolders.find((x: any) => x.IsEditable === true) &&
        (this.form.value.ContactFolders.find((x: any) => x.IsValid === false) ||
          this.form.value.CalendarFolders.find(
            (x: any) => x.IsValid === false
          ) ||
          this.form.value.TaskFolders.find((x: any) => x.IsValid === false)))
    ) {
      this.saveFolderChanges(true);
      return;
    }

    this.saveNewFolder();
    this.saveFolderChanges();
    let action = this.isFilters
      ? this.bulkAddService.saveBulkFolders(this.form.getRawValue())
      : this.bulkAddService.addAccounts(this.form.getRawValue());

    action.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      (resp) => {
        if (this.isFilters) {
          this.router.navigate(['/synchronization/status']);
          return;
        }
        this.router.navigate(['/synchronization/status'], {
          queryParams: { startSync: true },
        });
      },
      (err) => {}
    );
  }

  //add new folder logic
  addNewContactFolder() {
    this.addNewFolder('ContactFolders');
    this.editNewContactFolder = true;
  }

  addNewTaskFolder() {
    this.addNewFolder('TaskFolders');
    this.editNewTaskFolder = true;
  }

  addNewCalendarFolder() {
    this.addNewFolder('CalendarFolders');
    this.editNewCalendarFolder = true;
  }

  addNewFolder(listName: string) {
    this.saveNewFolder();
    this.saveFolderChanges();

    this.addFolderList = listName;
  }

  discardNewFolder() {
    this.editNewContactFolder = false;
    this.editNewCalendarFolder = false;
    this.editNewTaskFolder = false;

    this.form.patchValue({ NewName: '' });
    this.addFolderList = '';
    this.isNewFolderValid = false;
  }

  saveNewFolder(onSave?: boolean) {
    this.trimNewFolderName();
    if (!this.isNewFolderValid) {
      if (onSave) {
        this.showFolderNameIncorrect();
      } else {
        this.discardNewFolder();
      }
      return;
    }

    let list = this.form.get(this.addFolderList) as FormArray;
    let newName = this.form.value.NewName;
    list.push(
      this.formBuilder.group({
        Name: newName,
        IsEditable: false,
        IsValid: true,
        IsSelected: true,
      })
    );

    this.discardNewFolder();
  }

  trimNewFolderName() {
    if (this.isTrimNeeded) {
      this.form.patchValue({ NewName: this.form.value.NewName.trim() });
      this.validateFolderName();
    }
  }

  trimEditableFolderName() {
    let list = this.form.get(this.editFolderList) as FormArray;
    if (this.isTrimNeeded) {
      list
        .at(this.editableFolderIdx)
        .patchValue({ Name: list.value[this.editableFolderIdx].Name.trim() });
      this.onFolderNameChanged();
    }
  }

  validateFolderName() {
    let newName = this.form.value.NewName;
    if (!newName) {
      this.isNewFolderValid = false;
      return;
    }
    let list = this.form.get(this.addFolderList) as FormArray;
    for (let i = 0; i < list.value.length; i++) {
      if (list.value[i].Name.toLowerCase() == newName.toLowerCase()) {
        this.isNewFolderValid = false;
        return;
      }
    }

    this.isNewFolderValid = true;
  }

  onFolderNameChanged() {
    let list = this.form.get(this.editFolderList) as FormArray;
    let curValue = list.value[this.editableFolderIdx].Name.toLowerCase();
    let curControl = list.controls[this.editableFolderIdx];
    for (let i = 0; i < list.value.length; i++) {
      if (i == this.editableFolderIdx) {
        continue;
      }

      if (list.value[i].Name.toLowerCase() === curValue) {
        curControl.patchValue({ IsValid: false });
        return;
      }
    }

    curControl.patchValue({ IsValid: true });
  }

  //edit folder name logic

  editFolder(listName: string, idx: number) {
    this.saveNewFolder();
    this.saveFolderChanges();

    this.editFolderList = listName;
    this.editableFolderIdx = idx;
    let list = this.form.get(listName) as FormArray;
    let folder = list.controls[idx];

    this.previousEditableFolderName = folder.value.Name;
    folder.patchValue({ IsEditable: true, IsValid: true });
  }

  saveFolderChanges(onSave?: boolean) {
    if (this.editableFolderIdx == -1) {
      return;
    }
    this.trimEditableFolderName();

    let list = this.form.get(this.editFolderList) as FormArray;

    if (!list.value[this.editableFolderIdx].IsValid) {
      if (onSave) {
        this.showFolderNameIncorrect();
      } else {
        this.discardFolderChanges();
      }
      return;
    }

    //delete item if empty name on save
    if (list.value[this.editableFolderIdx].Name.length == 0) {
      list.removeAt(this.editableFolderIdx);
    } else {
      list.controls[this.editableFolderIdx].patchValue({
        IsEditable: false,
        IsValid: true,
      });
    }

    this.previousEditableFolderName = '';
    this.editableFolderIdx = -1;
    this.isEditableFolderValid = false;
    this.editFolderList = '';
  }

  discardFolderChanges() {
    if (this.editableFolderIdx == -1) {
      return;
    }

    let list = this.form.get(this.editFolderList) as FormArray;
    list.controls[this.editableFolderIdx].patchValue({
      IsEditable: false,
      IsValid: true,
      Name: this.previousEditableFolderName,
    });

    this.previousEditableFolderName = '';
    this.editableFolderIdx = -1;
    this.isEditableFolderValid = false;
    this.editFolderList = '';
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
