import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { AccountsService } from 'src/app/shared/services/account.service';
import { DeviceService } from 'src/app/shared/services/device.service';
import { PurchaseService } from 'src/app/shared/services/purchase.service';
import { SyncService } from 'src/app/shared/services/sync.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
  animations: [
    trigger('smileAnimation', [
      state('in', style({ opacity: 1 })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate(
          '0.4s ease',
          style({
            opacity: 1,
          })
        ),
      ]),
      transition('* => void', [
        style({ opacity: 1 }),
        animate(
          '0.4s ease',
          style({
            opacity: 0,
          })
        ),
      ]),
    ]),
  ],
})
export class StatusComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  currentUser: any;
  progressBar: any;
  autoSyncStatus: boolean;
  activeAccounts = 0;
  stats: any;
  subscriptionEndDateValid = false;
  temp: any = [];
  countVisible: boolean;
  smileVisible: boolean;
  failedStatuses = [3, 9, 10, 12, 13];
  successStatuses = [11];
  sharingApp: boolean;
  showSharingFirst: boolean;
  smallScr: boolean;
  isPurchaseLogicAvailable = false;
  startSync = true;
  maxContactLimitation: boolean = false;
  isOnlyContactEnabled: boolean = false;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private accountsService: AccountsService,
    private syncService: SyncService,
    private userService: UserBaseService,
    private deviceService: DeviceService,
    private dialog: NgbModal,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private purchaseService: PurchaseService
  ) {}

  ngOnInit() {
    //subscription valid?

    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.currentUser = resp;

        if (this.currentUser) {
          const today = new Date();
          if (
            this.currentUser.Subscription &&
            new Date(this.currentUser.Subscription.EndDate) > today
          ) {
            this.subscriptionEndDateValid = true;
          } else {
            this.subscriptionEndDateValid = false;
          }

          this.autoSyncStatus = this.currentUser.IsAutoSyncEnabled;
        }
      });

    // set size property
    this.smallScr = window.innerWidth < 420;

    this.isPurchaseLogicAvailable =
      this.deviceService.isPurchaseLogicAvailabe();

    this.activatedRoute.queryParams.subscribe((params) => {
      if (this.activeAccounts > 1) {
        this.sync();
      } else {
        this.startSync = params['startSync'] === 'true';
      }
    });

    // sharing app?
    this.deviceService.isShareApp
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        this.sharingApp = resp;
      });

    // subscribe for progress info
    this.syncService.progress
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.progressBar = resp;

        // update UI
        this.changeDetectorRef.detectChanges();

        // update page if progress == 100
        if (
          this.progressBar &&
          this.progressBar.actionType === 'Sync' &&
          this.progressBar.progress === 100 &&
          this.progressBar.update
        ) {
          this.load();

          // data load started, no need to update again
          this.progressBar.update = false;
          this.syncService.setProgress(this.progressBar);
        }
      });

    // get active accounts count
    this.accountsService.activeAccounts
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: number) => {
        this.activeAccounts = resp;

        if (this.activeAccounts > 1 && this.startSync) {
          this.startSync = false;
          this.sync();
        }
      });

    // load data
    this.load();
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  load() {
    // load accounts
    this.accountsService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((re: any) => {
        // try to load stats then enough accounts found
        if (re.SyncAccounts.length > 0) {
          // load type stats
          this.syncService
            .getTypeStats()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((resp: any) => {
              // set data
              this.stats = {
                Graph: {
                  0: {},
                  1: {},
                  2: {},
                  3: {},
                },
                Enabled: resp.Enabled,
                Types: [],
                TotalCount: {
                  Failed: 0,
                  Type: 'Total',
                },
                Accounts: resp.Accounts,
                Date: resp.Date,
                Filtered: resp.Filtered,
                Total: resp.Total,
                TooOld: resp.TooOld,
                OverallStatus: resp.OverallStatus,
                Icon: 'exclamark',
                ShareOverallStatus: resp.ShareOverallStatus,
                SharingCount: resp.SharingCount,
                SharingIcon: resp.SharingIcon,
                ManualSyncAvailable: true,
              };

              if (this.stats.Accounts && this.stats.Accounts.length > 0) {
                for (var i = 0; i < this.stats.Accounts.length; i++) {
                  var otherIdx = -1;
                  for (var j = 0; j < re.SyncAccounts.length; j++) {
                    if (
                      re.SyncAccounts[j].AccountId == this.stats.Accounts[i].Id
                    ) {
                      otherIdx = j;
                      break;
                    }
                  }

                  if (otherIdx < 0) {
                    continue;
                  }

                  this.stats.Accounts[i].IsSyncCompanySource =
                    re.SyncAccounts[otherIdx].HasBulkCredentials;
                  this.stats.Accounts[i].IsAlternativeCredentials =
                    re.SyncAccounts[otherIdx].IsAlternativeCredentials;
                }
              }

              // calculate addtional status line
              if (resp.ManualSyncStatus && !resp.ManualSyncStatus.IsAvailable) {
                let string_in;

                this.stats.ManualSyncAvailable = false;

                this.translate.get('IN').subscribe((res: string) => {
                  string_in =
                    res + ' ' + resp.ManualSyncStatus.LeftTillRestartValue;
                });

                switch (resp.ManualSyncStatus.LeftTillRestartType) {
                  case 2:
                    let string_months;

                    this.translate.get('MONTHS').subscribe((res: string) => {
                      string_months = res;
                    });

                    this.translate
                      .get('SYNC_SUBSTATUS', {
                        var0: string_in + ' ' + string_months,
                      })
                      .subscribe((res: string) => {
                        this.stats.ManualSyncStatus = res;
                      });

                    break;

                  case 3:
                    if (resp.ManualSyncStatus.LeftTillRestartValue < 2) {
                      let string_tomorrow;

                      this.translate
                        .get('TOMORROW')
                        .subscribe((res: string) => {
                          string_tomorrow = res;
                        });

                      this.translate
                        .get('SYNC_SUBSTATUS', { var0: string_tomorrow })
                        .subscribe((res: string) => {
                          this.stats.ManualSyncStatus = res;
                        });
                    } else {
                      let string_days;

                      this.translate.get('DAYS').subscribe((res: string) => {
                        string_days = res;
                      });

                      this.translate
                        .get('SYNC_SUBSTATUS', {
                          var0: string_in + ' ' + string_days,
                        })
                        .subscribe((res: string) => {
                          this.stats.ManualSyncStatus = res;
                        });
                    }

                    break;

                  case 4:
                    let string_hours;

                    this.translate.get('HOURS').subscribe((res: string) => {
                      string_hours = res;
                    });

                    this.translate
                      .get('SYNC_SUBSTATUS', {
                        var0: string_in + ' ' + string_hours,
                      })
                      .subscribe((res: string) => {
                        this.stats.ManualSyncStatus = res;
                      });

                    break;

                  case 5:
                    let string_hour;

                    this.translate
                      .get('LESS_THAN_HOUR')
                      .subscribe((res: string) => {
                        string_hour = res;
                      });

                    this.translate
                      .get('SYNC_SUBSTATUS', { var0: string_hour })
                      .subscribe((res: string) => {
                        this.stats.ManualSyncStatus = res;
                      });

                    break;
                }
              } else if (
                this.currentUser &&
                this.currentUser.IsTrial &&
                this.isPurchaseLogicAvailable
              ) {
                let periodName = 'DAYS';
                let periodValue = this.currentUser.DaysTillTrialEnd;

                if (this.currentUser.DaysTillTrialEnd === 0) {
                  periodName = 'HOURS';
                  periodValue = this.currentUser.HoursTillTrialEnd;
                }

                let periodTranslation;

                this.translate.get(periodName).subscribe((res: string) => {
                  periodTranslation = res;
                });

                this.translate
                  .get('UNLIMITED_SYNC_VALID_TILL', {
                    var0: periodValue + ' ' + periodTranslation,
                  })
                  .subscribe((res: string) => {
                    this.stats.ManualSyncStatus = res;
                  });
              }

              if (this.failedStatuses.indexOf(this.stats.OverallStatus) > -1) {
                this.stats.Icon = 'sad';
              } else if (
                this.successStatuses.indexOf(this.stats.OverallStatus) > -1
              ) {
                this.stats.Icon = 'smile';
              }

              if (resp.Enabled) {
                this.stats.Enabled.forEach((x: string) => {
                  if (x === 'Contact' && resp.Enabled.length === 1) {
                    this.isOnlyContactEnabled = true;
                  }
                });
              }

              // calculate remaining data & set order
              if (resp.Types[0]) {
                for (const i of Object.keys(resp.Types)) {
                  switch (resp.Types[i].Type) {
                    case 'Contact':
                      if (resp.Types[i].Limited > 0) {
                        this.maxContactLimitation = true;
                      }
                      this.setData(resp.Types[i], 0);
                      break;
                    case 'Calendar':
                      this.setData(resp.Types[i], 1);
                      break;
                    case 'Task':
                      this.setData(resp.Types[i], 2);
                      break;
                  }
                }

                // animate bars
                for (const i of Object.keys(this.stats.Types)) {
                  this.animateBars(parseInt(i), this.stats.Types[i]);
                }

                // animate filtered stats
                this.animateBars(3, {
                  Failed: 0,
                  Synced: this.stats.Filtered,
                  Total: this.stats.Total,
                  NotSynced: this.stats.Total - this.stats.Filtered,
                });
              }

              setTimeout(() => {
                this.stats.Graph = this.temp;
                this.countVisible = true;
              });

              // count which block to show first, Sync or Share?
              this.countBlock();
            });
        } else {
          // set data
          this.stats = {
            Types: [...Array(3)],
            TooOld: false,
            Icon: 'exclamark',
            OverallStatus: 1,
            ShareOverallStatus: 1,
            SharingIcon: 'exclamark',
          };
        }

        // count which block to show first, Sync or Share?
        this.countBlock();
      });
  }

  countBlock() {
    // count which block to show first, Sync or Share?
    if (
      (this.stats &&
        this.stats.ShareOverallStatus &&
        this.currentUser &&
        this.sharingApp) ||
      (this.currentUser &&
        this.stats &&
        this.stats.ShareOverallStatus === 4 &&
        this.stats.OverallStatus !== 11)
    ) {
      this.showSharingFirst = true;
    }
  }

  setData(type: any, index: number) {
    this.stats.Types[index] = type;
    this.stats.Types[index].Total = type.Failed + type.NotSynced + type.Synced;
    this.stats.TotalCount.Failed += type.Failed;
  }

  getIntWidth(int: number) {
    let width = 14;

    switch (int.toString().length) {
      case 1:
        width = this.smallScr ? 20 : 16;
        break;

      case 2:
        width = this.smallScr ? 24 : 22;
        break;

      case 3:
        width = this.smallScr ? 26 : 24;
        break;

      case 4:
        width = this.smallScr ? 28 : 24;
        break;

      case 5:
        width = this.smallScr ? 30 : 28;
        break;

      case 6:
        width = this.smallScr ? 32 : 30;
        break;
    }

    return width;
  }

  animateBars(type: number, data: any) {
    // set widths for bars
    const widths = {
      Synced: (data.Synced * 100) / data.Total,
      NotSynced: (data.NotSynced * 100) / data.Total,
      Failed: (data.Failed * 100) / data.Total,
    };

    // recalculate proportions for bars
    const syncedMinWidth = this.getIntWidth(data.Synced),
      minwidth = 14,
      maxWidth = 30;

    // synced bar part
    if (data.Synced > 0 && widths.Synced < syncedMinWidth) {
      widths.Synced += syncedMinWidth;

      if (widths.NotSynced > maxWidth && widths.Failed > maxWidth) {
        widths.NotSynced -= syncedMinWidth / 2;
        widths.Failed -= syncedMinWidth / 2;
      } else if (widths.NotSynced > maxWidth && widths.Failed < maxWidth) {
        widths.NotSynced -= syncedMinWidth;
      } else if (widths.Failed > maxWidth && widths.NotSynced < maxWidth) {
        widths.Failed -= syncedMinWidth;
      }
    }

    // not synced bar part
    if (data.NotSynced > 0 && widths.NotSynced < minwidth) {
      widths.NotSynced += minwidth;

      if (widths.Synced > maxWidth && widths.Failed > maxWidth) {
        widths.Synced -= minwidth / 2;
        widths.Failed -= minwidth / 2;
      } else if (widths.Synced > maxWidth && widths.Failed < maxWidth) {
        widths.Synced -= minwidth;
      } else if (widths.Failed > maxWidth && widths.Synced < maxWidth) {
        widths.Failed -= minwidth;
      }
    }

    // failed bar part
    if (data.Failed > 0 && widths.Failed < minwidth) {
      widths.Failed += minwidth;

      if (widths.Synced > maxWidth && widths.NotSynced > maxWidth) {
        widths.Synced -= minwidth / 2;
        widths.NotSynced -= minwidth / 2;
      } else if (widths.Synced > maxWidth && widths.NotSynced < maxWidth) {
        widths.Synced -= minwidth;
      } else if (widths.NotSynced > maxWidth && widths.Synced < maxWidth) {
        widths.NotSynced -= minwidth;
      }
    }

    for (const j in data) {
      if (j === 'Synced' || j === 'Failed' || j === 'NotSynced') {
        let width = widths[j],
          padding = 0,
          left = 0;

        if (j === 'Synced') {
          padding = this.smallScr ? 44 : 50;

          // if no items synced or changed, set as 100%
          if (
            data.Synced === 0 &&
            data.Failed === 0 &&
            data.NotSynced === 0 &&
            this.stats.Types.length > 0
          ) {
            width = 100;
          }
        } else if (j === 'Failed') {
          left = data.Synced ? widths.Synced : 0;

          if (data.Synced === 0) {
            padding = this.smallScr ? 44 : 50;
          }
        }

        // set style
        if (!this.temp[type]) {
          this.temp[type] = {};
        }

        this.temp[type][j] = {
          left: left + '%',
          width: width + '%',
          'padding-left': padding + 'px',
        };
      }
    }

    setTimeout(() => {
      this.smileVisible = true;
    }, 1100);
  }

  autoSyncToggle() {
    this.syncService
      .toggleAutoSync()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.autoSyncStatus = !this.autoSyncStatus;
      });
  }

  resetFilters() {
    // confirmation modal
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'RESET_FILTERS',
      text: 'FILTERS_RESET_CONFIRMATION',
      primaryButton: 'yesButton',
    };

    modal.componentInstance.modalData = modalData;

    modal.result.then((data) => {
      if (data === 'primary') {
        this.syncService
          .clearFilters({
            Users: [this.currentUser.Id],
            IsCalendarChecked: true,
            IsContactChecked: true,
            IsTaskChecked: true,
          })
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(() => {
            this.syncService.checkBackupAccounts();
          });
      }
    });
  }

  sync() {
    if (this.maxContactLimitation && this.isOnlyContactEnabled) {
      this.openMaximuOfContacts();
      return;
    }
    this.syncService.checkBackupAccounts();
  }

  reconnect(account: any) {
    if (this.stats.OverallStatus === 5) {
      this.router.navigate(['/synchronization/management']);
      return;
    }

    if (
      account.System === 'Google' ||
      account.System === 'OutlookRest' ||
      account.System === 'Office365' ||
      account.IsSyncCompanySource ||
      (account.System == 'Exchange' && account.IsAlternativeCredentials)
    ) {
      this.accountsService
        .getSyncAccountReconnectUrl({
          Provider: account.System,
          Id: account.Id,
          Migrate: true,
        })
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: any) => {
          window.location.href = resp;
        });
      return;
    }

    // fix model for modal component
    account.AccountId = account.Id;
    account.Username = account.Email;

    // reconnect modal
    this.dialog.reconnectAccount(account).then(
      (resp: any) => {
        if (resp !== 1) {
          return;
        }

        // reload status page data
        this.load();

        const modal = this.dialog.open(ModalComponent, {
          backdrop: 'static',
          keyboard: false,
        });
        let modalData: Modal = {
          title: 'SUCCESS',
          text: 'ACCOUNT_RECONNECTED',
        };
        modal.componentInstance.modalData = modalData;
      },
      () => {
        // do nothing on no
      }
    );
  }

  openHelp(text: string) {
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'INFORMATION',
      text: text,
      support: text === 'ITEMS_SYNCED_DESC' ? false : true,
    };

    modal.componentInstance.modalData = modalData;
  }

  openMaximuOfContacts() {
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'INFO',
      text: 'REACHED_MAXIMUM_NUMBER_OF_CONTACTS',
    };
    modal.componentInstance.modalData = modalData;
  }

  openInfoPlans() {
    if (!this.isPurchaseLogicAvailable || this.subscriptionEndDateValid) {
      this.openHelp('AUTO_SYNC_DESC');
    } else {
      this.openInfoModal('INFORMATION', 'ADD_ACCOUNT_LIMIT_TEXT');
    }
  }

  openInfoModal(title: string, text: string) {
    this.registerDialogIsShown(title);
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: title,
      text: 'AUTO_SYNC_DESC_WITHOUT_SECOND_P',
      primaryButton: 'UPGRADE_NOW_IN_CAPITALS',
      secondaryButton: 'ALWAYS_FREE_IN_CAPITALS',
      buttonTitle: 'RECOMMENDED_PLAN',
      table: [
        {
          td: [
            'FREE_PLAN_DRAWBACK_1',
            'FREE_PLAN_DRAWBACK_2',
            'FREE_PLAN_DRAWBACK_3',
          ],
          th: 'FREE_PLAN_PLAIN',
        },
        {
          td: [
            'PREMIUM_PLAN_BENEFIT_1',
            'PREMIUM_PLAN_BENEFIT_2',
            'PREMIUM_PLAN_BENEFIT_3',
          ],
          th: 'PREMIUM_PLAN',
        },
      ],
    };

    modal.componentInstance.modalData = modalData;
    modal.result.then((data) => {
      if (data === 'primary') {
        this.purchase(title);
        return;
      }
      this.userClickedFreeOffer(title);
    });
  }

  userClickedFreeOffer(title: string) {
    this.purchaseService
      .sendFreePlanOffer('Free plan dialog: ' + title)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {});
  }

  purchase(title: string) {
    this.purchaseService
      .trackUserGoPremiumPress('Free plan dialog: ' + title)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.router.navigate(['/purchase']);
      });
  }

  registerDialogIsShown(title: string) {
    this.purchaseService
      .limitationsDialogIsShown(title)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {});
  }
}
