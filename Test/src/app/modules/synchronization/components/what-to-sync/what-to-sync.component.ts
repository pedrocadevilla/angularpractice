import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { GuidService } from 'src/app/shared/services/guid.service';
import { HelpService } from 'src/app/shared/services/help.service';
import { SyncService } from 'src/app/shared/services/sync.service';
import { DataTypePipe } from 'src/app/shared/utils/data-type-pipe';
import { DefaultFolderNamingPipe } from 'src/app/shared/utils/default-folder-naming-pipe';
import {
  IWTSChoiceInTypeModel,
  IWTSFolderInChoiceModel,
  IWTSFolderInMapModel,
  IWTSMapInTypeModel,
  IWTSType,
  IWTSTypesModel,
} from '../../models/iwts-type-model';
import { IWTSSelectFolderResponse } from '../../models/select-folder-response-model';
import { FiltersService } from '../../services/filter.service';

@Component({
  selector: 'app-what-to-sync',
  templateUrl: './what-to-sync.component.html',
  styleUrls: ['./what-to-sync.component.scss'],
})
export class WhatToSyncComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  progressBar: any;
  loaded = false;
  types: Array<IWTSType>;
  failedAccountsUserNameList: Array<string>;
  showMobileDialog = false;

  constructor(
    private router: Router,
    private filtersService: FiltersService,
    private dialog: NgbModal,
    private translate: TranslateService,
    private syncService: SyncService,
    private guidService: GuidService,
    private helpService: HelpService,
    private dataTypePipe: DataTypePipe,
    private defaultFolderNamingPipe: DefaultFolderNamingPipe
  ) {}

  ngOnInit() {
    // get filters
    this.filtersService
      .getWhatToSync()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (resp: any) => {
          if (resp) {
            this.types = resp.Types;

            this.translateDefaultNames();

            this.prepareErrors();

            this.countNotFailedFoldersCount();

            this.checkIfMobileDialogNeeded();

            this.loaded = true;
          }
        },
        (err) => {
          this.router.navigate(['/synchronization/management'], {
            replaceUrl: true,
          });
        }
      );

    // subscribe for progress info
    this.syncService.progress
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res) => {
        this.progressBar = res;
      });
  }

  checkIfMobileDialogNeeded() {
    //CALENDAR(1el), CONTACTS(2el), TASKS (3el) choices length longer than that is possible to fit in
    if (this.types.find((el) => el.Choices && el.Choices.length > 5)) {
      this.showMobileDialog = true;
    }
  }

  countNotFailedFoldersCount() {
    for (let i = 0; i < this.types.length; i++) {
      for (let j = 0; j < this.types[i].Maps.length; j++) {
        this.types[i].Maps[j].NotFailedAccountsCount = this.types[i].Maps[
          j
        ].Folders.filter((el: any) => !el.IsAccountFailed).length;
      }
    }
  }

  translateDefaultNames() {
    for (let i = 0; i < this.types.length; i++) {
      for (let j = 0; j < this.types[i].Maps.length; j++) {
        for (let z = 0; z < this.types[i].Maps[j].Folders.length; z++) {
          if (
            this.types[i].Maps[j].Folders[z].IsDefault &&
            this.defaultFolderNamingPipe.transform(
              this.types[i].Maps[j].Folders[z].System,
              this.dataTypePipe.transform(this.types[i].Type)
            )
          ) {
            this.types[i].Maps[j].Folders[z].translatedFolderName =
              this.translate.instant(
                this.defaultFolderNamingPipe.transform(
                  this.types[i].Maps[j].Folders[z].System,
                  this.dataTypePipe.transform(this.types[i].Type)
                )
              );
          }
        }
      }

      for (let j = 0; j < this.types[i].Choices.length; j++) {
        for (let z = 0; z < this.types[i].Choices[j].Folders.length; z++) {
          if (
            this.types[i].Choices[j].Folders[z].IsDefault &&
            this.defaultFolderNamingPipe.transform(
              this.types[i].Choices[j].System,
              this.dataTypePipe.transform(this.types[i].Type)
            )
          ) {
            this.types[i].Choices[j].Folders[z].translatedFolderName =
              this.translate.instant(
                this.defaultFolderNamingPipe.transform(
                  this.types[i].Choices[j].System,
                  this.dataTypePipe.transform(this.types[i].Type)
                )
              );
          }
        }
      }
    }
  }

  prepareErrors() {
    this.failedAccountsUserNameList = [];
    //error for dialog
    for (let i = 0; i < this.types.length; i++) {
      for (let j = 0; j < this.types[i].Choices.length; j++) {
        if (
          this.types[i].Choices[j].IsFailed &&
          !this.failedAccountsUserNameList.find(
            (el) => el === this.types[i].Choices[j].UserName
          )
        ) {
          this.failedAccountsUserNameList.push(
            this.types[i].Choices[j].UserName
          );
        }
      }
    }

    if (
      this.failedAccountsUserNameList &&
      this.failedAccountsUserNameList.length > 0
    ) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'ERROR',
        text: this.translate.instant(
          'SOURCES_CANNOT_BE_ACCESSED_PLEASE_RECONNECT_SOURCES_TO_MAP',
          {
            var0:
              this.failedAccountsUserNameList
                .toString()
                .replace(/,/g, '</br>') + '</br>',
          }
        ),
        textTranslated: true,
      };

      modal.componentInstance.modalData = modalData;
    }

    //disable types
    for (let i = 0; i < this.types.length; i++) {
      if (
        this.types[i].Choices.filter((el) => el.IsFailed || !el.IsAccessable)
          .length >=
        this.types[i].Choices.length - 1
      ) {
        this.types[i].isDisabled = true;
      }
    }

    //errors not for dialog
    for (let i = 0; i < this.types.length; i++) {
      this.types[i].failedSourcesList = [];
      this.types[i].noPermissionsSourcesList = [];
      this.types[i].notSupportedSourcesList = [];

      for (let j = 0; j < this.types[i].Choices.length; j++) {
        if (
          this.types[i].Choices[j].IsFailed &&
          !this.types[i].failedSourcesList.some(
            (el) =>
              el.UserName === this.types[i].Choices[j].UserName &&
              el.System === this.types[i].Choices[j].System
          )
        ) {
          this.types[i].failedSourcesList.push({
            UserName: this.types[i].Choices[j].UserName,
            System: this.types[i].Choices[j].System,
          });
        }
        if (
          !this.types[i].Choices[j].IsAccessable &&
          this.types[i].Choices[j].IsSupported &&
          !this.types[i].noPermissionsSourcesList.some(
            (el) =>
              el.UserName === this.types[i].Choices[j].UserName &&
              el.System === this.types[i].Choices[j].System
          )
        ) {
          this.types[i].noPermissionsSourcesList.push({
            UserName: this.types[i].Choices[j].UserName,
            System: this.types[i].Choices[j].System,
          });
        }
        if (
          !this.types[i].Choices[j].IsSupported &&
          !this.types[i].notSupportedSourcesList.some(
            (el) =>
              el.UserName === this.types[i].Choices[j].UserName &&
              el.System === this.types[i].Choices[j].System
          )
        ) {
          this.types[i].notSupportedSourcesList.push({
            UserName: this.types[i].Choices[j].UserName,
            System: this.types[i].Choices[j].System,
          });
        }
      }
    }
  }

  createMap(map: IWTSMapInTypeModel): IWTSMapInTypeModel {
    return {
      GlobalId: map.GlobalId,
      IsSuspended: map.IsSuspended,
      Folders: this.createMapFolderItems(map.Folders),
      Show: map.Show,
      NotFailedAccountsCount: map.Folders.filter((el) => !el.IsAccountFailed)
        .length,
    };
  }

  createMapFolderItems(folders: any): Array<IWTSFolderInMapModel> {
    let array: any = [];
    folders.forEach((folder: any) => {
      array.push(this.createMapFolderItem(folder));
    });

    return array;
  }

  createMapFolderItem(folder: IWTSFolderInMapModel): IWTSFolderInMapModel {
    return {
      UserName: folder.UserName,
      System: folder.System,
      SyncDirectionRights: folder.SyncDirectionRights,
      IsAccountEnabled: folder.IsAccountEnabled,
      IsAccountFailed: folder.IsAccountFailed,
      MapLinkId: folder.MapLinkId,
      ReplicaId: folder.ReplicaId,
      FolderId: folder.FolderId,
      PrimaryId: folder.PrimaryId,
      FolderName: folder.FolderName,
      IsDefault: folder.IsDefault,
      FolderAccessRights: folder.FolderAccessRights,
      wasFolderCreated: folder.wasFolderCreated,
      translatedFolderName: folder.translatedFolderName,
    };
  }

  createChoicesFolderItem(
    folder: IWTSFolderInChoiceModel
  ): IWTSFolderInChoiceModel {
    return {
      FolderId: folder.FolderId,
      PrimaryId: folder.PrimaryId,
      FolderName: folder.FolderName,
      IsDefault: folder.IsDefault,
      FolderAccessRights: folder.FolderAccessRights,
      IsMapped: folder.IsMapped,
      MapLinkId: folder.MapLinkId,
      wasFolderCreated: folder.wasFolderCreated,
      translatedFolderName: folder.translatedFolderName,
    };
  }

  selectSource(typeIndex: number, choice: IWTSChoiceInTypeModel) {
    if (choice.IsFailed) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'ERROR',
        text: this.translate.instant('DATATYPE_FOR_SOURCE_CANT_BE_ACCESSED', {
          var0: this.translate.instant(
            this.dataTypePipe.transform(this.types[typeIndex].Type) +
              '_TYPE'
          ),
          var1: choice.UserName,
        }),
        textTranslated: true,
      };

      modal.componentInstance.modalData = modalData;
    } else if (!choice.IsSupported) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'INFO',
        text: this.translate.instant('DATATYPE_FOR_SOURCE_NOT_SUPPORTED', {
          var0: this.translate.instant(
            this.dataTypePipe.transform(this.types[typeIndex].Type)
          ),
          var1: choice.UserName,
        }),
        textTranslated: true,
      };

      modal.componentInstance.modalData = modalData;
    } else if (!choice.IsAccessable) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'INFO',
        text: this.translate.instant('NO_ACCESS_FOR_DATATYPE_FOR_SOURCE', {
          var0: this.translate
            .instant(
              this.dataTypePipe.transform(
                this.types[typeIndex].Type
              ) + '_TYPE'
            )
            .toLowerCase(),
          var1: choice.UserName,
        }),
        textTranslated: true,
      };

      modal.componentInstance.modalData = modalData;
    }
  }

  selectFolderMobile(typeIndex: number, mapIndex: number) {
    this.dialog
      .selectFolder({
        title:
          this.dataTypePipe.transform(this.types[typeIndex].Type) ===
          'CONTACTS'
            ? this.translate.instant('SELECT_CONTACTS_FOLDER_GROUP')
            : this.dataTypePipe.transform(
                this.types[typeIndex].Type
              ) === 'CALENDAR'
            ? this.translate.instant('SELECT_CALENDAR')
            : this.translate.instant('SELECT_TASKS_FOLDER_LIST'),
        types: this.types,
        typeIndex: typeIndex,
        mapIndex: mapIndex,
      })
      .then(
        (resp: IWTSSelectFolderResponse) => {
          if (resp && resp.folderInChoicesIndex! >= 0) {
            this.checkBeforeFolderAdd(
              resp.typeIndex,
              resp.mapIndex,
              resp.choiceIndex,
              resp.folderInChoicesIndex!
            );
          } else if (resp) {
            this.createNewGroupFolder(
              resp.typeIndex,
              resp.mapIndex,
              resp.choiceIndex
            );
          }
        },
        () => {
          // do nothing on no
          return;
        }
      );
  }

  createNewGroupFolder(
    typeIndex: number,
    mapIndex: number,
    choiceIndex: number
  ) {
    let dataType = this.types[typeIndex].Type;
    const dataSystem = this.types[typeIndex].Choices[choiceIndex].System;
    let choiceFolders = this.types[typeIndex].Choices[choiceIndex].Folders;
    this.dialog
      .createFolder({
        title: this.translate.instant(
          'NEW_' + this.dataTypePipe.transform(dataType)
        ),
        text: this.translate.instant(
          'ENTER_FOLDER_NAME_' +
            this.dataTypePipe.transform(dataType)
        ),
        dataType: this.dataTypePipe.transform(dataType),
        dataSystem,
      })
      .then(
        (resp: string) => {
          let createdFolder = this.createChoicesFolderItem({
            FolderId: '',
            PrimaryId: '',
            FolderName: resp,
            IsDefault: false,
            FolderAccessRights: 4,
            IsMapped: true,
            MapLinkId: this.guidService.newGuid(),
            wasFolderCreated: true,
            translatedFolderName: '',
          });

          choiceFolders.push(createdFolder);

          this.checkBeforeFolderAdd(
            typeIndex,
            mapIndex,
            choiceIndex,
            choiceFolders.length - 1,
            true
          );
        },
        () => {
          // do nothing on no
          return;
        }
      );
  }

  checkBeforeFolderAdd(
    typeIndex: number,
    mapIndex: number,
    choiceIndex: number,
    folderInChoicesIndex: number,
    wasChoiceCreated?: boolean
  ) {
    let choices = this.types[typeIndex].Choices;
    let choice = choices[choiceIndex];
    let choiceFolder = choice.Folders[folderInChoicesIndex];
    let sameReplicaFolderInMapMapLinkId: any;

    if (
      this.types[typeIndex].Maps[mapIndex].Folders.find(
        (el) =>
          el.ReplicaId === choice.ReplicaId &&
          el.MapLinkId !== choiceFolder.MapLinkId
      )
    ) {
      sameReplicaFolderInMapMapLinkId = this.types[typeIndex].Maps[
        mapIndex
      ].Folders.find(
        (el) =>
          el.ReplicaId === choice.ReplicaId &&
          el.MapLinkId !== choiceFolder.MapLinkId
      )?.MapLinkId;
    }

    if (
      sameReplicaFolderInMapMapLinkId &&
      (!choiceFolder.IsMapped || wasChoiceCreated)
    ) {
      //find same replica folder in map, delete it and remove from choices
      this.types[typeIndex].Maps[mapIndex].Folders.splice(
        this.types[typeIndex].Maps[mapIndex].Folders.findIndex(
          (el) => el.MapLinkId === sameReplicaFolderInMapMapLinkId
        ),
        1
      );
      for (let i = 0; i < this.types[typeIndex].Choices.length; i++) {
        for (
          let j = 0;
          j < this.types[typeIndex].Choices[i].Folders.length;
          j++
        ) {
          if (
            this.types[typeIndex].Choices[i].Folders[j].MapLinkId ===
            sameReplicaFolderInMapMapLinkId
          ) {
            this.types[typeIndex].Choices[i].Folders[j].MapLinkId = '';
            this.types[typeIndex].Choices[i].Folders[j].IsMapped = false;
          }
        }
      }

      choiceFolder.MapLinkId = this.guidService.newGuid();
      choiceFolder.IsMapped = true;

      this.addFolderToMap(typeIndex, mapIndex, choice, choiceFolder);
    } else if (choiceFolder.IsMapped && !wasChoiceCreated) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'CONFIRM',
        text: 'FOLDER_GROUP_IS_ALREADY_MAPPED',
        primaryButton: 'REMAP',
        secondaryButton: 'CANCEL',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then((data) => {
        if (data === 'primary') {
          for (let i = 0; i < this.types[typeIndex].Maps.length; i++) {
            for (
              let j = 0;
              j < this.types[typeIndex].Maps[i].Folders.length;
              j++
            ) {
              if (
                this.types[typeIndex].Maps[i].Folders[j].MapLinkId ===
                choiceFolder.MapLinkId
              ) {
                this.types[typeIndex].Maps[i].Folders.splice(j, 1);
              }
            }
          }

          this.addFolderToMap(typeIndex, mapIndex, choice, choiceFolder);

          if (sameReplicaFolderInMapMapLinkId) {
            //find samee replice folder in map, delete it and remove from choices
            this.types[typeIndex].Maps[mapIndex].Folders.splice(
              this.types[typeIndex].Maps[mapIndex].Folders.findIndex(
                (el) => el.MapLinkId === sameReplicaFolderInMapMapLinkId
              ),
              1
            );
            for (let i = 0; i < this.types[typeIndex].Choices.length; i++) {
              for (
                let j = 0;
                j < this.types[typeIndex].Choices[i].Folders.length;
                j++
              ) {
                if (
                  this.types[typeIndex].Choices[i].Folders[j].MapLinkId ===
                  sameReplicaFolderInMapMapLinkId
                ) {
                  this.types[typeIndex].Choices[i].Folders[j].MapLinkId = '';
                  this.types[typeIndex].Choices[i].Folders[j].IsMapped = false;
                }
              }
            }
          }
        }
      });
    } else {
      choiceFolder.MapLinkId = this.guidService.newGuid();
      choiceFolder.IsMapped = true;

      this.addFolderToMap(typeIndex, mapIndex, choice, choiceFolder);
    }
  }

  addFolderToMap(
    typeIndex: number,
    mapIndex: number,
    choice: IWTSChoiceInTypeModel,
    folder: IWTSFolderInChoiceModel,
    folderInMapIndex?: number
  ) {
    let mapsFolders = this.types[typeIndex].Maps[mapIndex].Folders;

    let folderFromChoice: IWTSFolderInMapModel = {
      UserName: choice.UserName,
      System: choice.System,
      SyncDirectionRights: choice.SyncDirectionRights,
      IsAccountEnabled: true,
      IsAccountFailed: choice.IsFailed,
      ReplicaId: choice.ReplicaId,
      MapLinkId: folder.MapLinkId ?? null,
      FolderId: folder.FolderId ?? null,
      PrimaryId: folder.PrimaryId ?? null,
      FolderName: folder.FolderName ?? null,
      FolderAccessRights: folder.FolderAccessRights ?? null,
      IsDefault: folder.IsDefault ?? null,
      wasFolderCreated: folder.wasFolderCreated ?? undefined,
      translatedFolderName: folder.translatedFolderName ?? undefined,
    };

    mapsFolders.push(this.createMapFolderItem(folderFromChoice));

    this.countNotFailedFoldersCount();
  }

  addMap(typeIndex: number) {
    let maps = this.types[typeIndex].Maps;
    maps.push(
      this.createMap({
        GlobalId: this.guidService.newGuid(),
        IsSuspended: false,
        Folders: [],
        Show: true,
        NotFailedAccountsCount: 0,
      })
    );
  }

  deleteEmptyMap() {
    for (let i = 0; i < this.types.length; i++) {
      for (let j = 0; j < this.types[i].Maps.length; j++) {
        if (this.types[i].Maps[j].Folders.length === 0) {
          this.types[i].Maps.splice(j, 1);
        }
      }
    }
  }

  deleteFolder(typeIndex: number, mapIndex: number, folderIndex: number) {
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'CONFIRM',
      text: 'REMOVE_FOLDER_ARE_YOU_SURE',
      primaryButton: 'yesButton',
    };

    modal.componentInstance.modalData = modalData;

    modal.result.then((data) => {
      if (data === 'primary') {
        for (let i = 0; i < this.types[typeIndex].Choices.length; i++) {
          for (
            let j = 0;
            j < this.types[typeIndex].Choices[i].Folders.length;
            j++
          ) {
            if (
              this.types[typeIndex].Choices[i].Folders[j].MapLinkId ===
              this.types[typeIndex].Maps[mapIndex].Folders[folderIndex]
                .MapLinkId
            ) {
              this.types[typeIndex].Choices[i].Folders[j].MapLinkId = '';
              this.types[typeIndex].Choices[i].Folders[j].IsMapped = false;
            }
          }
        }

        this.types[typeIndex].Maps[mapIndex].Folders.splice(folderIndex, 1);

        this.countNotFailedFoldersCount();
      }
    });
  }

  deleteMap(typeIndex: number, mapIndex: number) {
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'CONFIRM',
      text: 'REMOVE_FOLDER_GROUP_ARE_YOU_SURE',
      primaryButton: 'yesButton',
    };

    modal.componentInstance.modalData = modalData;

    modal.result.then((data) => {
      if (data === 'primary') {
        for (let i = 0; i < this.types[typeIndex].Choices.length; i++) {
          for (
            let j = 0;
            j < this.types[typeIndex].Choices[i].Folders.length;
            j++
          ) {
            for (
              let z = 0;
              z < this.types[typeIndex].Maps[mapIndex].Folders.length;
              z++
            ) {
              if (
                this.types[typeIndex].Choices[i].Folders[j].MapLinkId ===
                this.types[typeIndex].Maps[mapIndex].Folders[z].MapLinkId
              ) {
                this.types[typeIndex].Choices[i].Folders[j].MapLinkId = '';
                this.types[typeIndex].Choices[i].Folders[j].IsMapped = false;
              }
            }
          }
        }

        this.types[typeIndex].Maps.splice(mapIndex, 1);
      }
    });
  }

  toggleMap(typeIndex: number, mapIndex: number) {
    if (!this.types[typeIndex].Maps[mapIndex].IsSuspended) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'CONFIRM',
        text: 'DISABLE_FOLDER_GROUP_ARE_YOU_SURE',
        primaryButton: 'yesButton',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then((data) => {
        if (data === 'primary') {
          this.types[typeIndex].Maps[mapIndex].IsSuspended =
            !this.types[typeIndex].Maps[mapIndex].IsSuspended;
        } else {
          this.types[typeIndex].Maps[mapIndex].IsSuspended =
            !this.types[typeIndex].Maps[mapIndex].IsSuspended;
        }
      });
    }
  }

  toggleIsEnabled(typeIndex: number) {
    this.types[typeIndex].IsEnabled = !this.types[typeIndex].IsEnabled;
  }

  checkBeforeSave() {
    let showOneFolderInMapError = false;
    for (let i = 0; i < this.types.length; i++) {
      if (
        this.types[i].IsEnabled &&
        !this.types[i].isDisabled &&
        this.types[i].Maps.find(
          (map) => !map.IsSuspended && map.Folders && map.Folders.length === 1
        )
      ) {
        showOneFolderInMapError = true;
      }
    }

    if (showOneFolderInMapError) {
      const modal = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'INFO',
        text: 'UNMAPPED_FOLDER_GROUP_ARE_YOU_SURE',
        primaryButton: 'yesButton',
      };

      modal.componentInstance.modalData = modalData;

      modal.result.then((data) => {
        if (data === 'primary') {
          this.save();
        }
      });
    } else {
      this.save();
    }
  }

  save() {
    this.deleteEmptyMap();
    this.filtersService
      .updateWhatToSync({ Types: this.types })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((_) => {
        this.router.navigate(['/synchronization/management']);
      });
  }

  advancedSettings() {
    this.dialog.advancedSettings(this.types).then(
      (resp: Array<IWTSType>) => {
        for (let i = 0; i < this.types.length; i++) {
          for (let j = 0; j < resp.length; j++) {
            if (this.types[i].Type === resp[j].Type) {
              this.types[i].AutoSyncNewFolders = resp[j].AutoSyncNewFolders;
              this.types[i].AutoDeleteRemovedFolders =
                resp[j].AutoDeleteRemovedFolders;
            }
          }
        }
      },
      () => {
        // do nothing on no
      }
    );
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  openHelp() {
    this.helpService.open();
  }
}
