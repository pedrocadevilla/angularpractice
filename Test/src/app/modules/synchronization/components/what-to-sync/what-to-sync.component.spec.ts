import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatToSyncComponent } from './what-to-sync.component';

describe('WhatToSyncComponent', () => {
  let component: WhatToSyncComponent;
  let fixture: ComponentFixture<WhatToSyncComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WhatToSyncComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatToSyncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
