import { Component, NgModule, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { AccountsService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.scss'],
})
export class FinishComponent implements OnInit {
  private ngUnsubscribe: Subject<any> = new Subject();
  isLoaded = false;
  responseReceived = false;
  isError = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private accountsService: AccountsService,
    private dialog: NgbModal,
    private translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.isError = params['errorCode'] ? true : false;
      if (this.isError) {
        const modalTerm = this.dialog.open(ModalComponent, {
          backdrop: 'static',
          keyboard: false,
        });
        let modalData: Modal = {
          title: 'ERROR',
          text: params['errorCode'],
        };

        modalTerm.componentInstance.modalData = modalData;

        return;
      }
      this.activatedRoute.paramMap.subscribe((params) => {
        let inviteId: string = params.get('id')!;
        this.accountsService
          .completeAcceptSyncAccountInvite(inviteId)
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(
            (resp: any) => {
              this.responseReceived = true;
              this.isLoaded = true;
            },
            (err) => {
              this.isLoaded = true;
              this.isError = true;
            }
          );
      });
    });
  }
}
