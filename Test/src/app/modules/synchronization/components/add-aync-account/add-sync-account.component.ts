import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { AddModel } from 'src/app/shared/models/account-model';
import { FeaturesEnum } from 'src/app/shared/models/features-model';
import { Modal } from 'src/app/shared/models/modal-model';
import { AccountsService } from 'src/app/shared/services/account.service';
import { DeviceService } from 'src/app/shared/services/device.service';
import { FeaturesService } from 'src/app/shared/services/features.service';
import { PurchaseService } from 'src/app/shared/services/purchase.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { SyncAccountsFeature } from '../../models/sync-account-feature-model';
import { SyncCompanyModel } from '../../models/sync-companies-model';

@Component({
  selector: 'app-add-sync-account',
  templateUrl: './add-sync-account.component.html',
  styleUrls: ['./add-sync-account.component.scss'],
})
export class AddSyncAccountComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  currentUser: any;
  sync2cloudAccept: boolean;
  inviteid: string;
  sharingApp: boolean;
  subscriptionEndDateValid: boolean = false;
  returnUrl: string;
  activeAccounts = 0;
  mobile: boolean;
  isPurchaseLogicAvailable: boolean;
  isPrePaid: boolean;
  isBeta: boolean = false;
  isAlpha: boolean = false;
  syncCompanies: SyncCompanyModel[];
  isNotEnterprise: boolean = true;
  syncAccountsFeature: SyncAccountsFeature;

  constructor(
    private accountsService: AccountsService,
    private userService: UserBaseService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialog: NgbModal,
    private deviceService: DeviceService,
    private translate: TranslateService,
    private featuresService: FeaturesService,
    private purchaseService: PurchaseService
  ) {
    this.sync2cloudAccept = false;
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.sync2cloudAccept = params['sync2cloud'] === 'true';
      this.inviteid = params['inviteid'];
      this.returnUrl = params['returnUrl'];
    });

    this.accountsService
      .getSyncCompanies()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        this.syncCompanies = resp;
        for (let i = 0; i < this.syncCompanies.length; i++) {
          let system =
            this.syncCompanies[i].syncSystem === 'Office365'
              ? 'OutlookRest'
              : this.syncCompanies[i].syncSystem;
          this.syncCompanies[i].systemSvg =
            '/Images/svg-sprite.svg#' + system.toLowerCase() + '-circle-icon';
        }
      });

    //subscription valid?
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.currentUser = resp;

        if (this.currentUser) {
          if (this.currentUser.Subscription) {
            this.isPrePaid = this.currentUser.Subscription.IsPrePaid;
          }
          const today = new Date();
          if (
            this.currentUser.Subscription &&
            new Date(this.currentUser.Subscription.EndDate) > today
          ) {
            this.subscriptionEndDateValid = true;
          } else {
            this.subscriptionEndDateValid = false;
          }

          this.isNotEnterprise =
            this.isPrePaid ||
            (this.currentUser && !this.currentUser.Subscription) ||
            (this.currentUser &&
              this.currentUser.Subscription &&
              this.currentUser.Subscription.SubscriptionStatus === 'New') ||
            (this.currentUser &&
              this.currentUser.Subscription &&
              !this.subscriptionEndDateValid);
        }
      });

    // sharing app?
    this.deviceService.isShareApp
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: boolean) => {
        this.sharingApp = resp;
      });

    // subscribe for user info
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.currentUser = resp;
        this.isAlpha = this.userService.isInRole('Alpha Tester');
        this.isBeta = this.isAlpha || this.userService.isInRole('Beta Tester');
      });

    this.accountsService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: number) => {
        this.accountsService.activeAccounts
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe((resp: number) => {
            this.activeAccounts = resp;
          });
      });

    // mobile device? AAA
    this.mobile = this.deviceService.isMobileApp();

    this.isPurchaseLogicAvailable =
      this.deviceService.isPurchaseLogicAvailabe();

    this.featuresService
      .getFeature<SyncAccountsFeature>(FeaturesEnum.SyncAccounts)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        this.syncAccountsFeature = resp;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  select(provider: string) {
    if (!this.validate(provider)) return;

    if (
      provider === 'Google' ||
      provider === 'Office365' ||
      provider === 'OutlookRest' ||
      provider === 'SalesForce'
    ) {
      if (this.sync2cloudAccept == true) {
        this.returnUrl =
          '/share/accept-on-account?inviteid=' +
          this.inviteid +
          '&system=google&select=1&sync2cloud=true';
      }

      let model: AddModel = { Provider: provider, ReturnUrl: this.returnUrl };

      this.accountsService
        .add(model)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((resp: any) => {
          window.location.href = resp;
        });
    } else {
      let params: {} = {};
      if (this.sync2cloudAccept === true) {
        params = {
          inviteid: this.inviteid,
          sync2cloud: true,
          returnUrl: this.returnUrl,
        };
      }
      this.router.navigate(
        ['/synchronization/addsyncaccount/add-' + provider.toLowerCase()],
        { queryParams: params }
      );
    }
  }

  addFromSyncCompany(syncCompany: SyncCompanyModel) {
    if (!this.validate()) return;

    this.router.navigate(['/synchronization/bulkaddoutlook'], {
      queryParams: { tenant: syncCompany.externalId },
    });
  }

  selectBulk(provider: string) {
    if (!this.validate(provider, true)) return;

    this.accountsService
      .bulkAdd({ Provider: provider })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        window.location.href = resp;
      });
  }

  openHelp(title: string, text: string, button: string) {
    const modalTerm = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: this.translate.instant(title, {
        var0: this.currentUser.SyncAccountsLimit,
      }),
      text: this.translate.instant(text, {
        var0: this.currentUser.SyncAccountsLimit,
      }),
      primaryButton: button,
    };

    modalTerm.componentInstance.modalData = modalData;

    modalTerm.result.then((data: string) => {
      if (data === 'primary') {
        if (button === 'LEARN_MORE') {
          window.open('http://www.syncgene.com?external=true', '_blank');
          return;
        }
        window.open('http://www.syncgene.com/support?external=true', '_blank');
      }
    });
  }

  validate(provider?: string, isBulk?: boolean) {
    //TODO: for Tadas
    // use this.syncAccountsFeature for calculations
    //possible error codes: ACCOUNT_LIMIT_REACHED_GO_PREMIUM and ACCOUNT_LIMIT_REACHED_CONTACT_SUPPORT
    //first error for free users, another one for paid

    let clickedEnterprise =
      (isBulk && provider === 'Office365') ||
      (isBulk && provider === 'Exchange') ||
      provider === 'SalesForce';
    if (
      (!clickedEnterprise && this.currentUser.CanAddSyncAccount) ||
      (clickedEnterprise &&
        this.subscriptionEndDateValid &&
        !this.isPrePaid &&
        this.currentUser.CanAddSyncAccount &&
        this.currentUser.Subscription.SubscriptionStatus != 'New')
    ) {
      return true;
    } else {
      if (
        this.subscriptionEndDateValid &&
        this.currentUser.Subscription.SubscriptionStatus != 'New'
      ) {
        if (clickedEnterprise) {
          !this.isPrePaid
            ? this.openHelp(
                'CANNOT_ADD_ACCOUNT_CONTACT_SUPPORT_TITLE',
                'CANNOT_ADD_ACCOUNT_CONTACT_SUPPORT_TEXT',
                'CONTACT_US'
              )
            : this.openUpgradeModal(
                'UPGRADE_TO_ENTERPRISE',
                'UPGRADE_TO_ENTERPRISE_TO_UNLOCK'
              );
        } else {
          this.openHelp(
            'CANNOT_ADD_ACCOUNT_CONTACT_SUPPORT_TITLE',
            'CANNOT_ADD_ACCOUNT_CONTACT_SUPPORT_TEXT',
            'CONTACT_US'
          );
        }
      } else {
        if (!this.isPurchaseLogicAvailable) {
          clickedEnterprise
            ? this.openUpgradeModal(
                'UPGRADE_TO_ENTERPRISE',
                'UPGRADE_TO_ENTERPRISE_TO_UNLOCK'
              )
            : this.openHelp(
                'ADD_ACCOUNT_LIMIT_TITLE_VAR0',
                'ADD_ACCOUNT_LIMIT_TEXT',
                'LEARN_MORE'
              );
        } else {
          clickedEnterprise
            ? this.openUpgradeModal(
                'UPGRADE_TO_ENTERPRISE',
                'UPGRADE_TO_ENTERPRISE_TO_UNLOCK'
              )
            : this.openInfoModal(
                this.translate.instant('ADD_ACCOUNT_LIMIT_TITLE_VAR0', {
                  var0: this.currentUser.SyncAccountsLimit,
                }),
                'ADD_ACCOUNT_LIMIT_TEXT'
              );
        }
      }
    }

    return false;
  }

  openInfoModal(title: string, text: string) {
    this.registerDialogIsShown(title);
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: title,
      titleTranslated: true,
      textArray: [text, 'FREE_PREMIUM_DIFFERENCE'],
      primaryButton: 'UPGRADE_NOW_IN_CAPITALS',
      secondaryButton: 'ALWAYS_FREE_IN_CAPITALS',
      buttonTitle: 'RECOMMENDED_PLAN',
      table: [
        {
          td: [
            'FREE_PLAN_DRAWBACK_1',
            'FREE_PLAN_DRAWBACK_2',
            'FREE_PLAN_DRAWBACK_3',
          ],
          th: 'FREE_PLAN_PLAIN',
        },
        {
          td: [
            'PREMIUM_PLAN_BENEFIT_1',
            'PREMIUM_PLAN_BENEFIT_2',
            'PREMIUM_PLAN_BENEFIT_3',
          ],
          th: 'PREMIUM_PLAN',
        },
      ],
    };

    modal.componentInstance.modalData = modalData;
    modal.result.then((data) => {
      if (data === 'primary') {
        this.purchase(title);
        return;
      }
      this.userClickedFreeOffer(title);
    });
  }

  userClickedFreeOffer(title: string) {
    this.purchaseService
      .sendFreePlanOffer('Free plan dialog: ' + title)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {});
  }

  purchase(title: string) {
    this.purchaseService
      .trackUserGoPremiumPress('Free plan dialog: ' + title)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.router.navigate(['/purchase']);
      });
  }

  openUpgradeModal(title: string, text: string) {
    this.registerDialogIsShown(title);
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: title,
      text: text,
      primaryButton: 'UPGRADE_NOW_IN_CAPITALS',
      table: [
        {
          td: [
            'ENTERPRISE_PLAN_BENEFIT_1',
            'ENTERPRISE_PLAN_BENEFIT_2',
            'ENTERPRISE_PLAN_BENEFIT_3',
            'ENTERPRISE_PLAN_BENEFIT_4',
          ],
          th: 'ENTERPRISE_PLAN_BENEFITS',
        },
      ],
    };

    modal.componentInstance.modalData = modalData;
    modal.result.then((data) => {
      if (data === 'primary') {
        this.isNotEnterprise
          ? this.router.navigate(['/purchase'], {
              queryParams: { enterprise: true },
            })
          : (window.location.href = '/teams/licenses/changeplan');
      }
    });
  }

  registerDialogIsShown(title: string) {
    this.purchaseService
      .limitationsDialogIsShown(title)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {});
  }
}
