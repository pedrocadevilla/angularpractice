import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSyncAccountComponent } from './add-sync-account.component';

describe('AddSyncAccountComponent', () => {
  let component: AddSyncAccountComponent;
  let fixture: ComponentFixture<AddSyncAccountComponent>;
  1;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddSyncAccountComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSyncAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
