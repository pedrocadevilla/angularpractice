import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { AddModel } from 'src/app/shared/models/account-model';
import { errorMessageObject } from 'src/app/shared/models/error-message-model';
import { Modal } from 'src/app/shared/models/modal-model';
import { AccountsService } from 'src/app/shared/services/account.service';
import { initializeForm } from '../../models/exchange-form-constructor';
import { errorMessages } from '../../models/exchange-form-err-message';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.scss'],
  animations: [
    trigger('inputAnimation', [
      state(
        'in',
        style({ height: 'auto', opacity: 1, 'margin-bottom': 'auto' })
      ),
      transition('void => *', [
        style({
          height: 0,
          opacity: 0,
          'margin-bottom': 0,
        }),
        animate('0.25s ease'),
      ]),
      transition('* => void', [
        animate(
          '0.25s ease',
          style({
            height: 0,
            opacity: 0,
            'margin-bottom': 0,
          })
        ),
      ]),
    ]),
  ],
})
export class ExchangeComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  form: FormGroup;
  activeAccounts = 0;
  returnUrl: string;
  sync2cloud = false;
  inviteId = '';
  invalid = {
    username: false,
    password: false,
    server: false,
  };
  simpleEmailPattern = /\S+@\S+\.\S+/;
  usernameErrorList: errorMessageObject[] = [];
  passwordErrorList: errorMessageObject[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private accountsService: AccountsService,
    private dialog: NgbModal
  ) {
    this.usernameErrorList = errorMessages.userNameMessages;
    this.passwordErrorList = errorMessages.userNameMessages;
    this.form = initializeForm();
  }

  ngOnInit() {
    // subscribe to router event for params
    var userName: string;
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.returnUrl = params['returnUrl'];
      this.inviteId = params['inviteid'];
      userName = params['accountName'];
      this.sync2cloud = params['sync2cloud'] == 'true';
    });

    // get accounts count
    this.accountsService.activeAccounts
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: number) => {
        this.activeAccounts = resp;
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  add() {
    if (!this.form.valid) {
      const modalTerm = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'ERROR',
        text: 'EXCHANGE_USERNAME_WITHOUT_SERVER_USED',
      };

      modalTerm.componentInstance.modalData = modalData;
      return;
    }

    // add missing attributes
    this.form.value.provider = 'Exchange';

    // skip server if auto checkbox is checked
    if (this.form.value.autoDiscovery) {
      this.form.value.server = null;
    }

    if (this.returnUrl !== undefined && this.returnUrl.indexOf('share/') > -1) {
      this.form.value.addedForSharing = true;
    }

    this.accountsService
      .add(this.form.value)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        () => {
          // redirect to return url if we have it
          if (this.inviteId) {
            this.router.navigateByUrl(
              '/share/accept-on-account?inviteid=' +
                this.inviteId +
                '&system=exchange&select=1&sync2cloud=' +
                this.sync2cloud
            );
          } else if (this.returnUrl) {
            this.router.navigateByUrl(this.returnUrl);
          } else {
            let text = 'SYNC_ACCOUNT_ADDED';
            let redirectUrl = '/synchronization/addsyncaccount';

            // text for account added bar
            if (this.activeAccounts > 0) {
              text = 'SYNC_ACCOUNT_ADDED_START_SYNCHRONIZATION';
              redirectUrl = '/synchronization/management';
            }

            this.router.navigate([redirectUrl], {
              queryParams: {
                username: this.form.value.username,
                code: text,
              },
            });
          }
        },
        (err) => {
          if (
            this.form.value.autoDiscovery &&
            err.error.Code !== 'ACCOUNT_ALLREADY_IN_LIST'
          ) {
            this.form.value.server = null;
          }

          if (err.error.Code === 'UNVERIFIED_ACCOUNT') {
            const modalTerm = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            let modalData: Modal = {
              title: 'ERROR',
              text: 'INCORRECT_EMAIL_OR_PASSWORD',
            };

            modalTerm.componentInstance.modalData = modalData;
          } else if (
            err.error.Code === 'EXCHANGE_ACCOUNT_VERIFICATION_FAILED_TRY_AGAIN'
          ) {
            var tmp = { autoDiscovery: false };
            this.form.patchValue(tmp);

            const modalTerm = this.dialog.open(ModalComponent, {
              backdrop: 'static',
              keyboard: false,
            });
            let modalData: Modal = {
              title: 'ERROR',
              text: 'EXCHANGE_ACCOUNT_VERIFICATION_FAILED_TRY_AGAIN',
            };

            modalTerm.componentInstance.modalData = modalData;
          } else if (err.error.Code === 'SYNC_ACCOUNT_RECONNECTED') {
            // redirect to return url if we have it
            if (this.returnUrl) {
              this.router.navigateByUrl(this.returnUrl);
            } else {
              let text = 'SYNC_ACCOUNT_ADDED';
              let redirectUrl = '/synchronization/addsyncaccount';

              // text for account added bar
              if (this.activeAccounts > 1) {
                text = 'SYNC_ACCOUNT_ADDED_START_SYNCHRONIZATION';
                redirectUrl = '/synchronization/management';
              }

              this.router.navigate([redirectUrl], {
                queryParams: {
                  username: this.form.value.username,
                  code: text,
                },
              });
            }
          }
        }
      );
  }

  addExternal() {
    let model: AddModel = { Provider: 'Office365', ReturnUrl: this.returnUrl };

    this.accountsService
      .add(model)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        window.location.href = resp;
      });
  }

  openHelp(text: string) {
    const modal = this.dialog.open(ModalComponent, {
      backdrop: 'static',
      keyboard: false,
    });
    let modalData: Modal = {
      title: 'INFORMATION',
      text: text,
    };

    modal.componentInstance.modalData = modalData;
  }
}
