import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { AccountsService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-direction',
  templateUrl: './direction.component.html',
  styleUrls: ['./direction.component.scss'],
})
export class DirectionComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  form: FormGroup;
  loaded = false;
  permissions: any;
  activeAccounts: any;
  isDisabled = false;
  isSalesForceError = false;
  showSalesForceError = false;
  providerTypePermissions: any = {
    1: 0,
    2: 0,
    3: 0,
  };
  allowedProviderTypePermissions: any = {
    1: [33, 23, 32],
    2: [33, 23, 32, 13, 31],
    3: [33, 23, 32],
  };

  constructor(
    private router: Router,
    private accountsService: AccountsService,
    private dialog: NgbModal,
    private translate: TranslateService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    // load permissions
    this.accountsService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        this.activeAccounts = resp.SyncAccounts.filter(
          (account: any) => account.HasSyncableStatus === true
        );
        if (this.activeAccounts.length !== 2) {
          this.router.navigate(['/synchronization/management']);
        }
        this.accountsService
          .getPermissions()
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe((resp: any) => {
            this.permissions = resp.Permissions;
            for (let i = 0; i < this.permissions.length; i++) {
              //set provider types permissions of source1 and source 2, accordingly (1number-perm1, 2number-perm2)
              this.providerTypePermissions[this.permissions[i].ProviderType] =
                this.permissions[i].Accounts.find(
                  (account: any) =>
                    account.Id === this.activeAccounts[0].AccountId
                ).Permissions *
                  10 +
                this.permissions[i].Accounts.find(
                  (account: any) =>
                    account.Id === this.activeAccounts[1].AccountId
                ).Permissions;
              //set permissions to none in front if permissions not allowed
              if (
                this.allowedProviderTypePermissions[
                  this.permissions[i].ProviderType
                ].find(
                  (el: number) =>
                    el ===
                    this.providerTypePermissions[
                      this.permissions[i].ProviderType
                    ]
                ) == undefined
              ) {
                this.providerTypePermissions[
                  this.permissions[i].ProviderType
                ] = 0;
                this.isDisabled = true;
              }
            }

            // init form
            this.form = this.formBuilder.group({
              providerType1Permissions: this.providerTypePermissions[1],
              providerType2Permissions: this.providerTypePermissions[2],
              providerType3Permissions: this.providerTypePermissions[3],
            });

            this.onChanges();
            this.loaded = true;
            if (!this.validate()) {
              //show salesforce error only after page init
              this.showSalesForceError = true;
              return;
            }

            if (this.isDisabled) {
              const modalTerm = this.dialog.open(ModalComponent, {
                backdrop: 'static',
                keyboard: false,
              });
              let modalData: Modal = {
                title: 'ERROR',
                text: 'DIRECTION_SETTINGS_CONFIGURED_INCORRECTLY',
              };

              modalTerm.componentInstance.modalData = modalData;
            }
          });
      });
  }

  setFormValues() {
    this.form.setValue(
      {
        providerType1Permissions: this.providerTypePermissions[1],
        providerType2Permissions: this.providerTypePermissions[2],
        providerType3Permissions: this.providerTypePermissions[3],
      },
      { emitEvent: false }
    );
  }

  markFormControlsAsTouched() {
    this.form.controls.providerType1Permissions.markAsTouched();
    this.form.controls.providerType2Permissions.markAsTouched();
    this.form.controls.providerType3Permissions.markAsTouched();
  }

  validate() {
    //check for SalesForce errors. Salesforce can be set as viewer only (if 1 acc salesforce -> allowed values is 23, if 2 acc salesforce -> allowed 32 only)
    if (
      this.activeAccounts[0].ProviderCode == 'SalesForce' &&
      (this.form.value.providerType1Permissions !== 32 ||
        this.form.value.providerType2Permissions !== 32 ||
        this.form.value.providerType3Permissions !== 32)
    ) {
      this.providerTypePermissions[1] = 32;
      this.providerTypePermissions[2] = 32;
      this.providerTypePermissions[3] = 32;
      this.isSalesForceError = true;
    }

    if (
      this.activeAccounts[1].ProviderCode == 'SalesForce' &&
      (this.form.value.providerType1Permissions !== 23 ||
        this.form.value.providerType2Permissions !== 23 ||
        this.form.value.providerType3Permissions !== 23)
    ) {
      this.providerTypePermissions[1] = 23;
      this.providerTypePermissions[2] = 23;
      this.providerTypePermissions[3] = 23;
      this.isSalesForceError = true;
    }

    if (this.isSalesForceError) {
      if (this.showSalesForceError) {
        const modalTerm = this.dialog.open(ModalComponent, {
          backdrop: 'static',
          keyboard: false,
        });
        let modalData: Modal = {
          title: 'ERROR',
          text: 'SALESFORCE_SYNCDIRECTION_ERROR',
        };

        modalTerm.componentInstance.modalData = modalData;
      }

      this.markFormControlsAsTouched();
      this.setFormValues();
      this.isDisabled = false;
      this.isSalesForceError = false;
      return false;
    }
    return true;
  }

  onChanges(): void {
    this.form.valueChanges.subscribe((val) => {
      if (!this.validate()) {
        return;
      }
      this.providerTypePermissions[1] = val.providerType1Permissions;
      this.providerTypePermissions[2] = val.providerType2Permissions;
      this.providerTypePermissions[3] = val.providerType3Permissions;
      //if no empty selections left (zeros), enable form saving
      if (
        this.providerTypePermissions[1] != 0 &&
        this.providerTypePermissions[2] != 0 &&
        this.providerTypePermissions[3] != 0
      ) {
        this.isDisabled = false;
      }
    });
  }

  save() {
    for (let i = 0; i < this.permissions.length; i++) {
      //update permissions of 2 active accounts
      if (this.permissions[i].ProviderType === 1) {
        this.permissions[i].Accounts.find(
          (account: any) => account.Id === this.activeAccounts[0].AccountId
        ).Permissions =
          (this.form.value.providerType1Permissions -
            (this.form.value.providerType1Permissions % 10)) /
          10;
        this.permissions[i].Accounts.find(
          (account: any) => account.Id === this.activeAccounts[1].AccountId
        ).Permissions = this.form.value.providerType1Permissions % 10;
      } else if (this.permissions[i].ProviderType === 2) {
        this.permissions[i].Accounts.find(
          (account: any) => account.Id === this.activeAccounts[0].AccountId
        ).Permissions =
          (this.form.value.providerType2Permissions -
            (this.form.value.providerType2Permissions % 10)) /
          10;
        this.permissions[i].Accounts.find(
          (account: any) => account.Id === this.activeAccounts[1].AccountId
        ).Permissions = this.form.value.providerType2Permissions % 10;
      } else if (this.permissions[i].ProviderType === 3) {
        this.permissions[i].Accounts.find(
          (account: any) => account.Id === this.activeAccounts[0].AccountId
        ).Permissions =
          (this.form.value.providerType3Permissions -
            (this.form.value.providerType3Permissions % 10)) /
          10;
        this.permissions[i].Accounts.find(
          (account: any) => account.Id === this.activeAccounts[1].AccountId
        ).Permissions = this.form.value.providerType3Permissions % 10;
      }
    }

    this.accountsService
      .setPermissions({ Permissions: this.permissions })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        this.router.navigate(['/synchronization/management']);
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }
}
