import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AccountsService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-status-form',
  templateUrl: './status-form.component.html',
  styleUrls: ['./status-form.component.scss'],
})
export class StatusFormComponent implements OnInit {
  account: any;
  form: FormGroup;
  partialLoader = false;
  retryOld = true;
  autoDiscovery = true;
  success = false;
  unverifiedAccount = false;
  invalid = {
    Password: '',
    Url: false,
  };

  constructor(
    public formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    public accountsService: AccountsService
  ) {}

  ngOnInit() {
    // init form
    this.form = this.formBuilder.group({
      Username: [this.account.Username],
      Password: ['', Validators.required],
      Url: [''],
    });

    // exucute retry on load
    this.retry();
  }

  close() {
    if (this.unverifiedAccount) {
      this.activeModal.close(2);
    } else {
      this.activeModal.dismiss();
    }
  }

  retry() {
    let firstErrInput;

    // reset errors
    this.invalid = {
      Password: '',
      Url: false,
    };

    // custom validation
    if (!this.form.value.Password && !this.retryOld) {
      this.invalid.Password = 'REQUIRED_FIELD';

      if (!firstErrInput) {
        firstErrInput = 'password';
      }
    }

    if (firstErrInput) {
      document.getElementById(firstErrInput)?.focus();
      return;
    }

    // disable input fields
    this.form.get('Username')?.disable();
    this.form.get('Password')?.disable();
    this.form.get('Url')?.disable();

    // enable partial loader
    this.partialLoader = true;

    // add missing data to model
    this.form.value.Id = this.account.AccountId;

    if (!this.form.value.Password) {
      this.form.value.ValidateOldFirst = true;
    } else {
      this.form.value.ValidateOldFirst = false;
    }

    // call back-end
    this.accountsService.retryAddAccount(this.form.value).subscribe(
      () => {
        this.activeModal.close(1);
      },
      (err) => {
        this.retryOld = false;

        // disable partial loader
        this.partialLoader = false;

        // enable input fields
        this.form.get('Password')?.enable();
        this.form.get('Url')?.enable();

        // show error code returned from backend
        if (err.status === 400 && err.error) {
          if (
            err.error.Code === 'ACCOUNT_USED_BY_OTHERS' ||
            err.error.Code === 'USER_DELETED_OFFSIDE'
          ) {
            this.autoDiscovery = false;
          } else if (err.error.Code === 'INCORRECT_PASSWORD') {
            this.invalid.Password = 'INCORRECT_PASSWORD_CLICK_TO_RESET';
            document.getElementById('password')?.focus();
          } else if (
            err.error.Code === 'UNVERIFIED_ACCOUNT' ||
            'TWO_STEP_VERIFICATION_MUST_BE_ENABLED'
          ) {
            this.unverifiedAccount = true;
          }
        }
      }
    );
  }
}
