import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { AccountsService } from 'src/app/shared/services/account.service';
import { DeviceService } from 'src/app/shared/services/device.service';
import { SyncService } from 'src/app/shared/services/sync.service';
import { UserBaseService } from 'src/app/shared/services/user-base.service';
import { Stats } from '../../models/status-model';
declare var Morris: any;

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit, AfterViewInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  currentUser: any;
  progressBar: any;
  activeAccounts = 0;
  today = new Date();
  colors = [
    '#3167AC',
    '#AAD5EB',
    '#6EAEE1',
    '#6CC5FF',
    '#CBECF3',
    '#5E85BF',
    '#224E7E',
    '#351D61',
    '#A582D0',
    '#501EA6',
    '#CB97FF',
    '#EBE3FD',
    '#2973DB',
    '#6F9CB0',
    '#554167',
  ];
  accountData: any = [];
  accountColors: any = [];
  typeData: any = [];
  typeView: string = 'simple';
  accountView: string = 'simple';
  groupBy: string = 'GROUP_BY_TYPE';
  dates: any = {
    startdate: new Date(),
    enddate: new Date(new Date().setUTCHours(23, 59, 59, 999)),
    selected: 'THIS_WEEK',
  };
  byType: any = {
    Contact: 0,
    Calendar: 0,
    Task: 0,
  };
  typeTotal = 0;
  stats: Stats;
  slideConfig = {
    infinite: false,
    dots: true,
    prevArrow:
      '<button class="slick-prev slick-arrow" type="button"><svg><use xlink:href="/Images/svg-sprite.svg#arrow-l-icon"></use></svg></button>',
    nextArrow:
      '<button class="slick-next slick-arrow" type="button"><svg><use xlink:href="/Images/svg-sprite.svg#arrow-r-icon"></use></svg></button>',
    slidesToShow: 2,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 1250,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: false,
        },
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
        },
      },
    ],
  };
  hidePurchaseLogic: boolean;
  loaded = false;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private syncService: SyncService,
    private userService: UserBaseService,
    private accountsService: AccountsService,
    private deviceService: DeviceService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    // detect mobile app
    this.hidePurchaseLogic = this.deviceService.isPurchaseLogicAvailabe();

    // load accounts
    this.accountsService.get().pipe(takeUntil(this.ngUnsubscribe)).subscribe();

    // subscribe for progress info
    this.syncService.progress
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.progressBar = resp;

        // update UI
        this.changeDetectorRef.detectChanges();

        // update page if progress == 100
        if (
          this.progressBar &&
          this.progressBar.actionType === 'Sync' &&
          this.progressBar.progress === 100 &&
          this.progressBar.update
        ) {
          this.load();

          // data load started, no need to update again
          this.progressBar.update = false;
          this.syncService.setProgress(this.progressBar);
        }
      });

    // subscribe for user info
    this.userService.currentUser
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.currentUser = resp;
      });

    // get active accounts count
    this.accountsService.activeAccounts
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: number) => {
        this.activeAccounts = resp;
      });

    // set date range on init
    const start = new Date(),
      day = start.getDay(),
      diff = start.getDate() - day;

    this.dates.startdate = new Date(start.setDate(diff));
  }

  ngAfterViewInit() {
    // load report for selected date range
    this.load();
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  toggleView() {
    // set view for group by value
    if (this.groupBy === 'GROUP_BY_TYPE') {
      if (this.typeView === 'simple') {
        this.typeView = 'details';
        this.setByTypeDetails();
      } else {
        this.typeView = 'simple';
        this.setByType();
      }
    } else {
      if (this.accountView === 'simple') {
        this.accountView = 'details';
        this.setByAccountDetails();
      } else {
        this.accountView = 'simple';
        this.setByAccount();
      }
    }
  }

  isInt(value: any) {
    let x;

    if (isNaN(value)) {
      return false;
    }

    x = parseFloat(value);
    return (x | 0) === x;
  }

  setByType() {
    setTimeout(() => {
      const typeData = [],
        newColors = [];
      this.typeTotal = 0;

      for (const i of Object.keys(this.stats.Types)) {
        // set by type graph data
        this.byType[this.stats.Types[parseInt(i)].Type] = this.stats.Types[parseInt(i)].Changes;

        // set by type graph total
        this.typeTotal += this.stats.Types[parseInt(i)].Changes;

        // set data for donut
        typeData.push({
          label: this.translate.instant(
            'DONUT_TYPE_' + this.stats.Types[parseInt(i)].Type.toUpperCase()
          ),
          value: this.stats.Types[parseInt(i)].Changes,
        });

        // set colors
        switch (this.stats.Types[parseInt(i)].Type) {
          case 'Contact':
            newColors.push(this.colors[0]);
            break;
          case 'Calendar':
            newColors.push(this.colors[1]);
            break;
          case 'Task':
            newColors.push(this.colors[2]);
            break;
        }
      }

      // set morris or update donut for type
      Morris.Donut({
        element: 'graph-by-type',
        colors: newColors,
        resize: true,
        data: typeData,
      });
    });
  }

  setByTypeDetails() {
    // add missing types and order them
    if (this.stats.Types.length < 3) {
      const tempTypes = [
        {
          Type: 'Contact',
          Inserted: 0,
          Updated: 0,
          Deleted: 0,
          Changes: 0,
        },
        {
          Type: 'Calendar',
          Inserted: 0,
          Updated: 0,
          Deleted: 0,
          Changes: 0,
        },
        {
          Type: 'Task',
          Inserted: 0,
          Updated: 0,
          Deleted: 0,
          Changes: 0,
        },
      ];

      for (const i of Object.keys(this.stats.Types)) {
        for (const j in tempTypes) {
          if (this.stats.Types[parseInt(j)].Type === tempTypes[j].Type) {
            tempTypes[j] = this.stats.Types[parseInt(j)];
            break;
          }
        }
      }

      this.stats.Types = tempTypes;
    }

    setTimeout(() => {
      // set morris or update donut for types
      for (const i of Object.keys(this.stats.Types)) {
        let index: number = -1;
        const typeData: any = [],
          typeColors: any = [];

        // set data & colors
        for (const j in this.stats.Types[parseInt(i)]) {
          if (j !== 'Type') {
            if (j !== 'Changes') {
              // set order
              switch (j) {
                case 'Inserted':
                  index = 0;
                  break;
                case 'Updated':
                  index = 1;
                  break;
                case 'Deleted':
                  index = 2;
                  break;
              }
              if (index !== -1) {
                typeData[index] = {
                  label: this.translate.instant(
                    'DONUT_TYPE_' + j.toUpperCase()
                  ),
                  value: (this.stats as any).Types[parseInt(i)][j],
                };

                typeColors[index] = this.colors[index];
              }
            }
          }
        }

        // set graph element
        Morris.Donut({
          element: this.stats.Types[parseInt(i)].Type.toLowerCase() + i,
          colors: typeColors,
          resize: true,
          data: typeData,
        });
      }
    });
  }

  setByAccount() {
    setTimeout(() => {
      let changes = 0;
      const accountData = [];

      for (const i of Object.keys(this.stats.Accounts)) {
        // count account graph data
        changes =
          this.stats.Accounts[parseInt(i)].Deleted +
          this.stats.Accounts[parseInt(i)].Inserted +
          this.stats.Accounts[parseInt(i)].Updated;

        // set data for donut
        accountData.push({
          label: this.translate.instant('CHANGES'),
          value: changes,
        });
      }

      // set morris or update donut for type
      Morris.Donut({
        element: 'graph-by-account',
        colors: this.colors,
        resize: true,
        data: accountData,
      });
    });
  }

  setByAccountDetails() {
    setTimeout(() => {
      // set morris or update donut for accounts
      for (const i of Object.keys(this.stats.Accounts)) {
        let index: number = -1,
          accountColors: any = [];
        const accountData: any = [];

        if (
          this.stats.Accounts[parseInt(i)].Username !== '??' &&
          this.stats.Accounts[parseInt(i)].Username !== 'Undefined'
        ) {
          if (!this.stats.Accounts[parseInt(i)].Invalid) {
            // set data & colors
            for (const j in this.stats.Accounts[parseInt(i)]) {
              if (j !== 'Type') {
                if (j !== 'Failed') {
                  // set order
                  switch (j) {
                    case 'Inserted':
                      index = 0;
                      break;
                    case 'Updated':
                      index = 1;
                      break;
                    case 'Deleted':
                      index = 2;
                      break;
                  }
                  if (index !== -1) {
                    accountData[index] = {
                      label: this.translate.instant(
                        'DONUT_TYPE_' + j.toUpperCase()
                      ),
                      value: (this.stats as any).Accounts[parseInt(i)][j],
                    };

                    accountColors[index] = this.colors[index];
                  }
                }
              }
            }
          } else {
            // set failed graph data
            accountData.push({
              label: this.translate.instant('DONUT_TYPE_FAILED'),
              value: 0,
            });

            accountColors = ['#ef452e']; // red error color
          }

          // set graph element
          Morris.Donut({
            element: this.stats.Accounts[parseInt(i)].Username,
            colors: accountColors,
            resize: true,
            data: accountData,
          });
        }
      }
    });
  }

  setDate(date: any) {
    // set date if not currently selected
    if (date !== this.dates.selected) {
      this.dates.selected = date;

      let start = new Date(),
        end = new Date();
      const day = start.getDay();

      switch (date) {
        case 'TODAY':
          // nothing to do here
          break;
        case 'THIS_WEEK':
          start = new Date(start.setDate(start.getDate() - day));
          break;
        case 'LAST_WEEK':
          start = new Date(start.setDate(start.getDate() - (day + 7)));
          end = new Date(end.setDate(end.getDate() - (day + 1)));
          break;
        case 'LAST_2_WEEKS':
          start = new Date(start.setDate(start.getDate() - (day + 14)));
          end = new Date(end.setDate(end.getDate() - (day + 1)));
          break;
        case 'LAST_4_WEEKS':
          start = new Date(start.setDate(start.getDate() - (day + 28)));
          end = new Date(end.setDate(end.getDate() - (day + 1)));
          break;
      }

      // set date
      this.dates.startdate = new Date(start.setUTCHours(0, 0, 0, 0));
      this.dates.enddate = new Date(end.setUTCHours(23, 59, 59, 999));

      // load report for selected date range
      this.load();
    }
  }
  /* TODO MAKE QUESTION ABOUT HOW THIS SHOULD WORK AND WHY CHANGE SET GROUP BY IS NOT BEEN CALLED */
  setGroupBy(by: string) {
    if (this.groupBy !== by) {
      this.groupBy = by;

      if (by === 'GROUP_BY_TYPE') {
        if (this.typeView === 'simple') {
          this.setByType();
        } else if (this.typeView === 'details') {
          this.setByTypeDetails();
        }
      } else if (by === 'GROUP_BY_ACCOUNT') {
        if (this.accountView === 'simple') {
          this.setByAccount();
        } else if (this.accountView === 'details') {
          this.setByAccountDetails();
        }
      }
    }
  }

  load() {
    this.syncService
      .getAccountStats(this.dates)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (resp: any) => {
          this.stats = resp;

          if (this.stats && this.stats.Types.length > 0) {
            // reset data
            this.byType = {
              Contact: 0,
              Calendar: 0,
              Task: 0,
            };

            // set data to current view
            if (this.groupBy === 'GROUP_BY_TYPE') {
              if (this.typeView === 'simple') {
                this.setByType();
              } else {
                this.setByTypeDetails();
              }
            } else {
              if (this.accountView === 'simple') {
                this.setByAccount();
              } else {
                this.setByAccountDetails();
              }
            }
          }

          this.loaded = true;
        },
        (err) => {
          this.loaded = true;
        }
      );
  }

  addGoogle() {
    this.accountsService
      .add({ Provider: 'Google' })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        window.location.href = resp;
      });
  }

  addOutlookRest() {
    this.accountsService
      .add({ Provider: 'OutlookRest' })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        window.location.href = resp;
      });
  }

  sync() {
    this.syncService.checkBackupAccounts();
  }
}
