import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject, takeUntil } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { Modal } from 'src/app/shared/models/modal-model';
import { HelpService } from 'src/app/shared/services/help.service';
import { RestoreService } from 'src/app/shared/services/restore.service';
import { SyncService } from 'src/app/shared/services/sync.service';

@Component({
  selector: 'app-backup-restore',
  templateUrl: './backup-restore.component.html',
  styleUrls: ['./backup-restore.component.scss'],
})
export class BackupRestoreComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  currentAccount = 0;
  currentReplica = 0;
  progressBar: any;
  backupAccounts: any;
  backupFolders: any;
  allFoldersSelected = false;
  partiallySelected = false;
  loaded = false;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private restoreService: RestoreService,
    private syncService: SyncService,
    private dialog: NgbModal,
    private helpService: HelpService
  ) {}

  ngOnInit() {
    // get backup accounts
    this.restoreService
      .get()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.backupAccounts = resp;

        // load backup folders
        if (this.backupAccounts && this.backupAccounts.length > 0) {
          this.loadBackupFolders();
        } else {
          this.loaded = true;
        }
      });

    // subscribe for progress info
    this.syncService.progress
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.progressBar = resp;

        // update UI
        this.changeDetectorRef.detectChanges();
      });

    // subscribe for signalR changes
    this.restoreService.restoreProgress
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp: any) => {
        if (
          resp &&
          this.backupFolders &&
          this.backupFolders.Id === resp.accountId
        ) {
          if (resp.backupedFolderIds) {
            for (const k of Object.keys(resp.backupedFolderIds)) {
              for (const i in this.backupFolders.Replicas) {
                if (
                  this.backupFolders.Replicas[i].TypeName === resp.folderType
                ) {
                  for (const j in this.backupFolders.Replicas[i].Folders) {
                    if (
                      this.backupFolders.Replicas[i].Folders[j].PrimaryId ===
                      resp.backupedFolderIds[k]
                    ) {
                      this.backupFolders.Replicas[i].Folders[j].LastRestored =
                        resp.time;
                      break;
                    }
                  }
                }
              }
            }
          }

          // update UI
          this.changeDetectorRef.detectChanges();
        }
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  loadBackupFolders() {
    // get backup folders
    this.restoreService
      .getBackupFolders(this.backupAccounts[this.currentAccount].AccountId)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((resp) => {
        this.backupFolders = resp;
        let index = 0;

        for (const i in this.backupFolders.Replicas) {
          if (this.backupFolders.Replicas[i].IsSelected) {
            index = +i;
            break;
          }
        }

        this.selectReplica(index);
        this.loaded = true;
      });
  }

  countAllFoldersCheckbox() {
    let enabledCount = 0;

    for (const i in this.backupFolders.Replicas[this.currentReplica].Folders) {
      if (
        this.backupFolders.Replicas[this.currentReplica].Folders[i].IsSelected
      ) {
        enabledCount += 1;
      }
    }

    if (enabledCount > 0) {
      this.allFoldersSelected = true;
    } else {
      this.allFoldersSelected = false;
    }

    // partially enabled?
    if (
      enabledCount > 0 &&
      enabledCount <
        this.backupFolders.Replicas[this.currentReplica].Folders.length
    ) {
      this.partiallySelected = true;
    } else {
      this.partiallySelected = false;
    }
  }

  toggleAccount(dir: boolean) {
    let doLoad = false;

    if (dir) {
      if (this.backupAccounts.length > this.currentAccount + 1) {
        this.currentAccount = this.currentAccount + 1;
        doLoad = true;
      }
    } else {
      if (this.currentAccount > 0) {
        this.currentAccount = this.currentAccount - 1;
        doLoad = true;
      }
    }

    if (doLoad) {
      this.loadBackupFolders();
    }
  }

  selectReplica(index: number) {
    this.currentReplica = index;

    for (const i in this.backupFolders.Replicas) {
      if (+i === index) {
        this.backupFolders.Replicas[i].IsSelected = true;
      } else {
        this.backupFolders.Replicas[i].IsSelected = false;
      }
    }

    // count all folders checkbox
    this.countAllFoldersCheckbox();
  }

  toggleExclude() {
    this.backupFolders.ExcludeRemovedFolders =
      !this.backupFolders.ExcludeRemovedFolders;
  }

  toggleAllFolders() {
    this.allFoldersSelected = !this.allFoldersSelected;

    if (this.backupFolders.Replicas !== null) {
      for (const i of Object.keys(
        this.backupFolders.Replicas[this.currentReplica].Folders
      )) {
        this.backupFolders.Replicas[this.currentReplica].Folders[i].IsSelected =
          this.allFoldersSelected;
      }
    }
  }

  toggleFolder(index: number) {
    this.backupFolders.Replicas[this.currentReplica].Folders[index].IsSelected =
      !this.backupFolders.Replicas[this.currentReplica].Folders[index]
        .IsSelected;

    // count all folders checkbox
    this.countAllFoldersCheckbox();
  }

  changeBackupFolder(index: number, id: string) {
    for (const i in this.backupFolders.Replicas[this.currentReplica].Folders[
      index
    ].BackupedFolders) {
      if (
        this.backupFolders.Replicas[this.currentReplica].Folders[index]
          .BackupedFolders[i].BackupedFolderId === id
      ) {
        this.backupFolders.Replicas[this.currentReplica].Folders[
          index
        ].BackupedFolders[i].IsSelected = true;
      } else {
        this.backupFolders.Replicas[this.currentReplica].Folders[
          index
        ].BackupedFolders[i].IsSelected = false;
      }
    }
  }

  restore() {
    let doRestore = false;

    if (this.backupFolders) {
      for (const i in this.backupFolders.Replicas[this.currentReplica]
        .Folders) {
        if (
          this.backupFolders.Replicas[this.currentReplica].Folders[i].IsSelected
        ) {
          doRestore = true;
          break;
        }
      }
    }

    if (doRestore) {
      // start restore
      this.restoreService.startRestore(this.backupFolders);
    } else {
      const modalTerm = this.dialog.open(ModalComponent, {
        backdrop: 'static',
        keyboard: false,
      });
      let modalData: Modal = {
        title: 'ERROR',
        text: 'SELECT_AT_LEAST_ONE_FOLDER_TO_RESTORE',
      };

      modalTerm.componentInstance.modalData = modalData;
    }
  }

  openHelp() {
    this.helpService.open();
  }
}
