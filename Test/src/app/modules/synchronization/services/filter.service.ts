import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
/*
TODO CHECK FILTER SERVICES ON BACKEND
*/
@Injectable()
export class FiltersService {
  constructor(private http: HttpClient) {}

  get() {
    return this.http.get('/api/filters');
  }

  save(model: any) {
    return this.http.post('/api/filters/update', model);
  }

  getWhatToSync() {
    return this.http.get('/api/whattosync');
  }

  updateWhatToSync(model: any) {
    return this.http.post('/api/whattosync/update', model);
  }
}
