import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  AddSyncCompanyAccountsModel,
  SaveBulkFoldersModel,
} from '../models/add-sync-model';

@Injectable()
export class BulkService {
  public data: any;

  constructor(private http: HttpClient) {}

  setData(data: any) {
    this.data = data;
  }

  getData() {
    return this.data;
  }

  addAccounts(model: AddSyncCompanyAccountsModel) {
    return this.http.post('/api/Synchronization/AddSyncCompanyUsers', model);
  }

  getCompanyUsers(companyId: string) {
    return this.http.get(
      '/api/Synchronization/GetSyncCompanyUsers/' + companyId
    );
  }

  getFolders() {
    return this.http.get('/api/Synchronization/GetBulkFolders');
  }

  saveBulkFolders(model: SaveBulkFoldersModel) {
    return this.http.post('/api/Synchronization/SaveBulkFolders', model);
  }
}
