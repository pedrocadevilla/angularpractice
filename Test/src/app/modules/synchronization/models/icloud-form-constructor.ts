import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  passwordMatchValidator,
  patternValidator,
} from 'src/app/shared/directives/password.directive';

export function initializeForm() {
  return new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      patternValidator,
    ]),
  });
}
