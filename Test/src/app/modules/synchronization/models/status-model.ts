export interface StatisticsAccountModel {
  Username: string;
  Invalid: boolean;
  Updated: number;
  Deleted: number;
  Inserted: number;
  Failed: number;
  System: string;
}

export interface StatisticsAccountTypeModel {
  Type: string;
  Changes: number;
  Inserted: number;
  Deleted: number;
  Updated: number;
}

export interface Stats {
  Accounts: StatisticsAccountModel[];
  Types: StatisticsAccountTypeModel[];
  LastDate: string;
  TotalSessions: number;
}
