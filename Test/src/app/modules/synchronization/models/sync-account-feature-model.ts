export interface SyncAccountsFeature {
  IsAvailable: boolean;
  MaxSyncAccountsCount: number;
  CurrentAccountsCount: number;
  CanAddSyncAccount: boolean;
  ErrorCode: string;
}
