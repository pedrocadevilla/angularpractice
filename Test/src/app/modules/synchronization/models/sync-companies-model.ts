export interface SyncCompanyModel {
  id: string;
  externalId: string;
  syncSystem: string;
  name: string;
  systemSvg: string;
}
