export const errorMessages = {
  userNameMessages: [
    { message: 'The field is required', key: 'required' },
    { message: 'The email is invalid', key: 'email' },
  ],
  passwordMessages: [
    { message: 'The field is required', key: 'required' },
    {
      message:
        'Password cannot contain any of the following characters: & + Space',
      key: 'pattern',
    },
    {
      message: 'Password is too short',
      key: 'minLength',
    },
  ],
};
