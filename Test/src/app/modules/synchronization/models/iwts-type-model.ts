export interface IWTSType {
  Type: number;
  AccountsCount: number;
  IsEnabled: boolean;
  AutoSyncNewFolders: boolean;
  AutoDeleteRemovedFolders: boolean;
  isDisabled: boolean;
  Maps: Array<IWTSMapInTypeModel>;
  Choices: Array<IWTSChoiceInTypeModel>;
  failedSourcesList: Array<IWTSSourcesWithIssuesList>;
  noPermissionsSourcesList: Array<IWTSSourcesWithIssuesList>;
  notSupportedSourcesList: Array<IWTSSourcesWithIssuesList>;
}

export interface IWTSMapInTypeModel {
  GlobalId: string;
  IsSuspended: boolean;
  Show: boolean;
  NotFailedAccountsCount: number;
  Folders: Array<IWTSFolderInMapModel>;
}

export interface IWTSChoiceInTypeModel {
  ReplicaId: string;
  UserName: string;
  System: number;
  SyncDirectionRights: number;
  IsAccessable: boolean;
  IsFailed: boolean;
  CanCreateFolder: boolean;
  IsSupported: boolean;
  Folders: Array<IWTSFolderInChoiceModel>;
}

export interface IWTSSourcesWithIssuesList {
  UserName: string;
  System: number;
}

export interface IWTSFolderInChoiceModel {
  FolderId: string;
  PrimaryId: string;
  FolderName: string;
  translatedFolderName?: string;
  IsDefault: boolean;
  FolderAccessRights: number;
  IsMapped: boolean;
  MapLinkId: string;
  wasFolderCreated?: boolean;
}

export interface IWTSFolderInMapModel {
  UserName: string;
  System: number;
  SyncDirectionRights: number;
  IsAccountEnabled: boolean;
  IsAccountFailed: boolean;
  MapLinkId: string;
  ReplicaId: string;
  FolderId: string;
  PrimaryId: string;
  FolderName: string;
  translatedFolderName?: string;
  IsDefault: boolean;
  FolderAccessRights: number;
  wasFolderCreated?: boolean;
}

export interface IWTSTypesModel {
  Types: Array<IWTSType>;
  HadAnyRecordsInDb: boolean;
}
