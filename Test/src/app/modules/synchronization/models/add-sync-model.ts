export interface SaveBulkFoldersModel {
  SyncDefaultCalendarFolder: boolean;
  SyncDefaultTaskFolder: boolean;
  SyncDefaultContactFolder: boolean;
  ContactFolders: FolderModelLite[];
  TaskFolders: FolderModelLite[];
  CalendarFolders: FolderModelLite[];
}

export interface AddSyncCompanyAccountsModel extends SaveBulkFoldersModel {
  StartSync: boolean;
  CompanyId: string;
  Users: BulkUserModel[];
  SyncSystem: SyncSystem;
}

export interface FolderModelLite {
  Name: string;
  IsSelected: boolean;
}

export interface BulkUserModel extends ClientUserInfo {
  Status: string;
  IsEnabled: boolean;
}

export interface ClientUserInfo {
  Name: string;
  Email: string;
  Id: string;
  UniqueMailboxId: string;
  LastName: string;
  FirstName: string;
}

enum SyncSystem {
  UndefindOrMerged = 0,
  ICloud = 4,
  Exchange = 1,
  Google = 2,
  Outlook = 3,
  GoogleDrive = 5,
  SkyDrive = 6,
  DropBox = 7,
  Office365 = 8,
  OutlookRest = 9,
  SalesForce = 10,
}
