export interface IWTSSelectFolderResponse {
  typeIndex: number;
  mapIndex: number;
  choiceIndex: number;
  folderInChoicesIndex?: number;
}
